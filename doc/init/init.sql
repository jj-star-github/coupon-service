/* 10:39:59 AM qm_coupon_online qm_coupon */
ALTER TABLE `t_coupon` ADD INDEX `idx_status_deleted_use_time_end` (`status`, `is_deleted`, `use_time_end`);
/* 4:51:10 PM qm-test-24 qm_coupon */
ALTER TABLE `t_coupon` DROP INDEX `idx_status_use_time_end_is_deleted`;
/* 2:16:31 PM qm-test-24 qm_coupon */
ALTER TABLE `t_coupon` ADD INDEX `idx_original_status_deleted_amount` (`original_id`, `status`, `is_deleted`, `order_amount`);
/* 2:18:36 PM qm-test-24 qm_coupon */
ALTER TABLE `t_coupon` DROP INDEX `i_original_id`;
/* 2:19:41 PM qm-test-24 qm_coupon */
ALTER TABLE `t_coupon` ADD INDEX `idx_original_deleted` (`original_id`, `is_deleted`);


