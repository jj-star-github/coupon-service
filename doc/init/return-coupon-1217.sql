CREATE TABLE `t_coupon_event_log`
(
  `id`              bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_code`     varchar(32)         NOT NULL DEFAULT '' COMMENT '优惠券编码',
  `biz_type`        int(11)             NOT NULL COMMENT '1=退单归还 2= 退单注销',
  `biz_desc`        varchar(32)         NOT NULL DEFAULT '' COMMENT '1=退单归还 2= 退单注销',
  `remark`          varchar(255)        NOT NULL DEFAULT '' COMMENT '备注',
  `biz_code`        varchar(64)         NOT NULL DEFAULT '' COMMENT '业务单号',
  `source_biz_code` varchar(64)         NOT NULL DEFAULT '' COMMENT '原业务单号',
  `gmt_create`      datetime            NOT NULL COMMENT '创建时间',
  `gmt_modifed`     datetime            NOT NULL COMMENT '更新时间',
  `operator_name`   varchar(10)         NOT NULL DEFAULT '' COMMENT '操作人',
  `operator_id`     int(11)             NOT NULL COMMENT '操作人id',
  `current_shop_id` int(11)             NOT NULL COMMENT '当前门店',
  PRIMARY KEY (`id`),
  KEY `idx_type_sourcebizcode` (`biz_type`, `source_biz_code`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;