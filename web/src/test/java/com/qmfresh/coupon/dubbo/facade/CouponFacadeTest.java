package com.qmfresh.coupon.dubbo.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.client.CouponDTO;
import com.qmfresh.coupon.client.CouponExpiredDTO;
import com.qmfresh.coupon.client.CouponQueryDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = "spring.profiles.active=site")
@RunWith(SpringRunner.class)
public class CouponFacadeTest {

    @Resource
    private CouponFacade couponFacade;

    @Test
    public void test_list() {
        CouponQueryDTO couponQueryDTO = new CouponQueryDTO();
        couponQueryDTO.setGivenOrderCode("9001020121716555310");
        List<CouponDTO> couponDTOList = couponFacade.list(couponQueryDTO);
        System.out.println(JSON.toJSONString(couponDTOList));
    }

    /**
     * 失效优惠券
     */
    @Test
    public void test_expired_coupons() {

        CouponExpiredDTO couponExpiredDTO = new CouponExpiredDTO();
        couponExpiredDTO.setCouponCodes(Arrays.asList("58764932568580096"));
        couponExpiredDTO.setCurrentShopId(10);
        couponExpiredDTO.setOperatorId(1);
        couponExpiredDTO.setOperatorName("系统");
        couponExpiredDTO.setRefundCode("123");
        couponExpiredDTO.setSourceOrderCode("123");
        couponFacade.expiredGivenCoupons(couponExpiredDTO);

    }
}
