package com.qmfresh.coupon.services.entity;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponCheckReturnBean;
//import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public class CouponTest {

    private Coupon expiredCoupon;
    private Coupon returnCoupon;
    private Coupon availableCoupon;
    private Coupon usedCoupon;
    private Coupon futureCoupon;

    {
        Coupon origin = new Coupon();
        origin.setId(4696814);
        origin.setOriginalId(409);
        origin.setCouponCode("49714599036977152");
        origin.setCouponName("立减券");
        origin.setCouponCondition(1);
        origin.setNeedMoney(50);
        origin.setCouponType(3);
        origin.setSubMoney(new BigDecimal("8.00"));
        origin.setCouponDiscount(null);
        origin.setUseTimeStart(1604937601);
        origin.setUseTimeEnd((int) (System.currentTimeMillis()/1000 +1000000));
        origin.setApplicableGoodsType(1);
        origin.setClass1Ids(null);
        origin.setClass2Ids(null);
        origin.setOrderNo("1001016039734137534");
        origin.setOrderAmount(new BigDecimal("54.00"));
        origin.setGoodsNum(6);
        origin.setUserId(199L);
        origin.setSourceSystem(1);
        origin.setLimitChannel(0);
        origin.setUseInstruction("全部商品可用，满20元可减免2元");
        origin.setSourceOrderNo("1001016039727138834");
        origin.setSourceShopId(10);
        origin.setUseShopId(10);
        origin.setCouponPrice(new BigDecimal("2.00"));
        origin.setStatus(1);
        origin.setUseTime(1603973393);
        origin.setIsDeleted(0);
        origin.setUT(1603972376);

        String originJson = JSON.toJSONString(origin);

        //失效

        expiredCoupon = JSON.parseObject(originJson, Coupon.class);
        expiredCoupon.setStatus(CouponStatus.EXPIRED.getCode());


        returnCoupon = JSON.parseObject(originJson, Coupon.class);
        returnCoupon.setStatus(CouponStatus.RETURN.getCode());

        availableCoupon = JSON.parseObject(originJson, Coupon.class);
        availableCoupon.setStatus(CouponStatus.UNUSED.getCode());

        usedCoupon = JSON.parseObject(originJson, Coupon.class);
        usedCoupon.setStatus(CouponStatus.USED.getCode());

        futureCoupon = JSON.parseObject(originJson, Coupon.class);
        futureCoupon.setStatus(CouponStatus.UNUSED.getCode());
        futureCoupon.setUseTimeStart((int) ((System.currentTimeMillis() / 1000) + 10000));
    }

    @Test
    public void test_check_coupon_use_with_expired() {
        CouponCheckReturnBean check = Coupon.check(expiredCoupon, false);
        assertFalse(check.getSuccessFlag());
        assertEquals(check.getType(), CouponCheckType.EXPIRED.getCode());
    }

    @Test
    public void test_check_coupon_use_with_return() {
        CouponCheckReturnBean check = Coupon.check(returnCoupon, false);
        assertFalse(check.getSuccessFlag());
        assertEquals(check.getType(), CouponCheckType.RETURNED.getCode());
    }

    @Test
    public void test_check_coupon_use_with_available() {
        CouponCheckReturnBean check = Coupon.check(availableCoupon, false);
        assertTrue(check.getSuccessFlag());
        assertNull(check.getType());

    }

    @Test
    public void test_check_coupon_use_with_used() {
        CouponCheckReturnBean check = Coupon.check(usedCoupon, false);
        assertFalse(check.getSuccessFlag());
        assertEquals(check.getType(), CouponCheckType.USED.getCode());
    }

    @Test
    public void test_check_coupon_use_with_future() {
        CouponCheckReturnBean check = Coupon.check(futureCoupon, true);
        assertTrue(check.getSuccessFlag());
    }
}
