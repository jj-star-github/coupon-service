package com.qmfresh.coupon.services.service.impl;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.ProQueryCouponRequestBean;
import com.qmfresh.coupon.interfaces.dto.coupon.ProQueryCouponReturnBean;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponInfoBean;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponReturnBean;
import com.qmfresh.coupon.interfaces.enums.MemberFinalEnum;
import com.qmfresh.coupon.services.platform.application.apiCoupon.IApiCouponService;
import com.qmfresh.coupon.services.service.ICouponConsumerService;
import com.qmfresh.coupon.services.service.ICouponService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=test"})
@RunWith(SpringRunner.class)
@Slf4j
public class CouponServiceImplTest {


    @Resource
    private ICouponService couponService;
    @Resource
    private IApiCouponService apiCouponService;

    @Test
    public void test_query_coupon_info_list() {
        QueryCouponInfoBean param = new QueryCouponInfoBean();
        param.setPageIndex(1);
        param.setPageSize(30);
        ListWithPage<QueryCouponReturnBean> page = couponService.queryCouponInfoList(param);
        log.warn("{}", JSON.toJSONString(page));
    }


    @Test
    public void test_query_coupon_activity_can_use() {
        ProQueryCouponRequestBean proQueryCouponRequestBean = new ProQueryCouponRequestBean();
        proQueryCouponRequestBean.setApprovalStatus(1);
        proQueryCouponRequestBean.setOrderChannel(5);
        List<ProQueryCouponReturnBean> returnBeans = apiCouponService.queryCouponActivityCanUse(proQueryCouponRequestBean);
        assertTrue(returnBeans.size() > 0);
    }

    @Resource
    private ICouponConsumerService iCouponConsumerService;
}
