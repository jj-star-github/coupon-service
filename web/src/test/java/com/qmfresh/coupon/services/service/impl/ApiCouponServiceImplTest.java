package com.qmfresh.coupon.services.service.impl;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponCheckRequestBean;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponCheckReturnBean;
import com.qmfresh.coupon.services.entity.bo.CouponReturnBo;
import com.qmfresh.coupon.services.entity.bo.CouponReturnParamBo;
import com.qmfresh.coupon.services.entity.command.CouponPrintCheckCommand;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
@Slf4j
public class ApiCouponServiceImplTest {


    private CouponCheckRequestBean ccrbWithReturnCode;
    private CouponCheckRequestBean ccrbWithExpired;
    private CouponCheckRequestBean ccrbWithUsed;
    private CouponCheckRequestBean ccrbWithUnused;

    {
        ccrbWithReturnCode = new CouponCheckRequestBean();
        ccrbWithExpired = new CouponCheckRequestBean();
        ccrbWithUsed = new CouponCheckRequestBean();
        ccrbWithUnused = new CouponCheckRequestBean();
    }

    /**
     * 测试优惠券已经过期
     */
}
