package com.qmfresh.coupon.test;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.controller.ApiCouponController;
import com.qmfresh.coupon.controller.CouponController;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityConditionRule;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityTimeRule;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponForAppRequestBean;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponUseRequestBean;
import com.qmfresh.coupon.interfaces.dto.coupon.CreateActivityBean;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponInfoBean;
import com.qmfresh.coupon.interfaces.dto.coupon.ReceiveCouponDTO;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ApiCouponTest {
    @Resource
    private ApiCouponController controller;
    @Resource
    private CouponController couponController;

    @Test
    public void queryCouponByUserId(){
        CouponForAppRequestBean bean = new CouponForAppRequestBean();
        bean.setUserId(123L);
        System.out.println("查询优惠券————"+JSON.toJSONString(controller.queryCouponByUserId(bean)));
    }

    @Test
    public void queryUseingCoupon(){
        QueryCouponInfoBean bean = new QueryCouponInfoBean();
        bean.setPageSize(20);
        bean.setPageIndex(1);
        System.out.println("查询有效优惠券————"+JSON.toJSONString(controller.queryUseingCoupon(bean)));
    }

    @Test
    public void sendCouponForApp(){
        SendCouponDto dto = new SendCouponDto();
        dto.setCouponId(446);
        List useIdList = new ArrayList();
        useIdList.add(282811);
        dto.setUserIdList(useIdList);
        dto.setSendNum(1);
        dto.setLimitChannel(0);
        System.out.println("发放优惠券————"+JSON.toJSONString(controller.sendCouponForApp(dto)));
    }

    @Test
    public void useCoupon() {
        CouponUseRequestBean couponUse = new CouponUseRequestBean();
        couponUse.setBizChannel(1);
        couponUse.setCouponId(1);
        couponUse.setCouponPrice(new BigDecimal("0.15"));
        couponUse.setGoodsNum(2);
        couponUse.setOrderNo("1223334433");
        couponUse.setShopId(4);
        couponUse.setSourceSystem(1);
        couponUse.setTotalAmount(new BigDecimal("1.35"));
        couponUse.setUserId(2l);
        couponUse.setUseTimestamp(new Date().getTime());
        System.out.println("使用优惠券----"+ JSON.toJSONString(controller.useCoupon(couponUse)));
    }

    @Test
    public void createCoupon(){
        CreateActivityBean coupon = new CreateActivityBean();
        coupon.setActivityName("测试001");
        coupon.setApplicableGoodsType(1);
        coupon.setCouponType(1);
        coupon.setUseCondition(0);
        coupon.setUseInstruction("全部商品可用，可减免2元");
        coupon.setUseTimeType(1);
        coupon.setLimitChannel(0);
        ActivityConditionRule activityConditionRule = new ActivityConditionRule();
        activityConditionRule.setSubMoney(2);
        coupon.setConditionRule(activityConditionRule);
        ActivityTimeRule timeRule = new ActivityTimeRule();
        timeRule.setTimeStart(1606492801);
        timeRule.setTimeEnd(1606751999);
        timeRule.setIsAllDay(1);
        timeRule.setHmsStart("08:00:00");
        timeRule.setHmsEnd("10:00:00");
        coupon.setTimeRule(timeRule);
        coupon.setCreateId(444);
        coupon.setCreateName("测试001");
        coupon.setOperatorId(44);
        coupon.setOperatorName("444");
        System.out.println("创建优惠券————"+JSON.toJSONString(couponController.createCouponActivity(coupon)));
    }

    @Test
    public void receiveCoupon(){
        ReceiveCouponDTO dto = new ReceiveCouponDTO();
        dto.setUserId(93743);
        dto.setActivityId(111);
        dto.setCouponId(485);
        System.out.println("领取优惠券————"+JSON.toJSONString(controller.receiveCoupon(dto)));
    }
}
