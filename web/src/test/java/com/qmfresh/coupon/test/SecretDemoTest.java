/*
 * Copyright (C) 2019 All rights reserved
 * Author: Ivan Shen
 * Date: 2019/6/10
 * Description:SecretDemoTest.java
 */
package com.qmfresh.coupon.test;

import com.alibaba.druid.support.json.JSONUtils;
import com.qmfresh.coupon.services.common.utils.Md5Util;
import com.qmfresh.coupon.services.common.utils.SecretUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ivan Shen
 */
public class SecretDemoTest {

    private static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQNCIprB9wUwZAUy1g3m4NBAOs5Ply0PfKiMEL\n" +
            "gKHUsBw1ElMB7T0yzoMtHXilYvg5gHzwG9EoH5FdvuY9hdiop9CaduuRD097ZnftaKOPlag77FWU\n" +
            "KsRk3sUAlEYXldY20KrLmXzWLxVfryO/YXUpYbI+KJJaI8gYKSi/6nkcuwIDAQAB";

    private static String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBANA0IimsH3BTBkBTLWDebg0EA6zk\n" +
            "+XLQ98qIwQuAodSwHDUSUwHtPTLOgy0deKVi+DmAfPAb0SgfkV2+5j2F2Kin0Jp265EPT3tmd+1o\n" +
            "o4+VqDvsVZQqxGTexQCURheV1jbQqsuZfNYvFV+vI79hdSlhsj4oklojyBgpKL/qeRy7AgMBAAEC\n" +
            "gYARZ/pfL4OlWVuRbyLBEXdz4R0JjE8KeUvuA8bX1lYxONPy3+hOpe3S3I7865TVhtOnwwUu8bRn\n" +
            "vVPi2X3YMONu94EM5+8PiEm4SH4oqpPZt5LBZgAfKYlITHBN+jkKqYLuSWAFiaTFrGgXoTXYy5c6\n" +
            "yTA7UgkBvyAnpCtz47QFmQJBAPaPMYJGMFd2ps0d/AQ6XaB1OiDMchjqIa2+iBlxnu4zM47hozTp\n" +
            "Q5OWlpECDITK+Qc3bb56fjYNADa1vhu5gJcCQQDYLPj1lQCvMPjA2+vYKyHReuqVr3xbMDICma2U\n" +
            "hvY5A4HFWIK5yUpRNxwJ1Mp72hAwGbK7eIp7lszMyRHTnKV9AkAFBb+1bqaXcYROU1kJ4QJ9PUYU\n" +
            "2vVMCqDrACGXmxfotERNmc0QS9wjioLAq3ED13qhKgDjS218vmENvEAGIo47AkEAvcSE7DjYN/Ka\n" +
            "e9rTJ5l9f4ISikJZvUcKr8OuYBM19IjCk7YGVAeDCNaC85JsCds8mK0GfJHtp16S/DaKQqRhlQJA\n" +
            "NJp8V5B7+N9jJ+5UVXUEkYcaKr1sA56g0WzPTVi4Q+5zr56ucP87O0AS5DxTv88QhioADqmesfNx\n" +
            "CJPzSSkmbQ==";

    public static void main(String[] args) throws Exception {
        Map<String, Object> param = new HashMap<>();
        param.put("id", 1);
        param.put("ip", "127.0.0.1");
        param.put("status", 1);
        String original = JSONUtils.toJSONString(param);
        System.out.println("原文:" + original);
        String originalMd5 = Md5Util.getMD5(original);
        System.out.println("原文MD5:" + originalMd5);
        byte[] signature = SecretUtil.sign(originalMd5.getBytes(), privateKey);
        boolean status = SecretUtil.verify(originalMd5.getBytes(), signature, publicKey);
        System.out.println("验证情况：" + status);
    }
}
