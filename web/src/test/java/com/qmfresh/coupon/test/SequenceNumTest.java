package com.qmfresh.coupon.test;

import com.qmfresh.coupon.services.common.utils.SnowflakeIdWorker;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public class SequenceNumTest {

    public static void main(String[] args) {
        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(1L, 2L);
        long id = snowflakeIdWorker.nextId();
        String idString = String.valueOf(id);
        System.out.println(idString);


        System.out.println(1L <<22 | 1<<17 | 1<<12 | 2048);
        System.out.println(1<<10);
        System.out.println(1<<20);
        System.out.println(1<<20 | 1<< 10);
        System.out.println(1L<<62);

        String stringId = String.valueOf(snowflakeIdWorker.nextId());
        System.out.println(stringId.substring(1));
        System.out.println(stringId.substring(0));

    }
}
