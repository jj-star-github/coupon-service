package com.qmfresh.coupon.test;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.controller.ApiCouponController;
import com.qmfresh.coupon.controller.CouponController;
import com.qmfresh.coupon.controller.CouponTemplateController;
import com.qmfresh.coupon.controller.facade.dto.GoodsConditionDTO;
import com.qmfresh.coupon.controller.facade.dto.QueryCouponTemplateDTO;
import com.qmfresh.coupon.controller.facade.dto.ShopConditionDTO;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityGoodsRule;
import com.qmfresh.coupon.interfaces.dto.coupon.IdParamBean;
import com.qmfresh.coupon.interfaces.dto.coupon.ProQueryCouponRequestBean;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvailableCouponQuery;
import com.qmfresh.coupon.services.platform.application.template.CouponTemplateService;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateTargetGoodsManager;
import com.qmfresh.coupon.services.platform.domain.model.template.TemplateConditionRule;
import com.qmfresh.coupon.services.platform.domain.model.template.TemplateTimeRule;
import com.qmfresh.coupon.services.platform.domain.model.template.command.*;
import com.qmfresh.coupon.services.platform.domain.model.template.impl.CouponTemplateManagerImpl;
import com.qmfresh.coupon.services.platform.infrastructure.dubbo.CouponTemplateApiServiceImpl;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetGoods;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest(properties = {"spring.profiles.active=test"})
@RunWith(SpringRunner.class)
@Slf4j
public class CouponTemplateTest {
    @Resource
    private CouponTemplateService templateService;
    @Resource
    private ICouponTemplateManager couponTemplateManager;
    @Resource
    private CouponTemplateManagerImpl couponTemplateManagerImpl;
    @Resource
    private CouponTemplateController couponTemplateController;
    @Resource
    private ApiCouponController apiCouponController;
    @Resource
    private CouponController couponController;
    @Resource
    private ICouponTemplateTargetGoodsManager targetGoodsManager;
    @Resource
    private CouponTemplateApiServiceImpl apiService;

    @Test
    public void create(){
        TemplateConditionRule conditionRule = new TemplateConditionRule();
        conditionRule.setSubMoney(5);
        conditionRule.setUseReceiveNum(5);
        conditionRule.setCouponMaxSendNum(99999);
        conditionRule.setShopInstruction("测试店使用");
        TemplateTimeRule timeRule = new TemplateTimeRule();
        timeRule.setTimeStart(1604937601);
        timeRule.setTimeEnd(1609763119);
        timeRule.setIsAllDay(0);
        List<TemplateTargetGoodsCommand> goodsCommands = new ArrayList<>();
        TemplateTargetGoodsCommand goods = new TemplateTargetGoodsCommand();
        goods.setGoodsId("10006");
        goods.setGoodsName("小白菜");
        goods.setGoodsType(3);
        goodsCommands.add(goods);
        List<TemplateTargetShopsCommand> shopsCommands = new ArrayList<>();
        TemplateTargetShopsCommand shopsCommand = new TemplateTargetShopsCommand();
        shopsCommand.setShopId(10);
        shopsCommand.setShopName("测试店");
        shopsCommand.setShopType(3);
        shopsCommands.add(shopsCommand);
        CouponTemplateCreateCommand createCommand = CouponTemplateCreateCommand.create("test券模板",1,30,2,conditionRule,1,timeRule,1,
                "全部商品可用，满30元可减免2元",2,3,123,"123",goodsCommands,shopsCommands);
        templateService.createCouponTemplate(createCommand);
    }

    @Test
    public void queryById(){
        System.out.println("id查询————"+JSON.toJSONString(couponTemplateManager.queryCouponTemplateById(23)));
    }

    @Test
    public void couponSendByTemplate(){
        CouponSendCommand command = new CouponSendCommand();
        command.setTemplateId(1);
        command.setSendNumber(5);
        command.setUserId(666);
        System.out.println("封装信息————"+JSON.toJSONString(couponTemplateManager.couponSendByTemplate(command)));
    }

    @Test
    public void queryByCondition(){
        QueryCouponTemplateDTO dto = new QueryCouponTemplateDTO();
        dto.setCouponType(2);
        System.out.println("查询券模板————"+JSON.toJSONString(couponTemplateController.queryByCondition(dto)));
    }

    @Test
    public void bingTemplate(){
        List<Integer> templateIds = new ArrayList<>();
        templateIds.add(725);
        templateIds.add(2);
        System.out.println("绑定券模板————"+JSON.toJSONString(couponTemplateManager.bingTemplate("1","测试会场",templateIds)));
    }

    @Test
    public void queryCouponActivityCanUse(){
        ProQueryCouponRequestBean bean = new ProQueryCouponRequestBean();
        bean.setApprovalStatus(1);
        bean.setOrderChannel(5);
        System.out.println("查询可用优惠券————"+JSON.toJSONString(apiCouponController.queryMemCoupon(bean)));
    }

    @Test
    public void queryCouponDetailInfo(){
        IdParamBean bean = new IdParamBean();
        bean.setId(15);
        System.out.println("查询券模板明细————"+JSON.toJSONString(couponController.queryCouponDetailInfo(bean)));
    }

    @Test
    public void buildApplicableGoodsRule(){
        CouponTemplate couponActivity = couponTemplateManager.selectById(23);
        ActivityGoodsRule goodsRule = couponTemplateManager.buildApplicableGoodsRule(couponActivity);
        String json=JSON.toJSONString(goodsRule);
        System.out.println("goodsRule 转化————"+json);
    }

    @Test
    public void buildUseInstruction(){
        String yhh = "全部商品可用，可减免20元";
        String useInstruction[] = yhh.split("，");
    }

    @Test
    public void queryGoods(){
        List<CouponTemplateTargetGoods> goodsList = targetGoodsManager.queryByTemplateId(23);
        List<String> goodsNameList = goodsList.stream().map(CouponTemplateTargetGoods::getGoodsName).collect(Collectors.toList());
        List<String> goodsSplitList = new ArrayList<>();
        if(goodsNameList.size()<=3){
            goodsSplitList.addAll(goodsNameList);
        }else {
            goodsSplitList.addAll(goodsNameList.subList(0,3));
        }
        String result=  goodsSplitList.stream().collect(Collectors.joining("、"));
        System.out.println("goodsSplitList————"+result);
    }

    @Test
    public void queryCouponById(){
        IdParamBean bean = new IdParamBean();
        bean.setId(30);
        System.out.println("查询券模板————"+JSON.toJSONString(couponTemplateController.queryById(bean)));
    }

    @Test
    public void queryGoodsById(){
        GoodsConditionDTO bean = new GoodsConditionDTO();
        bean.setTemplateId(717);
        System.out.println("查询券模板商品————"+JSON.toJSONString(couponTemplateController.queryGoodsById(bean)));
    }

    @Test
    public void queryShopById(){
        ShopConditionDTO bean = new ShopConditionDTO();
        bean.setTemplateId(717);
        System.out.println("查询券模板门店————"+JSON.toJSONString(couponTemplateController.queryShopById(bean)));
    }

    @Test
    public void verifyGoods(){
        CouponTemplate couponTemplate = couponTemplateManager.selectById(1);
        List<Sku> skuList = new ArrayList<>();
        Sku sku = new Sku();
        sku.setAmount(new BigDecimal(1));
        sku.setSkuId(10006);
        sku.setPrice(new BigDecimal("29.996"));
        skuList.add(sku);
        System.out.println("验证商品————"+JSON.toJSONString(couponTemplateManagerImpl.verifyGoods(skuList,couponTemplate)));
    }

    @Test
    public void availableSendCouponList(){
        System.out.println("crm列表————"+JSON.toJSONString(couponController.availableSendCouponList(new MemberAvailableCouponQuery())));
    }

    @Test
    public void num(){
        Integer number = 99;
        String tag = new BigDecimal(number).divide(new BigDecimal("10")) + "折";
        System.out.println("计算———"+tag);
    }

    @Test
    public void apiService(){
        List<Sku> skuList = new ArrayList<>();
        Sku sku = new Sku();
        sku.setAmount(new BigDecimal(1));
        sku.setClass1Id(56);
        sku.setClass2Id(273);
        sku.setGoodType(2);
        sku.setPrice(new BigDecimal(59.99));
        sku.setSkuId(22489);
        sku.setSsuId(30843);
        skuList.add(sku);
        System.out.println("计算———"+JSON.toJSONString(apiService.buildReturnApp(93743L,10,skuList)));
    }
}
