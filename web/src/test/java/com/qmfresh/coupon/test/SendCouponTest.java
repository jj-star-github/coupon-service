package com.qmfresh.coupon.test;


import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.client.RpcGoodsDTO;
import com.qmfresh.coupon.client.coupon.QueryCouponByGoodsDTO;
import com.qmfresh.coupon.client.coupon.QueryCouponEventDTO;
import com.qmfresh.coupon.dubbo.CouponQueryInfo.facade.CouponQueryInfoFacade;
import com.qmfresh.coupon.interfaces.enums.MemberFinalEnum;
import com.qmfresh.coupon.services.platform.application.coupon.SendCouponService;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendBySendActivityContext;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendContext;
import com.qmfresh.coupon.services.platform.application.coupon.enums.SendBusinessTypeEnums;
import com.qmfresh.coupon.services.platform.application.template.CouponTemplateService;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityTypeEnums;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;

@SpringBootTest(properties = {"spring.profiles.active=test"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class SendCouponTest {
    @Resource
    private SendCouponService sendCouponService;

    @Test
    public void sendCouponBySendActivity(){
        SendBySendActivityContext sendActivityContext  = new SendBySendActivityContext("123", SendBusinessTypeEnums.MEMBER_NEW_ACTIVITY.getType(),
                ActivityTypeEnums.NEW_USER.getCode(), 123, null, null, null);
        System.out.println("======"+ JSON.toJSONString(sendCouponService.sendCouponBySendActivity(sendActivityContext)));
    }

    @Test
    public void sendCoupon(){
        SendContext sendContext = new SendContext("1010987652345",
                SendBusinessTypeEnums.PROMOTION_ACTIVITY.getType(), 124,
                "1010987652345", 1,
                3, 10, null, null, null);
        System.out.println("sendCoupon======"+ JSON.toJSONString(sendCouponService.sendCoupon(sendContext)));
    }

    /**
     * 月礼券的发送
     */
    @Test
    public void sendMonth(){
        sendCouponService.sendMonth(MemberFinalEnum.GOLD_MEMBER);
    }


    @Test
    public void hmSendByType(){
        SendCouponDto dto = new SendCouponDto();
        dto.setCouponId(1);
        dto.setSendNum(2);
        dto.setUserIdList(Arrays.asList(123,124));
        sendCouponService.hmSendByType(dto);
    }

    @Resource
    private CouponTemplateService couponTemplateService;
    @Test
    public void modifyStatus() {
        couponTemplateService.modifyStatus();
    }

    @Resource
    private CouponQueryInfoFacade couponQueryInfoFacade;
    @Test
    public void queryBySku() {
        QueryCouponByGoodsDTO query = new QueryCouponByGoodsDTO();
        query.setUserId(234);
        query.setShopId(10);
        RpcGoodsDTO to = new RpcGoodsDTO();
        to.setClass1Id(25);
        to.setSkuId(10023);
        query.setRpcGoodsDTO(to);
        System.out.println(JSON.toJSONString(couponQueryInfoFacade.queryBySku(query)));
    }

    @Test
    public void queryCouponEvent() {
        QueryCouponEventDTO query = new QueryCouponEventDTO();
        query.setSourceBizCode("90010191209105007826");
        System.out.println(JSON.toJSONString(couponQueryInfoFacade.queryUseCoupon(query)));
    }
}
