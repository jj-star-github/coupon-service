package com.qmfresh.coupon.test;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.client.CouponQueryInfoAPI;
import com.qmfresh.coupon.client.using.CouponActivityIdDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest(properties = {"spring.profiles.active=test"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class CouponQueryInfoAPITest {
    @Resource
    private CouponQueryInfoAPI api;
    @Test
    public void queryByActivityId(){
        CouponActivityIdDTO activityIdDTO = new CouponActivityIdDTO();
        activityIdDTO.setActivityId(407);
        System.out.println("优惠券————"+JSON.toJSONString(api.queryByActivityId(activityIdDTO)));
    }
}
