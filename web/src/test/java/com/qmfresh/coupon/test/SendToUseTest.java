package com.qmfresh.coupon.test;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.OSS;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.OSSObject;
import com.qmfresh.coupon.controller.facade.SendActivityFacade;
import com.qmfresh.coupon.controller.facade.dto.*;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.platform.application.coupon.SendCouponService;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityRule;
import com.qmfresh.coupon.services.platform.domain.model.member.MemberServerManager;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.oss.OssUtils;
import com.qmfresh.coupon.services.platform.infrastructure.oss.SslUtils;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
@Slf4j
public class SendToUseTest {
    public static final int BATCH_SIZE = 400;
    @Resource
    private SendActivityFacade facade;
    @Resource
    private SendCouponService sendCouponService;
    @Resource
    private OssUtils ossUtils;
    @Resource
    private MemberServerManager memberServerManager;

    @Test
    public void create(){
        CreateSendToUseDTO createSendToUseDTO = new CreateSendToUseDTO();
        createSendToUseDTO.setActivityName("定向发放测试3");
        createSendToUseDTO.setSendType(0);
        createSendToUseDTO.setActivityType(101);
        SendActivityRule sendActivityRule = new SendActivityRule();
        List<SendActivityCouponDTO> activityCouponDTOList = new ArrayList<>();
        SendActivityCouponDTO dto = new SendActivityCouponDTO();
        dto.setCouponTemplateId(717);
        dto.setCouponName("23团长激励券");
        dto.setCouponType(1);
        dto.setRemark("全部商品可用，可减免10元");
        dto.setSendNum(2);
        activityCouponDTOList.add(dto);
        createSendToUseDTO.setActivityCouponDTOList(activityCouponDTOList);
        createSendToUseDTO.setOperatorId(137);
        createSendToUseDTO.setOperatorName("sys_test1");
        facade.createSendToUse(createSendToUseDTO);

    }

    @Test
    public void querySendActivity(){
        QuerySendActivityDTO dto = new QuerySendActivityDTO();
        dto.setSendType(0);
        dto.setActivityName("");
        dto.setCreateName("");
        System.out.println("列表查询————"+JSON.toJSONString(facade.querySendActivity(dto)));
    }
//    {"activityName":"","createName":"","operatorId":137,"operatorName":"sys_test1","pageNum":1,"pageSize":10,"sendType":0,"shopId":326}
    @Test
    public void closedActivity(){
        SendActivityDTO dto = new SendActivityDTO();
        dto.setId(1346413023346925572L);
        dto.setOperatorId(137);
        dto.setOperatorName("sys_test1");
        facade.closedActivity(dto);
    }

    @Test
    public void queryCouponByActivityId(){
        System.out.println("活动奖品查询————"+JSON.toJSONString(facade.queryCouponByActivityId(5L)));
    }

    @Test
    public void activityEffect(){
        System.out.println("活动效果分析————"+JSON.toJSONString(facade.activityEffect(123L)));
    }

    @Test
    public void queryCouponDetail(){
        CouponDetailDTO dto = new CouponDetailDTO();
        dto.setSearchUserId(93743L);
        dto.setCouponCode("");
        dto.setPageNum(1);
        dto.setPageSize(10);
        System.out.println("发放明细————"+JSON.toJSONString(facade.queryCouponDetail(dto)));
    }
//    {"templateId":"","userId":"93743","couponCode":"","pageNum":1,"pageSize":10}

    @Test
    public void time(){
        Calendar beforeTime = Calendar.getInstance();
        beforeTime.add(Calendar.MINUTE, -5);
        Date beforeDate = beforeTime.getTime();
        System.out.println("时间————" + beforeDate +"---" +new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(beforeDate));
    }

    @Test
    public void url() throws IOException {
        OSS ossClient = ossUtils.create();
        InputStream inputStream = ossClient.getObject(bucket, "coupon/sendToUser/dcbe740a-19fb-42fa-be53-f70e10ef7e3e.xlsx").getObjectContent();
        XSSFWorkbook wb = new XSSFWorkbook(inputStream);
        XSSFSheet sheet = wb.getSheetAt(0);
        int rowNo = sheet.getLastRowNum();
        List<Integer> userIdList = new ArrayList<>();
        for (int i = 1; i <= rowNo; i++) {
            XSSFRow row = sheet.getRow(i);
            XSSFCell cell = row.getCell(0);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            String cellValue = cell.getStringCellValue();
            userIdList.add(Integer.valueOf(cellValue));
        }
        System.out.println("userIdList:" + userIdList + " ");
        int pageSize = 400;
        int userNum = 0;
        for (int i = 0; i < userIdList.size(); i+=pageSize) {
            List<Integer> subUserIdList = new ArrayList<>();
            int toIndex = i + pageSize>userIdList.size()?userIdList.size():i+pageSize;
            subUserIdList.addAll(userIdList.subList(i, toIndex));
            System.out.println(subUserIdList.size()+"subUserIdList:" + subUserIdList + " ");
            userNum = toIndex++;
        }
        System.out.println("userNum:" + userNum );
    }

    @Test
    public void sendToUser(){
        int beginTime = DateUtil.getCurrentTimeIntValue();
        XxlJobLogger.log("定向发放-Begin:" + beginTime);
        sendCouponService.sendToUser();
        int endTime = DateUtil.getCurrentTimeIntValue();
        XxlJobLogger.log("定向发放-End:" + endTime,"用时:" + (endTime - beginTime));
    }

    @Value("${oss.bucket}")
    private String bucket;
    @Test
    public void oss() throws IOException {
        OSS ossClient = ossUtils.create();
        // ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
//        ossClient.getObject(new GetObjectRequest(bucket, "dcbe740a-19fb-42fa-be53-f70e10ef7e3e.xlsx"), new File("导入文件.xlsx"));
        OSSObject ossObject = ossClient.getObject(bucket, "coupon/sendToUser/dcbe740a-19fb-42fa-be53-f70e10ef7e3e.xlsx");

// 读取文件内容。
        System.out.println("Object content:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(ossObject.getObjectContent(),"GBK"));
        while (true) {
            String line = reader.readLine();
            if (line == null) break;

            System.out.println("\n" + line);
        }
// 数据读取完成后，获取的流必须关闭，否则会造成连接泄漏，导致请求无连接可用，程序无法正常工作。
        reader.close();

// 关闭OSSClient。
        ossClient.shutdown();
    }

    @Test
    public void download(){
        OSS ossClient = ossUtils.create();
        ossClient.getObject(new GetObjectRequest(bucket, "coupon/sendToUser/dcbe740a-19fb-42fa-be53-f70e10ef7e3e.xlsx"), new File("dcbe740a-19fb-42fa-be53-f70e10ef7e3e.xlsx"));

    }

    @Test
    public void member(){
        int pageSize = 400;
        int pageNum = 1;
        int userNum = 0;
        int lastSize = 0;
        boolean notIsEnd = true;
        while (notIsEnd) {
            // 组装用户map
            List<Integer> userIdList = memberServerManager.getUserIdList(10003, pageNum, pageSize);
            if (CollectionUtils.isEmpty(userIdList)) {
                return ;
            }
            notIsEnd = userIdList.size() >= pageSize;
            lastSize = notIsEnd ? pageSize : userIdList.size();
            pageNum++;
        }
        userNum = (pageNum - 1) * pageSize + lastSize;
        System.out.println("pageNum:" + pageNum );
        System.out.println("userNum:" + userNum );
    }
}
