
package com.qmfresh.coupon.test;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.controller.CouponController;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetailQuery;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponCount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Description:
 * @Version: V1.0
 */
@SpringBootTest(properties = {"spring.profiles.active=test"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class queryCouponListTest {

    @Autowired
    private CouponController couponController;
    @Test
    public  void queryCouponListDetailTest() {
        QueryCouponCount queryCouponCount = new QueryCouponCount();
        //queryCouponCount.setCouponType(QueryCouponUseRuleEnums.FULL_DISCOUNT.getCode());
        //queryCouponCount.setStatus(CouponUseStatusEnums.NOTUSE_COUPON.getCode());
        //queryCouponCount.setCouponCode("01070405717575598082");
        //queryCouponCount.setOriginalId(19);
       // queryCouponCount.setPageNum(2);
        //queryCouponCount.setPageSize(5);
        //queryCouponCount.setSourceOrderNo("12354667765458");
        queryCouponCount.setStartTime(1610527697);
        queryCouponCount.setEndTime(1610527861);
        System.out.println("查询优惠券详情——"+JSON.toJSONString(couponController.queryCouponListDetail(queryCouponCount)));
    }

    @Test
    public void memberCouponTest() {
        MemberCouponDetailQuery query = new MemberCouponDetailQuery();
        query.setStatus(0);
        query.setUserId(282828);
        System.out.println("查询会员优惠券明细——"+JSON.toJSONString(couponController.queryCouponDetail(query)));
    }
}
