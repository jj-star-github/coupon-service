package com.qmfresh.coupon;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import io.micrometer.core.instrument.MeterRegistry;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@EnableDubbo
@SpringBootApplication
@MapperScan(basePackages = {"com.qmfresh.coupon.services.mapper",
                            "com.qmfresh.coupon.services.platform.infrastructure.mapper"})
public class CouponServiceApp {

    public static void main(String[] args) {
        SpringApplication.run(CouponServiceApp.class);
    }

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        paginationInterceptor.setLimit(-1);
        return paginationInterceptor;
    }

    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
        return registry -> registry.config().commonTags("application", "coupon-service");
    }
}
