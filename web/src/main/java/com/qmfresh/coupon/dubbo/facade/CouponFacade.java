package com.qmfresh.coupon.dubbo.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.client.CouponDTO;
import com.qmfresh.coupon.client.CouponExpiredDTO;
import com.qmfresh.coupon.client.CouponQueryDTO;
import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.services.entity.Operator;
import com.qmfresh.coupon.services.entity.command.CouponQueryCommand;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class CouponFacade {

    @Resource
    private ICouponManager couponManager;

    public List<CouponDTO> list(CouponQueryDTO couponQueryDTO) {
        List<Coupon> coupons = couponManager.list(CouponQueryCommand.create(couponQueryDTO.getUseOrderCode(), couponQueryDTO.getGivenOrderCode()));

        if (CollectionUtils.isEmpty(coupons)) {
            return new ArrayList<>();
        }
        for (Coupon coupon : coupons) {
            coupon.setValidityPeriod(Coupon.buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd()));
        }
        return CouponDTOs.convert(coupons);
    }


    public void expiredGivenCoupons(CouponExpiredDTO couponExpiredDTO) {
        if (CollectionUtils.isEmpty(couponExpiredDTO.getCouponCodes())) {
            throw new BusinessException("优惠券列表为空");
        }
        if (StringUtils.isBlank(couponExpiredDTO.getRefundCode())) {
            throw new BusinessException("退货单号不能为空");
        }
        if (StringUtils.isBlank(couponExpiredDTO.getOperatorName())) {
            throw new BusinessException("操作人名称不能为空");
        }

        List<Coupon> coupons = couponManager.list(CouponQueryCommand.create(couponExpiredDTO.getCouponCodes()));

        if (CollectionUtils.isEmpty(coupons)) {
            log.info("为查询到当前优惠券：{}", JSON.toJSONString(couponExpiredDTO));
            return;
        }

        couponManager.expiredCoupon(coupons,
                Operator.create(couponExpiredDTO.getOperatorId(), couponExpiredDTO.getOperatorName()),
                couponExpiredDTO.getRefundCode(),
                couponExpiredDTO.getSourceOrderCode(),
                couponExpiredDTO.getCurrentShopId()
        );

    }
}
