package com.qmfresh.coupon.dubbo.facade;

import com.qmfresh.coupon.client.CouponDTO;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 优惠券对象
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public class CouponDTOs {
    public static List<CouponDTO> convert(List<Coupon> coupons) {
        if (CollectionUtils.isEmpty(coupons)) {
            return new ArrayList<>();
        }
        List<CouponDTO> tmp = new ArrayList<>();
        for (Coupon coupon : coupons) {
            tmp.add(convert(coupon));
        }
        return tmp;
    }

    public static CouponDTO convert(Coupon coupon) {
        CouponDTO couponDTO = new CouponDTO();
        couponDTO.setCouponId(coupon.getId());
        couponDTO.setCouponCode(coupon.getCouponCode());
        couponDTO.setCouponName(coupon.getCouponName());
        couponDTO.setCouponActivityId(coupon.getOriginalId().longValue());
        couponDTO.setLimitChannel(coupon.getLimitChannel());
        couponDTO.setPromotionId(coupon.getPromotionActivityId() == null ? 0 : coupon.getPromotionActivityId().longValue());
        couponDTO.setSourceOrderCode(coupon.getSourceOrderNo());
        couponDTO.setSourceShopId(coupon.getSourceShopId());
        couponDTO.setSourceUserId(coupon.getUserId().intValue());
        couponDTO.setStatus(coupon.getStatus());
        couponDTO.setUseInstruction(coupon.getUseInstruction());
        couponDTO.setUseOrderCode(coupon.getOrderNo());
        couponDTO.setUseShopId(coupon.getUseShopId());
        couponDTO.setUseTime(coupon.getUseTime());
        couponDTO.setApplicableGoodsType(coupon.getApplicableGoodsType());
        couponDTO.setUseTimeStart(coupon.getUseTimeStart());
        couponDTO.setUseTimeEnd(coupon.getUseTimeEnd());
        couponDTO.setClass1Ids(coupon.getClass1Ids());
        couponDTO.setClass2Ids(coupon.getClass2Ids());
        couponDTO.setValidityPeriod(coupon.getValidityPeriod());
        return couponDTO;
    }


}
