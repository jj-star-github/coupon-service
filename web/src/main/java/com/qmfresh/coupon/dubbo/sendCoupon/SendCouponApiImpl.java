package com.qmfresh.coupon.dubbo.sendCoupon;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.client.SendCouponAPI;
import com.qmfresh.coupon.client.send.SendCouponByActivityDTO;
import com.qmfresh.coupon.client.send.SendCouponByTemplateDTO;
import com.qmfresh.coupon.client.send.SendCouponReturnDTO;
import com.qmfresh.coupon.controller.facade.SendCouponFacade;
import com.qmfresh.coupon.services.platform.application.coupon.vo.SendCouponReturnVo;
import com.qmfresh.coupon.services.platform.domain.shared.LogExceptionStackTrace;
import com.qmgyl.rpc.result.CodeMsg;
import com.qmgyl.rpc.result.DubboResult;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SendCouponApiImpl implements SendCouponAPI {

    @Resource
    private SendCouponFacade sendCouponFacade;


    @Override
    public DubboResult<List<SendCouponReturnDTO>> sendCouponByTemplate(SendCouponByTemplateDTO templateDTO) {
        log.info("RPC根据Template发券：sendCouponByTemplate param={}", JSON.toJSONString(templateDTO));
        try {
            List<SendCouponReturnVo> returnVoList = sendCouponFacade.sendCoupon(templateDTO);
            if (CollectionUtils.isEmpty(returnVoList)) {
                return DubboResult.buildFailureResult(new CodeMsg(9999l, "发券失败"));
            }
            return DubboResult.buildSuccessResult(returnVoList.stream().map(SendCouponReturnVo :: createDto).collect(Collectors.toList()));
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("RPC根据Template发券：sendCouponByTemplate 业务异常 errorMsg={}", be.getErrorMsg());
            return DubboResult.buildFailureResult(new CodeMsg(9999l, be.getErrorMsg()));
        } catch (Exception e) {
            log.error("RPC根据Template发券：sendCouponByTemplate 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return DubboResult.buildFailureResult(new CodeMsg(9999l, "系统异常"));
        }

    }

    @Override
    public DubboResult<List<SendCouponReturnDTO>> sendCouponByActivity(SendCouponByActivityDTO activityDTO) {
        log.info("RPC根据发放活动发券：sendCouponByActivity param={}", JSON.toJSONString(activityDTO));
        try {
            List<SendCouponReturnVo> returnVoList = sendCouponFacade.sendCoupon(activityDTO);
            if (CollectionUtils.isEmpty(returnVoList)) {
                return DubboResult.buildFailureResult(new CodeMsg(9999l, "发券失败"));
            }
            return DubboResult.buildSuccessResult(returnVoList.stream().map(SendCouponReturnVo :: createDto).collect(Collectors.toList()));
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("RPC根据发放活动发券：sendCouponByActivity 业务异常 errorMsg={}", be.getErrorMsg());
            return DubboResult.buildFailureResult(new CodeMsg(9999l, be.getErrorMsg()));
        } catch (Exception e) {
            log.error("RPC根据发放活动发券：sendCouponByActivity 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return DubboResult.buildFailureResult(new CodeMsg(9999l, "系统异常"));
        }
    }
}
