package com.qmfresh.coupon.dubbo.CouponQueryInfo.facade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.alibaba.nacos.common.utils.StringUtils;
import com.qmfresh.coupon.client.coupon.QueryCouponByGoodsDTO;
import com.qmfresh.coupon.client.coupon.QueryCouponEventDTO;
import com.qmfresh.coupon.client.coupon.QueryCouponReturnDTO;
import com.qmfresh.coupon.services.entity.CouponEventLog;
import com.qmfresh.coupon.services.entity.CouponStatus;
import com.qmfresh.coupon.services.platform.domain.model.coupon.CouponEventLogManager;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponUseManager;
import com.qmfresh.coupon.services.platform.domain.model.template.CouponTemplateInfo;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.model.template.command.Sku;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateStatusTypeEnums;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.CouponUse;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import org.apache.commons.collections.CollectionUtils;
import org.eclipse.jetty.util.StringUtil;
import org.springframework.stereotype.Component;

import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;

import lombok.extern.slf4j.Slf4j;

/**
 */
@Component
@Slf4j
public class CouponQueryInfoFacade {

    @Resource
    private ICouponManager couponManager;
    @Resource
    private ICouponTemplateManager templateManager;
    @Resource
    private CouponEventLogManager couponEventLogManager;
    @Resource
    private ICouponUseManager couponUseManager;

    /**
     * 查询用户可用优惠券
     * @param queryDTO
     * @return
     */
    public List<QueryCouponReturnDTO> queryBySku(QueryCouponByGoodsDTO queryDTO) {
        if (null == queryDTO.getUserId()) {
            throw new BusinessException("用户ID不可为空");
        }
        List<QueryCouponReturnDTO> couponReturnDTOList = new ArrayList<>();
        //查询用户可以使用的优惠券
        List<Coupon> couponList = couponManager.queryByUser(queryDTO.getUserId(), CouponStatus.UNUSED.getCode(), queryDTO.getChannel());
        if (CollectionUtils.isEmpty(couponList)) {
            return couponReturnDTOList;
        }
        List<Integer> templateIdList = couponList.stream().map(Coupon :: getOriginalId).distinct().collect(Collectors.toList());
    // 查询模板信息
    List<CouponTemplate> templateList =
        templateManager.queryByIdList(templateIdList, TemplateStatusTypeEnums.IN_USE_STATUS.getCode());
        if (CollectionUtils.isEmpty(templateList)) {
            return couponReturnDTOList;
        }
        Map<Integer, List<Coupon>> couponMap = couponList.stream().collect(Collectors.groupingBy(Coupon::getOriginalId));
        List<Sku> skuList = Arrays.asList(new Sku(queryDTO.getRpcGoodsDTO()));

        //循环券模板进行匹配校验
        for (CouponTemplate  template : templateList){
            if (templateManager.verifyShop(queryDTO.getShopId(), template.getApplicableShopType(), template.getId())) { // 过滤门店
                List<Sku> skus = templateManager.buildGoodsInfo(skuList, template);
                if (CollectionUtils.isNotEmpty(skus)) {
                    List<Coupon> coupons = couponMap.get(template.getId());
                    if (CollectionUtils.isNotEmpty(coupons)) {
                        for(Coupon coupon : coupons) {
                            couponReturnDTOList.add(coupon.createQueryCouponReturnDTO(template));
                        }
                    }
                }
            }
        }
        return couponReturnDTOList;
    }

    /**
     * 查新退单回退优惠券
     */
    public QueryCouponReturnDTO queryCouponEvent(QueryCouponEventDTO queryCouponEventDTO) {
        if(null == queryCouponEventDTO.getBizType() ||
                (StringUtil.isEmpty(queryCouponEventDTO.getBizCode()) && StringUtil.isEmpty(queryCouponEventDTO.getSourceBizCode()))) {
            throw new BusinessException("请求参数不可为空");
        }
        //查询业务便跟记录
        CouponEventLog couponEventLog = couponEventLogManager.queryEventLog(queryCouponEventDTO.getBizType(), queryCouponEventDTO.getBizCode(), queryCouponEventDTO.getSourceBizCode());
        if (null == couponEventLog) {
            return null;
        }
        //查询优惠券
        Coupon coupon = couponManager.queryByIdOrCode(null , couponEventLog.getCouponCode(), null);
        if (null == coupon) {
            log.warn("券操作业务查询，优惠券不存在");
            return null;
        }

        //查询券模板
        CouponTemplate couponTemplate = templateManager.selectById(coupon.getOriginalId());
        if (null == couponTemplate) {
            log.warn("券操作业务查询，优惠券模板不存在");
            return null;
        }

        //封装优惠券信息
        return coupon.createQueryCouponReturnDTO(couponTemplate);
    }

    /**
     * 查新退单回退优惠券
     */
    public QueryCouponReturnDTO queryUseCoupon(QueryCouponEventDTO queryCouponEventDTO) {
        if(StringUtil.isEmpty(queryCouponEventDTO.getSourceBizCode())) {
            throw new BusinessException("请求参数不可为空");
        }
        //查询业务便跟记录
        CouponUse couponUse = couponUseManager.queryByOrderNo(queryCouponEventDTO.getSourceBizCode());
        if (null == couponUse) {
            return null;
        }
        //查询优惠券
        Coupon coupon = couponManager.queryByIdOrCode(couponUse.getCouponId() , null, null);
        if (null == coupon) {
            log.warn("券操作业务查询，优惠券不存在");
            return null;
        }
        if (CouponStatus.USED.getCode() == coupon.getStatus() && queryCouponEventDTO.getSourceBizCode().equals(coupon.getOrderNo())) {
            log.warn("券操作业务查询，优惠券退还失败");
            return null;
        }

        //查询券模板
        CouponTemplate couponTemplate = templateManager.selectById(coupon.getOriginalId());
        if (null == couponTemplate) {
            log.warn("券操作业务查询，优惠券模板不存在");
            return null;
        }

        //封装优惠券信息
        return coupon.createQueryCouponReturnDTO(couponTemplate);
    }

}