package com.qmfresh.coupon.dubbo;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.client.CouponDTO;
import com.qmfresh.coupon.client.CouponQueryDTO;
import com.qmfresh.coupon.client.CouponQueryRemote;
import com.qmfresh.coupon.dubbo.facade.CouponFacade;
import com.qmgyl.rpc.result.DubboResult;
import com.qmgyl.rpc.result.SysErrCodes;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Service
@Slf4j
public class CouponQueryRemoteImpl implements CouponQueryRemote {

    @Resource
    private CouponFacade couponFacade;

    @Override
    public DubboResult<List<CouponDTO>> list(CouponQueryDTO couponQueryDTO) {
        try {
            log.info("查询优惠券列表 list param= {}", JSON.toJSONString(couponQueryDTO));
            List<CouponDTO> couponDTOS = couponFacade.list(couponQueryDTO);
            log.info("查询优惠券列表 list return= {}", JSON.toJSONString(couponDTOS));
            return DubboResult.buildSuccessResult(couponDTOS);
        } catch (Exception e) {
            log.error("查询优惠券列表有： {} ", JSON.toJSONString(couponQueryDTO), e);
            return DubboResult.buildFailureResult(SysErrCodes.SYS_ILLEGAL_PARAM);
        }
    }
}
