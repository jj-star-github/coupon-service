package com.qmfresh.coupon.dubbo.CouponQueryInfo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qmfresh.coupon.client.CouponQueryInfoAPI;
import com.qmfresh.coupon.client.common.CodeMsg;
import com.qmfresh.coupon.client.common.DubboResult;
import com.qmfresh.coupon.client.coupon.QueryCouponByGoodsDTO;
import com.qmfresh.coupon.client.coupon.QueryCouponEventDTO;
import com.qmfresh.coupon.client.coupon.QueryCouponReturnDTO;
import com.qmfresh.coupon.client.using.CouponActivityIdDTO;
import com.qmfresh.coupon.client.using.CouponQueryResultDTO;
import com.qmfresh.coupon.dubbo.CouponQueryInfo.facade.CouponQueryInfoFacade;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityGoodsRule;
import com.qmfresh.coupon.services.platform.application.coupon.vo.SendCouponReturnVo;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;


import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CouponQueryInfoServiceImpl implements CouponQueryInfoAPI {

    @Resource
    private ICouponTemplateManager couponTemplateManager;
    @Resource
    private CouponQueryInfoFacade couponQueryInfoFacade;

    @Override
    public DubboResult<CouponQueryResultDTO> queryByActivityId(CouponActivityIdDTO activityIdDTO) {
        log.info("查询优惠券活动参数，activity={}",activityIdDTO.getActivityId());
        DubboResult dubboResult = new DubboResult();
        try{
            CouponTemplate couponActivity = couponTemplateManager.selectById(activityIdDTO.getActivityId());
            CouponQueryResultDTO couponQueryResultDTO = new CouponQueryResultDTO();
            if(couponActivity == null){
                log.warn("无优惠券活动信息，activity={}",activityIdDTO.getActivityId());
                return null;
            }
            couponQueryResultDTO.setId(couponActivity.getId());
            couponQueryResultDTO.setCouponName(couponActivity.getCouponName());
            couponQueryResultDTO.setCouponCondition(couponActivity.getCouponCondition());
            couponQueryResultDTO.setNeedMoney(couponActivity.getNeedMoney());
            couponQueryResultDTO.setCouponType(couponActivity.getCouponType());
            couponQueryResultDTO.setCouponRule(couponActivity.getCouponRule());
            couponQueryResultDTO.setUseTimeType(couponActivity.getUseTimeType());
            couponQueryResultDTO.setUseTimeRule(couponActivity.getUseTimeRule());
            couponQueryResultDTO.setApplicableGoodsType(couponActivity.getApplicableGoodsType());

            ActivityGoodsRule goodsRule = couponTemplateManager.buildApplicableGoodsRule(couponActivity);
            couponQueryResultDTO.setApplicableGoodsRule(JSON.toJSONString(goodsRule));
            couponQueryResultDTO.setUseInstruction(couponActivity.getUseInstruction());
            couponQueryResultDTO.setStatus(couponActivity.getStatus());
            couponQueryResultDTO.setCreatedId(couponActivity.getCreatedId());
            couponQueryResultDTO.setCreatedName(couponActivity.getCreatedName());
            couponQueryResultDTO.setIsDeleted(couponActivity.getIsDeleted());
            couponQueryResultDTO.setLimitChannel(couponActivity.getLimitChannel());

            dubboResult.success(couponQueryResultDTO);
        }catch (Exception e){
            log.warn("查询优惠券活动信息异常");
            dubboResult.failure(new CodeMsg(20001001L, "系统错误"),e.getMessage());
        }
        return dubboResult;
    }

    @Override
    public com.qmgyl.rpc.result.DubboResult<List<QueryCouponReturnDTO>> queryBySku(QueryCouponByGoodsDTO queryDTO) {
        log.info("查询商品可用优惠券：queryBySku param={}", JSON.toJSONString(queryDTO));
        try {
            return com.qmgyl.rpc.result.DubboResult.buildSuccessResult(couponQueryInfoFacade.queryBySku(queryDTO));
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("查询商品可用优惠券：queryBySku 业务异常 errorMsg={}", be.getErrorMsg());
            return com.qmgyl.rpc.result.DubboResult.buildFailureResult(new com.qmgyl.rpc.result.CodeMsg(9999l, be.getErrorMsg()));
        } catch (Exception e) {
            log.error("查询商品可用优惠券：queryBySku 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return com.qmgyl.rpc.result.DubboResult.buildFailureResult(new com.qmgyl.rpc.result.CodeMsg(9999l, "系统异常"));
        }
    }

    @Override
    public com.qmgyl.rpc.result.DubboResult<QueryCouponReturnDTO> queryCouponEvent(QueryCouponEventDTO queryDTO) {
        log.info("查询业务优惠券操作记录：queryCouponEvent param={}", JSON.toJSONString(queryDTO));
        try {
            return com.qmgyl.rpc.result.DubboResult.buildSuccessResult(couponQueryInfoFacade.queryCouponEvent(queryDTO));
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("查询业务优惠券操作记录：queryCouponEvent 业务异常 errorMsg={}", be.getErrorMsg());
            return com.qmgyl.rpc.result.DubboResult.buildFailureResult(new com.qmgyl.rpc.result.CodeMsg(9999l, be.getErrorMsg()));
        } catch (Exception e) {
            log.error("查询业务优惠券操作记录：queryCouponEvent 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return com.qmgyl.rpc.result.DubboResult.buildFailureResult(new com.qmgyl.rpc.result.CodeMsg(9999l, "系统异常"));
        }
    }

    @Override
    public com.qmgyl.rpc.result.DubboResult<QueryCouponReturnDTO> queryUseCoupon(QueryCouponEventDTO queryDTO) {
        log.info("根据订单号查询使用的优惠券信息：queryUseCoupon param={}", JSON.toJSONString(queryDTO));
        try {
            return com.qmgyl.rpc.result.DubboResult.buildSuccessResult(couponQueryInfoFacade.queryUseCoupon(queryDTO));
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("根据订单号查询使用的优惠券信息：queryUseCoupon 业务异常 errorMsg={}", be.getErrorMsg());
            return com.qmgyl.rpc.result.DubboResult.buildFailureResult(new com.qmgyl.rpc.result.CodeMsg(9999l, be.getErrorMsg()));
        } catch (Exception e) {
            log.error("根据订单号查询使用的优惠券信息：queryUseCoupon 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return com.qmgyl.rpc.result.DubboResult.buildFailureResult(new com.qmgyl.rpc.result.CodeMsg(9999l, "系统异常"));
        }
    }
}
