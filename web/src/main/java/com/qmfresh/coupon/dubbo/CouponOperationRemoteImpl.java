package com.qmfresh.coupon.dubbo;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.client.CouponExpiredDTO;
import com.qmfresh.coupon.client.CouponOperationRemote;
import com.qmfresh.coupon.dubbo.facade.CouponFacade;
import com.qmgyl.rpc.result.DubboResult;
import com.qmgyl.rpc.result.SysErrCodes;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Service
@Slf4j
public class CouponOperationRemoteImpl implements CouponOperationRemote {

    @Resource
    private CouponFacade couponFacade;

    /**
     * 优惠券失效
     *
     * @param couponExpiredDTO 优惠券失效
     * @return 优惠券失效结果
     */
    @Override
    public DubboResult<String> expiredGivenCoupons(CouponExpiredDTO couponExpiredDTO) {
        try {
            log.info("请求参数expiredGivenCoupons {}", JSON.toJSONString(couponExpiredDTO));
            couponFacade.expiredGivenCoupons(couponExpiredDTO);
            return DubboResult.buildSuccessResult("");
        } catch (Exception e) {
            log.error("失效优惠券失败：{}", JSON.toJSONString(couponExpiredDTO), e);
            return DubboResult.buildFailureResult(SysErrCodes.SYS_ERR);
        }
    }
}
