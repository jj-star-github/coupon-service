package com.qmfresh.coupon.converter;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * JSON编码和解码数据
 * 支持返回base64
 * 
 * @author Ivan
 */
public class JsonWrapperHttpMessageConverter extends com.qmfresh.coupon.converter.MobHttpMessageConverter<Object> {
    private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    private static final MediaType PROTOBUF = new MediaType("application", "x-protobuf", DEFAULT_CHARSET);
    //http头字段名 返回的数据组织类型
    public static  final String HTTP_HEAD_RESPONSE_DATA_TYPE = "QM-RES-DATA-TYPE";

    //返回的数据为json字符串
    private static final String QM_RESPONSE_DATA_TYPE_JSON = "json/text";
    //返回的数据位json格式并base64加密后字符串
    private static final String QM_RESPONSE_DATA_TYPE_JSON_BASE64 = "json/base64";

    public static  final String QM_ORGIN_RESPONSE_DATA_TYPE_JSON = "orgin/text";

    private static final com.qmfresh.coupon.converter.QMJsonValueSerializer QM_JSON_VALUE_SERIALIZER = new com.qmfresh.coupon.converter.QMJsonValueSerializer();

    public JsonWrapperHttpMessageConverter() {
        super(new MediaType("application", "json", DEFAULT_CHARSET), MediaType.ALL);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    protected boolean canRead(MediaType mediaType) {
        if(isCompatibleWithProto(mediaType)){
            return false;
        }
        return super.canRead(mediaType);
    }

    @Override
    protected boolean canWrite(MediaType mediaType) {
        if(isCompatibleWithProto(mediaType)){
            return false;
        }
        return super.canWrite(mediaType);
    }

    /**
     * 验证请求 media类型是否是protobuf
     * @param mediaType
     * @return
     */
    private boolean isCompatibleWithProto(MediaType mediaType){
        // 默认为json
        if (mediaType == null || MediaType.ALL.equals(mediaType)) {
            return false;
        }

        return PROTOBUF.isCompatibleWith(mediaType);
    }
    @Override
    protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        return JSON.parseObject(inputMessage.getBody(), clazz);
    }

    @Override
    protected void writeInternal(Object object, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        byte[] encodedParam;
        if (object instanceof ServiceResult) {
            int flag = isJsonContent(outputMessage);
            if(flag!=0){
                encodedParam = JSON.toJSONString(object, QM_JSON_VALUE_SERIALIZER).getBytes();
            } else {
                encodedParam = Base64.encodeBase64(JSON.toJSONBytes(object));
            }

        }else if(object instanceof springfox.documentation.spring.web.json.Json ||
                object instanceof springfox.documentation.swagger.web.UiConfiguration){
            //排除swagger相关返回
            int flag = isJsonContent(outputMessage);
            if(flag==0){
                encodedParam = Base64.encodeBase64(JSON.toJSONBytes(object));
            } else {
                encodedParam = JSON.toJSONString(object, QM_JSON_VALUE_SERIALIZER).getBytes();

            }
        } else {
            ServiceResult response = new ServiceResult();
            response.setBody(object);
            response.setErrorCode(0);
            response.setMessage("success");
            response.setSuccess(true);
            int flag = isJsonContent(outputMessage);
            if(flag==0){
                encodedParam = Base64.encodeBase64(JSON.toJSONBytes(response));
            } else if(flag == 2){
                encodedParam = JSON.toJSONString(object, QM_JSON_VALUE_SERIALIZER).getBytes();
            }else{
                encodedParam = JSON.toJSONString(response, QM_JSON_VALUE_SERIALIZER).getBytes();
            }

        }

        if(object instanceof ArrayList){//排除swagger相关返回
            ArrayList<Object> arrayList = (ArrayList<Object>)object;
            if(arrayList!=null && arrayList.size()>0){
                if(arrayList.get(0) instanceof springfox.documentation.swagger.web.SwaggerResource){
                    int flag = isJsonContent(outputMessage);
                    if(flag == 1){
                        encodedParam = JSON.toJSONString(object, QM_JSON_VALUE_SERIALIZER).getBytes();
                    } else if(flag == 2){
                        encodedParam = JSON.toJSONString(object, QM_JSON_VALUE_SERIALIZER).getBytes();
                    }else {
                        encodedParam = Base64.encodeBase64(JSON.toJSONBytes(object));
                    }
                }
            }
        }

        outputMessage.getBody().write(encodedParam);
    }

    protected int isJsonContent(HttpOutputMessage outputMessage){
        ServletServerHttpResponse response = (ServletServerHttpResponse)outputMessage;
        String dataType = response.getServletResponse().getHeader(HTTP_HEAD_RESPONSE_DATA_TYPE);

        if(StringUtils.hasText(dataType) && dataType.equalsIgnoreCase(QM_RESPONSE_DATA_TYPE_JSON_BASE64)) {
            // base64
            response.getServletResponse().setHeader(HTTP_HEAD_RESPONSE_DATA_TYPE, QM_RESPONSE_DATA_TYPE_JSON_BASE64);
            return 0;
        }

        if(StringUtils.hasText(dataType) && dataType.equalsIgnoreCase(QM_ORGIN_RESPONSE_DATA_TYPE_JSON)) {
            // base64
            response.getServletResponse().setHeader(HTTP_HEAD_RESPONSE_DATA_TYPE, QM_ORGIN_RESPONSE_DATA_TYPE_JSON);
            return 2;
        }

        response.getServletResponse().setHeader(HTTP_HEAD_RESPONSE_DATA_TYPE, QM_RESPONSE_DATA_TYPE_JSON);
        // 默认json
        return 1;
    }
}
