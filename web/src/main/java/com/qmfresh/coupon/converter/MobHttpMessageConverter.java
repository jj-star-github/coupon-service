package com.qmfresh.coupon.converter;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;

import java.io.IOException;

/**
 * 解决content type为*的问题
 *
 * @author Ivan
 */
public abstract class MobHttpMessageConverter<T> extends AbstractHttpMessageConverter<T> {
    public MobHttpMessageConverter() {
    }

    public MobHttpMessageConverter(MediaType supportedMediaType) {
        super(supportedMediaType);
    }

    public MobHttpMessageConverter(MediaType... supportedMediaTypes) {
        super(supportedMediaTypes);
    }


    protected void addDefaultHeaders(HttpHeaders headers, T t, MediaType contentType) throws IOException {
        if (headers.getContentType() == null) {
            MediaType contentTypeToUse = contentType;
            if (contentType == null || contentType.isWildcardType() || contentType.isWildcardSubtype()) {
                contentTypeToUse = getDefaultContentType(t);
            }

            if (contentTypeToUse != null) {
                headers.setContentType(contentTypeToUse);
            }
        }
        if (headers.getContentLength() < 0) {
            Long contentLength = getContentLength(t, headers.getContentType());
            if (contentLength != null) {
                headers.setContentLength(contentLength);
            }
        }
    }
}
