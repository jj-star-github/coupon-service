package com.qmfresh.coupon.converter;

import com.alibaba.fastjson.serializer.ValueFilter;

public class QMJsonValueSerializer implements ValueFilter {
    @Override
    public Object process(Object object, String name, Object value) {
        if(value instanceof Long){
            return value.toString();
        }
        return value;
    }
}
