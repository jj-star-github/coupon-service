package com.qmfresh.coupon.controller.facade.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("促销失败原因")
public class CouponFailReason implements Serializable {

    @ApiModelProperty("券失败标题")
    private String title;
    @ApiModelProperty("券失败原因")
    private String reason;
    @ApiModelProperty("优惠券码")
    private String couponCode;
    @ApiModelProperty("发送订单号")
    private String sourceCode;
    @ApiModelProperty("退款订单号")
    private String refundCode;
    @ApiModelProperty("会员卡号")
    private String userCardCode;
    @ApiModelProperty("用户手机号")
    private String userPhone;
}
