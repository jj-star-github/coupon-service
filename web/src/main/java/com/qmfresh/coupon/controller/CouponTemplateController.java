package com.qmfresh.coupon.controller;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.controller.facade.TemplateFacade;
import com.qmfresh.coupon.controller.facade.dto.GoodsConditionDTO;
import com.qmfresh.coupon.controller.facade.dto.QueryCouponTemplateDTO;
import com.qmfresh.coupon.controller.facade.dto.ShopConditionDTO;
import com.qmfresh.coupon.interfaces.dto.coupon.IdParamBean;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.controller.facade.dto.CreateCouponTemplateDTO;
import com.qmfresh.coupon.services.common.business.ServiceResultUtil;
import com.qmfresh.coupon.services.platform.domain.model.template.CouponTemplateInfo;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateGoodsVo;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateInfoVo;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateShopVo;
import com.qmfresh.coupon.services.platform.domain.shared.BizCode;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.service.DLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/couponTemplate")
@Slf4j
public class CouponTemplateController {

    @Resource
    private DLock dLock;
    @Resource
    private TemplateFacade templateFacade;

    /**
     * 创建优惠券模板
     */
    @RequestMapping(value = "/createCouponTemplate", method = RequestMethod.POST)
    public ServiceResult createCouponTemplate(@RequestBody CreateCouponTemplateDTO dto) {
        log.info("创建券模板活动-请求 dto={}", JSON.toJSONString(dto));
        try{
            boolean result = dLock.tryLock(DLock.createTemplateKey(dto.getOperatorId().longValue()),15000L);
            if(!result){
                return ServiceResultUtil.resultError(BizCode.ERROR.getCode(),"用户正在创建券模板！");
            }
            templateFacade.createCouponTemplate(dto);
            return ServiceResult.success(true);
        }catch (BusinessException be){
            log.warn("创建券模板活动-业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.ERROR.getCode(),be.getErrorMsg());
        }catch (Exception e){
            log.error("创建券模板活动：接口异常 e={}", e.getMessage());
            return ServiceResultUtil.resultError(BizCode.ERROR.getCode(),e.getMessage());
        }
    }

    /**
     * 券模板列表
     */
    @RequestMapping(value = "/queryByCondition", method = RequestMethod.POST)
    public ServiceResult<Page<TemplateInfoVo>> queryByCondition(@RequestBody QueryCouponTemplateDTO dto){
        log.info("券模板列表查询：请求 dto={}", JSON.toJSONString(dto));
        try {
            Page<TemplateInfoVo> templateInfoVoPage = templateFacade.queryCouponTemplate(dto);
            log.info("券模板列表查询：返回 dto={}", JSON.toJSONString(templateInfoVoPage));
            return ServiceResult.success(templateInfoVoPage);
        } catch (BusinessException be) {
            log.warn("券模板列表查询：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("券模板列表查询：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     * 券模板信息
     */
    @RequestMapping(value = "/queryById", method = RequestMethod.POST)
    public ServiceResult<CouponTemplateInfo>    queryById(@RequestBody IdParamBean bean){
        log.info("券模板信息查询：请求 dto={}", JSON.toJSONString(bean));
        try {
            CouponTemplateInfo couponTemplateInfo = templateFacade.queryById(bean.getId());
            return ServiceResult.success(couponTemplateInfo);
        } catch (BusinessException be) {
            log.warn("券模板信息查询：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("券模板信息查询：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     * 券模板商品信息
     */
    @RequestMapping(value = "/queryGoodsById", method = RequestMethod.POST)
    public ServiceResult<Page<TemplateGoodsVo>> queryGoodsById(@RequestBody GoodsConditionDTO dto){
        log.info("券模板商品信息查询：请求 dto={}", JSON.toJSONString(dto));
        try {
            Page<TemplateGoodsVo> goodsVoPage = templateFacade.queryGoodsByCondition(dto);
            return ServiceResult.success(goodsVoPage);
        } catch (BusinessException be) {
            log.warn("券模板商品信息查询：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("券模板商品信息查询：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }


    /**
     * 券模板门店信息
     */
    @RequestMapping(value = "/queryShopById", method = RequestMethod.POST)
    public ServiceResult<Page<TemplateShopVo>> queryShopById(@RequestBody ShopConditionDTO dto){
        log.info("券模板门店信息查询：请求 dto={}", JSON.toJSONString(dto));
        try {
            Page<TemplateShopVo> shopVoPage = templateFacade.queryShopByCondition(dto);
            return ServiceResult.success(shopVoPage);
        } catch (BusinessException be) {
            log.warn("券模板门店信息查询：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("券模板门店信息查询：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }
}
