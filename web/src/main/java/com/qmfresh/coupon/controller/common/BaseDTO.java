package com.qmfresh.coupon.controller.common;

import lombok.Data;

import java.io.Serializable;

/**
 * http://wiki.qmgyl.net/pages/viewpage.action?pageId=15204503
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class BaseDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    private Integer shopId;
    private Integer channel;
    private Integer version;
    private String ip;
    private Integer operatorId;
    private String operatorName;


}
