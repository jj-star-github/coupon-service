package com.qmfresh.coupon.controller;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.controller.facade.SendActivityFacade;
import com.qmfresh.coupon.controller.facade.dto.*;
import com.qmfresh.coupon.services.common.business.ServiceResultUtil;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityCouponVO;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityEffectVO;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityVO;
import com.qmfresh.coupon.services.platform.domain.model.coupon.CouponDetailVO;
import com.qmfresh.coupon.services.platform.domain.shared.BizCode;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.infrastructure.oss.OssUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import com.qmfresh.coupon.services.common.business.ServiceResult;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 发券活动
 */
@RestController
@RequestMapping(value = "/sendActivity")
@Slf4j
public class SendActivityController {

    @Resource
    private SendActivityFacade sendActivityFacade;
    @Resource
    private OssUtils ossUtils;

    /**
     * 创建活动
     *
     * @param createSendActivity
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ServiceResult queryCouponNumCanUse(@RequestBody CreateSendActivityDTO createSendActivity) {
        log.info("发放活动创建： param={}", JSON.toJSONString(createSendActivity));
        try {
            sendActivityFacade.createSendActivity(createSendActivity);
            return ServiceResult.success(null);
        } catch (BusinessException be) {
            log.warn("发放活动创建：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("发放活动创建：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     *
     * 查询活动奖品
     * @param param
     * @return
     */
    @RequestMapping(value = "/queryCoupon", method = RequestMethod.POST)
    public ServiceResult<List<SendActivityCouponVO>> queryMemCoupon(@RequestBody SendActivityDTO param) {
        log.info("查询活动奖品： param={}", JSON.toJSONString(param));
        try {
            List<SendActivityCouponVO> couponVOList = sendActivityFacade.queryCouponByActivityId(param.getId());
            return ServiceResult.success(couponVOList);
        } catch (BusinessException be) {
            log.warn("查询活动奖品：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("查询活动奖品：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     *
     * 关闭活动
     * @param param
     * @return
     */
    @RequestMapping(value = "/close", method = RequestMethod.POST)
    public ServiceResult close(@RequestBody SendActivityDTO param) {
        log.info("关闭活动： param={}", JSON.toJSONString(param));
        try {
            sendActivityFacade.closedActivity(param);
            return ServiceResult.success(null);
        } catch (BusinessException be) {
            log.warn("关闭活动：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("关闭活动：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     *
     * 关闭活动
     * @param param
     * @return
     */
    @RequestMapping(value = "/open", method = RequestMethod.POST)
    public ServiceResult open(@RequestBody SendActivityDTO param) {
        log.info("启动活动： param={}", JSON.toJSONString(param));
        try {
            sendActivityFacade.openActivity(param);
            return ServiceResult.success(null);
        } catch (BusinessException be) {
            log.warn("启动活动：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("启动活动：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     * 编辑活动绑定的优惠券
     * @param bean
     * @return
     */
    @RequestMapping(value = "/updateActivityCoupon", method = RequestMethod.POST)
    public ServiceResult updateActivityCoupon(@RequestBody UpdateSendActivityCouponDTO bean) {
        log.info("发放活动查询：请求 param={}", JSON.toJSONString(bean));
        try {
            sendActivityFacade.updateActivityCoupon(bean);
            return ServiceResult.success(null);
        } catch (BusinessException be) {
            log.warn("发放活动查询：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("发放活动查询：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     * 删除活动绑定的优惠券
     * @param bean
     * @return
     */
    @RequestMapping(value = "/deleteActivityCoupon", method = RequestMethod.POST)
    public ServiceResult deleteActivityCoupon(@RequestBody UpdateSendActivityCouponDTO bean) {
        log.info("发放活动查询：请求 param={}", JSON.toJSONString(bean));
        try {
            sendActivityFacade.deleteActivityCoupon(bean);
            return ServiceResult.success(null);
        } catch (BusinessException be) {
            log.warn("发放活动查询：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("发放活动查询：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    @RequestMapping(value = "/addActivityCoupon", method = RequestMethod.POST)
    public ServiceResult addActivityCoupon(@RequestBody AddSendActivityCouponDTO bean) {
        log.info("发放活动绑定优惠券：请求 param={}", JSON.toJSONString(bean));
        try {
            sendActivityFacade.addActivityCoupon(bean);
            return ServiceResult.success(null);
        } catch (BusinessException be) {
            log.warn("发放活动绑定优惠券：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("发放活动绑定优惠券：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    @ApiOperation("活动信息")
    @PostMapping("/page")
    public ServiceResult<Page<SendActivityVO>> pageGeneralCpActivity(@RequestBody QuerySendActivityDTO querySendActivityDTO) {
        log.info("发放活动查询：请求 param={}", JSON.toJSONString(querySendActivityDTO));
        try {
            Page<SendActivityVO> sendActivityVOPage = sendActivityFacade.querySendActivity(querySendActivityDTO);
            return ServiceResult.success(sendActivityVOPage);
        } catch (BusinessException be) {
            log.warn("发放活动查询：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("发放活动查询：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     * 创建活动
     */
    @RequestMapping(value = "/createSendToUse", method = RequestMethod.POST)
    public ServiceResult createSendToUse(@RequestBody CreateSendToUseDTO dto) {
        log.info("定向发放创建： param={}", JSON.toJSONString(dto));
        try {
            sendActivityFacade.createSendToUse(dto);
            return ServiceResult.success(true);
        } catch (BusinessException be) {
            log.warn("定向发放创建：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        } catch (Exception e) {
            log.error("定向发放创建：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     * 活动效果分析
     */
    @RequestMapping(value = "/activityEffect", method = RequestMethod.POST)
    public ServiceResult<List<SendActivityEffectVO>> activityEffect(@RequestBody SendActivityDTO param){
        log.info("活动效果分析： param={}", JSON.toJSONString(param));
        try{
            List<SendActivityEffectVO> sendActivityEffectVOList = sendActivityFacade.activityEffect(param.getId());
            return ServiceResult.success(sendActivityEffectVOList);
        }catch (BusinessException be){
            log.warn("活动效果分析：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        }catch (Exception e){
            log.error("活动效果分析：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     * 发放明细
     */
    @RequestMapping(value = "queryCouponDetail",method = RequestMethod.POST)
    public ServiceResult<Page<CouponDetailVO>> queryCouponDetail(@RequestBody CouponDetailDTO detailDTO){
        log.info("发放明细： param={}", JSON.toJSONString(detailDTO));
        try{
            Page<CouponDetailVO> couponDetailVOPage = sendActivityFacade.queryCouponDetail(detailDTO);
            return ServiceResult.success(couponDetailVOPage);
        }catch (BusinessException be){
            log.warn("发放明细：业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResult.fail(BizCode.ERROR, be.getErrorMsg());
        }catch (Exception e){
            log.error("发放明细：接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResult.fail(BizCode.ERROR, "系统异常");
        }
    }

    /**
     * 上传
     */
    @RequestMapping(value = "upload",method = RequestMethod.POST)
    public ServiceResult upload(@RequestPart(value = "file", required = false) MultipartFile mfile){
        int beginTime = DateUtil.getCurrentTimeIntValue();
        log.info("upload beginTime:" + beginTime);
        String fileName = ossUtils.upload(mfile);
        int endTime = DateUtil.getCurrentTimeIntValue();
        log.info("upload endTime:" + endTime + ",upload time:" + (endTime - beginTime));
        return ServiceResult.success(fileName);
    }

}
