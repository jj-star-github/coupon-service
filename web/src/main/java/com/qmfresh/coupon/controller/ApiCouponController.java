/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/6
 * Description:CouponApiController.java
 */
package com.qmfresh.coupon.controller;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.controller.facade.CouponWebFacade;
import com.qmfresh.coupon.controller.facade.SendCouponFacade;
import com.qmfresh.coupon.controller.facade.TemplateFacade;
import com.qmfresh.coupon.controller.facade.dto.CouponReturnDTO;
import com.qmfresh.coupon.controller.facade.dto.CouponReturnParamDTO;
import com.qmfresh.coupon.controller.facade.dto.QueryByUserAndGoodsDTO;
import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.services.platform.application.coupon.vo.CouponCampOnReturnBean;
import com.qmfresh.coupon.services.platform.application.coupon.vo.SendCouponReturnVo;
import com.qmfresh.coupon.services.platform.domain.shared.BizCode;
import com.qmfresh.coupon.services.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.PromotionProtocol;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.services.common.business.ServiceResultUtil;
import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.services.common.exception.ExceptionTypeEnum;
import com.qmfresh.coupon.services.common.utils.ListUtil;
import com.qmfresh.coupon.services.entity.bo.CouponReturnBo;
import com.qmfresh.coupon.services.entity.bo.CouponReturnParamBo;
import com.qmfresh.coupon.services.entity.command.CouponPrintCheckCommand;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;
import com.qmfresh.coupon.services.service.IApiCouponService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lxc
 */
@RestController
@RequestMapping(value = "/apiCoupon")
@Slf4j
public class ApiCouponController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiCouponController.class);

    @Autowired
    private IApiCouponService apiCouponService;

    @Resource
    private SendCouponFacade sendCouponFacade;

    @Resource
    private CouponWebFacade couponWebFacade;

    /**
     * 会员查询我的可用优惠券个数
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryCouponNumCanUse", method = RequestMethod.POST)
    public ServiceResult queryCouponNumCanUse(@RequestBody UserIdParamBean bean) {
        if (bean == null || bean.getUserId() == null) {
            return ServiceResultUtil.resultError("入参用户id不能为空");
        }
        return ServiceResultUtil.resultSuccess(apiCouponService.queryCouponNumCanUse(bean));
    }

    /**
     * 会员查询我的优惠券
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryMemCoupon", method = RequestMethod.POST)
    public ServiceResult<ReturnAppBean> queryMemCoupon(@RequestBody UserIdParamBean bean) {
        if (bean == null || bean.getUserId() == null) {
            return ServiceResultUtil.resultError("入参用户id不能为空");
        }
        return ServiceResultUtil.resultSuccess(apiCouponService.queryMemCoupon(bean));
    }

    /**
     * 促销查询可发放的优惠券
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryCouponActivityCanUse", method = RequestMethod.POST)
    public ServiceResult<List<ProQueryCouponReturnBean>> queryMemCoupon(@RequestBody ProQueryCouponRequestBean bean) {
        Assert.notNull(bean, "查询可用活动请求参数不能为空");
        return ServiceResultUtil.resultSuccess(apiCouponService.queryCouponActivityCanUse(bean));
    }

    /**
     * 收银触发发放优惠券
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/cashTriggerSendCoupon", method = RequestMethod.POST)
    public ServiceResult<List<CashQuerySendCouponReturnBean>> cashTriggerSendCoupon(@RequestBody CashQuerySendCouponRequestBean bean) {
        LOGGER.info("发送优惠券：cashTriggerSendCoupon param={}", JSON.toJSONString(bean));
        Assert.notNull(bean, "收银端触发赠送优惠券请求参数不能为空");
        Assert.notNull(bean.getActivityId(), "收银端触发赠送优惠券活动id不能为空");
        Assert.notNull(bean.getSendNum(), "收银端触发赠送优惠券赠送能为空");
        return ServiceResultUtil.resultSuccess(apiCouponService.cashTriggerSendCoupon(bean, true));
    }

    /**
     * 收银触发发放优惠券
     *
     * @param bean 优惠券发送
     * @return
     */
    @RequestMapping(value = "/cashTriggerSendCouponNew", method = RequestMethod.POST)
    public ServiceResult<List<CashQuerySendCouponReturnBean>> cashTriggerSendCouponNew(@RequestBody CashQuerySendCouponRequestBean bean) {
        LOGGER.info("发送优惠券：cashTriggerSendCouponNew param={}", JSON.toJSONString(bean));
        Assert.notNull(bean, "收银端触发赠送优惠券请求参数不能为空");
        Assert.notNull(bean.getActivityId(), "收银端触发赠送优惠券活动id不能为空");
        Assert.notNull(bean.getSendNum(), "收银端触发赠送优惠券赠送能为空");
        Assert.notNull(bean.getRealAmount(), "实付金额不能为空");
        return ServiceResultUtil.resultSuccess(apiCouponService.cashTriggerSendCoupon(bean, true));
    }

    /**
     * 云POS项目发券 （支持券码传入）
     *
     * @param bean 优惠券发送
     * @return
     */
    @RequestMapping(value = "/cloudPosSendCoupon", method = RequestMethod.POST)
    public ServiceResult<List<SendCouponReturnVo>> cloudPosSendCoupon(@RequestBody CashQuerySendCouponRequestBean bean) {
        LOGGER.info("发送优惠券：cashTriggerSendCoupon param={}", JSON.toJSONString(bean));
        try {
            List<SendCouponReturnVo> sendCouponReturnVoList = sendCouponFacade.sendCoupon(bean);
            return ServiceResultUtil.resultSuccess(sendCouponReturnVoList);
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("发送优惠券：cashTriggerSendCoupon 业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResultUtil.resultError(9999, be.getErrorMsg());
        } catch (Exception e) {
            log.error("发送优惠券：cashTriggerSendCoupon 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultUtil.resultError(9999, "系统异常");
        }
    }


    /**
     * 结算页查询所有未过期的优惠券
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/settlementQueryMemCoupon", method = RequestMethod.POST)
    public ServiceResult<ReturnAppBean> settlementQueryMemCoupon(@RequestBody SettQueryCouponRequestBean bean) {
        if (bean == null || bean.getUserId() == null) {
            return ServiceResultUtil.resultError("入参用户id不能为空");
        }
        return ServiceResultUtil.resultSuccess(apiCouponService.settQueryMemCoupon(bean));
    }

    /**
     * 结算页查询所有未过期的优惠券(分页)
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryMyUnusedCouponWithPage")
    public ServiceResult queryMyUnusedCouponWithPage(@RequestBody SettQueryCouponRequestBean bean) {
        if (bean == null || bean.getUserId() == null) {
            return ServiceResultUtil.resultError("入参用户id不能为空");
        }
        return ServiceResultUtil.resultSuccess(apiCouponService.queryMyUnusedCouponWithPage(bean));
    }

    /**
     * 提交订单确认扣减优惠券钱，再次查询
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryMemCouponAgain", method = RequestMethod.POST)
    public ServiceResult<QueryAgainReturnBean> queryMemCouponAgain(@RequestBody QueryAgainRequestBean bean) {
        if (bean == null || bean.getUserId() == null) {
            return ServiceResultUtil.resultError("入参用户id不能为空");
        }
        Assert.notNull(bean.getCouponId(), "查询的优惠券id不能为空");
        return ServiceResultUtil.resultSuccess(apiCouponService.queryMemCouponAgain(bean));
    }

    /**
     * 优惠券使用
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/useCoupon", method = RequestMethod.POST)
    public ServiceResult<CouponUseReturnBean> useCoupon(@RequestBody CouponUseRequestBean bean) {
        if (bean == null || bean.getCouponId() == null) {
            return ServiceResultUtil.resultError("优惠券id不能为空");
        }
        if ((null == bean.getBizChannel() && bean.getUserId() == null)) {
            return ServiceResultUtil.resultError("用户ID不可为空");
        }
        if ((null != bean.getBizChannel() && bean.getOrderNo() == null)) {
            return ServiceResultUtil.resultError("订单号不可为空");
        }
        return ServiceResultUtil.resultSuccess(apiCouponService.useCoupon(bean));
    }


    @RequestMapping(value = "/checkCoupon")
    public ServiceResult checkCoupon(@RequestBody CouponCheckRequestBean bean) {
        LOGGER.info("优惠券验证：checkCoupon param={}", JSON.toJSONString(bean));
        try {
            return ServiceResultUtil.resultSuccess(couponWebFacade.checkCoupon(bean));
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("优惠券验证：checkCoupon 业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResultUtil.resultError(9999, be.getErrorMsg());
        } catch (Exception e) {
            log.error("优惠券验证：checkCoupon 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultUtil.resultError(9999, "系统异常");
        }

    }

    /**
     * 接口补打检查
     *
     * @param bean 补打请求
     * @return 接口补打
     */
    @PostMapping(value = "/print/check")
    @ApiOperation("云pos优惠券补打检查")
    public ServiceResult<List<CouponCheckReturnBean>> printCheck(@RequestBody CouponPrintCheckCommand bean) {
        //这里userId非必传
        log.info("订单优惠券补打检查：{}", JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(apiCouponService.printCheck(bean));
    }

    /**
     * 根据coupon信息校验优惠券，收银用
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/checkCouponByCouponInfo")
    public ServiceResult checkCouponByCouponInfo(@RequestBody CouponCheckRequestBean bean) {
        return ServiceResultUtil.resultSuccess(apiCouponService.checkCouponByCouponInfo(bean));
    }

    /**
     * 补打优惠券
     *
     * @param param
     * @return
     */
    @RequestMapping(value = "/regainCoupon")
    public ServiceResult regainCoupon(@RequestBody PromotionProtocol param) {
        return ServiceResultUtil.resultSuccess(apiCouponService.regainCoupon(param));
    }

    /**
     * 查询发放到用户手中的优惠券
     *
     * @return
     */
    @RequestMapping(value = "/querySentCouponList")
    public ServiceResult querySentCouponList(@RequestBody QuerySentCouponListRequestBean bean) {
        return ServiceResultUtil.resultSuccess(apiCouponService.querySentCouponList(bean));
    }

    /**
     * 分页
     * 查询发放到用户手中的优惠券
     *
     * @return
     */
    @RequestMapping(value = "/queryMyCouponListWithPage")
    public ServiceResult queryMyCouponListWithPage(@RequestBody QuerySentCouponListRequestBean bean) {
        return ServiceResultUtil.resultSuccess(apiCouponService.querySentCouponListWithPage(bean));
    }

    /**
     * 分页
     * 查询发放到用户手中的优惠券 商城
     * @return
     */
    @RequestMapping(value = "/queryMyCouponListWithPageForMall")
    public ServiceResult queryMyCouponListWithPageForMall(@RequestBody QuerySentCouponListRequestBean bean){
        return ServiceResultUtil.resultSuccess(apiCouponService.querySentCouponListWithPageForMall(bean));
    }

    /**
     * 根据优惠券ID查询优惠券信息
     * 促销
     *
     * @param bean 查询优惠券信息
     * @return
     */
    @RequestMapping(value = "/queryCouponInfo", method = RequestMethod.POST)
    public ServiceResult<CouponCampOnReturnBean> queryCouponInfo(@RequestBody CashQuerySendCouponRequestBean bean) {
        LOGGER.info("券码预占：queryCouponInfo param={}", JSON.toJSONString(bean));
        try {
            CouponCampOnReturnBean couponCampOnReturnBean = sendCouponFacade.campOn(bean.getActivityId(), bean.getSourceShopId(), bean.getSendNum());
            return ServiceResultUtil.resultSuccess(couponCampOnReturnBean);
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("券码预占：queryCouponInfo 业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResultUtil.resultError(9999, be.getErrorMsg());
        } catch (Exception e) {
            log.error("券码预占：queryCouponInfo 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultUtil.resultError(9999, "系统异常");
        }
    }

    /**
     * 根据useID查询优惠券信息（线上商城用）
     */
    @RequestMapping(value = "/queryCouponByUserId", method = RequestMethod.POST)
    public ServiceResult<List<CouponForAppReturnBean>> queryCouponByUserId(@RequestBody CouponForAppRequestBean bean) {
        Assert.notNull(bean, "App查询优惠券信息-请求参数不可为空");
        Assert.notNull(bean.getUserId(), "App查询优惠券信息-UserId不可为空");
        return ServiceResultUtil.resultSuccess(apiCouponService.queryCouponByUserId(bean));
    }

    /**
     * 发放优惠券（线上商城用）
     */
    @RequestMapping(value = "/sendCouponForApp", method = RequestMethod.POST)
    public ServiceResult<List<SendCouponReturnVo>> sendCouponForApp(@RequestBody SendCouponDto bean) {
        LOGGER.info("线上商城发券：sendCouponForApp param={}", JSON.toJSONString(bean));
        try {
            List<SendCouponReturnVo> sendCouponReturnVoList = sendCouponFacade.sendCouponForApp(bean);
            return ServiceResultUtil.resultSuccess(sendCouponReturnVoList);
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("线上商城发券：sendCouponForApp 业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResultUtil.resultError(9999, be.getErrorMsg());
        } catch (Exception e) {
            log.error("线上商城发券：sendCouponForApp 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultUtil.resultError(9999, "系统异常");
        }
    }

    /**
     * 查询状态为生效中的优惠券活动(线上商城用)
     *
     * @return
     */
    @RequestMapping(value = "/queryUseingCoupon", method = RequestMethod.POST)
    public ServiceResult<ListWithPage<ProQueryCouponReturnBean>> queryUseingCoupon(@RequestBody QueryCouponInfoBean bean) {
        LOGGER.info("into method queryUseingCoupon,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(apiCouponService.queryUseingCoupon(bean));
    }

    /**
     * 优惠券退款
     *
     * @param returnParamDTO 优惠券退参数
     * @return 优惠券退参数
     */
    @PostMapping(value = "/return/coupon")
    public ServiceResult<CouponReturnDTO> couponReturn(@RequestBody CouponReturnParamDTO returnParamDTO) {
        LOGGER.info("couponReturn：{}", JSON.toJSONString(returnParamDTO));
        CouponReturnParamBo couponReturnParamBo = CouponReturnParamDTO.create(returnParamDTO);
        CouponReturnBo couponReturnBo = apiCouponService.returnCoupon(couponReturnParamBo);
        CouponReturnDTO couponReturnDTO = CouponReturnDTO.create(couponReturnBo);
        return ServiceResultUtil.resultSuccess(couponReturnDTO);
    }


    /**
     * 用户领取优惠券（流量池小程序领）
     */
    @RequestMapping(value = "/receiveCoupon",method = RequestMethod.POST)
    public ServiceResult<SendCouponReturnVo> receiveCoupon(@RequestBody ReceiveCouponDTO receiveCouponDTO){
        LOGGER.info("流量池小程序领：receiveCoupon param={}", JSON.toJSONString(receiveCouponDTO));
        try {
            SendCouponReturnVo sendCouponReturnVo = sendCouponFacade.receiveCoupon(receiveCouponDTO);
            return ServiceResultUtil.resultSuccess(sendCouponReturnVo);
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            log.warn("流量池小程序领：receiveCoupon 业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResultUtil.resultError(9999, be.getErrorMsg());
        } catch (Exception e) {
            log.error("流量池小程序领：receiveCoupon 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultUtil.resultError(9999, "系统异常");
        }
    }

}
