/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/26
 * Description:CouponController.java
 */
package com.qmfresh.coupon.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qmfresh.coupon.controller.facade.SendCouponFacade;
import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.CreateActivityBean;
import com.qmfresh.coupon.interfaces.dto.coupon.IdParamBean;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponInfoBean;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponReturnBean;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvailableCouponQuery;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvilableCoupon;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetail;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetailQuery;
import com.qmfresh.coupon.interfaces.dto.member.MemberSendCouponParam;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.services.common.business.ServiceResultUtil;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponCount;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponListBean;
import com.qmfresh.coupon.services.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;
import com.qmfresh.coupon.services.service.ICouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @author lxc
 */
@RestController
@RequestMapping(value = "/coupon")
public class CouponController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponController.class);

    @Autowired
    private ICouponService couponApiService;

    /**
     * 创建优惠券活动
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/createCouponActivity", method = RequestMethod.POST)
    public ServiceResult createCouponActivity(@RequestBody CreateActivityBean bean) {
        LOGGER.info("into method createCouponActivity,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponApiService.createCouponActivity(bean));
    }

    /**
     * 查询优惠券列表
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryCouponInfoList", method = RequestMethod.POST)
    public ServiceResult<ListWithPage<QueryCouponReturnBean>> queryCouponInfoList(@RequestBody QueryCouponInfoBean bean) {
        LOGGER.info("into method queryCouponInfoList,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponApiService.queryCouponInfoList(bean));
    }

    /**
     * 查询优惠券明细
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryCouponDetailInfo", method = RequestMethod.POST)
    public ServiceResult<CreateActivityBean> queryCouponDetailInfo(@RequestBody IdParamBean bean) {
        LOGGER.info("into method queryCouponDetailInfo,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponApiService.queryCouponDetailInfo(bean));
    }

    /**
     * 优惠券修改
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/modifyCouponInfo", method = RequestMethod.POST)
    public ServiceResult modifyCouponInfo(@RequestBody CreateActivityBean bean) {
        LOGGER.info("into method modifyCouponInfo,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponApiService.modifyCouponInfo(bean));
    }

    /**
     * 优惠券删除
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/delCouponActivity", method = RequestMethod.POST)
    public ServiceResult delCouponActivity(@RequestBody IdParamBean bean) {
        LOGGER.info("into method delCouponActivity,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponApiService.delCouponActivity(bean));
    }

    @RequestMapping(value = "/sendCouponToUser",method = RequestMethod.POST)
    public ServiceResult sendCouponToUser(@RequestBody SendCouponDto param){
        LOGGER.info("into method sendCouponToUser, 入参：" + JSON.toJSONString(param));
        return ServiceResultUtil.resultError("该功能已暂停使用请联系开发人员");
    }

    /**
     * 查询会员用户的优惠券列表
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryCouponDetail", method = RequestMethod.POST)
    public ServiceResult<ListWithPage<MemberCouponDetail>> queryCouponDetail(@RequestBody MemberCouponDetailQuery bean) {
        LOGGER.info("into method queryCouponDetail,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponApiService.queryMemberCouponDetail(bean));
    }

    /**
     * 查询会员中台可发放优惠券列表
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/availableSendCouponList", method = RequestMethod.POST)
    public ServiceResult<ListWithPage<MemberAvilableCoupon>> availableSendCouponList(@RequestBody MemberAvailableCouponQuery bean) {
        LOGGER.info("into method availableSendCouponList,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponApiService.queryMemberAvilableCouponList(bean));
    }

    @Resource
    private SendCouponFacade sendCouponFacade;
    /**
     * 优惠券删除
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/sendCouponToMember", method = RequestMethod.POST)
    public ServiceResult sendCouponToMember(@RequestBody MemberSendCouponParam bean) {
        LOGGER.info("发送优惠券：sendCouponToMember param={}", JSON.toJSONString(bean));
        try {
            return ServiceResultUtil.resultSuccess(sendCouponFacade.sendCouponToMember(bean));
        } catch (com.qmfresh.coupon.services.platform.domain.shared.BusinessException be) {
            LOGGER.warn("发送优惠券：sendCouponToMember 业务异常 errorMsg={}", be.getErrorMsg());
            return ServiceResultUtil.resultError(9999, be.getErrorMsg());
        } catch (Exception e) {
            LOGGER.error("发送优惠券：sendCouponToMember 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultUtil.resultError(9999, "系统异常");
        }
    }





    /**
     * 优惠券后台管理需要查询展示的详情信息
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/queryCouponListDetail", method = RequestMethod.POST)
    public ServiceResult<Page<QueryCouponListBean>> queryCouponListDetail(@RequestBody QueryCouponCount data) {
        LOGGER.info("into method queryCouponListDetail,入参:" + JSON.toJSONString(data));
        return ServiceResultUtil.resultSuccess(couponApiService.queryCouponListDetail(data));
    }
}
