package com.qmfresh.coupon.controller.facade.dto;

import java.util.List;

import lombok.Data;

@Data
public class SendActivity {
    /**
     * 活动ID
     */
    private Long id;

    /**
     * 活动名
     */
    private String activityName;

    /**
     * 活动状态
     */
    private Integer status;

    /**
     * 关联优惠券名
     */
    private List<String> couponName;

    /**
     * 关联优惠券描述
     */
    private List<String> couponRemark;

}
