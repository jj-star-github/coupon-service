package com.qmfresh.coupon.controller;

import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.vo.TestVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "测试模块", description = "测试模块描述")
@RestController
public class TestController {

    @ApiOperation("测试接口")
    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public TestVO input(@RequestBody TestVO vo) {
        vo.setData("test");
        return vo;
    }

    @ApiOperation("测试异常接口")
    @RequestMapping(value = "/testException", method = RequestMethod.POST)
    public void testException() {
        throw new BusinessException("业务异常");
    }

}
