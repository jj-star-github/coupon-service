package com.qmfresh.coupon.controller.facade.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CouponTemplateTargetGoodsDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 商品名
     */
    private String goodsName;
    /**
     * 商品类型，1.全品，2.sku,3.class1,4.class2,5.ssu,6.saleClass1,7.saleClass2,8.会场
     */
    private Integer goodsType;
}
