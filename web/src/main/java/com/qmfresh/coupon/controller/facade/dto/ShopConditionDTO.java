package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;
import lombok.Data;

import java.io.Serializable;

@Data
public class ShopConditionDTO extends BaseDTO implements Serializable {
    private Integer id;
    private Integer templateId;
    private Integer queryShopId;
    private String shopName;
    private int pageNum = 1;
    private int pageSize = 20;
}
