package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;
import lombok.Data;

import java.io.Serializable;

@Data
public class CouponDetailDTO extends BaseDTO implements Serializable {
    /**
     * 券模板ID
     */
    private Integer templateId;
    /**
     * userId
     */
    private Long searchUserId;
    /**
     * 券码
     */
    private String couponCode;

    private int pageNum = 1;
    private int pageSize = 20;
}
