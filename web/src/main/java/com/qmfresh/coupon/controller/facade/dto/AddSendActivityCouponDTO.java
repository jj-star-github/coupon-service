package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;

import lombok.Data;

import java.util.List;

@Data
public class AddSendActivityCouponDTO extends BaseDTO {

    private Long activityId;

    private SendActivityCouponDTO activityCouponDTO;
}
