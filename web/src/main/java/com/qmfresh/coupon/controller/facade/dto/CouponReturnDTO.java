package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.services.entity.bo.CouponReturnBo;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class CouponReturnDTO implements Serializable {

    /**
     * 1=已退 0=退成功 2=退失败
     */
    private Integer status;
    /**
     * 退还的优惠券码
     */
    private String couponCode;
    /**
     * 源订单号
     */
    private String sourceOrderCode;
    /**
     * 备注
     */
    private String remark;

    public static CouponReturnDTO create(CouponReturnBo couponReturnBo) {
        CouponReturnDTO couponReturnDTO = new CouponReturnDTO();
        couponReturnDTO.couponCode = couponReturnBo.getCouponCode();
        couponReturnDTO.sourceOrderCode = couponReturnBo.getSourceOrderCode();
        couponReturnDTO.status = couponReturnBo.getStatus();
        couponReturnDTO.remark = couponReturnBo.getRemark();
        return couponReturnDTO;
    }
}
