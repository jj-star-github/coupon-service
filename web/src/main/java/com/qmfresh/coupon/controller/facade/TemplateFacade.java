package com.qmfresh.coupon.controller.facade;

import com.qmfresh.coupon.controller.facade.dto.*;
import com.qmfresh.coupon.services.platform.application.coupon.vo.SendCouponReturnVo;
import com.qmfresh.coupon.services.platform.application.template.CouponTemplateService;
import com.qmfresh.coupon.services.platform.domain.model.template.CouponTemplateInfo;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateTargetGoodsManager;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateTargetShopManager;
import com.qmfresh.coupon.services.platform.domain.model.template.command.CouponTemplateCreateCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.CouponTemplateQueryCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.GoodsQueryCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.ShopQueryCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateGoodsVo;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateInfoVo;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateShopVo;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
@Slf4j
public class TemplateFacade {
    @Resource
    private CouponTemplateService couponTemplateService;
    @Resource
    private ICouponTemplateManager couponTemplateManager;
    @Resource
    private ICouponTemplateTargetGoodsManager goodsManager;
    @Resource
    private ICouponTemplateTargetShopManager shopManager;

    public void createCouponTemplate(CreateCouponTemplateDTO dto){
        CouponTemplateCreateCommand command = CouponTemplateCreateCommand.create(dto.getActivityName(),dto.getUseCondition(),
                dto.getNeedMoney(),dto.getCouponType(),dto.getConditionRule(),dto.getUseTimeType(),dto.getTimeRule(),
                dto.getApplicableGoodsType(),dto.getUseInstruction(),dto.getLimitChannel(),dto.getApplicableShopType(),
                dto.getOperatorId(),dto.getOperatorName(),dto.getGoodsList(),dto.getShopList());
        couponTemplateService.createCouponTemplate(command);
    }

    public Page<TemplateInfoVo> queryCouponTemplate(QueryCouponTemplateDTO dto){
        CouponTemplateQueryCommand queryCommand = new CouponTemplateQueryCommand(dto.getCouponName(),
                dto.getCouponType(),dto.getLimitChannel(),dto.getStatus(),dto.getGoodsType(),dto.getShopType(),dto.getBingTemplate(),
                dto.getPageNum(),dto.getPageSize());
        return couponTemplateManager.queryByCondition(queryCommand);
    }

    public CouponTemplateInfo queryById(Integer id){
        return couponTemplateManager.queryCouponTemplateById(id);
    }

    public Page<TemplateShopVo> queryShopByCondition(ShopConditionDTO dto){
        ShopQueryCommand shopQueryCommand = new ShopQueryCommand(dto.getTemplateId(),dto.getQueryShopId(),dto.getShopName(),dto.getPageNum(),dto.getPageSize());
        return shopManager.queryByCondition(shopQueryCommand);
    }

    public Page<TemplateGoodsVo> queryGoodsByCondition(GoodsConditionDTO dto){
        GoodsQueryCommand goodsQueryCommand = new GoodsQueryCommand(dto.getTemplateId(),dto.getGoodsId(),dto.getGoodsName(),dto.getPageNum(),dto.getPageSize());
        return goodsManager.queryByCondition(goodsQueryCommand);
    }
}
