package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityRule;
import lombok.Data;

import java.util.List;

@Data
public class CreateSendActivityDTO extends BaseDTO {

    private String activityName;

    private Integer sendType;

    private Integer activityType;

    private SendActivityRule sendRule;

    private List<SendActivityCouponDTO> activityCouponDTOList;

}
