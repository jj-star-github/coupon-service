package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("门店改价")
@Data
public class QueryByUserAndGoodsDTO extends BaseDTO {

    @ApiModelProperty("用户ID")
    private Integer userId;

    @ApiModelProperty("渠道 1:线下，2:线上")
    private Integer channel;

    @ApiModelProperty("商品信息")
    private GoodsDTO goods;

}
