package com.qmfresh.coupon.controller.facade;


import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.controller.facade.dto.*;
import com.qmfresh.coupon.services.platform.domain.model.activity.*;
import com.qmfresh.coupon.services.platform.domain.model.activity.command.CreateSendActivityCommand;
import com.qmfresh.coupon.services.platform.domain.model.activity.command.QuerySendActivityCommand;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivitySendTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivitySendUserTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityStatusEnums;
import com.qmfresh.coupon.services.platform.domain.model.coupon.CouponDetailVO;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.QueryCouponDetailCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.domain.shared.Operator;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.domain.shared.Subject;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivity;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivityCoupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.CouponMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import org.springframework.stereotype.Component;

import com.qmfresh.coupon.services.service.DLock;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SendActivityFacade {

    @Resource
    private SendActivityManager sendActivityManager;
    @Resource
    private SendActivityCouponManager sendActivityCouponManager;
    @Resource
    private DLock dLock;
    @Resource
    private ICouponTemplateManager templateManager;
    @Resource
    private ICouponManager couponManager;

    /**
     * 创建活动
     * @return
     */
    public void createSendActivity(CreateSendActivityDTO createSendActivityDTO) {
        //防止重复提交
        boolean lock = dLock.tryLock(DLock.createSendActivityLock(createSendActivityDTO.getOperatorId()), 3*1000l);
        if (!lock) {
            throw new BusinessException("正在创建中请勿重复操作");
        }
        Operator operator = Operator.create(createSendActivityDTO.getOperatorId(), createSendActivityDTO.getOperatorName(),
                Subject.create(Subject.Platform.INTANCE));
        CreateSendActivityCommand createSendActivityCommand = new CreateSendActivityCommand(createSendActivityDTO.getActivityName(),createSendActivityDTO.getSendType(),
                createSendActivityDTO.getActivityType(),createSendActivityDTO.getSendRule(),"", operator);
        List<SendActivityCouponVO> sendActivityCouponVOList = createSendActivityDTO.getActivityCouponDTOList().stream().map(SendActivityCouponDTO :: createVO).collect(Collectors.toList());
        createSendActivityCommand.setSendActivityCouponVOList(sendActivityCouponVOList);
        sendActivityManager.createActivity(createSendActivityCommand);
    }

    public Page<SendActivityVO> querySendActivity(QuerySendActivityDTO querySendActivityDTO){
        QuerySendActivityCommand sendActivityCommand = new QuerySendActivityCommand(querySendActivityDTO.getSendType(),
                querySendActivityDTO.getActivityType(), querySendActivityDTO.getActivityStatus(),
                querySendActivityDTO.getActivityName(), querySendActivityDTO.getPageNum(), querySendActivityDTO.getPageSize(),
                querySendActivityDTO.getBeginTime(),querySendActivityDTO.getEndTime(),querySendActivityDTO.getCreateName());
        return sendActivityManager.pageActivity(sendActivityCommand);
    }

    /**
     * 关闭活动
     * @param sendActivityDTO
     */
    public void closedActivity(SendActivityDTO sendActivityDTO) {
        if (null == sendActivityDTO.getId()) {
            return;
        }
        boolean lock = dLock.tryLock(DLock.updateSendActivityLock(sendActivityDTO.getId()), 3*1000l);
        if (!lock) {
            throw new BusinessException("正在创建中请勿重复操作");
        }
        Operator operator = Operator.create(sendActivityDTO.getOperatorId(), sendActivityDTO.getOperatorName(),
                Subject.create(Subject.Platform.INTANCE));
        sendActivityManager.updateStatus(sendActivityDTO.getId(), ActivityStatusEnums.CLOSED.getCode(), operator);
    }

    /**
     * 启动活动
     * @param sendActivityDTO
     */
    public void openActivity(SendActivityDTO sendActivityDTO) {
        if (null == sendActivityDTO.getId()) {
            return;
        }
        boolean lock = dLock.tryLock(DLock.updateSendActivityLock(sendActivityDTO.getId()), 3*1000l);
        if (!lock) {
            throw new BusinessException("正在操作中请勿重复操作");
        }
        Operator operator = Operator.create(sendActivityDTO.getOperatorId(), sendActivityDTO.getOperatorName(),
                Subject.create(Subject.Platform.INTANCE));
        sendActivityManager.updateStatus(sendActivityDTO.getId(), ActivityStatusEnums.RUNNING.getCode(), operator);
    }

    /**
     * 删除活动
     * @param sendActivityDTO
     */
    public void deleteActivity(SendActivityDTO sendActivityDTO) {
        if (null == sendActivityDTO.getId()) {
            return;
        }
        boolean lock = dLock.tryLock(DLock.updateSendActivityLock(sendActivityDTO.getId()), 3*1000l);
        if (!lock) {
            throw new BusinessException("正在创建中请勿重复操作");
        }
        Operator operator = Operator.create(sendActivityDTO.getOperatorId(), sendActivityDTO.getOperatorName(),
                Subject.create(Subject.Platform.INTANCE));
        sendActivityManager.delete(sendActivityDTO.getId(), operator);
    }

    /**
     * 查看活动关联的优惠券信息
     * @param activityId 活动ID
     * @return
     */
    public List<SendActivityCouponVO> queryCouponByActivityId(Long activityId) {
        List<SendActivityCoupon> sendActivityCouponList = sendActivityCouponManager.queryByActivityId(activityId);
        if (CollectionUtils.isEmpty(sendActivityCouponList)) {
            return null;
        }
        return sendActivityCouponList.stream().map(SendActivityCoupon :: createVO).collect(Collectors.toList());
    }

    /**
     * 修改绑定优惠券信息
     */
    public void updateActivityCoupon(UpdateSendActivityCouponDTO dto) {
        if (null == dto.getId()) {
            return;
        }
        boolean lock = dLock.tryLock(DLock.sendActivityCouponLock(dto.getId()), 3*1000l);
        if (!lock) {
            throw new BusinessException("正在创建中请勿重复操作");
        }
        Operator operator = Operator.create(dto.getOperatorId(), dto.getOperatorName(),
                Subject.create(Subject.Platform.INTANCE));
        sendActivityCouponManager.updateSendNum(dto.getActivityId(), dto.getId(), dto.getSendNum(), operator);
    }

    /**
     * 删除绑定的优惠券
     */
    public void deleteActivityCoupon(UpdateSendActivityCouponDTO dto) {
        if (null == dto.getId()) {
            return;
        }
        boolean lock = dLock.tryLock(DLock.sendActivityCouponLock(dto.getId()), 3*1000l);
        if (!lock) {
            throw new BusinessException("正在创建中请勿重复操作");
        }
        Operator operator = Operator.create(dto.getOperatorId(), dto.getOperatorName(),
                Subject.create(Subject.Platform.INTANCE));
        sendActivityCouponManager.delete(dto.getActivityId(), dto.getId(), operator);
    }

    /**
     * 绑定优惠券
     */
    public void addActivityCoupon(AddSendActivityCouponDTO dto) {
        if (null == dto.getActivityId()) {
            return;
        }
        if (null == dto.getActivityCouponDTO()) {
            return;
        }
        boolean lock = dLock.tryLock(DLock.sendActivityCouponLock(dto.getActivityId()), 3*1000l);
        if (!lock) {
            throw new BusinessException("正在操作中请勿重复操作");
        }
        List<Integer> addTemplateId = Arrays.asList(dto.getActivityCouponDTO().getCouponTemplateId());
        Operator operator = Operator.create(dto.getOperatorId(), dto.getOperatorName(),
                Subject.create(Subject.Platform.INTANCE));
        //查询活动已绑定的券信息
        List<SendActivityCoupon> oldCouponList = sendActivityCouponManager.queryByActivityId(dto.getActivityId(), addTemplateId);
        if (!CollectionUtils.isEmpty(oldCouponList)) {
            throw new BusinessException("绑定优惠券已存在请确认");
        }
        List<SendActivityCoupon> activityCoupon = Arrays.asList(new SendActivityCoupon(dto.getActivityCouponDTO().createVO(), dto.getActivityId(), operator));
        sendActivityCouponManager.insertBatch(activityCoupon, dto.getActivityId());
    }

    /**
     * 创建活动
     * @return
     */
    public void createSendToUse(@RequestBody CreateSendToUseDTO dto) {
        boolean lock = dLock.tryLock(DLock.createSendActivityLock(dto.getOperatorId()), 3*1000L);
        if (!lock) {
            throw new BusinessException("正在创建中请勿重复操作");
        }
        Operator operator = Operator.create(dto.getOperatorId(), dto.getOperatorName(), Subject.create(Subject.Platform.INTANCE));
        CreateSendActivityCommand createSendActivityCommand = new CreateSendActivityCommand(dto.getActivityName(),dto.getSendType(),
                dto.getActivityType(),dto.getSendRule(),"", operator);
        List<SendActivityCoupon> sendActivityCouponList = dto.getActivityCouponDTOList().stream().map(SendActivityCouponDTO:: createSendToUse).collect(Collectors.toList());
        createSendActivityCommand.setSendActivityCouponList(sendActivityCouponList);
        sendActivityManager.createSendToUse(createSendActivityCommand);
    }

    /**
     * 活动效果分析
     * @return
     */
    public List<SendActivityEffectVO> activityEffect(Long activityId){

        List<SendActivityEffectVO> sendActivityEffectVOList = new ArrayList<>();
        List<SendActivityCoupon> sendActivityCouponList = sendActivityCouponManager.queryByActivityId(activityId);
        if (CollectionUtils.isEmpty(sendActivityCouponList)) {
            return sendActivityEffectVOList;
        }
        SendActivity sendActivity = sendActivityManager.getById(activityId);
        SendActivityRule sendRule = JSON.parseObject(sendActivity.getSendRule(),new TypeReference<SendActivityRule>(){});
        Integer sendType = sendActivity.getSendType();
        Map<Integer, SendActivityCoupon> couponMap = sendActivityCouponList.stream().collect(Collectors.toMap(SendActivityCoupon::getTemplateId, sendActivityCoupon -> sendActivityCoupon));
        List<Integer> templateIdList = sendActivityCouponList.stream().map(SendActivityCoupon::getTemplateId).collect(Collectors.toList());
        List<CouponTemplate> couponTemplateList = templateManager.queryByIdList(templateIdList,null);
        if(CollectionUtils.isEmpty(couponTemplateList)){
            return sendActivityEffectVOList;
        }
        sendActivityEffectVOList = couponTemplateList.stream().map(couponTemplate -> {
                  SendActivityCoupon sendActivityCoupon = couponMap.get(couponTemplate.getId());
                  int sendCouponNum = 0;
                  int sendUserNum = 0;
//                  if (ActivitySendTypeEnums.APPOINT.getCode().equals(sendType)
//                      && ActivitySendUserTypeEnums.SOMEUSER.getCode().equals(sendRule.getSendUseType())) {
//                      sendCouponNum = couponManager.sendNum(couponTemplate.getId(), null);
//                      sendUserNum = couponManager.useNum(couponTemplate.getId(), null);
//                  }
                  int useCouponNum = couponManager.sendNum(couponTemplate.getId(), 1);
                  int useUserNum = couponManager.useNum(couponTemplate.getId(), 1);
                  return CouponTemplate.createEffectVo(
                      sendActivityCoupon,
                      couponTemplate,
                      sendCouponNum,
                      sendUserNum,
                      useCouponNum,
                      useUserNum);
                })
            .collect(Collectors.toList());
        return sendActivityEffectVOList;
    }

    /**
     * 发放明细
     * @return
     */
    public Page<CouponDetailVO> queryCouponDetail(CouponDetailDTO detailDTO){
        QueryCouponDetailCommand couponDetailCommand = new QueryCouponDetailCommand(detailDTO.getTemplateId(),
                detailDTO.getSearchUserId(),detailDTO.getCouponCode(),detailDTO.getPageNum(),detailDTO.getPageSize());
        return couponManager.queryCouponDetail(couponDetailCommand);
    }
}
