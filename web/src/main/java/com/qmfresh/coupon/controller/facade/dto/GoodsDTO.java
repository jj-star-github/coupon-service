package com.qmfresh.coupon.controller.facade.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("门店改价")
@Data
public class GoodsDTO implements Serializable {
    @ApiModelProperty("商品类型")
    private Integer goodType;

    @ApiModelProperty("SKUID")
    private Integer skuId;

    @ApiModelProperty("SSUID")
    private Integer ssuId;

    @ApiModelProperty("class1")
    private Integer class1Id;

    @ApiModelProperty("class2")
    private Integer class2Id;

    @ApiModelProperty("线上商城一级类目")
    private Integer saleClass1Id;

    @ApiModelProperty("线上商城二级类目")
    private Integer saleClass2Id;

    @ApiModelProperty("会场ID")
    private String meetingId;
}
