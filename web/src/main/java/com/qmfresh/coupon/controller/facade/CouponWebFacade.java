package com.qmfresh.coupon.controller.facade;

import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponCheckRequestBean;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponCheckReturnBean;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.domain.model.template.TemplateVerifyReturn;
import com.qmfresh.coupon.services.platform.domain.model.template.command.Sku;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateVerifyCommand;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CouponWebFacade {

    @Resource
    private ICouponManager couponManager;
    @Resource
    private ICouponTemplateManager couponTemplateManager;

    public CouponCheckReturnBean checkCoupon(CouponCheckRequestBean bean) {
        log.info("优惠券验证 checkCoupon,{}", JSON.toJSONString(bean));
        CouponCheckReturnBean returnBean = new CouponCheckReturnBean();
        //根据couponCode查询coupon的详细信息，看是否可用，如果可用继续，否则返回不可用信息
        Long userId = null;
        if (bean.getUserId() != null && 5 == bean.getBizChannel()) { //只有线上商城验证
            userId = bean.getUserId();
        }
        Integer id = StringUtils.isEmpty(bean.getCouponId()) ? null : Integer.valueOf(bean.getCouponId());
        if (StringUtils.isBlank(bean.getCouponCode()) && null == id) {
            throw new BusinessException("请选择有效的优惠券");
        }
        Coupon coupon = couponManager.queryByIdOrCode(id, bean.getCouponCode(), userId);
        returnBean = Coupon.check(coupon, false);
        if (returnBean != null && !returnBean.getSuccessFlag()) {
            log.info("优惠券验证-失败,{}", JSON.toJSONString(returnBean));
            return returnBean;
        }
        //验证验证券模板
        TemplateVerifyReturn verifyReturn = couponTemplateManager.verifyTemplate(new TemplateVerifyCommand(bean, coupon.getOriginalId()));
        if (!verifyReturn.getSuccessFlag()) {
            returnBean.setSuccessFlag(verifyReturn.getSuccessFlag());
            returnBean.setReason(verifyReturn.getReason());
            return returnBean;
        }
        returnBean = Coupon.buildInfo(verifyReturn.getCouponTemplate(), coupon, returnBean);
        returnBean.setUsefulGoodsList(verifyReturn.getSkuList().stream().map(Sku :: createGoods).collect(Collectors.toList()));
        log.info("优惠券验证 checkCoupon 返回,{}", JSON.toJSONString(returnBean));
        return returnBean;
    }

}
