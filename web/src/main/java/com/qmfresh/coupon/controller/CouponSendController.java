/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/6
 * Description:CouponSendController.java
 */
package com.qmfresh.coupon.controller;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.services.common.business.ServiceResultUtil;
import com.qmfresh.coupon.services.service.ICouponSendService;
import com.qmfresh.coupon.services.service.ICouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author lxc
 */
@RestController
@RequestMapping(value = "/coupon")
public class CouponSendController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponSendController.class);

    @Autowired
    private ICouponSendService couponSendService;
    @Autowired
    private ICouponService couponService;

    /**
     * 创建优惠券发放
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/couponSend", method = RequestMethod.POST)
    public ServiceResult couponSend(@RequestBody CreateCouponSendBean bean) {
        LOGGER.info("into method couponSend,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponSendService.couponSend(bean));
    }

    /**
     * 优惠券审批
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/couponSendApproval", method = RequestMethod.POST)
    public ServiceResult couponSendApproval(@RequestBody ApprovalCouponSendBean bean) {
        LOGGER.info("into method couponSend,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponSendService.couponSendApproval(bean));
    }

    /**
     * 查询优惠券发放列表
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryCouponSendList", method = RequestMethod.POST)
    public ServiceResult<ListWithPage<QueryCouponSendReturnBean>> queryCouponSendList(@RequestBody QueryCouponSendListBean bean) {
        LOGGER.info("into method queryCouponSendList,入参:" + JSON.toJSONString(bean));
        return ServiceResultUtil.resultSuccess(couponSendService.queryCouponSendList(bean));
    }

    /**
     * 优惠券发放修改
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/modifyCouponSendInfo", method = RequestMethod.POST)
    public ServiceResult<CouponModifyReturnBean> modifyCouponSendInfo(@RequestBody CreateCouponSendBean bean) {
        return ServiceResultUtil.resultSuccess(couponSendService.modifyCouponSendInfo(bean));
    }

    /**
     * 优惠券发放删除
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/delCouponSendActivity", method = RequestMethod.POST)
    public ServiceResult delCouponSendActivity(@RequestBody IdParamBean bean) {
        return ServiceResultUtil.resultSuccess(couponSendService.delCouponSendActivity(bean));
    }

    /**
     * 优惠券发放活动暂停
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/pauseSendActivity", method = RequestMethod.POST)
    public ServiceResult pauseSendActivity(@RequestBody IdParamBean bean) {
        return ServiceResultUtil.resultSuccess(couponSendService.pauseSendActivity(bean));
    }

    /**
     * 优惠券发放活动恢复暂停
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/recoverPauseSendActivity", method = RequestMethod.POST)
    public ServiceResult recoverPauseSendActivity(@RequestBody IdParamBean bean) {
        return ServiceResultUtil.resultSuccess(couponSendService.recoverPauseSendActivity(bean));
    }

    /**
     * 查询优惠券发放明细
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/querySendDetailInfo", method = RequestMethod.POST)
    public ServiceResult<CouponSendDetailReturnBean> querySendDetailInfo(@RequestBody IdParamBean bean) {
        return ServiceResultUtil.resultSuccess(couponSendService.querySendDetailInfo(bean));
    }

    /**
     * 查询优惠券发放详情
     *
     * @param bean
     * @return
     */
    @RequestMapping(value = "/queryCouponSendDetail", method = RequestMethod.POST)
    public ServiceResult<CouponSendDetailDto> queryCouponSendDetail(@RequestBody IdParamBean bean) {
        return ServiceResultUtil.resultSuccess(couponService.queryCouponSendById(bean));
    }

    /**
     * 根据条件查询优惠券
     *
     * @param couponInfoDto
     * @return
     */
    @RequestMapping(value = "/queryCouponByCondition", method = RequestMethod.POST)
    public ServiceResult<List<CouponInfoDto>> queryCouponByCondition(@RequestBody CouponInfoDto couponInfoDto) {
        LOGGER.info("into method queryCouponByCondition,入参:" + JSON.toJSONString(couponInfoDto));
        return ServiceResultUtil.resultSuccess(couponService.queryCouponInfoByCondition(couponInfoDto));
    }
}