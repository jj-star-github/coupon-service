package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;

import lombok.Data;

@Data
public class SendActivityDTO extends BaseDTO {

    private Long id;

}
