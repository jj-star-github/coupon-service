package com.qmfresh.coupon.controller.facade.dto;

import java.io.Serializable;

public class CouponTemplateTargetShopDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 门店名
     */
    private String shopName;
    /**
     * 门店类型,1.大区,2.片区,3.门店，4.仓
     */
    private Integer shopType;
}
