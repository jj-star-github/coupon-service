package com.qmfresh.coupon.controller.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.client.send.SendCouponByActivityDTO;
import com.qmfresh.coupon.client.send.SendCouponByTemplateDTO;
import com.qmfresh.coupon.interfaces.dto.coupon.CashQuerySendCouponRequestBean;
import com.qmfresh.coupon.interfaces.dto.member.MemberSendCouponParam;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendBySendActivityContext;
import com.qmfresh.coupon.services.platform.application.coupon.vo.CouponCampOnReturnBean;
import com.qmfresh.coupon.interfaces.dto.coupon.ReceiveCouponDTO;
import com.qmfresh.coupon.services.platform.application.coupon.SendCouponService;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendContext;
import com.qmfresh.coupon.services.platform.application.coupon.enums.SendBusinessTypeEnums;
import com.qmfresh.coupon.services.platform.application.coupon.vo.SendCouponReturnVo;
import com.qmfresh.coupon.services.platform.domain.model.template.CouponSendByTemplate;
import com.qmfresh.coupon.services.platform.domain.model.template.CouponTemplateInfo;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.model.template.StartEndTime;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateConditionTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateCouponTypeEnums;
import com.qmfresh.coupon.services.platform.domain.shared.BaseUtil;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;
import com.qmfresh.coupon.services.service.DLock;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class SendCouponFacade {

    @Resource
    private SendCouponService sendCouponService;
    @Resource
    private ICouponTemplateManager couponTemplateManager;
    @Resource
    private DLock dLock;

    /**
     * 优惠券预占
     * @param activityId
     * @param shopId
     * @param sendNum
     * @return
     */
    public CouponCampOnReturnBean campOn(Integer activityId, Integer shopId, Integer sendNum) {
        //查询模板信息
        CouponTemplateInfo couponTemplateInfo = couponTemplateManager.queryCouponTemplateById(activityId);
        if (null == couponTemplateInfo) {
            throw new BusinessException("券模板信息有误请确认");
        }
        CouponCampOnReturnBean returnBean = new CouponCampOnReturnBean();
        // 券类型
        Integer couponType = couponTemplateInfo.getCouponType();
        String couponName = "优惠券";
        if(couponType.equals(TemplateCouponTypeEnums.COUPON_MANJIAN.getCode())){
            couponName = "满减券";
        }else if(couponType.equals(TemplateCouponTypeEnums.COUPON_DISCOUNT.getCode())){
            couponName = "折扣券";
        }else if(couponType.equals(TemplateCouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())){
            couponName = "随机券";
        }
        if(couponTemplateInfo.getCouponCondition().equals(TemplateConditionTypeEnums.NO_USE_CONDITION.getCode())){
            couponName = "无门槛券";
        }
        // 组装返回信息
        returnBean.setCouponId(activityId);
        returnBean.setCouponName(couponName);
        //封装优惠券码集合
        List<String> couponCodeList = sendCouponService.getCouponCode(shopId, sendNum);
        returnBean.setCouponCodeList(couponCodeList);
        returnBean.setUseInstruction(couponTemplateInfo.getUseInstruction());
        StartEndTime startEndTime = CouponSendByTemplate.buildStartEndTime(couponTemplateInfo.getTimeRule(), couponTemplateInfo.getUseTimeType());
        returnBean.setValidityPeriod(BaseUtil.buildValidityPeriod(startEndTime.getTimeStart(), startEndTime.getTimeEnd()));
        return returnBean;
    }

    /**
     * 云POS机根据订单发券
     * @param requestBean
     * @return
     */
    public List<SendCouponReturnVo> sendCoupon(CashQuerySendCouponRequestBean requestBean) {
        SendContext sendContext = new SendContext(requestBean.getSourceOrderNo(),
                SendBusinessTypeEnums.PROMOTION_ACTIVITY.getType(), 0,
                requestBean.getSourceOrderNo(), requestBean.getActivityId(),
                requestBean.getSendNum(), requestBean.getSourceShopId(), null, requestBean.getCouponCode(), requestBean.getPromotionId());
        List<SendCouponReturnVo> sendCouponReturnVoList = sendCouponService.sendCoupon(sendContext);
        return sendCouponReturnVoList;
    }

    /**
     * 线上商城发券
     * @param sendCouponDto
     * @return
     */
    public List<SendCouponReturnVo> sendCouponForApp(SendCouponDto sendCouponDto) {
        List<SendCouponReturnVo> sendCouponReturnVoList = new ArrayList<>();
        for(Integer userId : sendCouponDto.getUserIdList()) {
            String businessCode = userId.toString()+sendCouponDto.getCouponId().toString();
            SendContext sendContext = new SendContext(businessCode, SendBusinessTypeEnums.UP_SHOP_NEW_USER_ACTIVITY.getType(),
                    userId, null, sendCouponDto.getCouponId(), sendCouponDto.getSendNum(), 99999, null, null, null);
            sendCouponReturnVoList.addAll(sendCouponService.sendCoupon(sendContext));
        }
        return sendCouponReturnVoList;
    }

    public Boolean sendCouponToMember(MemberSendCouponParam param) {
        log.info("会员中台发放券参数 sendCouponToMember:" + JSON.toJSON(param));
        Assert.notNull(param.getUserId(), "发券人不可为空");
        Assert.notNull(param.getCouponId(), "发放券id不可为空");
        Assert.notNull(param.getUserIdList(), "未选中会员用户");
        Assert.notNull(param.getSendNum(), "发放数量不可为空");
        List<Integer> userIdList = param.getUserIdList();
        if (userIdList.size() > 500) {
            throw new BusinessException("会员定向发送优惠券人数不能超过500");
        }
        SendCouponDto sendCouponDto = new SendCouponDto();
        sendCouponDto.setUserIdList(userIdList);
        sendCouponDto.setSendNum(param.getSendNum());
        sendCouponDto.setCouponId(param.getCouponId());
        sendCouponService.hmSendByType(sendCouponDto);
        return true;
    }

    /**
     * 流量池领券活动
     * @param receiveCouponDTO
     * @return
     */
    public SendCouponReturnVo receiveCoupon(ReceiveCouponDTO receiveCouponDTO){
        String day = DateUtil.dateToStr(new Date(), "yyyyMMdd");
        String key = DLock.receiveCoupon(day, receiveCouponDTO.getUserId(), receiveCouponDTO.getActivityId());
        boolean result = dLock.tryLock(key, DLock.DAY_1);
        if(!result){
            throw new BusinessException("优惠券已领取");
        }
        try {
            String businessCode = day + receiveCouponDTO.getUserId().toString() + receiveCouponDTO.getActivityId().toString();
            SendContext sendContext = new SendContext(businessCode,
                    SendBusinessTypeEnums.FLOW_ACTIVITY.getType(),
                    receiveCouponDTO.getUserId(),
                    StringUtil.EMPTY_STRING,
                    receiveCouponDTO.getCouponId(),
                    1,
                    99999,
                    null,
                    null,
                    null);
            List<SendCouponReturnVo> sendCouponReturnVoList = sendCouponService.sendCoupon(sendContext);
            return sendCouponReturnVoList.get(0);
        } catch (BusinessException be) {
            log.warn("receiveCoupon流量池活动：业务异常, errorMsg={}", be.getErrorMsg());
            throw be;
        } catch (Exception e) {
            log.error("receiveCoupon流量池活动 领取优惠券异常 param={}", JSON.toJSONString(receiveCouponDTO));
            dLock.unlock(key);
            throw new BusinessException("领取失败");
        }
    }

    //=======================================  RPC  =========================================

    /**
     * 根据优惠券模板ID发券
     * @param sendCouponDTO
     * @return
     */
    public List<SendCouponReturnVo> sendCoupon(SendCouponByTemplateDTO sendCouponDTO) {
        SendContext sendContext = new SendContext(sendCouponDTO.getBusinessCode(),
                sendCouponDTO.getBusinessType(), sendCouponDTO.getUserId(),
                sendCouponDTO.getOrderNo(), sendCouponDTO.getTemplateId(),
                sendCouponDTO.getSendNum(), sendCouponDTO.getShopId(), null, null, null);
        List<SendCouponReturnVo> sendCouponReturnVoList = sendCouponService.sendCoupon(sendContext);
        return sendCouponReturnVoList;
    }

    /**
     * 根据发放活动类型来发券
     * @param sendCouponByActivityDTO
     * @return
     */
    public List<SendCouponReturnVo> sendCoupon(SendCouponByActivityDTO sendCouponByActivityDTO) {
        SendBySendActivityContext sendActivityContext  = new SendBySendActivityContext(sendCouponByActivityDTO.getBusinessCode(), sendCouponByActivityDTO.getBusinessType(),
                sendCouponByActivityDTO.getActivityType(), sendCouponByActivityDTO.getUserId(),
                sendCouponByActivityDTO.getShopId(), sendCouponByActivityDTO.getSendNum(), null);
        List<SendCouponReturnVo> sendCouponReturnVoList = sendCouponService.sendCouponBySendActivity(sendActivityContext);
        return sendCouponReturnVoList;
    }

}
