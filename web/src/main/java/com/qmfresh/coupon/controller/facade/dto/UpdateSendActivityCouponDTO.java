package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;
import lombok.Data;

@Data
public class UpdateSendActivityCouponDTO extends BaseDTO {

    private Long id;

    private Long activityId;

    /**
     * 每人发放储量
     */
    private Integer sendNum;


}
