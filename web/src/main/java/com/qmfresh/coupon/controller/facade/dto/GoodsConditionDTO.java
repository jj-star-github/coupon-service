package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;
import lombok.Data;

import java.io.Serializable;

@Data
public class GoodsConditionDTO extends BaseDTO implements Serializable {
    private Integer id;
    private Integer templateId;
    private String goodsId;
    private String goodsName;
    private int pageNum = 1;
    private int pageSize = 20;
}
