package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


@Data
public class QuerySendActivityDTO extends BaseDTO {

    private String activityName;

    /**
     * 发放类型 1：定向 2：系统 ActivitySendTypeEnums
     */
    private Integer sendType;

    /**
     * 活动类型  ActivityTypeEnums
     */
    private Integer activityType;

    /**
     * 活动状态 ActivityStatusEnums
     */
    private Integer activityStatus;

    /**
     *活动开始时间
     */
    private Date beginTime;

    /**
     *活动结束时间
     */
    private Date endTime;

    /**
     *创建人
     */
    private String createName;

    private int pageNum = 1;
    private int pageSize = 20;

}
