package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;
import lombok.Data;

import java.io.Serializable;

@Data
public class QueryCouponTemplateDTO extends BaseDTO implements Serializable {
    /**
     * 模板名称
     */
    private String couponName;
    /**
     * 券模板类型
     */
    private Integer couponType;
    /**
     * 券模板渠道
     */
    private Integer limitChannel;
    /**
     * 券模板状态
     */
    private Integer status;
    /**
     * 券模板商品类型1.全品，2.sku,3.class1,4.class2,5.ssu,6.saleClass1,7.saleClass2,8.会场
     */
    private Integer goodsType;
    /**
     * 券模板门店类型1全部，2指定可用，3指定不可用
     */
    private Integer shopType;
    /**
     * 券模板绑定关系0未绑定，1会场
     */
    private Integer bingTemplate;


    private int pageNum = 1;
    private int pageSize = 20;
}
