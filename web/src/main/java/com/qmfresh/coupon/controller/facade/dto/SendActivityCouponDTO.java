package com.qmfresh.coupon.controller.facade.dto;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityCouponRule;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityCouponVO;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivityCoupon;
import lombok.Data;

@Data
public class SendActivityCouponDTO {

    private Long id;

    /**
     * 优惠券模板ID
     */
    private Integer couponTemplateId;

    /**
     * 优惠券名
     */
    private String couponName;

    /**
     * 优惠券类型
     */
    private Integer couponType;

    /**
     * 每人发放储量
     */
    private Integer sendNum;

    /**
     * 优惠券描述
     */
    private String remark;

    /**
     * 预计发放总量
     */
    private SendActivityCouponRule willSendNum;

    public SendActivityCouponVO createVO() {
        if (null == this.getCouponTemplateId()) {
            throw new BusinessException("请选择有效的券模板");
        }
        if (null == this.getSendNum()) {
            throw new BusinessException("请输入发送数量");
        }
        SendActivityCouponVO couponVO = new SendActivityCouponVO();
        couponVO.setTemplateId(this.getCouponTemplateId());
        couponVO.setSendNum(this.getSendNum());
        couponVO.setCouponType(this.getCouponType());
        couponVO.setCouponName(this.getCouponName());
        couponVO.setRemark(this.getRemark());
        return couponVO;
    }

    public SendActivityCoupon createSendToUse(){
        if (null == this.getCouponTemplateId()) {
            throw new BusinessException("请选择有效的券模板");
        }
        if (null == this.getSendNum()) {
            throw new BusinessException("请输入发送数量");
        }
        SendActivityCoupon sendActivityCoupon = new SendActivityCoupon();
        sendActivityCoupon.setTemplateId(this.getCouponTemplateId());
        sendActivityCoupon.setSendNum(this.getSendNum());
        sendActivityCoupon.setCouponName(this.getCouponName());
        sendActivityCoupon.setCouponType(this.getCouponType());
        sendActivityCoupon.setRemark(this.getRemark());
        sendActivityCoupon.setSendRule(JSON.toJSONString(this.getWillSendNum()));
        return sendActivityCoupon;
    }

}
