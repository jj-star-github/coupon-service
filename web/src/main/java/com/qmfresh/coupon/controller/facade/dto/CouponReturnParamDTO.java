package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.services.entity.bo.CouponReturnParamBo;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class CouponReturnParamDTO implements Serializable {

    private String sourceOrderCode;

    public static CouponReturnParamBo create(CouponReturnParamDTO returnParamDTO) {
        CouponReturnParamBo couponReturnParamBo = new CouponReturnParamBo();
        couponReturnParamBo.setSourceOrderCode(returnParamDTO.getSourceOrderCode());
        return couponReturnParamBo;
    }
}
