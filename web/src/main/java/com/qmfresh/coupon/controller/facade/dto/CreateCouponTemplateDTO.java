package com.qmfresh.coupon.controller.facade.dto;

import com.qmfresh.coupon.controller.common.BaseDTO;
import com.qmfresh.coupon.services.platform.domain.model.template.TemplateConditionRule;
import com.qmfresh.coupon.services.platform.domain.model.template.TemplateTimeRule;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetGoodsCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetShopsCommand;
import lombok.Data;

import java.util.List;


@Data
public class CreateCouponTemplateDTO extends BaseDTO {

    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 使用门槛
     */
    private Integer useCondition;
    /**
     * 订单满足条件的金额
     */
    private Integer needMoney;
    /**
     * 券类型
     */
    private Integer couponType;
    /**
     * 活动条件规则
     */
    private TemplateConditionRule conditionRule;
    /**
     * 用券时间类型
     */
    private Integer useTimeType;
    /**
     * 活动时间规则
     */
    private TemplateTimeRule timeRule;
    /**
     * 适用商品类型
     */
    private Integer applicableGoodsType;
    /**
     * 使用说明
     */
    private String useInstruction;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;
    /**
     * 适用门店类型
     */
    private Integer applicableShopType;
    /**
     * 目标品
     */
    private List<TemplateTargetGoodsCommand> goodsList;
    /**
     * 目标店
     */
    private List<TemplateTargetShopsCommand> shopList;
}
