package com.qmfresh.coupon.handler;

import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.interfaces.enums.ResponseCode;
import com.qmfresh.coupon.services.common.exception.AbstractRuntimeException;
import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.services.common.exception.ExceptionTypeEnum;
import com.qmfresh.coupon.services.common.exception.InvalidParamException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class ExceptionHandlerBean extends ResponseEntityExceptionHandler {

    /**
     * 异常处理
     *
     * @param ex
     * @param request
     * @return
     * @throws IOException
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<ServiceResult> handleDataNotFoundException(RuntimeException ex, WebRequest request) {
        return getResponseEntity(ex, request);
    }

    /**
     * 根据各种异常构建 ResponseEntity 实体. 服务于以上各种异常
     *
     * @param exception
     * @param request
     * @return
     */
    private ResponseEntity<ServiceResult> getResponseEntity(RuntimeException exception, WebRequest request) {

        HttpStatus status = HttpStatus.OK;
        if (isProtobuf(request)) {
            throw new RuntimeException("bad proto");
        } else {
            ServiceResult response = new ServiceResult();
            response.setSuccess(false);
            if (exception instanceof InvalidParamException) {
                //参数异常
                InvalidParamException e = (InvalidParamException) exception;
                response.setErrorCode(100);
                response.setMessage(e.getMessage());
                response.setBody(e.getData());
            } else if (exception instanceof BusinessException) {
                //捕捉业务层异常
                BusinessException e = (BusinessException) exception;
                response.setErrorCode(e.getErrorCode());
                response.setMessage(e.getErrorMsg());
            } else if (exception instanceof AbstractRuntimeException) {
                response.setErrorCode(ResponseCode.SYSTEM_ERROR.getCode());
                response.setMessage(exception.getMessage());
            } else {
                status = getStatus(request);
                response.setErrorCode(ExceptionTypeEnum.UNKNOWN_EXCEPTION.getCode());
                response.setMessage(ExceptionTypeEnum.UNKNOWN_EXCEPTION.getDetail());
            }
            logger.error(response.getMessage(), exception);
            return new ResponseEntity<>(response, status);
        }
    }

    private HttpStatus getStatus(WebRequest request) {
        Integer statusCode = (Integer) request
                .getAttribute("javax.servlet.error.status_code", 1);
        if (statusCode != null) {
            try {
                return HttpStatus.valueOf(statusCode);
            } catch (Exception ex) {
            }
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    private boolean isProtobuf(WebRequest request) {
        String acceptMime = request.getHeader("Accept");
        if (!StringUtils.isEmpty(acceptMime)) {
            String type = acceptMime.substring(0, 1);
            if ("*".equals(type)) {
                // default is json
                return false;
            } else if (acceptMime.indexOf("application/x-protobuf") > -1) {
                return true;
            }
        }
        return false;
    }

} 