package com.qmfresh.coupon.client;

import com.qmgyl.rpc.result.DubboResult;

import java.util.List;

/**
 * 优化券查询接口
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface CouponQueryRemote {

    /**
     * 查询优惠券列表
     *
     * @param couponQueryDTO 优惠券查询
     * @return 优惠券列表
     */
    DubboResult<List<CouponDTO>> list(CouponQueryDTO couponQueryDTO);
}
