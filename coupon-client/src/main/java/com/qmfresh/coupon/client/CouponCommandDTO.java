package com.qmfresh.coupon.client;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
public class CouponCommandDTO implements Serializable {
    /**
     * 页码
     */
    private Integer pageNum;
    /**
     * 页大小
     */
    private Integer pageSize;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 门店id
     */
    private Integer shopId;

}
