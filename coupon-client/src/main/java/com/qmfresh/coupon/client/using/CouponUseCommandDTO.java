package com.qmfresh.coupon.client.using;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
public class CouponUseCommandDTO implements Serializable {

    private static final long serialVersionUID = 8757338761918281518L;
    /**
     * 优惠券id
     */
    private Integer couponId;
    /**
     * 优惠券编码
     */
    private String couponCode;
    /**
     * 订单金额（实付？应付）
     */
    private BigDecimal totalAmount;
    /**
     * 用户id,可以为空，默认为零
     */
    private Long userId;
    /**
     * 用这张券购买的商品数量
     */
    private Integer goodsNum;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 来源系统：拼团，线上，线下......(保留字段)
     */
    private Integer sourceSystem;
    /**
     * 使用优惠券的门店id
     */
    private Integer shopId;
    /**
     * 优惠券抵扣金额
     */
    private BigDecimal couponPrice;
    /**
     * 使用时间戳
     */
    private Long useTimestamp;
    /**
     * 渠道 1：收银 5：线上商城  10：外卖 15：门店订货
     */
    private Integer bizChannel;
}
