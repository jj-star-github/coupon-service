package com.qmfresh.coupon.client.template.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class ReturnAppDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 可用列表
     */
    private List<ReturnAppCanUseBean> canUseList;
    /**
     * 不可用列表
     */
    private List<ReturnAppCanNotUseBean> unUseList;
}
