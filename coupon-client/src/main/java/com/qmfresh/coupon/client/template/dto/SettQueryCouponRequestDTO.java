package com.qmfresh.coupon.client.template.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SettQueryCouponRequestDTO implements Serializable {
    /**
     * 会员id
     */
    private Long userId;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 商品信息
     */
    private List<GoodsInfo> goodsList;
}
