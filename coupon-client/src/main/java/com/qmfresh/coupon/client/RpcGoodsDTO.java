package com.qmfresh.coupon.client;

import java.io.Serializable;

import lombok.Data;

@Data
public class RpcGoodsDTO implements Serializable {
    /**
     * 商品类型
     */
    private Integer goodType;

    /**
     * SKUID
     */
    private Integer skuId;

    /**
     * SSUID
     */
    private Integer ssuId;

    /**
     * class1
     */
    private Integer class1Id;

    /**
     * class2
     */
    private Integer class2Id;

    /**
     * 线上商城一级类目
     */
    private Integer saleClass1Id;

    /**
     * 线上商城一级类目
     */
    private Integer saleClass2Id;

    /**
     * 会场ID
     */
    private String meetingId;
}
