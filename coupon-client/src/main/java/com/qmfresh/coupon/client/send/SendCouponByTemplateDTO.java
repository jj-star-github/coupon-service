package com.qmfresh.coupon.client.send;

import lombok.Data;
import lombok.ToString;

/**
 */
@Data
@ToString
public class SendCouponByTemplateDTO extends SendCouponBaseDTO {
    private static final long serialVersionUID = -2634608309253218048L;

    /**
     * 模板ID
     */
    private Integer templateId;
}
