package com.qmfresh.coupon.client.using;

import lombok.Data;

import java.io.Serializable;

@Data
public class CouponQueryResultDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键自增长
     */
    private Integer id;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    private Integer couponCondition;
    /**
     * 订单所需金额
     */
    private Integer needMoney;
    /**
     * 券类型
     */
    private Integer couponType;
    /**
     * 券规则
     */
    private String couponRule;
    /**
     * 使用时间类型
     */
    private Integer useTimeType;
    /**
     * 使用时间规则
     */
    private String useTimeRule;
    /**
     * 适用商品类型
     */
    private Integer applicableGoodsType;
    /**
     * 券适用商品规则
     */
    private String applicableGoodsRule;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 创建人id
     */
    private Integer createdId;
    /**
     * 创建人名称
     */
    private String createdName;
    /**
     * 创建时间
     */
    private Integer cT;
    /**
     * 更新时间
     */
    private Integer uT;
    /**
     * 有效性
     */
    private Integer isDeleted;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;
}
