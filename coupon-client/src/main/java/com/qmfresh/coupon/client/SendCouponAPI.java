package com.qmfresh.coupon.client;


import com.qmfresh.coupon.client.send.SendCouponByActivityDTO;
import com.qmfresh.coupon.client.send.SendCouponByTemplateDTO;
import com.qmfresh.coupon.client.send.SendCouponReturnDTO;
import com.qmgyl.rpc.result.DubboResult;

import java.util.List;

public interface SendCouponAPI {
    /**
     * 根据模板ID发券
     */
    DubboResult<List<SendCouponReturnDTO>> sendCouponByTemplate(SendCouponByTemplateDTO activityIdDTO);

    /**
     * 根据发券活动发券
     * @param activityIdDTO
     * @return
     */
    DubboResult<List<SendCouponReturnDTO>> sendCouponByActivity(SendCouponByActivityDTO activityIdDTO);
}
