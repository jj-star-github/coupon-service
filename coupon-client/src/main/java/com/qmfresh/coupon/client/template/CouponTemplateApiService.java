package com.qmfresh.coupon.client.template;

import com.qmfresh.coupon.client.template.dto.*;
import com.qmgyl.rpc.result.DubboResult;

import java.util.List;

public interface CouponTemplateApiService {
    /**
     * 券模板绑定会场
     */
    DubboResult<TemplateBingActResultDTO> bingTemplate(TemplateBingMeetingDTO dto);

    /**
     * 根据会场和useId查询对应的券模板信息
     */
    DubboResult<List<TemplateQueryByActResultDTO>> queryByAct(TemplateQueryByActDTO dto);

    /**
     * new settlement
     */
    DubboResult<ReturnAppDTO> newSettlementQuery(SettQueryCouponRequestDTO dto);
}
