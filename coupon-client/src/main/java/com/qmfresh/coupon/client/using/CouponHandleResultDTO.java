package com.qmfresh.coupon.client.using;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
public class CouponHandleResultDTO implements Serializable {
    /**
     * 1.优惠券已失效 2.优惠券已被使用 3. 补打失败，优惠券失败
     */
    private Integer type;
    /**
     * 退款原因
     */
    private String reason;
    /**
     * 优惠码
     */
    private String couponCode;
    /**
     * 使用订单编码
     */
    private String useOrderCode;
    /**
     * 退款订单编码
     */
    private String returnOrderCode;
    /**
     * 会员编码
     */
    private String vipCode;
    /**
     * 会员手机号码
     */
    private String phone;
}
