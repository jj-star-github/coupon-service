package com.qmfresh.coupon.client.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
@AllArgsConstructor
public class CodeMsg implements Serializable {

    private static final long serialVersionUID = 8617709713392755885L;

    private Long code;

    private String message;

}
