package com.qmfresh.coupon.client;

import com.qmfresh.coupon.client.common.DubboResult;
import com.qmfresh.coupon.client.coupon.QueryCouponByGoodsDTO;
import com.qmfresh.coupon.client.coupon.QueryCouponEventDTO;
import com.qmfresh.coupon.client.coupon.QueryCouponReturnDTO;
import com.qmfresh.coupon.client.send.SendCouponByActivityDTO;
import com.qmfresh.coupon.client.send.SendCouponReturnDTO;
import com.qmfresh.coupon.client.using.CouponActivityIdDTO;
import com.qmfresh.coupon.client.using.CouponQueryResultDTO;

import java.util.List;

public interface CouponQueryInfoAPI {
    /**
     * 优惠券使用
     *
     * @param activityIdDTO 优惠券活动id
     * @return 优惠券活动信息
     */
    DubboResult<CouponQueryResultDTO> queryByActivityId(CouponActivityIdDTO activityIdDTO);


    /**
     * 查询商品可用优惠券信息
     * @param queryDTO
     * @return
     */
    com.qmgyl.rpc.result.DubboResult<List<QueryCouponReturnDTO>> queryBySku(QueryCouponByGoodsDTO queryDTO);

    /**
     * 查询业务优惠券操作记录
     * @param queryDTO
     * @return
     */
    com.qmgyl.rpc.result.DubboResult<QueryCouponReturnDTO> queryCouponEvent(QueryCouponEventDTO queryDTO);

    /**
     * 根据订单号查询使用的优惠券信息
     * @param queryDTO
     * @return
     */
    com.qmgyl.rpc.result.DubboResult<QueryCouponReturnDTO> queryUseCoupon(QueryCouponEventDTO queryDTO);

}
