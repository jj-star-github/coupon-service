package com.qmfresh.coupon.client.common;

import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class PageDTO<T> {

    private Integer pageNum;
    private Integer pageSize;
    private T records;


}
