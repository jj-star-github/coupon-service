package com.qmfresh.coupon.client.send;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
 * 发券响应对象
 */
@Data
public class SendCouponReturnDTO implements Serializable {

    private static final long serialVersionUID = -2634608309253218048L;

    /**
     * 优惠券ID
     */
    private Integer couponId;
    /**
     * 优惠券名
     */
    private String couponName;
    /**
     * 优惠券码
     */
    private String couponCode;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 有效期
     */
    private String validityPeriod;
    /**
     * 优惠券绑定的订单号
     */
    private String sourceOrderNo;

    /**
     * UserId
     */
    private Long userId;

    /**
     * 剩余可领张数
     */
    private Integer residueNum;
}
