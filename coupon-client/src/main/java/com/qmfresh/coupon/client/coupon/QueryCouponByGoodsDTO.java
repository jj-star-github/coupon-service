package com.qmfresh.coupon.client.coupon;

import com.qmfresh.coupon.client.RpcGoodsDTO;
import com.qmfresh.coupon.client.send.SendCouponBaseDTO;

import lombok.Data;
import lombok.ToString;

/**
 */
@Data
@ToString
public class QueryCouponByGoodsDTO extends SendCouponBaseDTO {
    private static final long serialVersionUID = -2634608309253218048L;

    /**
     *
     */
    private Integer userId;

    /**
     *
     */
    private Integer shopId;

    /**
     *
     */
    private Integer channel;

    /**
     * 商品信息
     */
    private RpcGoodsDTO rpcGoodsDTO;
}
