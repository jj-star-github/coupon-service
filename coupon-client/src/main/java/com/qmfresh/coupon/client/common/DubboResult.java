package com.qmfresh.coupon.client.common;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Deprecated
@Data
@ToString
public class DubboResult<T> implements Serializable {

    private CodeMsg codeMsg;

    /**
     * 要传输的数据
     */
    private T data;

    /**
     * 创建成功
     */
    public void success(T data) {
        this.data = data;
        this.codeMsg = new CodeMsg(0L, (String)null);
    }

    /**
     * 创建失败
     */
    public void failure(CodeMsg codeMsg) {
        this.codeMsg = codeMsg;
    }

    public void failure(CodeMsg codeMsg, Object... msgParams) {
        this.codeMsg = new CodeMsg(codeMsg.getCode(), String.format(codeMsg.getMessage(), msgParams));
    }

}
