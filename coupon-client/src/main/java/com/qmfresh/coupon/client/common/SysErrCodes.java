package com.qmfresh.coupon.client.common;

import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class SysErrCodes {
    public static final String SYS_ERR_CODE_PREFIX = "200";

    public static final CodeMsg SYS_ERR = new CodeMsg(20001001L, "系统错误");
    public static final CodeMsg SYS_ILLEGAL_PARAM = new CodeMsg(20001002L, "参数错误:%s");
    public static final CodeMsg SYS_UNCHECKED_EXCEPTION = new CodeMsg(20001003L, "unchecked exception:%s");
    /**
     * 在调用之前, 降级非核心服务 MockClusterInvoker
     */
    public static final CodeMsg SYS_INTERFACE_DEMOTION_EXCEPTION = new CodeMsg(20001004L, "服务降级");
    public static final CodeMsg SYS_LIMITING_EXCEPTION = new CodeMsg(20001005L, "触发限流");

}
