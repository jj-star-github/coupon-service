package com.qmfresh.coupon.client.coupon;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 */
@Data
@ToString
public class QueryCouponEventDTO implements Serializable {

    private static final long serialVersionUID = -2634608309253218048L;

    /**
     * 业务单号 退单号
     */
    private String bizCode;

    /**
     * 42, "收银退单失效"
     */
    private Integer bizType;

    /**
     * 原业务单号  原订单号
     */
    private String sourceBizCode;

}