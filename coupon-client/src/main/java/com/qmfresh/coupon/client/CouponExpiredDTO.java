package com.qmfresh.coupon.client;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Slf4j
public class CouponExpiredDTO implements Serializable {
    /**
     * 优惠券列表
     */
    private List<String> couponCodes;
    /**
     * 操作者id
     */
    private Integer operatorId;
    /**
     * 操作者名称
     */
    private String operatorName;
    /**
     * 退货订单号
     */
    private String refundCode;
    /**
     * 源订单号
     */
    private String sourceOrderCode;
    /**
     * 当前门店id
     */
    private Integer currentShopId;
}
