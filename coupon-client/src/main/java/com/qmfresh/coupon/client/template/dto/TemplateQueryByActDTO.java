package com.qmfresh.coupon.client.template.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateQueryByActDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会场活动id
     */
    private String actId;
    /**
     * userId
     */
    private Integer userId;

}
