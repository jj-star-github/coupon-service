package com.qmfresh.coupon.client;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
public class CouponQueryDTO implements Serializable {
    /**
     * 使用订单号
     */
    private String useOrderCode;
    /**
     * 赠送订单号
     */
    private String givenOrderCode;
}
