package com.qmfresh.coupon.client.template.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TemplateBingMeetingDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会场活动id
     */
    private String actId;
    /**
     * 会场活动名称
     */
    private String actName;
    /**
     * 优惠券ids
     */
    private List<Integer> templateIds;

}
