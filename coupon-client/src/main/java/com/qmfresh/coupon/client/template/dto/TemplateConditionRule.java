package com.qmfresh.coupon.client.template.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class TemplateConditionRule implements Serializable {
    /**
     * 折扣(折扣优惠券适用)
     */
    private Integer discount;
    /**
     * 最多优惠金额(折扣优惠券适用)
     */
    private BigDecimal maxDiscountMoney;
    /**
     * 减免金额(满减适用)
     */
    private Integer subMoney;
    /**
     * 随机减免金额起始金额
     */
    private Integer randomMoneyStart;
    /**
     * 随机减免金额截止金额
     */
    private Integer randomMoneyEnd;
    /**
     * 用户每次领取最大张数
     */
    private Integer useReceiveNum;
    /**
     * 券模板最大发放张数
     */
    private Integer couponMaxSendNum;
    /**
     * 门店说明
     */
    private String shopInstruction;
}
