package com.qmfresh.coupon.client.template.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateBingActResultDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 是否绑定成功
     */
    private Boolean successFlag;
    /**
     * 失败原因
     */
    private String reason;
}
