package com.qmfresh.coupon.client.template.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateQueryByActResultDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 优惠券id
     */
    private Integer templateId;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    private Integer couponCondition;
    /**
     * 优惠券类型
     */
    private Integer couponType;
    /**
     * 订单所需金额
     */
    private Integer needMoney;
    /**
     * 券规则
     */
    private TemplateConditionRule conditionRule;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 每人限领数量
     */
    private Integer useMaxReceiveNum;
    /**
     * 用户还能领取数量
     */
    private Integer useCanReceiveNum;
    /**
     * 券模板剩余数量
     */
    private Integer couponNumber;
    /**
     * 券模板名称
     */
    private String couponName;
    /**
     * 券模板时间
     */
    private String useTime;

}
