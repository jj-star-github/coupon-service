package com.qmfresh.coupon.client;

import com.qmfresh.coupon.client.common.DubboResult;
import com.qmfresh.coupon.client.common.PageDTO;
import com.qmfresh.coupon.client.given.CouponGivenResultDTO;
import com.qmfresh.coupon.client.given.CouponGivenCommandDTO;
import com.qmfresh.coupon.client.using.CouponHandleResultDTO;
import com.qmfresh.coupon.client.using.CouponReturnResultDTO;
import com.qmfresh.coupon.client.using.CouponReturnStatusCommandDTO;
import com.qmfresh.coupon.client.using.CouponUseCommandDTO;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface CouponRemote {

    /**
     * 优惠券使用
     *
     * @param couponUseDTO 优惠券查询对象
     * @return 优惠券使用处理结果
     */
    DubboResult<CouponHandleResultDTO> useCoupon(CouponUseCommandDTO couponUseDTO);

    /**
     * 优惠券赠送
     *
     * @param givenCommandDTO 赠送命令
     * @return 优惠券赠送结果
     */
    DubboResult<CouponGivenResultDTO> givenCoupon(CouponGivenCommandDTO givenCommandDTO);

    /**
     * 优惠券逆向检查
     *
     * @param commandDTO 优惠券退还状态检查命令
     * @return 退券状态检查
     */
    DubboResult<CouponReturnResultDTO> returnStatusCheck(CouponReturnStatusCommandDTO commandDTO);

    /**
     * 用户优惠券列表查询
     *
     * @return 分页优惠券结果
     */
    DubboResult<PageDTO<CouponDTO>> memberAvailableCouponPage(CouponCommandDTO couponCommandDTO);


}
