package com.qmfresh.coupon.client.coupon;

import com.qmfresh.coupon.client.send.SendCouponBaseDTO;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 */
@Data
@ToString
public class QueryCouponReturnDTO extends SendCouponBaseDTO {
    private static final long serialVersionUID = -2634608309253218048L;

    /**
     * 优惠券ID
     */
    private Integer id;

    /**
     * 券码
     */
    private String couponCode;

    /**
     * 优惠券名
     */
    private String couponName;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 有效期
     */
    private String validityPeriod;

    /**
     * 券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponType;

    /**
     * 折扣
     */
    private Integer discount;
    /**
     * 具体优惠金额，如果preferentialType是2，代表最多优惠的金额;
     */
    private BigDecimal preferentialMoney;

    /**
     * 需要满足金额
     */
    private BigDecimal needMoney;

    /**
     * 优惠金额  折扣券返回：Null
     */
    private BigDecimal subMoney;

    /**
     * 优惠券标签
     */
    private String tag;

    /**
     * 优惠券门槛
     */
    private Integer couponCondition;

}
