/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/8
 * Description:ReturnAppCanNotUseBean.java
 */
package com.qmfresh.coupon.client.template.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class ReturnAppCanNotUseBean implements Serializable {

    private static final long serialVersionUID = 1704768032606141171L;
    /**
     * 券id
     */
    private Integer id;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 使用说明
     */
    private String useInstruction;
    /**
     * 有效期
     */
    private String validityPeriod;
    /**
     * 不可用原因
     */
    private String unUseReason;
    /**
     * 优惠金额
     */
    private BigDecimal preferentialMoney;
    /**
     * 优惠类型1:固定金额2:折扣
     */
    private Integer preferentialType;
    /**
     * 折扣
     */
    private Integer discount;
    /**
     * 券活动名称
     */
    private String couponActivityName;
    /**
     * 券适用商品类型(1:全部可用2:指定可用3:指定不可用)
     */
    private Integer applicableGoodsType;

    private String couponCode;

    private Integer couponId;

    private Integer status;

    private Integer sourceShopId;

    private Integer useShopId;

    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;

    /**
     * 可用小时段
     */
    private String useHMSRule;

    public String getUseHMSRule() {
        return useHMSRule;
    }

    public void setUseHMSRule(String useHMSRule) {
        this.useHMSRule = useHMSRule;
    }

    public Integer getLimitChannel() {
        return limitChannel;
    }

    public void setLimitChannel(Integer limitChannel) {
        this.limitChannel = limitChannel;
    }

    public BigDecimal getPreferentialMoney() {
        return preferentialMoney;
    }

    public void setPreferentialMoney(BigDecimal preferentialMoney) {
        this.preferentialMoney = preferentialMoney;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getCouponActivityName() {
        return couponActivityName;
    }

    public void setCouponActivityName(String couponActivityName) {
        this.couponActivityName = couponActivityName;
    }

    public Integer getPreferentialType() {
        return preferentialType;
    }

    public void setPreferentialType(Integer preferentialType) {
        this.preferentialType = preferentialType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getUseInstruction() {
        return useInstruction;
    }

    public void setUseInstruction(String useInstruction) {
        this.useInstruction = useInstruction;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public String getUnUseReason() {
        return unUseReason;
    }

    public void setUnUseReason(String unUseReason) {
        this.unUseReason = unUseReason;
    }

    public Integer getApplicableGoodsType() {
        return applicableGoodsType;
    }

    public void setApplicableGoodsType(Integer applicableGoodsType) {
        this.applicableGoodsType = applicableGoodsType;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSourceShopId() {
        return sourceShopId;
    }

    public void setSourceShopId(Integer sourceShopId) {
        this.sourceShopId = sourceShopId;
    }

    public Integer getUseShopId() {
        return useShopId;
    }

    public void setUseShopId(Integer useShopId) {
        this.useShopId = useShopId;
    }
}
