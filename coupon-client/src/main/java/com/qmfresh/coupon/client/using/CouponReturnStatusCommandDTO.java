package com.qmfresh.coupon.client.using;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
public class CouponReturnStatusCommandDTO implements Serializable {

    /**
     * 原订单
     */
    private String orderCode;
    /**
     * 退款订单
     */
    private String returnOrderCode;
    /**
     * 优惠券码
     */
    private String couponCode;
    /**
     * 1=使用券 2=赠送券
     */
    private Integer type;
}
