package com.qmfresh.coupon.client.using;

import lombok.Data;

import java.io.Serializable;

@Data
public class CouponActivityIdDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    //优惠券活动id
    private Integer activityId;
}
