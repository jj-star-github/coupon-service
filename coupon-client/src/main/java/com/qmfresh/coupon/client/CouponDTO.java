package com.qmfresh.coupon.client;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
public class CouponDTO implements Serializable {
    /**
     * 优惠券id
     */
    private Integer couponId;
    /**
     * 优惠券模板（活动）id
     */
    private Long couponActivityId;
    /**
     * 促销id
     */
    private Long promotionId;
    /**
     * 优惠券编码
     */
    private String couponCode;
    /**
     * 优惠券名称
     */
    private String couponName;
    /**
     * 限制渠道
     */
    private Integer limitChannel;
    /**
     * 使用说明
     */
    private String useInstruction;
    /**
     * 优惠券状态
     */
    private Integer status;
    /**
     * 赠送订单
     */
    private String sourceOrderCode;
    /**
     * 绑定用户id
     */
    private Integer sourceUserId;
    /**
     * 赠送门店
     */
    private Integer sourceShopId;
    /**
     * 使用订单
     */
    private String useOrderCode;
    /**
     * 使用门店
     */
    private Integer useShopId;
    /**
     * 使用时间
     */
    private Integer useTime;
    /**
     * 需要的金额
     */
    private Integer needMoney;
    /**
     * 优惠金额
     */
    private BigDecimal subMoney;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    private Integer couponCondition;
    /**
     * 券使用开始时间
     */
    private Integer useTimeStart;
    /**
     * 券使用结束时间
     */
    private Integer useTimeEnd;
    /**
     * 券适用商品类型(1:全部可用2:指定可用3:指定不可用)
     */
    private Integer applicableGoodsType;
    /**
     * 1级分类
     */
    private String class1Ids;
    /**
     * 2级分类
     */
    private String class2Ids;
    /**
     * 券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponType;

    /**
     * 券有效期券码打印（必须）
     */
    private String validityPeriod;

}
