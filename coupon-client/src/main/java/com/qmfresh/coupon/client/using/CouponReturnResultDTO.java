package com.qmfresh.coupon.client.using;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
public class CouponReturnResultDTO implements Serializable {

    private Integer code;
}
