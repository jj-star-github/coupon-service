package com.qmfresh.coupon.client.send;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

/**
 */
@Data
@ToString
public class SendCouponBaseDTO implements Serializable {

    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 发放张数
     */
    private Integer sendNum;
    /**
     * 业务唯一标识
     */
    private String businessCode;
    /**
     * 发券业务类型
     */
    private Integer businessType;

    /**
     * 订单号  满赠活动用
     */
    private String orderNo;

    /**
     * 门店ID
     */
    private Integer shopId;
}
