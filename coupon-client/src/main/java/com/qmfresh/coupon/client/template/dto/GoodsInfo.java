package com.qmfresh.coupon.client.template.dto;


import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @program: coupon-service
 * @description: 商品信息
 * @author: xbb
 * @create: 2019-11-25 14:49
 **/
public class GoodsInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer ssuId;

    /**
     * 商品类型，1.sku，2.ssu
     */
    private Integer goodType=1;

    /**
     * 商品id
     */
    private Integer skuId;

    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 商品数量
     */
    private BigDecimal amount;

    /**
     * 券所属一级分类
     */
    private Integer class1Id;

    /**
     * 券所属二级分类
     */
    private Integer class2Id;

    /**
     * 线上商品所属一级分类
     */
    private Integer saleClass1Id;
    /**
     * 线上商品所属二级分类
     */
    private Integer saleClass2Id;
    /**
     * 会场id
     */
    private String meetingId;

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public Integer getSsuId() {
        return ssuId;
    }

    public void setSsuId(Integer ssuId) {
        this.ssuId = ssuId;
    }

    public Integer getGoodType() {
        return goodType;
    }

    public void setGoodType(Integer goodType) {
        this.goodType = goodType;
    }

    public Integer getSaleClass1Id() {
        return saleClass1Id;
    }

    public void setSaleClass1Id(Integer saleClass1Id) {
        this.saleClass1Id = saleClass1Id;
    }

    public Integer getSaleClass2Id() {
        return saleClass2Id;
    }

    public void setSaleClass2Id(Integer saleClass2Id) {
        this.saleClass2Id = saleClass2Id;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }
}
