package com.qmfresh.coupon.client;


import com.qmgyl.rpc.result.DubboResult;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface CouponOperationRemote {

    /**
     * 失效优惠券名单
     *
     * @param couponExpiredDTO 优惠券失效
     * @return 字符串
     */
    DubboResult<String> expiredGivenCoupons(CouponExpiredDTO couponExpiredDTO);
}
