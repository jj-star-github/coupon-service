/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:IModifyStatusTasjService.java
 */
package com.qmfresh.coupon.services.service;

/**
 * @author lxc
 */
public interface IModifyStatusTaskService {

    /**
     * 修改优惠券活动用券类型为1的，过期了，则把活动有效置为1
     */
    void modifyStatus();

    /**
     * 修改优惠券状态
     */
    void modifyCouponStatus();

    /**
     * 修改优惠券发放的状态
     */
    void modifyCouponSendStatus();
}
