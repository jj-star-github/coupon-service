package com.qmfresh.coupon.services.platform.domain.model.template.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityCouponCacheKey;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateTargetShopManager;
import com.qmfresh.coupon.services.platform.domain.model.template.command.GoodsQueryCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.ShopQueryCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetShopsCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateGoodsVo;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateShopVo;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.CouponTemplateTargetShopMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetGoods;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetShop;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ICouponTemplateTargetShopManagerImpl extends ServiceImpl<CouponTemplateTargetShopMapper,CouponTemplateTargetShop> implements ICouponTemplateTargetShopManager {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void insertBatch(Integer templateId, List<TemplateTargetShopsCommand> shopList) {
        List<CouponTemplateTargetShop> targetList = new ArrayList<>();
        shopList.forEach(
                templateTargetShopsCommand -> {
                    CouponTemplateTargetShop targetShop = new CouponTemplateTargetShop(templateTargetShopsCommand);
                    targetShop.setTemplateId(templateId);
                    targetList.add(targetShop);
                }
        );
        if(!targetList.isEmpty()){
            super.saveBatch(targetList);
        }
    }

    @Override
    public List<CouponTemplateTargetShop> queryByTemplateId(Integer templateId) {
        //缓存获取
//        String key = ActivityCouponCacheKey.SHOP_BY_TEMPLATE_ID.key(templateId);
//        String activityStr = stringRedisTemplate.opsForValue().get(key);
//        if (StringUtils.isNotEmpty(activityStr)) {
//            return JSON.parseObject(activityStr, new TypeReference<List<CouponTemplateTargetShop>>() {});
//        }
        //DB
        List<CouponTemplateTargetShop> targetShopList = baseMapper.queryByTemplateId(templateId);
//        activityStr = CollectionUtils.isEmpty(targetShopList) ? "" : JSON.toJSONString(targetShopList);
//        stringRedisTemplate.opsForValue().set(key, activityStr, 24*60*60*1000, TimeUnit.MILLISECONDS);
        return targetShopList;
    }

    @Override
    public Page<TemplateShopVo> queryByCondition(ShopQueryCommand command){
        Page<TemplateShopVo> shopVoPage = new Page<>();
        Integer totalCount = baseMapper.queryCount(command.getTemplateId(),command.getShopName(),command.getShopId());
        if(null == totalCount || 0 >= totalCount){
            return shopVoPage;
        }
        List<CouponTemplateTargetShop> goodsVoList = baseMapper.queryByPage(command.getTemplateId(),command.getShopName(),command.getShopId(),command.getStart(),command.getPageSize());
        List<TemplateShopVo> goodsInfoVoList = goodsVoList.stream().map(CouponTemplateTargetShop::createShopVo).collect(Collectors.toList());
        shopVoPage.setTotalCount(totalCount.longValue());
        shopVoPage.setRecords(goodsInfoVoList);
        return shopVoPage;
    }

    @Override
    public List<CouponTemplateTargetShop> queryByTemplateId(List<Integer> templateIdList) {
        List<CouponTemplateTargetShop> targetShopList = new ArrayList<>();
        for (Integer templateId : templateIdList){
            List<CouponTemplateTargetShop> shops = queryByTemplateId(templateId);
            targetShopList.addAll(shops);
        }
        return targetShopList;
    }
}
