/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/7
 * Description:NewMemSendCouponService.java
 */
package com.qmfresh.coupon.services.service;

import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.interfaces.enums.MemberFinalEnum;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;

import java.util.List;

/**
 * @author lxc
 */
public interface ICouponConsumerService {

    /**
     * 优惠券补偿
     *
     * @param bean
     */
    void couponReimburse(CouponRemunerateRequestBean bean);

    /**
     * 线上优惠券发放
     *
     * @param bean
     */
    void onlineCouponSend(CashQuerySendCouponRequestBean bean);

    /**
     * 销毁使用过的优惠券
     */
    void consumeUsedCoupon(List<CouponUseRequestBean> list);

    /**
     * 撤单优惠券还原
     * @param param
     */
    void orderCancel(OrderCancelRequestBean param);

}
