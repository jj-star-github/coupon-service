package com.qmfresh.coupon.services.entity.command;

import com.qmfresh.coupon.services.common.exception.BusinessException;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * @Description:
 * @Version: V1.0
 */
@Data
@ToString
@Slf4j
public class CouponQueryListCommand implements Serializable {

    private Integer pageNum;
    private Integer pageSize;

    //优惠券的模板id
    private Integer originalId;
    //优惠券码
    private String  couponCode;
    /**
     *优惠券绑定的订单号
     */
    private String sourceOrderNo;

    //券类型（1：满减、2:折扣、3：随机金额）
    private Integer couponType;

    //发券的开始时间
    private Integer startTime;
    //发券的结束时间
    private Integer endTime;
    //使用状态（0：未使用、1：已使用、2：已过期）
    private Integer status;
    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }
    public CouponQueryListCommand(){}
    public static CouponQueryListCommand create(Integer pageNum, Integer pageSize,
                                            Integer originalId, String couponCode, String sourceOrderNo,Integer couponType,Integer startTime, Integer endTime, Integer status ) {


        if (pageNum == null || pageNum <0) {
            throw new BusinessException("必须设置PageNum");
        }
        if (pageSize == null || pageNum <0) {
            throw new BusinessException("必须设置PageNum");
        }
        CouponQueryListCommand couponQueryListCommand = new CouponQueryListCommand();
        couponQueryListCommand.setPageNum(pageNum);
        couponQueryListCommand.setPageSize(pageSize);
        couponQueryListCommand.setOriginalId(originalId);
        couponQueryListCommand.setCouponCode(couponCode);
        couponQueryListCommand.setSourceOrderNo(sourceOrderNo);
        couponQueryListCommand.setStartTime(startTime);
        couponQueryListCommand.setEndTime(endTime);
        couponQueryListCommand.setCouponType(couponType);
        couponQueryListCommand.setStatus(status);

        return couponQueryListCommand;
    }
}
