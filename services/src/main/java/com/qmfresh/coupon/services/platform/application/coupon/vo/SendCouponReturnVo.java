package com.qmfresh.coupon.services.platform.application.coupon.vo;

import com.qmfresh.coupon.client.send.SendCouponReturnDTO;
import com.qmfresh.coupon.services.platform.domain.shared.BaseUtil;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 发券响应对象
 */
@Data
@Builder
public class SendCouponReturnVo implements Serializable {

    private static final long serialVersionUID = -2634608309253218048L;

    /**
     * 优惠券ID
     */
    private Integer couponId;
    /**
     * 优惠券名
     */
    private String couponName;
    /**
     * 优惠券码
     */
    private String couponCode;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 有效期
     */
    private String validityPeriod;
    /**
     * 优惠券绑定的订单号
     */
    private String sourceOrderNo;

    /**
     * UserId
     */
    private Long userId;

    /**
     * 剩余可领张数
     */
    private Integer residueNum;

    public static SendCouponReturnVo create(Coupon coupon) {
        SendCouponReturnVo returnVo = SendCouponReturnVo.builder()
                .couponCode(coupon.getCouponCode())
                .couponId(coupon.getId())
                .couponName(coupon.getCouponName())
                .sourceOrderNo(coupon.getSourceOrderNo())
                .userId(coupon.getUserId())
                .useInstruction(coupon.getUseInstruction())
                .validityPeriod(BaseUtil.buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd()))
                .residueNum(coupon.getResidueNum())
                .build();
        return returnVo;
    }

    public SendCouponReturnDTO createDto() {
        SendCouponReturnDTO returnDTO = new SendCouponReturnDTO();
        returnDTO.setCouponCode(this.getCouponCode());
        returnDTO.setCouponId(this.getCouponId());
        returnDTO.setCouponName(this.getCouponName());
        returnDTO.setResidueNum(this.getResidueNum());
        returnDTO.setSourceOrderNo(this.getSourceOrderNo());
        returnDTO.setUseInstruction(this.getUseInstruction());
        returnDTO.setUserId(this.getUserId());
        returnDTO.setValidityPeriod(this.getValidityPeriod());
        return returnDTO;
    }
}
