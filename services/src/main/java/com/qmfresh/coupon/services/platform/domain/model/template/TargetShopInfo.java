package com.qmfresh.coupon.services.platform.domain.model.template;

import lombok.Data;

import java.io.Serializable;

@Data
public class TargetShopInfo implements Serializable {
    /**
     * 券模板id
     */
    private Integer templateId;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 门店名
     */
    private String shopName;
    /**
     * 门店类型,1.大区,2.片区,3.门店，4.仓
     */
    private Integer shopType;
}
