package com.qmfresh.coupon.services.platform.application.coupon;

import com.qmfresh.coupon.interfaces.enums.MemberFinalEnum;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendBySendActivityContext;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendContext;
import com.qmfresh.coupon.services.platform.application.coupon.vo.SendCouponReturnVo;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;

import java.util.List;

/**
 *
 */
public interface SendCouponService {


    /**
     * 发券
     * @param sendContext
     * @return
     */
    List<SendCouponReturnVo> sendCoupon(SendContext sendContext);

    /**
     * 根据门店ID获取券码
     * 如果不传shopId 前三位会拼三位随机数
     * @param shopId
     * @return
     */
    List<String> getCouponCode(Integer shopId, Integer sendNum);


    /**
     * 根据发放活动发券
     * @param sendBySendActivityContext
     * @return
     */
    List<SendCouponReturnVo> sendCouponBySendActivity(SendBySendActivityContext sendBySendActivityContext);

    /**
     * 发放月礼券 月礼券不走send验证
     */
    void sendMonth(MemberFinalEnum memberFinalEnum);

    /**
     * 人工发券
     */
    void hmSendByType(SendCouponDto sendCouponDto);

    /**
     * 定向发券
     */
    void sendToUser();
}
