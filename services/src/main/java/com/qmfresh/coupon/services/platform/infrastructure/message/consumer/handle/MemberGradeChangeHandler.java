package com.qmfresh.coupon.services.platform.infrastructure.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.coupon.UserIdParamBean;
import com.qmfresh.coupon.interfaces.dto.vip.MemberGradeChangeEventDto;
import com.qmfresh.coupon.interfaces.dto.vip.MemberGradeDto;
import com.qmfresh.coupon.interfaces.enums.MemberGradeChangeEnum;
import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.services.platform.application.coupon.SendCouponService;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendBySendActivityContext;
import com.qmfresh.coupon.services.platform.application.coupon.enums.SendBusinessTypeEnums;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 */
@Service
public class MemberGradeChangeHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberGradeChangeHandler.class);
    @Autowired
    private SendCouponService sendCouponService;

    public void handleMessage(MemberGradeChangeEventDto bean){
        LOGGER.info("MemberGradeChangeHandler handleMessage begin....param:" + JSON.toJSONString(bean));
        //获取用户所升的等级
        MemberGradeUpBean userIdParamBean = mapParam(bean);
        if(userIdParamBean != null){
            //封装发券上线文发券
            String businessCode = userIdParamBean.getUserId().toString() + userIdParamBean.getActivityType();
            SendBySendActivityContext sendActivityContext  = new SendBySendActivityContext(businessCode, SendBusinessTypeEnums.MEMBER_UPDATE_ACTIVITY.getType(),
                    userIdParamBean.getActivityType(), userIdParamBean.getUserId().intValue(), null, null, null);
            sendCouponService.sendCouponBySendActivity(sendActivityContext);
        }
        LOGGER.info("MemberGradeChangeHandler handleMessage end....");
    }

    private MemberGradeUpBean mapParam(MemberGradeChangeEventDto bean){
        if(bean == null){
            return null;
        }
        MemberGradeDto from = bean.getFrom();
        MemberGradeDto to = bean.getTo();
        if(from != null && to != null){
            //升级
            if(from.getGradeId() != null && to.getGradeId() != null && from.getGradeId() < to.getGradeId()){
                Integer upgradeType = MemberGradeChangeEnum.getGradeChangeType(from.getGradeId(),to.getGradeId());
                if(upgradeType != null){
                    MemberGradeUpBean userIdParamBean = new MemberGradeUpBean();
                    userIdParamBean.setLimitChannel(to.getLimitChannel());
                    userIdParamBean.setUserId(to.getUserId().longValue());
                    userIdParamBean.setActivityType(upgradeType);
                    return userIdParamBean;
                }
            }else if(from.getGradeId() != null && to.getGradeId() != null && from.getGradeId() < to.getGradeId()){
                LOGGER.warn("降级参数：" + JSON.toJSONString(bean));
                //目前不支持降级处理
                throw new BusinessException("目前不支持降级处理");
            }
        }
        return null;
    }
}
