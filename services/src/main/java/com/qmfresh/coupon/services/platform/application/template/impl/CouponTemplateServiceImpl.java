package com.qmfresh.coupon.services.platform.application.template.impl;

import com.alibaba.fastjson.JSONObject;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityTimeRule;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.platform.application.template.CouponTemplateService;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.model.template.command.CouponTemplateCreateCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateUseTimeTypeEnums;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.CouponTemplateMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class CouponTemplateServiceImpl implements CouponTemplateService {
    @Resource
    private ICouponTemplateManager couponTemplateManager;
    @Resource
    private CouponTemplateMapper couponTemplateMapper;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createCouponTemplate(CouponTemplateCreateCommand command) {
        couponTemplateManager.createCouponTemplate(command);
    }

    @Override
    public void modifyStatus() {
        int now = DateUtil.getCurrentTimeIntValue();
        // 查优惠券活动表使用时间的类型是1的
        List<CouponTemplate> activityList = couponTemplateMapper.queryByTimeType(TemplateUseTimeTypeEnums.TIME_USE_FROM_TO.getCode());
        if(CollectionUtils.isEmpty(activityList)) {
            return;
        }
        List<Integer> ids = new ArrayList<>();
        for (CouponTemplate activity : activityList) {
            // 解析时间规则
            JSONObject timeRule = JSONObject.parseObject(activity.getUseTimeRule());
            // 时间规则
            ActivityTimeRule activityTimeRule = (ActivityTimeRule) JSONObject.toJavaObject(timeRule, ActivityTimeRule.class);
            if (now > activityTimeRule.getTimeEnd()) {
                ids.add(activity.getId());
            }
        }
        if (CollectionUtils.isNotEmpty(ids)) {
            couponTemplateMapper.updateOutTime(ids, new Date());
        }
    }
}
