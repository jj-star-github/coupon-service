package com.qmfresh.coupon.services.platform.infrastructure.mapper.template;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CouponTemplateTargetGoodsMapper extends BaseMapper<CouponTemplateTargetGoods> {
    /**
     *根据券模板id,goodsId查
     * @param templateId
     * @return
     */
    List<CouponTemplateTargetGoods> queryByTemplateId(@Param("templateId")Integer templateId,@Param("goodsId")String goodsId);

    /**
     *分页查
     * @param templateId
     * @return
     */
    List<CouponTemplateTargetGoods> queryByPage(@Param("templateId")Integer templateId,@Param("goodsName")String goodsName,@Param("goodsId")String goodsId,@Param("start")Integer start, @Param("length")Integer length);

    Integer queryCount(@Param("templateId")Integer templateId,@Param("goodsName")String goodsName,@Param("goodsId")String goodsId);
}
