package com.qmfresh.coupon.services.platform.domain.model.template.command;

import lombok.Data;

@Data
public class GoodsQueryCommand {
    private Integer templateId;
    private String goodsId;
    private String goodsName;
    private Integer pageNum;
    private Integer pageSize;
    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }

    public GoodsQueryCommand(){}

    public GoodsQueryCommand(Integer templateId,
                             String goodsId,
                             String goodsName,
                             Integer pageNum,
                             Integer pageSize){
        this.templateId=templateId;
        this.goodsId=goodsId;
        this.goodsName=goodsName;
        this.pageNum=pageNum;
        this.pageSize=pageSize;
    }
}
