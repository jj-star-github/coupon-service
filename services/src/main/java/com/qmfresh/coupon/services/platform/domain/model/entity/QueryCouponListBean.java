package com.qmfresh.coupon.services.platform.domain.model.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**优惠券后台管理需要查询展示的详情信息
 * @Description:
 * @Version: V1.0
 */
@Data
public class QueryCouponListBean  implements Serializable {

    //查询券的使用状态：（-1:全部、0：表示未使用、1：表示已经使用了、2：表示已经过期了）
    private Integer queryCouponUseStatus;

    //发送优惠券的使用时间（“use_time”）
    private String sendCouponUseTime;

    //查询优惠券的使用规则（1：满减优惠券、2：折扣优惠券、3:随机金额优惠券）
    private Integer queryCouponUseRule;

    //发送优惠券业务的标识（SEND_COUPON_TAG）
    private Integer sendCouponTag;

    //优惠券的使用订单号
    private String couponUseOrderId;

    //优惠券所使用的门店
    private Integer couponUseShop;


    /**
     * 用户id
     */
    private Long userId;
    /**
     * 优惠券码
     */
    private String  couponCode;
    /**
     * 券名称
     */
    private String couponName;

    /**
     * 订单金额
     */
    private BigDecimal orderAmount;
    /**
     *优惠券绑定的订单号
     */
    private String sourceOrderNo;

    /**
     * 使用说明
     */
    private String useInstruction;


    /**
     * 使用优惠券的门店id
     */
    private Integer useShopId;

    /**
     * 券使用开始时间
     */
    private String useTimeStart;

    /**
     * 券使用结束时间
     */
    private String useTimeEnd;


    /**
     * 券的创建时间
     */
    private String creatTime;
}
