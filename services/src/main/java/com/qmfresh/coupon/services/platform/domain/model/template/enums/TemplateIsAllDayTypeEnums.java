package com.qmfresh.coupon.services.platform.domain.model.template.enums;

import lombok.Getter;

@Getter
public enum TemplateIsAllDayTypeEnums {
    ALL_DAY(0, "全天"),
    IS_HMS(1, "时间段");

    private Integer code;

    private String remark;

    TemplateIsAllDayTypeEnums(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
