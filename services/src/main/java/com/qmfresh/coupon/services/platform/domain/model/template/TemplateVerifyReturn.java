package com.qmfresh.coupon.services.platform.domain.model.template;

import com.qmfresh.coupon.services.platform.domain.model.template.command.Sku;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import lombok.Data;

import java.util.List;

@Data
public class TemplateVerifyReturn {
    /**
     * 成功标记 0：成功，1：失败
     */
    private Boolean successFlag;

    /**
     * 失败原因
     */
    private String reason;

    /**
     * 符合的适用商品
     */
    private List<Sku> skuList;

    private CouponTemplate couponTemplate;

    public TemplateVerifyReturn(){}

    public TemplateVerifyReturn(String reason){
        this.successFlag = false;
        this.reason = reason;
    }

    public TemplateVerifyReturn(List<Sku> skuList, CouponTemplate couponTemplate){
        this.successFlag = true;
        this.skuList = skuList;
        this.couponTemplate = couponTemplate;
    }
}
