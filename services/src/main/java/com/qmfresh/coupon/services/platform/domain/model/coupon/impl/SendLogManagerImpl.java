package com.qmfresh.coupon.services.platform.domain.model.coupon.impl;

import com.qmfresh.coupon.services.platform.domain.model.coupon.SendLogManager;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.SendLogMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.SendLog;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import lombok.extern.slf4j.Slf4j;

/**
 *
 */
@Service
@Slf4j
public class SendLogManagerImpl extends ServiceImpl<SendLogMapper, SendLog>
        implements SendLogManager {

    @Override
    public SendLog queryByBusiness(String businessCode, Integer businessType) {
        return baseMapper.queryByBusiness(businessCode, businessType);
    }
}
