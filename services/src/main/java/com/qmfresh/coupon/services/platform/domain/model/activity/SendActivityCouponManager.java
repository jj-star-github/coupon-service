package com.qmfresh.coupon.services.platform.domain.model.activity;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.services.platform.domain.shared.Operator;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivityCoupon;

import java.util.List;

/**
 * 发放活动绑定优惠券
 */
public interface SendActivityCouponManager extends IService<SendActivityCoupon> {

    /**
     * 根据活动ID查询发放信息
     * @param activityId
     * @return
     */
    List<SendActivityCoupon> queryByActivityId(Long activityId);

    /**
     * 根据活动ID查询发放信息
     * @param activityId
     * @return
     */
    List<SendActivityCoupon> queryByActivityId(Long activityId, List<Integer> templateIdList);

    /**
     *
     * @param activityIdList
     * @return
     */
    List<SendActivityCoupon> queryByActivityId(List<Long> activityIdList);

    /**
     * 解绑活动的发放券信息
     * @param activityId
     * @param id
     */
    void delete(Long activityId, Long id, Operator operator);

    /**
     * 修改执行券发放数量
     * @param activityId
     * @param id
     * @param sendNum
     */
    void updateSendNum(Long activityId, Long id, Integer sendNum, Operator operator);

    /**
     * 批量插入发放券的信息
     * @param activityCouponList
     * @param activityId
     */
    void insertBatch(List<SendActivityCoupon> activityCouponList, Long activityId);

    /**
     * 修改send_rule
     * @param id
     * @param sendRule
     */
    void updateSendRule(Long id, String sendRule,Long activityId);
}
