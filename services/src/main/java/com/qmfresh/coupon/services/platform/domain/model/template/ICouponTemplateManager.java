package com.qmfresh.coupon.services.platform.domain.model.template;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.client.template.dto.TemplateBingActResultDTO;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityGoodsRule;
import com.qmfresh.coupon.services.platform.domain.model.template.command.*;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateInfoVo;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;

import java.util.List;


public interface ICouponTemplateManager extends IService<CouponTemplate> {

    /**
     * 创建券模板
     */
    void createCouponTemplate(CouponTemplateCreateCommand command);

    /**
     * 根据id查询券模板基础信息
     */
    CouponTemplate selectById(Integer templateId);

    /**
     * 根据id查询券模板
     */
    CouponTemplateInfo queryCouponTemplateById(Integer templateId);

    /**
     * 根据券模板id封装信息发放优惠券
     */
    CouponSendByTemplate couponSendByTemplate(CouponSendCommand sendCommand);

    /**
     * 分页查
     */
    Page<TemplateInfoVo> queryByCondition(CouponTemplateQueryCommand queryCommand);

    /**
     * 券模板验证订单信息
     */
    TemplateVerifyReturn verifyTemplate(TemplateVerifyCommand verifyCommand);

    /**
     * 绑定券模板
     */
    TemplateBingActResultDTO bingTemplate(String actId,String actName,List<Integer> templateIds);

    /**
     * 根据ID批量查询
     * @param idList
     * @return
     */
    List<CouponTemplate> queryByIdList(List<Integer> idList ,Integer status);

    /**
     * 会场查询券模板
     */
    List<CouponTemplate> queryByAct(String actId);

    ActivityGoodsRule buildApplicableGoodsRule(CouponTemplate couponTemplate);

    /**
     * 验证 template 的门店
     * @param shopId
     * @param shopType
     * @param templateId
     * @return
     */
    boolean verifyShop(Integer shopId,Integer shopType,Integer templateId);

    /**
     * 验证 template 的 商品
     * @param skuList
     * @param couponTemplate
     * @return
     */
    boolean verifyGoods(List<Sku> skuList, CouponTemplate couponTemplate);

    /**
     *
     * @param skuList
     * @param couponTemplate
     * @return
     */
    List<Sku> buildGoodsInfo(List<Sku> skuList,CouponTemplate couponTemplate);

}
