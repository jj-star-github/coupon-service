package com.qmfresh.coupon.services.common.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wyh on 2018/1/29.
 *
 * @author wyh
 */
public class ListUtil {
    private static final Logger logger = LoggerFactory.getLogger(ListUtil.class);

    /**
     * chops a list into non-view sublists of length L
     *
     * @param list
     * @param L
     * @param <T>
     * @return
     */
    public static <T> List<List<T>> chopped(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(list.subList(i, Math.min(N, i + L))));
        }
        return parts;
    }

    /**
     * String字符串转成List<Integer>数据格式 String str = "1,2,3,4,5,6" -> List<Long> listLong [1,2,3,4,5,6];
     *
     * @param strArr
     * @return
     */
    public static List<Integer> stringToIntegerList(String strArr) {
        return Arrays.stream(strArr.split(","))
            .map(s -> Integer.parseInt(s.trim()))
            .collect(Collectors.toList());
    }

    public static List<Long> stringToLongList(String strArr) {
        return Arrays.stream(strArr.split(","))
            .map(s -> Long.parseLong(s.trim()))
            .collect(Collectors.toList());
    }

    public static List<String> stringToList(String strArr) {
        return Arrays.stream(strArr.split(","))
            .collect(Collectors.toList());
    }

    /**
     * 用于把List&lt;Object>转换成Map&lt;String,Object>形式，便于存入缓存
     *
     * @param keyName 主键属性
     * @param list 集合
     * @return 返回对象
     */
    public static <T> Map<String, T> listToMap(String keyName, List<T> list) {
        Map<String, T> m = new HashMap<>();
        try {
            for (T t : list) {
                PropertyDescriptor pd = new PropertyDescriptor(keyName,
                    t.getClass());
                Method getMethod = pd.getReadMethod();// 获得get方法
                Object o = getMethod.invoke(t);// 执行get方法返回一个Object
                m.put(o.toString(), t);
            }
            return m;
        } catch (Exception e) {
            logger.error("Convert List to Map failed");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 默认为id
     *
     * @param list
     * @param <T>
     * @return
     */
    public static <T> Map<String, T> listToMap(List<T> list) {
        return ListUtil.listToMap("id", list);
    }

    public static <T> boolean isNullOrEmpty(List<T> list) {
        return list == null || list.isEmpty();
    }
}
