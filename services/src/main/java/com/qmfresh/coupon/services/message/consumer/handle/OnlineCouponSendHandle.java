/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/7
 * Description:NewMemSendCouponHandle.java
 */
package com.qmfresh.coupon.services.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.coupon.CashQuerySendCouponRequestBean;
import com.qmfresh.coupon.interfaces.dto.coupon.UserIdParamBean;
import com.qmfresh.coupon.services.service.ICouponConsumerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lxc
 */
@Service
public class OnlineCouponSendHandle {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(OnlineCouponSendHandle.class);

    @Autowired
    private ICouponConsumerService sendCouponService;

    public void handleMessage(CashQuerySendCouponRequestBean bean) {
        LOGGER.info("OnlineCouponSendHandle handleMessage begin....参数:" + JSON.toJSONString(bean));
        sendCouponService.onlineCouponSend(bean);
        LOGGER.info("OnlineCouponSendHandle handleMessage end ....");
    }
}
