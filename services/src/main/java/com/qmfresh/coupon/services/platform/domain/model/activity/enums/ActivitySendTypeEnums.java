package com.qmfresh.coupon.services.platform.domain.model.activity.enums;

import lombok.Getter;

/**
 *
 */
@Getter
public enum ActivitySendTypeEnums {
    /**
     * 定向发放
     */
    APPOINT(0, "定向发放"),
    /**
     * 系统发放
     */
    SYSTEM(1, "系统发放");

    private Integer code;
    private String name;

    ActivitySendTypeEnums(Integer code, String name) {
        this.code = code;
        this.name = name;
    }
}
