package com.qmfresh.coupon.services.platform.application.coupon.context;

import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Data
public class SendContext {

    private String businessCode;
    private Integer businessType;
    private Integer userId;
    private String sourceOrderCode;
    private Integer couponTemplate;
    private Integer sendNum;
    private List<String> couponCode;
    private Integer shopId;
    private Integer sourceSystem;
    /**
     * 促销活动id
     */
    private Integer promotionId;

    public SendContext(){}

    public SendContext (String businessCode,
                                     Integer businessType,
                                     Integer userId,
                                     String sourceOrderCode,
                                     Integer couponTemplate,
                                     Integer sendNum,
                                     Integer shopId,
                                     Integer sourceSystem,
                                     List<String> couponCode,
                        Integer promotionId) {
        if (StringUtils.isBlank(businessCode)){
            throw new BusinessException("业务唯一编码不可为空");
        }
        if (null == businessType){
            throw new BusinessException("业务类型不可为空");
        }
        if (null == couponTemplate) {
            throw new BusinessException("优惠券模板不可为空");
        }
        if (null == sendNum && CollectionUtils.isEmpty(couponCode)) {
            throw new BusinessException("发券数量不可为空");
        }
        this.businessCode = businessCode;
        this.businessType = businessType;
        this.userId = userId;
        this.shopId = shopId;
        this.sourceOrderCode = sourceOrderCode;
        this.couponTemplate = couponTemplate;
        this.sendNum = CollectionUtils.isEmpty(couponCode) ? sendNum : couponCode.size();
        this.couponCode = couponCode;
        this.sourceSystem = sourceSystem;
        this.promotionId = promotionId;
    }

}
