package com.qmfresh.coupon.services.platform.domain.model.template.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class TemplateInfoVo implements Serializable {
    private Integer id;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    private Integer couponCondition;
    /**
     * 订单所需金额
     */
    private Integer needMoney;
    /**
     * 券类型1.减2.折3.随机
     */
    private Integer couponType;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 用户领取最大张数
     */
    private Integer useReceiveNum;
    /**
     * 优惠券模板最大发放张数
     */
    private Integer couponMaxSendNum;
    /**
     * 券门店使用说明
     */
    private String shopInstruction;
    /**
     * 券使用时间
     */
    private String useTime;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;
    /**
     * 适用门店类型1全部，2指定可用，3指定不可用
     */
    private Integer applicableShopType;
    /**
     * 适用商品类型，1.全品，2.sku,3.class1,4.class2,5.ssu,6.saleClass1,7.saleClass2,8.会场
     */
    private Integer applicableGoodsType;
    /**
     * 创建人名称
     */
    private String createdName;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 状态2:生效中3:已过期
     */
    private Integer status;
}
