/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/7
 * Description:NewMemSendCouponConsumer.java
 */
package com.qmfresh.coupon.services.message.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.interfaces.dto.coupon.CashQuerySendCouponRequestBean;
import com.qmfresh.coupon.services.message.consumer.handle.OnlineCouponSendHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author lxc
 * 线上消费送券
 */
@Service
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}OnlineCouponSendConsumer", topic = "${mq.topic.applySendCoupons}")
public class OnlineCouponSendConsumer extends AbstractRocketMQListener {
    @Resource
    private OnlineCouponSendHandle handle;
    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        CashQuerySendCouponRequestBean bean = JSON.parseObject(content,
                new TypeReference<CashQuerySendCouponRequestBean>() {
                });
        handle.handleMessage(bean);
    }
}
