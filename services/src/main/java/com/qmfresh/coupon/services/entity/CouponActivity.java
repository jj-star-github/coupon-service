package com.qmfresh.coupon.services.entity;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityTimeRule;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxc
 * @since 2019-08-01
 */
@TableName("t_coupon_activity")
public class CouponActivity extends Model<CouponActivity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增长
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 券名称
     */
	@TableField("coupon_name")
	private String couponName;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
	@TableField("coupon_condition")
	private Integer couponCondition;
    /**
     * 订单所需金额
     */
	@TableField("need_money")
	private Integer needMoney;
    /**
     * 券类型
     */
	@TableField("coupon_type")
	private Integer couponType;
    /**
     * 券规则
     */
	@TableField("coupon_rule")
	private String couponRule;
    /**
     * 使用时间类型
     */
	@TableField("use_time_type")
	private Integer useTimeType;
    /**
     * 使用时间规则
     */
	@TableField("use_time_rule")
	private String useTimeRule;
    /**
     * 适用商品类型
     */
	@TableField("applicable_goods_type")
	private Integer applicableGoodsType;
    /**
     * 券适用商品规则
     */
	@TableField("applicable_goods_rule")
	private String applicableGoodsRule;
    /**
     * 券使用说明
     */
	@TableField("use_instruction")
	private String useInstruction;
    /**
     * 状态
     */
	@TableField("status")
	private Integer status;
    /**
     * 创建人id
     */
	@TableField("created_id")
	private Integer createdId;
    /**
     * 创建人名称
     */
	@TableField("created_name")
	private String createdName;
    /**
     * 创建时间
     */
	@TableField("c_t")
	private Integer cT;
    /**
     * 更新时间
     */
	@TableField("u_t")
	private Integer uT;
    /**
     * 有效性
     */
	@TableField("is_deleted")
	private Integer isDeleted;
	/**
	 * 限定渠道(0:全渠道,1:线下，2:线上)
	 */
	@TableField("limit_channel")
	private Integer limitChannel;

	public Integer getLimitChannel() {
		return limitChannel;
	}

	public void setLimitChannel(Integer limitChannel) {
		this.limitChannel = limitChannel;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public Integer getCouponCondition() {
		return couponCondition;
	}

	public void setCouponCondition(Integer couponCondition) {
		this.couponCondition = couponCondition;
	}

	public Integer getNeedMoney() {
		return needMoney;
	}

	public void setNeedMoney(Integer needMoney) {
		this.needMoney = needMoney;
	}

	public Integer getCouponType() {
		return couponType;
	}

	public void setCouponType(Integer couponType) {
		this.couponType = couponType;
	}

	public String getCouponRule() {
		return couponRule;
	}

	public void setCouponRule(String couponRule) {
		this.couponRule = couponRule;
	}

	public Integer getUseTimeType() {
		return useTimeType;
	}

	public void setUseTimeType(Integer useTimeType) {
		this.useTimeType = useTimeType;
	}

	public String getUseTimeRule() {
		return useTimeRule;
	}

	public void setUseTimeRule(String useTimeRule) {
		this.useTimeRule = useTimeRule;
	}

	public Integer getApplicableGoodsType() {
		return applicableGoodsType;
	}

	public void setApplicableGoodsType(Integer applicableGoodsType) {
		this.applicableGoodsType = applicableGoodsType;
	}

	public String getApplicableGoodsRule() {
		return applicableGoodsRule;
	}

	public void setApplicableGoodsRule(String applicableGoodsRule) {
		this.applicableGoodsRule = applicableGoodsRule;
	}

	public String getUseInstruction() {
		return useInstruction;
	}

	public void setUseInstruction(String useInstruction) {
		this.useInstruction = useInstruction;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCT() {
		return cT;
	}

	public void setCT(Integer cT) {
		this.cT = cT;
	}

	public Integer getUT() {
		return uT;
	}

	public void setUT(Integer uT) {
		this.uT = uT;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	/**
	 * 验证优惠券小时段，默认true，不在小时段内false
	 */
	public static boolean inAvailableRuleTime(CouponTemplate couponTemplate){
		ActivityTimeRule timeRule = JSON.parseObject(couponTemplate.getUseTimeRule(),new TypeReference<ActivityTimeRule>(){});
		if(timeRule.getIsAllDay() != null && timeRule.getIsAllDay().equals(1)){
			//设置日期格式
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
			try {
				Date nowTime = df.parse(df.format(new Date()));
				//有效时间段
				Date hmsStart = df.parse(timeRule.getHmsStart());
				Date hmsEnd = df.parse(timeRule.getHmsEnd());
				if(nowTime.after(hmsStart) && nowTime.before(hmsEnd)){
					return true;
				}else {
					return false;
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
