package com.qmfresh.coupon.services.common.utils;

import java.util.Random;

/**
 * @ClassName CouponUtil
 * @Description TODO
 * @Author xbb
 * @Date 2020/4/7 11:09
 */
public class CouponUtil {

    public static final char[] codePostList = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k',
            'l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
            'P','Q','R','S','T','U','V','W','X','Y','Z'};

    public static final char[] numCodeList = {'0','1','2','3','4','5','6','7','8','9'};

    public static String getSequence(Integer length){
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0 ; i < length ; i++){
            Integer index = random.nextInt(codePostList.length);
            sb.append(codePostList[index]);
        }
        return sb.toString();
    }

    public static String getNumSequence(Integer length){
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0 ; i < length ; i++){
            Integer index = random.nextInt(numCodeList.length);
            sb.append(numCodeList[index]);
        }
        return sb.toString();
    }

    public static void main(String[] args){
        for(int i =0;i<100;i++){
//            String str = getSequence(4);
            String str = getNumSequence(4);
            System.out.println(str);
        }
    }
}
