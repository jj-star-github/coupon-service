package com.qmfresh.coupon.services.platform.application.template;


import com.qmfresh.coupon.services.platform.domain.model.template.command.CouponTemplateCreateCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.Sku;

import java.util.List;

public interface CouponTemplateService {
    /**
     * 创建优惠券活动
     */
    void createCouponTemplate(CouponTemplateCreateCommand command);

    /**
     * 定时任务修改状态
     */
    void modifyStatus();
}
