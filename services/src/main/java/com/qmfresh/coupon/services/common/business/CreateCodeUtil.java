package com.qmfresh.coupon.services.common.business;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wyh on 2018/6/23.
 *
 * @author wyh
 */
public class CreateCodeUtil {

    // 收货
    public static final String RECEIVE = "SH";

    // 调拨出库
    public static final String ALLOT_OUTBOUND = "AOB";

    // 调拨入库
    public static final String ALLOT_INBOUND = "AIB";

    // 报损出库
    public static final String REPORT_LOSS_OUTBOUND = "RLO";

    // 盘点任务
    public static final String INVENTORY_TASK = "IT";

    public static String genReceiveCode(String preStr, String preReceiveCode) {
        String retStr;
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        NumberFormat nf = new DecimalFormat("0000");
        if (preReceiveCode == null) {
            retStr = preStr + df.format(new Date()) + nf.format(1);
        } else {
            String yyyyMMDD = preReceiveCode.substring(preStr.length(), preStr.length() + 8);
            // 当前日已经存在数据
            if (yyyyMMDD.equals(df.format(new Date()))) {
                int currentSeq;
                try {
                    currentSeq = nf.parse(preReceiveCode.substring(preStr.length() + 8)).intValue() + 1;
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
                retStr = preStr + yyyyMMDD + nf.format(currentSeq);
            } else {
                retStr = preStr + df.format(new Date()) + nf.format(1);
            }
        }
        return retStr;
    }
}
