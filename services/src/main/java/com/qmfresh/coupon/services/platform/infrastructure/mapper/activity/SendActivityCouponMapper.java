package com.qmfresh.coupon.services.platform.infrastructure.mapper.activity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivityCoupon;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 发放活动绑定优惠券
 */
public interface SendActivityCouponMapper extends BaseMapper<SendActivityCoupon> {

    /**
     * 根据活动ID查询活动
     * @param activityId
     * @return
     */
    List<SendActivityCoupon> queryByActivityId(@Param("activityId")Long activityId);

    /**
     * 根据活动ID查询活动
     * @param activityIdList
     * @return
     */
    List<SendActivityCoupon> queryByActivityIdList(@Param("activityIdList")List<Long> activityIdList);

    /**
     * 根据活动ID查询活动
     * @param activityId
     * @return
     */
    List<SendActivityCoupon> queryByActivityAndTemp(@Param("activityId")Long activityId, @Param("templateIdList") List<Integer> templateIdList);

    /**
     * 修改发放张数
     * @param id
     * @param sendNum
     * @param activityId
     */
    void updateSendNum(@Param("id")Long id, @Param("sendNum") Integer sendNum, @Param("activityId")Long activityId, @Param("operatorId")Integer operatorId, @Param("operatorName")String operatorName);

    /**
     * 活动删除
     * @param id
     * @param activityId
     */
    void delete(@Param("id")Long id, @Param("activityId")Long activityId, @Param("operatorId")Integer operatorId, @Param("operatorName")String operatorName);

    /**
     * 批量插入
     * @param couponList
     */
    void insertBatch(@Param("couponList")List<SendActivityCoupon> couponList);

    /**
     * 修改send_rule
     * @param id
     * @param sendRule
     */
    void updateSendRule(@Param("id")Long id, @Param("sendRule") String sendRule);
}