package com.qmfresh.coupon.services.message.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.interfaces.dto.coupon.OrderCancelRequestBean;
import com.qmfresh.coupon.services.message.consumer.handle.OrderCancelHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 订单取消 优惠券回退
 */
@Service
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}OrderCancelConsumer", topic = "${mq.topic.orderCancel}")
public class OrderCancelConsumer extends AbstractRocketMQListener {

    @Resource
    private OrderCancelHandle handle;

    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        OrderCancelRequestBean bean = JSON.parseObject(content,
                new TypeReference<OrderCancelRequestBean>() {
                });
        handle.handleMessage(bean);
    }
}
