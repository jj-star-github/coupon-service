package com.qmfresh.coupon.services.platform.domain.model.coupon.impl;


import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.services.platform.domain.model.coupon.PromotionManager;
import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.PromotionServerApi;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 */
@Service
@Slf4j
public class PromotionManagerImpl implements PromotionManager {

    @Resource
    private PromotionServerApi promotionServerApi;

    @Override
    public Double getPromotionDetailNew(Long activityId) {
        ServiceResult<String> result = promotionServerApi.getPromotionDetailNew(activityId);
        if(null == result || !result.getSuccess()){
            return null;
        }
        log.info("促销返回信息:{} {}",activityId, JSON.toJSONString(result));
        JSONObject promotionInfo = JSONObject.parseObject(result.getBody());
        return new Double(promotionInfo.getString("meetMoney"));
    }


}
