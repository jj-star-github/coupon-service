package com.qmfresh.coupon.services.common.exception;

/**
 * 参数异常类
 *
 * @author wyh
 */
public class InvalidParamException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private Object data;

    public InvalidParamException(String msg) {
        super(msg);
    }

    public InvalidParamException(String msg, Object data) {
        super(msg);
        this.data = data;
    }

    public InvalidParamException(String msg, Throwable t) {
        super(msg, t);
    }
    
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
