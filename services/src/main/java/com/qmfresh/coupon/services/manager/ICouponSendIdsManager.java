package com.qmfresh.coupon.services.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.services.entity.CouponSendIds;
import com.qmfresh.coupon.interfaces.dto.coupon.ModifyRelationBean;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxc
 * @since 2019-08-02
 */
public interface ICouponSendIdsManager extends IService<CouponSendIds> {

    /**
     * 修改券发放关联的依赖关系
     * @param bean
     * @return
     */
    int modifySendRelationForCouponId(ModifyRelationBean bean);

    /**
     * 修改券发放关联的依赖关系
     * @param bean
     * @return
     */
    int modifySendRelationForSendId(ModifyRelationBean bean);

    /**
     * 软删除修改券发放关联表
     *
     * @param ids
     * @return
     */
    int delCouponIds(List<Integer> ids);
}
