package com.qmfresh.coupon.services.external.service;

import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.PromotionContext;
import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.PromotionProtocol;
import com.qmfresh.coupon.services.common.business.ServiceResult;

/**
 * @program: coupon-service
 * @description:
 * @author: xbb
 * @create: 2019-12-12 18:26
 **/
public interface IPromotionExternalApiService {

    /**
     * 补打优惠券前校验订单信息
     * @param param
     * @return
     */
    PromotionContext executeStrategy(PromotionProtocol param);


    ServiceResult<String> getPromotionDetailNew(Long activityId);
}
