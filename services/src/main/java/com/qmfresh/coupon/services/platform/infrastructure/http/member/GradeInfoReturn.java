/*
 * Copyright (C) 2018-2021 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.coupon.services.platform.infrastructure.http.member;

import com.qmfresh.coupon.interfaces.dto.member.GradeInfoRes;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 * @Date 2020/3/27 0027 20:22
 */
@Data
public class GradeInfoReturn implements Serializable {

    private static final long serialVersionUID = 1623932001880943982L;
    
    private Integer pageIndex;
    
    private Integer pageSize;
    
    private Integer totalCount;
    
    private List<GradeInfoRes> listData;

}
