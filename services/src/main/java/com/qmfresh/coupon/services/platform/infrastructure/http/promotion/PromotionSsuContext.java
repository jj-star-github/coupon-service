package com.qmfresh.coupon.services.platform.infrastructure.http.promotion;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
public class PromotionSsuContext {

    private Long activityId;

    private Integer labelId;

    /**
     * 是否满足促销
     */
    private Boolean isMeet;

    private Integer ssuFormatId;

    private Integer skuId;

    private Integer class2Id;

    private Integer class1Id;

    private String ssuName;

    /**
     * 下单量
     */
    private BigDecimal amount;

    /**
     * 原始金额
     */
    private BigDecimal originPrice;

    /**
     * 会员价
     */
    private BigDecimal vipPrice;

    /**
     * 折后价
     */
    private BigDecimal priceAfterDiscount;

    /**
     * 优惠后金额
     */
    private BigDecimal preferentialPrice;

    /**
     * 促销执行轨迹
     */
    private PromotionTrace promotionTrace;

    /**
     * 商品类型：1.普通商品，2.会员商品，3.改价商品，4.促销商品
     */
    private Integer priceType;

    private List<ActivityModule> activityModules;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getLabelId() {
        return labelId;
    }

    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    public Boolean getMeet() {
        return isMeet;
    }

    public void setMeet(Boolean meet) {
        isMeet = meet;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(BigDecimal originPrice) {
        this.originPrice = originPrice;
    }

    public BigDecimal getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(BigDecimal vipPrice) {
        this.vipPrice = vipPrice;
    }

    public BigDecimal getPriceAfterDiscount() {
        return priceAfterDiscount;
    }

    public void setPriceAfterDiscount(BigDecimal priceAfterDiscount) {
        this.priceAfterDiscount = priceAfterDiscount;
    }

    public BigDecimal getPreferentialPrice() {
        return preferentialPrice;
    }

    public void setPreferentialPrice(BigDecimal preferentialPrice) {
        this.preferentialPrice = preferentialPrice;
    }

    public PromotionTrace getPromotionTrace() {
        return promotionTrace;
    }

    public void setPromotionTrace(PromotionTrace promotionTrace) {
        this.promotionTrace = promotionTrace;
    }

    public Integer getPriceType() {
        return priceType;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }

    public List<ActivityModule> getActivityModules() {
        return activityModules;
    }

    public void setActivityModules(List<ActivityModule> activityModules) {
        this.activityModules = activityModules;
    }
}
