package com.qmfresh.coupon.services.platform.domain.model.entity;

import lombok.Data;

import java.io.Serializable;

/**查询满足条件的优惠券
 * @Description:
 * @Version: V1.0
 */
@Data
public class QueryCouponCount  implements Serializable {
    //优惠券的模板id
    private Integer originalId;
    //优惠券码
    private String  couponCode;
    /**
     *优惠券绑定的订单号
     */
    private String sourceOrderNo;

    //券类型（1：满减、2:折扣、3：随机金额）
    private Integer couponType;

    //发券的开始时间
    private Integer startTime;
    //发券的结束时间
    private Integer endTime;
    //使用状态（0：未使用、1：已使用、2：已过期）
    private Integer status;

    private Integer pageNum = 1;
    private Integer pageSize = 10;
}
