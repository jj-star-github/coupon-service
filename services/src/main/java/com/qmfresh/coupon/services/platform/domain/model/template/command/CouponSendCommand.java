package com.qmfresh.coupon.services.platform.domain.model.template.command;

import lombok.Data;

import java.io.Serializable;

@Data
public class CouponSendCommand implements Serializable {
    private Integer templateId;
    private Integer sendNumber;
    private Integer userId;
    private Boolean checkSendNumber; //是否校验发放数量 false：不需要验证库存和减库存

    public CouponSendCommand(){}

    public CouponSendCommand(Integer templateId,Integer sendNumber,Integer userId, Boolean checkSendNumber){
        this.templateId=templateId;
        this.sendNumber=sendNumber;
        this.userId=userId;
        this.checkSendNumber = checkSendNumber;
    }
}
