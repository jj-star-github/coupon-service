/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/26
 * Description:ICouponApiService.java
 */
package com.qmfresh.coupon.services.service;


import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.*;

import java.util.List;

/**
 * @author lxc
 */
public interface ICouponSendService {

    /**
     * 创建优惠券发放
     *
     * @param bean
     * @return
     */
    Boolean couponSend(CreateCouponSendBean bean);

    /**
     * 优惠券发放审批
     *
     * @param bean
     * @return
     */
    Boolean couponSendApproval(ApprovalCouponSendBean bean);

    /**
     * 查询优惠券发放列表
     *
     * @param bean
     * @return
     */
    ListWithPage<QueryCouponSendReturnBean> queryCouponSendList(QueryCouponSendListBean bean);

    /**
     * 优惠券发放修改
     * @param bean
     * @return
     */
    CouponModifyReturnBean modifyCouponSendInfo(CreateCouponSendBean bean);

    /**
     * 优惠券发放删除
     *
     * @param bean
     * @return
     */
    Boolean delCouponSendActivity(IdParamBean bean);

    /**
     * 优惠券发放活动暂停
     *
     * @param bean
     * @return
     */
    Boolean pauseSendActivity(IdParamBean bean);

    /**
     * 优惠券发放活动恢复暂停
     *
     * @param bean
     * @return
     */
    Boolean recoverPauseSendActivity(IdParamBean bean);

    /**
     * 查询优惠券发放明细
     * @param bean
     * @return
     */
    CouponSendDetailReturnBean querySendDetailInfo(IdParamBean bean);

}
