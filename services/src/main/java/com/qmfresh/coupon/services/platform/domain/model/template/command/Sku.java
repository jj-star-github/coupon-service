package com.qmfresh.coupon.services.platform.domain.model.template.command;

import com.qmfresh.coupon.client.RpcGoodsDTO;
import com.qmfresh.coupon.interfaces.dto.coupon.GoodsInfo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class Sku implements Serializable {
    /**
     * 商品类型，1.sku，2.ssu
     */
    private Integer goodType;
    /**
     * skuId
     */
    private Integer skuId;
    /**
     * ssuId
     */
    private Integer ssuId;
    /**
     * 商品所属一级分类
     */
    private Integer class1Id;
    /**
     * 商品所属二级分类
     */
    private Integer class2Id;
    /**
     * 线上商品所属一级分类
     */
    private Integer saleClass1Id;
    /**
     * 线上商品所属二级分类
     */
    private Integer saleClass2Id;
    /**
     * 商品单价
     */
    private BigDecimal price;
    /**
     * 会场id
     */
    private String meetingId;
    /**
     * 商品数量
     */
    private BigDecimal amount;

    public Sku(){}

    public Sku(GoodsInfo goodsInfo) {
        this.goodType = goodsInfo.getGoodType();
        this.skuId = goodsInfo.getSkuId();
        this.ssuId = goodsInfo.getSsuId();
        this.class1Id = goodsInfo.getClass1Id();
        this.class2Id = goodsInfo.getClass2Id();
        this.saleClass1Id = goodsInfo.getSaleClass1Id();
        this.saleClass2Id = goodsInfo.getSaleClass2Id();
        this.price = goodsInfo.getPrice();
        this.meetingId = goodsInfo.getMeetingId();
        this.amount = goodsInfo.getAmount();
    }

    public GoodsInfo createGoods() {
        GoodsInfo goodsInfo = new GoodsInfo();
        goodsInfo.setAmount(this.getAmount());
        goodsInfo.setClass1Id(this.getClass1Id());
        goodsInfo.setClass2Id(this.getClass2Id());
        goodsInfo.setPrice(this.getPrice());
        goodsInfo.setSkuId(this.getSkuId());
        goodsInfo.setSsuId(this.getSsuId());
        goodsInfo.setMeetingId(this.getMeetingId());
        goodsInfo.setSaleClass1Id(this.getSaleClass1Id());
        goodsInfo.setSaleClass2Id(this.getSaleClass2Id());
        return goodsInfo;
    }

    public Sku(RpcGoodsDTO goodsInfo) {
        this.goodType = goodsInfo.getGoodType();
        this.skuId = goodsInfo.getSkuId();
        this.ssuId = goodsInfo.getSsuId();
        this.class1Id = goodsInfo.getClass1Id();
        this.class2Id = goodsInfo.getClass2Id();
        this.saleClass1Id = goodsInfo.getSaleClass1Id();
        this.saleClass2Id = goodsInfo.getSaleClass2Id();
        this.meetingId = goodsInfo.getMeetingId();
    }
}
