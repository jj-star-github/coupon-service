package com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon;


import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.SendLog;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 发券日志表
 */
public interface SendLogMapper extends BaseMapper<SendLog> {

    /**
     * 根据业务标识查询发券记录
     * @param businessCode
     * @param businessType
     * @return
     */
    SendLog queryByBusiness(@Param("businessCode") String businessCode, @Param("businessType")Integer businessType);

    /**
     *
     * @param sendLog
     * @return
     */
    void insertSendLog(SendLog sendLog);

}