package com.qmfresh.coupon.services.platform.application.coupon.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.aliyun.oss.OSS;
import com.google.common.collect.Lists;
import com.qmfresh.coupon.interfaces.enums.MemberFinalEnum;
import com.qmfresh.coupon.services.common.utils.CouponUtil;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.common.utils.SnowflakeIdWorker;
import com.qmfresh.coupon.services.platform.application.coupon.SendCouponService;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendBySendActivityContext;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendContext;
import com.qmfresh.coupon.services.platform.application.coupon.vo.SendCouponReturnVo;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityCouponManager;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityCouponRule;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityManager;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityRule;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivitySendTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivitySendUserTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityStatusEnums;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.domain.model.coupon.SendLogManager;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.SendCouponCommand;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.SendCouponVO;
import com.qmfresh.coupon.services.platform.domain.model.member.MemberServerManager;
import com.qmfresh.coupon.services.platform.domain.model.template.CouponSendByTemplate;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.model.template.command.CouponSendCommand;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.coupon.services.platform.domain.shared.Operator;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.SendActivityCouponMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.SendActivityMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivity;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivityCoupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.SendLog;
import com.qmfresh.coupon.services.platform.infrastructure.oss.OssUtils;
import com.qmfresh.coupon.services.service.DLock;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SendCouponServiceImpl implements SendCouponService {

    public static final SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(0, 0);
    public static final String BASE_URL = "http://qm-oss.oss-cn-hangzhou.aliyuncs.com/";
    public static final String FILE_URL = "coupon/sendToUser/";
    @Value("${oss.bucket}")
    private String bucket;

    @Resource
    private DLock dLock;
    @Resource
    private SendLogManager sendLogManager;
    @Resource
    private ICouponManager couponManager;
    @Resource
    private ICouponTemplateManager iCouponTemplateManager;
    @Resource
    private SendActivityCouponManager sendActivityCouponManager;
    @Resource
    private SendActivityManager sendActivityManager;
    @Resource
    private MemberServerManager memberServerManager;
    @Resource
    private SendActivityCouponMapper sendActivityCouponMapper;
    @Resource
    private OssUtils ossUtils;

    /**
     * 根据business
     * 1. 根据业务Id和业务类型查询t_send_log 是否已发放
     * 2. 调用券模板发放券接口
     * 3. 获取优惠券码
     * 4. 组装发券上下文
     * 5. 封装返回信息
     * @param sendContext
     * @return
     */
    @Override
    public List<SendCouponReturnVo> sendCoupon(SendContext sendContext) {
        //验证业务是否已发券
        List<SendCouponReturnVo> sendCouponReturnVoList = this.checkBusiness(sendContext.getBusinessCode(), sendContext.getBusinessType());
        if (!CollectionUtils.isEmpty(sendCouponReturnVoList)){
            return sendCouponReturnVoList;
        }
        //验证模板是否存在
        CouponSendCommand sendCommand = new CouponSendCommand(sendContext.getCouponTemplate(), sendContext.getSendNum(), sendContext.getUserId(), true);
        CouponSendByTemplate sendByTemplate = iCouponTemplateManager.couponSendByTemplate(sendCommand);
        if (null == sendByTemplate) {
            log.error("sendCoupon 优惠券模板信息有误：param={}", JSON.toJSONString(sendContext));
            throw new BusinessException("券模板信息有误");
        }
        //生成券码
        List<String> couponCodeList = sendContext.getCouponCode();
        if(CollectionUtils.isEmpty(couponCodeList)) {
            couponCodeList = this.getCouponCode(sendContext.getShopId(), sendByTemplate.getAlreadySendNumber());
        }
        //优惠券落库
        SendCouponCommand couponCommand = new SendCouponCommand(sendContext, Arrays.asList(new SendCouponVO(sendByTemplate, couponCodeList)));
        List<Coupon> couponList = couponManager.newSendCoupon(couponCommand);
        sendCouponReturnVoList = couponList.stream().map(coupon -> {
            return SendCouponReturnVo.create(coupon);
        }).collect(Collectors.toList());
        return sendCouponReturnVoList;
    }

    /**
     * 根据发放活动来发券
     * 1. 验证业务是否已处理
     * 2. 验证活动是否存在
     * 3. 验证活动绑定的券模板是否正常
     * 4. 根据券模板封装优惠券信息
     * 5. 批量入库
     * @param sendBySendActivityContext
     * @return
     */
    @Override
    public List<SendCouponReturnVo> sendCouponBySendActivity(SendBySendActivityContext sendBySendActivityContext) {
        //验证业务是否已发券
        List<SendCouponReturnVo> sendCouponReturnVoList = this.checkBusiness(sendBySendActivityContext.getBusinessCode(), sendBySendActivityContext.getBusinessType());
        if (!CollectionUtils.isEmpty(sendCouponReturnVoList)){
            return sendCouponReturnVoList;
        }

        //获取发券活动
        List<SendActivity> sendActivityList = sendActivityManager.query(sendBySendActivityContext.getActivityType());
        if (CollectionUtils.isEmpty(sendActivityList)) {
            log.error("发券失败，活动信息不存在 param={}", JSON.toJSONString(sendBySendActivityContext));
            return null;
        }
        List<Long> activity = sendActivityList.stream().map(SendActivity::getId).collect(Collectors.toList());
        List<SendActivityCoupon> sendActivityCouponList = sendActivityCouponManager.queryByActivityId(activity);
        if (CollectionUtils.isEmpty(sendActivityCouponList)) {
            log.error("发券失败，活动信息不存在 param={}", JSON.toJSONString(sendBySendActivityContext));
            return null;
        }
        //封装券信息
        List<SendCouponVO> sendCouponVOList = new ArrayList();
        for(SendActivityCoupon activityCoupon : sendActivityCouponList) {
            Integer sendNum = null != sendBySendActivityContext.getSendNum() ? sendBySendActivityContext.getSendNum() : activityCoupon.getSendNum();
            //验证模板是否存在
            CouponSendCommand sendCommand = new CouponSendCommand(activityCoupon.getTemplateId(), sendNum, sendBySendActivityContext.getUserId(), true);
            CouponSendByTemplate sendByTemplate = iCouponTemplateManager.couponSendByTemplate(sendCommand);
            if (null == sendByTemplate || null == sendByTemplate.getAlreadySendNumber() || 0 == sendByTemplate.getAlreadySendNumber()) {
                log.error("发券失败，库存不足 param={}", JSON.toJSONString(sendCommand));
                continue;
            }

            //生成券码
            List<String> couponCodeList = this.getCouponCode(sendBySendActivityContext.getShopId(), sendByTemplate.getAlreadySendNumber());
            sendCouponVOList.add(new SendCouponVO(sendByTemplate, couponCodeList));
        }
        //优惠券落库
        SendCouponCommand couponCommand = new SendCouponCommand(sendBySendActivityContext, sendCouponVOList);
        List<Coupon> couponList = couponManager.newSendCoupon(couponCommand);
        if (CollectionUtils.isEmpty(couponList)) {
            log.error("发券失败，券信息封装有误 param={}", JSON.toJSONString(couponCommand));
            return null;
        }
        sendCouponReturnVoList = couponList.stream().map(coupon -> {
            return SendCouponReturnVo.create(coupon);
        }).collect(Collectors.toList());
        return sendCouponReturnVoList;
    }

    /**
     * 只用于月礼券发放使用， 注意：发放记录不会存入send_log表，防止一个用户多次操作使用t_coupon来验证
     * 1. 根据活动类型查询活动信息
     * 2. 根据活动查询活动绑定的券模板
     * 3. 循环批量获取用户信息
     * 4. 封装发券信息
     *
     * @param memberFinalEnum
     */
    @Override
    public void sendMonth(MemberFinalEnum memberFinalEnum) {
        List<SendActivity> sendActivityList = sendActivityManager.query(memberFinalEnum.getTypeInCoupon());
        if (CollectionUtils.isEmpty(sendActivityList)) {
            log.error("月礼券发放失败，{} 月礼券活动不存在", memberFinalEnum.getDesc());
            return;
        }
        //获取发放券模板
        List<SendActivityCoupon> sendActivityCouponList = sendActivityCouponManager.queryByActivityId(sendActivityList.get(0).getId());
        if(CollectionUtils.isEmpty(sendActivityCouponList)){
            log.error("月礼券发放失败，{} 月礼券活动:{}暂无奖品信息", memberFinalEnum.getDesc(), sendActivityList.get(0).getId());
            return;
        }
        this.sendCouponByMember(memberFinalEnum.getTypeInMember(),sendActivityCouponList,true);
    }

    private int sendCouponByMember(Integer memberType,List<SendActivityCoupon> sendActivityCouponList,Boolean isMonth){
        int pageSize = 400;
        int pageNum = 1;
        int userNum = 0;
        int lastSize = 0;
        boolean notIsEnd = true;
        while (notIsEnd) {
            // 组装用户map
            List<Integer> userIdList = memberServerManager.getUserIdList(memberType, pageNum, pageSize);
            if (CollectionUtils.isEmpty(userIdList)) {
                return userNum;
            }
            notIsEnd = userIdList.size() >= pageSize;
            lastSize = notIsEnd ? pageSize : userIdList.size();
            List<Coupon> couponList = this.getCouponInfoList(userIdList, sendActivityCouponList,isMonth);
            // 保存入库
            if (!CollectionUtils.isEmpty(couponList)) {
                couponManager.batchInsert(couponList);
            }
            pageNum++;
        }
        userNum = (pageNum - 2) * pageSize + lastSize;
        return userNum;
    }

    /**
     * 用户ID集合  优惠券模板集合
     * 1. 根据用户查询用于已经发的券信息
     * @param userIdList
     * @param sendActivityCouponList
     * @return
     */
    private List<Coupon> getCouponInfoList(List<Integer> userIdList, List<SendActivityCoupon> sendActivityCouponList,Boolean isMonth) {
        List<Coupon> returnCoupon = new ArrayList<>();
        List<Coupon> couponList = couponManager.queryByUserAndTemplate(userIdList, sendActivityCouponList.stream().map(SendActivityCoupon::getTemplateId).collect(Collectors.toList()));
        Map<Integer, List<Integer>> userAcMap = this.getUserAcMap(couponList);
        for (SendActivityCoupon sendActivityCoupon : sendActivityCouponList) {
            //查询模板信息
            CouponSendByTemplate couponSendByTemplate = iCouponTemplateManager.couponSendByTemplate(new CouponSendCommand(sendActivityCoupon.getTemplateId(), sendActivityCoupon.getSendNum(), null, false));
            if(null == couponSendByTemplate) {
                continue;
            }
            for(Integer userId : userIdList) {
                if (userAcMap.containsKey(userId) && userAcMap.get(userId).contains(sendActivityCoupon.getTemplateId()) && isMonth) {
                    continue;
                }
                //生成券码
                List<String> couponCodeList = this.getCouponCode(null, sendActivityCoupon.getSendNum());
                returnCoupon.addAll(new SendCouponVO(couponSendByTemplate, couponCodeList).createCoupon(0, "", userId, null));
            }
        }
        return returnCoupon;
    }

    private Map<Integer, List<Integer>> getUserAcMap(List<Coupon> couponList) {
        Map<Integer, List<Integer>> userAcMap = new HashMap<>();
        if (CollectionUtils.isEmpty(couponList)) {
            return userAcMap;
        }
        couponList.forEach(coupon -> {
            Integer userId = coupon.getUserId().intValue();
            if (userAcMap.containsKey(userId)) {
                List<Integer> activityIdList = userAcMap.get(userId);
                activityIdList.add(coupon.getOriginalId());
                userAcMap.put(userId, activityIdList);
            } else {
                userAcMap.put(userId, Lists.newArrayList(coupon.getOriginalId()));
            }
        });
        return userAcMap;
    }

    @Override
    public void hmSendByType(SendCouponDto sendCouponDto) {
        //查询模板信息
        CouponSendByTemplate couponSendByTemplate = iCouponTemplateManager.couponSendByTemplate(new CouponSendCommand(sendCouponDto.getCouponId(), sendCouponDto.getSendNum(), null, false));
        if(null == couponSendByTemplate) {
            return;
        }
        // 保存入库
        if (!CollectionUtils.isEmpty(sendCouponDto.getUserIdList())) {
            List<Coupon> couponList = new ArrayList<>();
            for (Integer userId : sendCouponDto.getUserIdList()) {
                List<String> couponCodeList = this.getCouponCode(null, sendCouponDto.getSendNum());
                couponList.addAll(new SendCouponVO(couponSendByTemplate, couponCodeList).createCoupon(0, "", userId, null));
                if(500 <=  couponList.size()) {
                    couponManager.batchInsert(couponList);
                    couponList = new ArrayList<>();
                }
            }
            if (!CollectionUtils.isEmpty(couponList)) {
                couponManager.batchInsert(couponList);
            }
        }
    }

    /**
     * 定向发券
     * 1.根据send_status查询t_send_activity待发放的定向活动list
     * 2.循环list，根据activity_type，区分实时定时发放
     * 3.实时直接发放，定时判断是否在当前时间前五分钟内再发放
     * 4.开始发放，send_status置为发放中
     * 5.全会员根据金红青会员分批发放
     * 6.特定发放，获取并解析阿里云文件分批发放
     * 7.发放完毕send_status置为已发放
     * @return
     */
    @Override
    public void sendToUser() {
        List<SendActivity> sendActivityList = sendActivityManager.queryBySendType(ActivitySendTypeEnums.APPOINT.getCode(),ActivityStatusEnums.CREATE.getCode());
        log.info("定向发放-获取活动" + JSON.toJSONString(sendActivityList));
        if(CollectionUtils.isEmpty(sendActivityList)){
            return;
        }
        List<Long> idList = sendActivityList.stream().map(SendActivity::getId).collect(Collectors.toList());
        List<SendActivityCoupon> activityCouponList = sendActivityCouponMapper.queryByActivityIdList(idList);
        log.info("定向发放-获取活动绑定的券" + JSON.toJSONString(activityCouponList));
        if(CollectionUtils.isEmpty(activityCouponList)){
            return;
        }
        Map<Long, List<SendActivityCoupon>> sendActivityCouponMap = activityCouponList.stream().collect(Collectors.groupingBy(SendActivityCoupon::getActivityId));
        for(SendActivity sendActivity : sendActivityList){
            List<SendActivityCoupon> activityCoupons = sendActivityCouponMap.get(sendActivity.getId());
            if(CollectionUtils.isEmpty(activityCoupons)){
                log.warn("定向发放-获取活动绑定的券为空");
                continue;
            }
            SendActivityRule sendRule = JSON.parseObject(sendActivity.getSendRule(),new TypeReference<SendActivityRule>(){});
            if(sendRule == null){
                log.warn("定向发放-活动规则异常:" + JSON.toJSONString(sendActivity));
                continue;
            }
            if(! this.isInTime(sendRule) && sendActivity.getActivityType().equals(ActivityTypeEnums.APPOINT_TIME.getCode())){
                log.info("定向发放-未到发放时间:" + JSON.toJSONString(sendActivity));
                continue;
            }
            //开始发放
            this.sendToUserAction(sendActivity.getId(),sendRule,activityCoupons);

        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void sendToUserAction(Long id,SendActivityRule sendRule,List<SendActivityCoupon> activityCoupons) {
        //更新活动状态为发放中
        sendActivityManager.updateStatus(id,ActivityStatusEnums.RUNNING.getCode(),Operator.createSystemOperator());
        int sendUserNum = 0;
        // 发放
        if (sendRule.getSendUseType().equals(ActivitySendUserTypeEnums.ALLUSER.getCode())) {
            //全会员
            int goldNum = this.sendCouponByMember(MemberFinalEnum.GOLD_MEMBER.getTypeInMember(),activityCoupons,false);
            log.info("定向发放-发放金番茄结束，数量：" + goldNum);
            int redNum = this.sendCouponByMember(MemberFinalEnum.RED_MEMBER.getTypeInMember(),activityCoupons,false);
            log.info("定向发放-发放红番茄结束，数量：" + redNum);
            int greenNum = this.sendCouponByMember(MemberFinalEnum.GREEN_MEMBER.getTypeInMember(),activityCoupons,false);
            log.info("定向发放-发放青番茄结束，数量：" + greenNum);
            sendUserNum = goldNum + redNum + greenNum;
            for (SendActivityCoupon sendActivityCoupon : activityCoupons){
                int sendNum = sendActivityCoupon.getSendNum() * sendUserNum;
                SendActivityCouponRule sendActivityCouponRule = new SendActivityCouponRule();
                sendActivityCouponRule.setWillSendCouponNum(sendNum);
                sendActivityCouponRule.setWillSendUserNum(sendUserNum);
                sendActivityCouponManager.updateSendRule(sendActivityCoupon.getId(),JSON.toJSONString(sendActivityCouponRule),id);
            }
        }else if(sendRule.getSendUseType().equals(ActivitySendUserTypeEnums.SOMEUSER.getCode())){
            //导入的会员
            if(sendRule.getFileName() == null){
                log.error("定向发放-活动未上传文件，sendActivityId:{}",id);
                throw new BusinessException("定向发放-活动未上传文件");
            }
            this.sendToSomeBody(sendRule.getFileName(),activityCoupons);
        }
        //更新活动状态为已发放
        sendActivityManager.updateFinishedStatus(id,ActivityStatusEnums.FINISHED.getCode(),Operator.createSystemOperator());

    }

    private void sendToSomeBody(String fileName,List<SendActivityCoupon> activityCoupons){
        log.info("定向发放-sendToSomeBody");
        List<Integer> userIdList = this.getFileData(fileName);
        if(CollectionUtils.isEmpty(userIdList)){
            log.error("定向发放-该文件下无会员:" +BASE_URL + FILE_URL + fileName);
            throw new BusinessException("定向发放-该文件下无会员");
        }
        int pageSize = 400;
        //分批发放
        log.info("定向发放-开始发放Excel会员,数量:" + userIdList.size());
        for (int i = 0; i < userIdList.size(); i+=pageSize) {
            List<Integer> subUserIdList = new ArrayList<>();
            int toIndex = i + pageSize > userIdList.size() ? userIdList.size() : i+pageSize;
            subUserIdList.addAll(userIdList.subList(i, toIndex));
            List<Coupon> couponList = this.getCouponInfoList(subUserIdList, activityCoupons,false);
            // 保存入库
            if (!CollectionUtils.isEmpty(couponList)) {
                couponManager.batchInsert(couponList);
            }
        }
    }

    @Override
    public List<String> getCouponCode(Integer shopId, Integer sendNum) {
        List<String> couponCode = new ArrayList<>();
        for (int i = 0; i < sendNum; i++) {
            String stringId = String.valueOf(snowflakeIdWorker.nextId());
            String shopIdStr = "";
            if(null == shopId){
                shopIdStr = CouponUtil.getNumSequence(3);
            }else {
                shopIdStr = shopId.toString();
                if (shopIdStr.length() == 1) {
                    shopIdStr = "00"+shopIdStr;
                } else if (shopIdStr.length() == 2) {
                    shopIdStr = "0"+shopIdStr;
                }
            }
            couponCode.add(shopIdStr + stringId);
        }

        return couponCode;
    }

    private List<SendCouponReturnVo> checkBusiness(String businessCode, Integer businessType) {
        //RedisLock
        boolean lock = dLock.tryLock(DLock.sendCouponLock(businessCode, businessType), 3* 1000L);
        if (!lock) {
            throw new BusinessException("正在发券中请勿重复操作");
        }
        //验证业务是否已发券
        List<Integer> couponIdList = this.getSendLog(businessCode, businessType);
        if (CollectionUtils.isEmpty(couponIdList)){
            return null;
        }
        List<Coupon> couponList = couponManager.queryById(couponIdList);
        return couponList.stream().map(coupon -> {return SendCouponReturnVo.create(coupon);}).collect(Collectors.toList());
    }

    private List<Integer> getSendLog(String businessCode, Integer businessType) {
        SendLog sendLog = sendLogManager.queryByBusiness(businessCode, businessType);
        if (null == sendLog || StringUtils.isEmpty(sendLog.getCouponIds())) {
            return null;
        }
        String[] idList = sendLog.getCouponIds().split(",");
        List<Integer> resultList = new ArrayList<>(idList.length);
        for (String id : idList){ resultList.add(Integer.valueOf(id));}
        return resultList;
    }

    public boolean isInTime(SendActivityRule sendRule){
        if(sendRule.getSendTime() == null){
            return false;
        }
        try {
            //发放时间
            Date sendTime = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss").parse(sendRule.getSendTime());
            //当前五分钟
            Calendar beforeTime = Calendar.getInstance();
            beforeTime.add(Calendar.MINUTE, -5);
            Date beforeDate = beforeTime.getTime();
            return DateUtil.isInTheTime(sendTime,beforeDate,new Date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Integer> getFileData(String fileName) {
        log.info("定向发放-getFileData:" + fileName);
        OSS ossClient = ossUtils.create();

        InputStream inputStream = ossClient.getObject(bucket, FILE_URL + fileName).getObjectContent();
        XSSFWorkbook wb = null;
        try {
            log.info("1111");
            wb = new XSSFWorkbook(inputStream);
        } catch (Exception e) {
            log.error("=================", LogExceptionStackTrace.errorStackTrace(e));
            throw new BusinessException("文件解析失败");
        }

        log.info("2222");
        XSSFSheet sheet = wb.getSheetAt(0);
        log.info("333");
        int rowNo = sheet.getLastRowNum();
        List<Integer> userIdList = new ArrayList<>();
        for (int i = 1; i <= rowNo; i++) {
            XSSFRow row = sheet.getRow(i);
            XSSFCell cell = row.getCell(0);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            String cellValue = cell.getStringCellValue();
            userIdList.add(Integer.valueOf(cellValue));
        }
        return userIdList;
    }
}
