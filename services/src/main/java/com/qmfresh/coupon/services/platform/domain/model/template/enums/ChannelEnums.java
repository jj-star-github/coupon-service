package com.qmfresh.coupon.services.platform.domain.model.template.enums;

import lombok.Getter;

@Getter
public enum ChannelEnums {

    ALL(0, "全渠道"),
    COUPON_DISCOUNT(1, "线下"),
    COUPON_RANDOM_REDUCE(2, "线上");

    private Integer code;

    private String remark;

    ChannelEnums(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
