package com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.coupon.client.template.dto.TemplateQueryByActResultDTO;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityCouponRule;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityEffectVO;
import com.qmfresh.coupon.services.platform.domain.model.template.*;
import com.qmfresh.coupon.services.platform.domain.model.template.command.CouponTemplateCreateCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetGoodsCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetShopsCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateConditionTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateStatusTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateInfoVo;
import com.qmfresh.coupon.services.platform.domain.shared.BaseUtil;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivityCoupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.CouponMapper;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Resource;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 券模板
 */
@TableName("t_coupon_template")
@Data
public class CouponTemplate implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 券名称
     */
    @TableField("coupon_name")
    private String couponName;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    @TableField("coupon_condition")
    private Integer couponCondition;
    /**
     * 订单所需金额
     */
    @TableField("need_money")
    private Integer needMoney;
    /**
     * 券类型
     */
    @TableField("coupon_type")
    private Integer couponType;
    /**
     * 券规则
     */
    @TableField("coupon_rule")
    private String couponRule;
    /**
     * 使用时间类型
     */
    @TableField("use_time_type")
    private Integer useTimeType;
    /**
     * 使用时间规则
     */
    @TableField("use_time_rule")
    private String useTimeRule;
    /**
     * 适用商品类型，1全部，2指定可用，3指定不可用，4会场
     */
    @TableField("applicable_goods_type")
    private Integer applicableGoodsType;
    /**
     * 券适用商品规则
     */
    @TableField("applicable_goods_rule")
    private String applicableGoodsRule;
    /**
     * 券使用说明
     */
    @TableField("use_instruction")
    private String useInstruction;
    /**
     * 状态
     */
    @TableField("status")
    private Integer status;
    /**
     * 创建人id
     */
    @TableField("created_id")
    private Integer createdId;
    /**
     * 创建人名称
     */
    @TableField("created_name")
    private String createdName;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 有效性
     */
    @TableField("is_deleted")
    private Integer isDeleted;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    @TableField("limit_channel")
    private Integer limitChannel;
    /**
     * 适用门店类型
     */
    @TableField("applicable_shop_type")
    private Integer applicableShopType;
    /**
     * 券模板剩余数量
     */
    @TableField("coupon_number")
    private Integer couponNumber;
    /**
     * 券模板绑定关系
     */
    @TableField("bing_template")
    private Integer bingTemplate;

    public CouponTemplate(){}

    public CouponTemplate(CouponTemplateCreateCommand command){
        //拼接商品说明
        this.setUseInstruction(command.getUseInstruction());
        if(!command.getApplicableGoodsType().equals(1)){
            String useInstruction[] = command.getUseInstruction().split("，");
            if(command.getApplicableGoodsType().equals(8)){
                this.setUseInstruction("会场商品可用，"+useInstruction[1]);
            }else {
                if(CollectionUtils.isNotEmpty(command.getGoodsList())){
                    List<String> goodsSplitList = new ArrayList<>();
                    List<String> goodsNameList = command.getGoodsList().stream().map(TemplateTargetGoodsCommand::getGoodsName).collect(Collectors.toList());
                    String et = "商品可用，";
                    if(goodsNameList.size()<=3){
                        goodsSplitList.addAll(goodsNameList);
                    }else {
                        goodsSplitList.addAll(goodsNameList.subList(0,3));
                        et = "等商品可用，";
                    }
                    String result=  goodsSplitList.stream().collect(Collectors.joining("、"));
                    this.setUseInstruction(result+et+useInstruction[1]);
                }
            }
        }
        String shopInstruction = "";
        if(command.getApplicableShopType().equals(1)){
            shopInstruction = "全部门店可用";
        }else if(command.getApplicableShopType().equals(2)){
            if(CollectionUtils.isNotEmpty(command.getShopList())){
                shopInstruction = buildShopInstruction(command.getShopList())+"可用";
            }
        }else if(command.getApplicableShopType().equals(3)){
            if(CollectionUtils.isNotEmpty(command.getShopList())){
                shopInstruction = "除"+buildShopInstruction(command.getShopList())+"以外可用";
            }
        }
        command.getConditionRule().setShopInstruction(shopInstruction);
        this.setCouponName(command.getActivityName());
        this.setCouponCondition(command.getUseCondition());
        if(command.getUseCondition().equals(TemplateConditionTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())){
            this.setNeedMoney(command.getNeedMoney());
        }
        this.setCouponType(command.getCouponType());
        this.setCouponRule(JSON.toJSONString(command.getConditionRule()));
        this.setUseTimeType(command.getUseTimeType());
        this.setUseTimeRule(JSON.toJSONString(command.getTimeRule()));
        this.setApplicableGoodsType(command.getApplicableGoodsType());
        this.setStatus(TemplateStatusTypeEnums.IN_USE_STATUS.getCode());
        this.setCreatedId(command.getCreateId());
        this.setCreatedName(command.getCreateName());
        this.setGmtCreate(new Date());
        this.setGmtModified(new Date());
        this.setIsDeleted(0);
        this.setLimitChannel(command.getLimitChannel());
        this.setApplicableShopType(command.getApplicableShopType());
        this.setCouponNumber(command.getConditionRule().getCouponMaxSendNum());
    }

    public TemplateInfoVo createTemplateInfoVo(){
        TemplateInfoVo templateInfoVo = new TemplateInfoVo();
        templateInfoVo.setId(this.getId());
        templateInfoVo.setCouponName(this.getCouponName());
        templateInfoVo.setCouponCondition(this.getCouponCondition());
        templateInfoVo.setNeedMoney(this.getNeedMoney());
        templateInfoVo.setCouponType(this.getCouponType());
        templateInfoVo.setUseInstruction(this.getUseInstruction());
        TemplateConditionRule couponRule = JSON.parseObject(this.getCouponRule(),new TypeReference<TemplateConditionRule>(){});
        if(couponRule.getShopInstruction() != null){
            templateInfoVo.setShopInstruction(couponRule.getShopInstruction());
        }
        if(couponRule.getCouponMaxSendNum() != null){
            templateInfoVo.setCouponMaxSendNum(couponRule.getCouponMaxSendNum());
        }
        if(couponRule.getUseReceiveNum() != null){
            templateInfoVo.setUseReceiveNum(couponRule.getUseReceiveNum());
        }
        TemplateTimeRule timeRule = JSON.parseObject(this.getUseTimeRule(),new TypeReference<TemplateTimeRule>(){});
        templateInfoVo.setUseTime(CouponTemplateInfo.buildTime(this.getUseTimeType(),timeRule)+","+CouponTemplateInfo.buildHms(timeRule));
        templateInfoVo.setLimitChannel(this.getLimitChannel());
        templateInfoVo.setApplicableGoodsType(this.getApplicableGoodsType());
        templateInfoVo.setApplicableShopType(this.getApplicableShopType());
        templateInfoVo.setCreatedName(this.getCreatedName());
        templateInfoVo.setGmtCreate(this.getGmtCreate());
        templateInfoVo.setStatus(this.getStatus());
        return templateInfoVo;
    }

    public String buildShopInstruction(List<TemplateTargetShopsCommand> shopList){
        List<String> shopNameList = shopList.stream().map(TemplateTargetShopsCommand::getShopName).collect(Collectors.toList());
        List<String> shopSplitList = new ArrayList<>();
        if(shopNameList.size()<=3){
            shopSplitList.addAll(shopNameList);
        }else {
            shopSplitList.addAll(shopNameList.subList(0,3));
        }
        return shopSplitList.stream().collect(Collectors.joining("、"));
    }

    public static TemplateQueryByActResultDTO creatByActResult(CouponTemplate couponTemplate,Integer useMaxReceiveNum,Integer useCanReceiveNum){

        TemplateQueryByActResultDTO resultDTO = new TemplateQueryByActResultDTO();
        resultDTO.setCouponCondition(couponTemplate.getCouponCondition());
        resultDTO.setCouponType(couponTemplate.getCouponType());
        resultDTO.setNeedMoney(couponTemplate.getNeedMoney());
        resultDTO.setTemplateId(couponTemplate.getId());
        resultDTO.setUseInstruction(couponTemplate.getUseInstruction());
        JSONObject couponRule = JSONObject.parseObject(couponTemplate.getCouponRule());
        resultDTO.setConditionRule((com.qmfresh.coupon.client.template.dto.TemplateConditionRule) JSONObject.toJavaObject(couponRule, com.qmfresh.coupon.client.template.dto.TemplateConditionRule.class));
        TemplateTimeRule timeRule = JSON.parseObject(couponTemplate.getUseTimeRule(),new TypeReference<TemplateTimeRule>(){});
        resultDTO.setUseTime(CouponTemplateInfo.buildTime(couponTemplate.getUseTimeType(),timeRule)+","+CouponTemplateInfo.buildHms(timeRule));
        resultDTO.setCouponName(couponTemplate.getCouponName());
        resultDTO.setCouponNumber(couponTemplate.getCouponNumber());
        resultDTO.setUseCanReceiveNum(useCanReceiveNum);
        resultDTO.setUseMaxReceiveNum(useMaxReceiveNum);
        return resultDTO;
    }

    public static SendActivityEffectVO createEffectVo(SendActivityCoupon sendActivityCoupon,CouponTemplate couponTemplate,
                                               int sendCouponNum,int sendUserNum,int useCouponNum,int useUserNum){
        SendActivityEffectVO sendActivityEffectVO = new SendActivityEffectVO();
        sendActivityEffectVO.setTemplateId(couponTemplate.getId());
        sendActivityEffectVO.setTemplateName(couponTemplate.getCouponName());
        sendActivityEffectVO.setLimitChannel(couponTemplate.getLimitChannel());
        sendActivityEffectVO.setCouponType(couponTemplate.getCouponType());
        sendActivityEffectVO.setUseInstruction(couponTemplate.getUseInstruction());
        sendActivityEffectVO.setCouponTime(CouponTemplateInfo.buildTime(couponTemplate.getUseTimeType(),JSON.parseObject(couponTemplate.getUseTimeRule(),new TypeReference<TemplateTimeRule>(){})));
        sendActivityEffectVO.setCouponHms(CouponTemplateInfo.buildHms(JSON.parseObject(couponTemplate.getUseTimeRule(),new TypeReference<TemplateTimeRule>(){})));
        String useInstruction[] = couponTemplate.getUseInstruction().split("，");
        sendActivityEffectVO.setApplicableGoods(useInstruction[0]);
        TemplateConditionRule couponRule = JSON.parseObject(couponTemplate.getCouponRule(),new TypeReference<TemplateConditionRule>(){});
        sendActivityEffectVO.setApplicableShop(couponRule.getShopInstruction());
        SendActivityCouponRule sendActivityCouponRule = JSON.parseObject(sendActivityCoupon.getSendRule(),new TypeReference<SendActivityCouponRule>(){});
        Integer willSendCouponNum = sendActivityCouponRule == null || sendActivityCouponRule.getWillSendCouponNum() == null ? 0 : sendActivityCouponRule.getWillSendCouponNum();
        sendActivityEffectVO.setWillSendCouponNum(willSendCouponNum);
        Integer willSendUserNum = sendActivityCouponRule == null || sendActivityCouponRule.getWillSendUserNum() == null ? 0 : sendActivityCouponRule.getWillSendUserNum();
        sendActivityEffectVO.setWillSendUserNum(willSendUserNum);
        sendActivityEffectVO.setSendCouponNum(sendCouponNum);
        sendActivityEffectVO.setSendUserNum(sendUserNum);
        sendActivityEffectVO.setUseCouponNum(useCouponNum);
        sendActivityEffectVO.setUseUserNum(useUserNum);
        BigDecimal couponConversion = willSendCouponNum == 0 ? BigDecimal.ZERO : new BigDecimal(useCouponNum).divide(new BigDecimal(willSendCouponNum),4,BigDecimal.ROUND_HALF_UP) ;
        BigDecimal userConversion = willSendUserNum == 0 ? BigDecimal.ZERO : new BigDecimal(useUserNum).divide(new BigDecimal(willSendUserNum),4,BigDecimal.ROUND_HALF_UP) ;
        sendActivityEffectVO.setCouponConversion(couponConversion.multiply(new BigDecimal("100")).setScale(2, RoundingMode.HALF_UP));
        sendActivityEffectVO.setUserConversion(userConversion.multiply(new BigDecimal("100")).setScale(2,RoundingMode.HALF_UP));
        return sendActivityEffectVO;
    }
}
