package com.qmfresh.coupon.services.platform.domain.model.template.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.client.template.dto.TemplateBingActResultDTO;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityGoodsRule;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityCouponCacheKey;
import com.qmfresh.coupon.services.platform.domain.model.template.*;
import com.qmfresh.coupon.services.platform.domain.model.template.command.*;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.*;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateInfoVo;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.infrastructure.dubbo.ShopAreaService;
import com.qmfresh.coupon.services.platform.infrastructure.dubbo.dto.ShopAreaDTO;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.CouponMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.CouponTemplateMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.CouponTemplateTargetGoodsMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetGoods;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetShop;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CouponTemplateManagerImpl extends ServiceImpl<CouponTemplateMapper, CouponTemplate> implements ICouponTemplateManager {

    //库存没有上限
    private static final Integer NO_LIMIT = 999999;
    @Resource
    private CouponMapper couponMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private ICouponTemplateTargetGoodsManager targetGoodsManager;
    @Resource
    private ICouponTemplateTargetShopManager targetShopManager;
    @Resource
    private ShopAreaService shopAreaService;
    @Resource
    private CouponTemplateTargetGoodsMapper goodsMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createCouponTemplate(CouponTemplateCreateCommand command) {
        //创建券模板
        CouponTemplate couponTemplate = new CouponTemplate(command);
        super.save(couponTemplate);
        //非全品类型绑定目标品
        if (!command.getApplicableGoodsType().equals(TemplateTargetGoodsTypeEnums.ALL_GOODS.getCode())) {
            if (!command.getApplicableGoodsType().equals(TemplateTargetGoodsTypeEnums.MEETING_GOODS.getCode())) {
                if (CollectionUtils.isEmpty(command.getGoodsList())) {
                    throw new BusinessException("请选择对应的品！");
                }
                targetGoodsManager.insertBatch(couponTemplate.getId(), command.getGoodsList());
            }
        }
        //非全店绑定模板店
        if (!command.getApplicableShopType().equals(TemplateShopTypeEnums.ALL_SHOP.getCode())) {
            if (CollectionUtils.isEmpty(command.getShopList())) {
                throw new BusinessException("请选择对应的店！");
            }
            targetShopManager.insertBatch(couponTemplate.getId(), command.getShopList());
        }
        cleanActivityCache(couponTemplate.getId());
    }

    @Override
    public CouponTemplateInfo queryCouponTemplateById(Integer templateId) {
        CouponTemplate couponTemplate = selectById(templateId);
        List<CouponTemplateTargetGoods> goodsList = targetGoodsManager.queryByTemplateId(templateId);
        List<CouponTemplateTargetShop> shopList = targetShopManager.queryByTemplateId(templateId);
        CouponTemplateInfo couponTemplateInfo = new CouponTemplateInfo(couponTemplate, goodsList, shopList);
        return couponTemplateInfo;
    }

    @Override

    public CouponSendByTemplate couponSendByTemplate(CouponSendCommand sendCommand) {
        CouponTemplate couponTemplate = selectById(sendCommand.getTemplateId());
        if (couponTemplate == null) {
            throw new BusinessException("没有该优惠券信息");
        }
        //userId不是必传
        int alreadySendNumber = 0;
        if (sendCommand.getUserId() != null && sendCommand.getUserId() > 0) {
            alreadySendNumber = couponMapper.countByUseId(sendCommand.getUserId(), sendCommand.getTemplateId());
        }
        CouponSendByTemplate couponSendByTemplate = new CouponSendByTemplate(couponTemplate,alreadySendNumber,sendCommand.getSendNumber(),sendCommand.getCheckSendNumber());
        if(couponSendByTemplate.getAlreadySendNumber()==0){
            log.info("封装券信息 ,id={} sendNumber={} ",sendCommand.getUserId(),sendCommand.getSendNumber());
            throw new BusinessException("用户超过限领张数！");
        }
        if(!(NO_LIMIT.compareTo(couponTemplate.getCouponNumber()) == 0) || sendCommand.getCheckSendNumber()){
            if(couponTemplate.getCouponNumber().compareTo(couponSendByTemplate.getAlreadySendNumber()) > 0){
                updateNumber(couponTemplate.getId(),couponSendByTemplate.getAlreadySendNumber());
            }else {
                couponSendByTemplate.setAlreadySendNumber(couponTemplate.getCouponNumber());
                couponSendByTemplate.setNotSendNumber(0);
                updateNumber(couponTemplate.getId(),Math.abs(couponTemplate.getCouponNumber()));
            }
        }
        return couponSendByTemplate;
    }

    @Override
    public Page<TemplateInfoVo> queryByCondition(CouponTemplateQueryCommand queryCommand) {
        Page<TemplateInfoVo> templateInfoVoPage = new Page<>();
        Integer totalCount = baseMapper.queryCount(queryCommand.getStatus(), queryCommand.getCouponType(),
                queryCommand.getLimitChannel(), queryCommand.getGoodsType(), queryCommand.getShopType(),
                queryCommand.getCouponName(), queryCommand.getBingTemplate());
        if (null == totalCount || 0 >= totalCount) {
            return templateInfoVoPage;
        }
        List<CouponTemplate> couponTemplateList = baseMapper.queryByCondition(queryCommand.getStatus(), queryCommand.getCouponType(),
                queryCommand.getLimitChannel(), queryCommand.getGoodsType(), queryCommand.getShopType(),
                queryCommand.getCouponName(), queryCommand.getBingTemplate(), queryCommand.getStart(), queryCommand.getPageSize());
        List<TemplateInfoVo> templateInfoVoList = couponTemplateList.stream().map(CouponTemplate::createTemplateInfoVo).collect(Collectors.toList());
        templateInfoVoPage.setTotalCount(totalCount.longValue());
        templateInfoVoPage.setRecords(templateInfoVoList);
        return templateInfoVoPage;
    }

    @Override
    public TemplateVerifyReturn verifyTemplate(TemplateVerifyCommand verifyCommand) {
        if (CollectionUtils.isEmpty(verifyCommand.getSkuList())) {
            log.warn("券模板验证-适用商品，couponSkuList={} sku={}", verifyCommand.getSkuList(), verifyCommand.getSkuList());
            return new TemplateVerifyReturn("适用商品总金额不足，无法用券");
        }
        Integer templateId = verifyCommand.getTemplateId();
        //查询券模板信息
        CouponTemplate couponTemplate = selectById(verifyCommand.getTemplateId());
        if (couponTemplate == null) {
            return new TemplateVerifyReturn("没有该优惠券信息");
        }
        //验证渠道
        if (!verifyCommand.getLimitChannel().equals(couponTemplate.getLimitChannel()) && couponTemplate.getLimitChannel() != 0) {
            log.warn("券模板验证-渠道不符，couponTemplate={} verifyCommand={}", couponTemplate, verifyCommand);
            return new TemplateVerifyReturn("优惠券使用渠道不符");
        }
        //验证小时段，日期段在coupon验证
        TemplateTimeRule timeRule = JSON.parseObject(couponTemplate.getUseTimeRule(), new TypeReference<TemplateTimeRule>() {
        });
        boolean availableResult = CouponTemplateInfo.inAvailableRuleTime(timeRule);
        if (!availableResult) {
            log.warn("券模板验证-时间不符，couponTemplate={} now={}", couponTemplate, new Date());
            return new TemplateVerifyReturn("优惠券不在有效时间段内,无法使用");
        }
        //验证门店
        if (!couponTemplate.getApplicableShopType().equals(TemplateShopTypeEnums.ALL_SHOP.getCode()) && null != verifyCommand.getShopId()) {
            boolean verifyShopResult = verifyShop(verifyCommand.getShopId(), couponTemplate.getApplicableShopType(), templateId);
            if (!verifyShopResult) {
                log.warn("券模板验证-门店不符，couponTemplate={} shopId={}", couponTemplate, verifyCommand.getShopId());
                return new TemplateVerifyReturn("该门店不在适用门店内");
            }
        }
        //验证适用品
        List<Sku> goodsInfoList = buildGoodsInfo(verifyCommand.getSkuList(),couponTemplate.getId(),couponTemplate.getApplicableGoodsType());
        //验证门槛
        boolean verifyGoodsResult = verifyGoods(goodsInfoList, couponTemplate);
        if (!verifyGoodsResult) {
            log.warn("券模板验证-适用商品，couponSkuList={} sku={}", verifyCommand.getSkuList(), goodsInfoList);
            return new TemplateVerifyReturn("适用商品总金额不足，无法用券");
        }
        return new TemplateVerifyReturn(goodsInfoList, couponTemplate);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public TemplateBingActResultDTO bingTemplate(String actId, String actName, List<Integer> templateIds) {
        TemplateBingActResultDTO resultDTO = new TemplateBingActResultDTO();
        List<CouponTemplateTargetGoods> goodsList = new ArrayList<>();
        List<CouponTemplate> couponTemplateList = baseMapper.queryUnBing();
        List<Integer> IdList = couponTemplateList.stream().map(CouponTemplate::getId).collect(Collectors.toList());
        for (Integer templateId : templateIds) {
            if (!IdList.contains(templateId)) {
                resultDTO.setSuccessFlag(false);
                resultDTO.setReason(templateId + "不在可绑定券模板内");
                return resultDTO;
            }
            CouponTemplateTargetGoods targetGoods = new CouponTemplateTargetGoods(templateId,actId,actName,TemplateTargetGoodsTypeEnums.MEETING_GOODS.getCode());
            goodsList.add(targetGoods);
        }
        baseMapper.updateBatch(templateIds, new Date());
        targetGoodsManager.saveBatch(goodsList);
        resultDTO.setSuccessFlag(true);
        return resultDTO;
    }

    @Override
    public List<CouponTemplate> queryByIdList(List<Integer> idList,Integer status) {
        return baseMapper.queryByIdList(idList,status);
    }

    @Override
    public List<CouponTemplate> queryByAct(String actId) {
        List<CouponTemplate> couponTemplateList = new ArrayList<>();
        List<CouponTemplateTargetGoods> targetGoodsList = goodsMapper.queryByTemplateId(null,actId);
        if(CollectionUtils.isEmpty(targetGoodsList)){
            return couponTemplateList;
        }
        List<Integer> templateIdList = targetGoodsList.stream().map(CouponTemplateTargetGoods::getTemplateId).collect(Collectors.toList());
    return queryByIdList(templateIdList, TemplateStatusTypeEnums.IN_USE_STATUS.getCode());
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateNumber(Integer id, Integer sendNumber) {
        if (sendNumber.compareTo(0) <= 0) {
            log.info("封装券信息 ,id={} sendNumber={} ", id, sendNumber);
            throw new BusinessException("券模板库存不足！");
        }
        baseMapper.updateNumber(id, sendNumber, new Date());
        CouponTemplate couponTemplate = selectById(id);
        if (couponTemplate.getCouponNumber().compareTo(0) < 0) {
            log.info("封装券信息 ,id={} sendNumber={} couponNumber={}", id, sendNumber, couponTemplate.getCouponNumber());
            throw new BusinessException("券模板库存不足！");
        }
        cleanActivityCache(id);
    }

    @Override
    public CouponTemplate selectById(Integer templateId) {
        //缓存获取
//        String key = ActivityCouponCacheKey.BY_TEMPLATE_ID.key(templateId);
//        String activityStr = stringRedisTemplate.opsForValue().get(key);
//        if (StringUtils.isNotEmpty(activityStr)) {
//            return JSON.parseObject(activityStr, new TypeReference<CouponTemplate>() {
//            });
//        }
        //DB
        CouponTemplate couponTemplate = baseMapper.queryById(templateId);
//        activityStr = null == couponTemplate ? "" : JSON.toJSONString(couponTemplate);
//        stringRedisTemplate.opsForValue().set(key, activityStr, 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
        return couponTemplate;
    }

    public void cleanActivityCache(Integer templateId) {
        stringRedisTemplate.opsForValue().getOperations().delete(ActivityCouponCacheKey.BY_TEMPLATE_ID.key(templateId));
    }

    @Override
    public boolean verifyShop(Integer shopId,Integer shopType,Integer templateId){
        //参数验证
        if (shopType == TemplateShopTypeEnums.ALL_SHOP.getCode() || null == shopId) {
            return true;
        }
        List<CouponTemplateTargetShop> shopList = targetShopManager.queryByTemplateId(templateId);
        if (CollectionUtils.isEmpty(shopList)) {
            return false;
        }
        //目标类型一致
        Integer targetShopType = shopList.get(0).getShopType();
        List<Integer> shopIdList = shopList.stream().map(CouponTemplateTargetShop::getShopId).collect(Collectors.toList());
        //店
        Boolean isShop = false;
        if (targetShopType.equals(TemplateTargetShopTypeEnums.SHOP_SHOP.getCode())) {
            isShop = shopIdList.contains(shopId);
        } else if (targetShopType.equals(TemplateTargetShopTypeEnums.DQ_SHOP.getCode())) {
            //大区
            ShopAreaDTO shopAreaDTO = shopAreaService.queryArea(shopId);
            isShop = shopAreaDTO.getDqDto() != null && shopIdList.contains(shopAreaDTO.getDqDto().getId());
        } else if (targetShopType.equals(TemplateTargetShopTypeEnums.PQ_SHOP.getCode())) {
            //片区
            ShopAreaDTO shopAreaDTO = shopAreaService.queryArea(shopId);
            isShop = shopAreaDTO.getPqDto() != null && shopIdList.contains(shopAreaDTO.getPqDto().getId());
        }
        return isShop ? shopType.equals(TemplateShopTypeEnums.CAN_SHOP.getCode()) : shopType.equals(TemplateShopTypeEnums.NO_SHOP.getCode());
    }
    @Override
    public boolean verifyGoods(List<Sku> skuList, CouponTemplate couponTemplate){
        if(CollectionUtils.isEmpty(skuList)){
            return false;
        }
        if (couponTemplate.getCouponCondition().equals(TemplateConditionTypeEnums.NO_USE_CONDITION.getCode())) {
            //无门槛
            return true;
        }
        //有门槛
        BigDecimal needMoney = new BigDecimal(couponTemplate.getNeedMoney());
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Sku sku : skuList) {
            totalPrice = totalPrice.add(sku.getPrice().multiply(sku.getAmount()).setScale(2, RoundingMode.HALF_UP));
        }
        return totalPrice.compareTo(needMoney) >= 0;
    }

    @Override
    public List<Sku> buildGoodsInfo(List<Sku> skuList, CouponTemplate couponTemplate) {
        return buildGoodsInfo(skuList,couponTemplate.getId(),couponTemplate.getApplicableGoodsType());
    }

    /**
     * 适用商品
     */
    public List<Sku> buildGoodsInfo(List<Sku> skuList,Integer templateId,Integer applicableGoodsType){
        List<Sku> effectiveGoodsList = new ArrayList<>();
        List<CouponTemplateTargetGoods> goodsList = targetGoodsManager.queryByTemplateId(templateId);
        Integer goodsType = applicableGoodsType;
        List<String> goodsIdList = goodsList.stream().map(CouponTemplateTargetGoods::getGoodsId).collect(Collectors.toList());
        for (Sku sku : skuList) {
            if (goodsType.equals(TemplateTargetGoodsTypeEnums.ALL_GOODS.getCode())) {
                effectiveGoodsList.add(sku);
                continue;
            }
            if (goodsType.equals(TemplateTargetGoodsTypeEnums.SKU_GOODS.getCode()) && goodsIdList.contains(sku.getSkuId().toString())) {
                effectiveGoodsList.add(sku);
                continue;
            }
            if (goodsType.equals(TemplateTargetGoodsTypeEnums.CLASS1_GOODS.getCode()) && goodsIdList.contains(sku.getClass1Id().toString())) {
                effectiveGoodsList.add(sku);
                continue;
            }
            if (goodsType.equals(TemplateTargetGoodsTypeEnums.CLASS2_GOODS.getCode()) && goodsIdList.contains(sku.getClass2Id().toString())) {
                effectiveGoodsList.add(sku);
                continue;
            }
            if (goodsType.equals(TemplateTargetGoodsTypeEnums.SSU_GOODS.getCode()) && goodsIdList.contains(sku.getSsuId().toString())) {
                effectiveGoodsList.add(sku);
                continue;
            }
            if (goodsType.equals(TemplateTargetGoodsTypeEnums.SALECLASS1_GOODS.getCode()) && goodsIdList.contains(sku.getSaleClass1Id().toString())) {
                effectiveGoodsList.add(sku);
                continue;
            }
            if (goodsType.equals(TemplateTargetGoodsTypeEnums.SALECLASS2_GOODS.getCode()) && goodsIdList.contains(sku.getSaleClass2Id().toString())) {
                effectiveGoodsList.add(sku);
                continue;
            }
            if (goodsType.equals(TemplateTargetGoodsTypeEnums.MEETING_GOODS.getCode()) && goodsIdList.contains(sku.getMeetingId())) {
                effectiveGoodsList.add(sku);
                continue;
            }
        }
        return effectiveGoodsList;
    }

    @Override
    public ActivityGoodsRule buildApplicableGoodsRule(CouponTemplate couponTemplate) {
        List<CouponTemplateTargetGoods> goodsList = targetGoodsManager.queryByTemplateId(couponTemplate.getId());
        List<String> goodsIdList = goodsList.stream().map(CouponTemplateTargetGoods::getGoodsId).collect(Collectors.toList());
        List<Integer> goodsIds = goodsIdList.stream().map(Integer::parseInt).collect(Collectors.toList());
        Integer goodsType = couponTemplate.getApplicableGoodsType();
        List<Integer> class1Ids = new ArrayList<>();
        List<Integer> class2Ids = new ArrayList<>();
        List<Integer> skuIds = new ArrayList<>();
        List<Integer> ssuIds = new ArrayList<>();

        if (goodsType.equals(TemplateTargetGoodsTypeEnums.ALL_GOODS.getCode())) {
            return new ActivityGoodsRule(class1Ids, class2Ids, skuIds, ssuIds);
        } else if (goodsType.equals(TemplateTargetGoodsTypeEnums.SKU_GOODS.getCode())) {
            return new ActivityGoodsRule(class1Ids, class2Ids, goodsIds, ssuIds);
        } else if (goodsType.equals(TemplateTargetGoodsTypeEnums.CLASS1_GOODS.getCode())) {
            return new ActivityGoodsRule(goodsIds, class2Ids, skuIds, ssuIds);
        } else if (goodsType.equals(TemplateTargetGoodsTypeEnums.CLASS2_GOODS.getCode())) {
            return new ActivityGoodsRule(class1Ids, goodsIds, skuIds, ssuIds);
        } else if (goodsType.equals(TemplateTargetGoodsTypeEnums.SSU_GOODS.getCode())) {
            return new ActivityGoodsRule(class1Ids, class2Ids, skuIds, goodsIds);
        }
        return new ActivityGoodsRule(class1Ids, class2Ids, skuIds, ssuIds);
    }
}
