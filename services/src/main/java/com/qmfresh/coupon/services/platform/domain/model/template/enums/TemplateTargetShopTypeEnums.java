package com.qmfresh.coupon.services.platform.domain.model.template.enums;

import lombok.Getter;

@Getter
public enum TemplateTargetShopTypeEnums {
    DQ_SHOP(1, "大区"),
    PQ_SHOP(2, "片区"),
    SHOP_SHOP(3, "门店"),
    WAREHOUSE_SHOP(4, "仓");

    private Integer code;

    private String remark;

    TemplateTargetShopTypeEnums(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
