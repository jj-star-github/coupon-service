package com.qmfresh.coupon.services.service;


import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import org.springframework.stereotype.Component;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
public interface DLock {

    String RECEIVE_COUPON = "coupon:receice:";
    String COUPON_SEND_LOCK = "coupon:sendCoupon:lock"; //发券防并发
    String CREATE_SEND_ACTIVITY_LOCK = "coupon:sendActivity:create:lock";
    String UPDATE_SEND_ACTIVITY_LOCK = "coupon:sendActivity:update:lock";
    String SEND_ACTIVITY_COUPON_LOCK = "coupon:sendActivity:coupon:lock";
    long MINUTE_1 = 60 * 1000;
    long HOUR_1 = MINUTE_1 * 60;
    long DAY_1 = HOUR_1 * 24;
    String COUPON_CREATE_TEMPLATE = "coupon:create:templata:";

    /**
     * 超时时间的设置是个艺术
     *
     * @param key     redisKey
     * @param timeout 超时时间
     * @return boolean
     */
    Boolean tryLock(String key, Long timeout);

    Boolean unlock(String key);

    static String receiveCoupon(String day, Integer userId, Integer activityId){
        if(userId == null){
            throw new BusinessException("会员Id不能为空");
        }
        if(activityId == null){
            throw new BusinessException("促销活动Id不能为空");
        }
        return RECEIVE_COUPON + userId + ":" + ":" + activityId;
    }

    static String sendCouponLock(String businessCode, Integer businessType) {
        return COUPON_SEND_LOCK + ":" + businessType + ":" + businessCode;
    }

    static String createSendActivityLock(Integer userId) {
        return CREATE_SEND_ACTIVITY_LOCK + ":" + userId;
    }

    static String updateSendActivityLock(Long activityId) {
        return UPDATE_SEND_ACTIVITY_LOCK + ":" + activityId;
    }

    static String sendActivityCouponLock(Long id) {
        return SEND_ACTIVITY_COUPON_LOCK + ":" + id;
    }

    static String createTemplateKey(Long operatorId){
        return COUPON_CREATE_TEMPLATE + operatorId;
    }
}
