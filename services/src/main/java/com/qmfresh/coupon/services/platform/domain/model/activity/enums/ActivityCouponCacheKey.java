package com.qmfresh.coupon.services.platform.domain.model.activity.enums;

/**
 *
 */
public enum ActivityCouponCacheKey {
    BY_ACTIVITY_ID, // 根据活动ID查询
    BY_ACTIVITY_TYPE, // 根据活动类型查询
    BY_TEMPLATE_ID,  //券模板基本信息
    SHOP_BY_TEMPLATE_ID,  //券模板店信息
    GOODS_BY_TEMPLATE_ID,  //券模板品信息
    ;
    private final String value;

    ActivityCouponCacheKey() {
        this.value = "coupon:activity:coupon" + name();
    }

    public String key() {
        return value;
    }

    public String key(Object... params) {
        StringBuilder key = new StringBuilder(value);
        if (params != null && params.length > 0) {
            for (Object param : params) {
                if (null != param) {
                    key.append(':');
                    key.append(String.valueOf(param));
                }
            }
        }
        return key.toString();
    }
}
