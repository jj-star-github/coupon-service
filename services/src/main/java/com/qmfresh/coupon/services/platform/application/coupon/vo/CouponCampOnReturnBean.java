package com.qmfresh.coupon.services.platform.application.coupon.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CouponCampOnReturnBean implements Serializable {
    
    private static final long serialVersionUID = -2634608309253218048L;
    
    /**
     * 券Id
     */
    private Integer couponId;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券码
     */
    private List<String> couponCodeList;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 有效期
     */
    private String validityPeriod;

    public CouponCampOnReturnBean(){}
    public CouponCampOnReturnBean(List<String> couponCodeList) {
        this.couponCodeList = couponCodeList;
    }

}
