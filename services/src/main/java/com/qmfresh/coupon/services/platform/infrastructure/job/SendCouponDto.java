package com.qmfresh.coupon.services.platform.infrastructure.job;

import java.util.List;

/**
 * @ClassName SendCouponDto
 * @Description TODO
 * @Author xbb
 * @Date 2020/4/9 20:15
 */
public class SendCouponDto {

    private List<Integer> userIdList;


    private Integer couponId;

    /**
     * 发放张数
     */
    private Integer sendNum;

    /**
     * 注册渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;

    public List<Integer> getUserIdList() {
        return userIdList;
    }

    public void setUserIdList(List<Integer> userIdList) {
        this.userIdList = userIdList;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getSendNum() {
        return sendNum;
    }

    public void setSendNum(Integer sendNum) {
        this.sendNum = sendNum;
    }

    public Integer getLimitChannel() {
        return limitChannel;
    }

    public void setLimitChannel(Integer limitChannel) {
        this.limitChannel = limitChannel;
    }
}
