package com.qmfresh.coupon.services.platform.domain.model.template.command;

import lombok.Data;

@Data
public class TemplateTargetGoodsCommand {
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 商品名
     */
    private String goodsName;
    /**
     * 商品类型
     */
    private Integer goodsType;
}
