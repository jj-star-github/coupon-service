package com.qmfresh.coupon.services.entity.bo;

import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class CouponReturnBo implements Serializable {

    private String sourceOrderCode;
    private String couponCode;
    /**
     * 1=已退 0=退成功 2=退失败 4 已过期
     */
    private Integer status;
    private String remark;

    public static CouponReturnBo success(Coupon coupon) {
        CouponReturnBo couponReturnBo = new CouponReturnBo();
        couponReturnBo.setSourceOrderCode(coupon.getOrderNo());
        couponReturnBo.setCouponCode(coupon.getCouponCode());
        couponReturnBo.setRemark("退成功");
        couponReturnBo.setStatus(0);
        return couponReturnBo;
    }

    public static CouponReturnBo notUsed(String sourceOrderCode) {
        CouponReturnBo couponReturnBo = new CouponReturnBo();
        couponReturnBo.setSourceOrderCode(sourceOrderCode);
        couponReturnBo.setCouponCode("");
        couponReturnBo.setStatus(2);
        couponReturnBo.setRemark("未找到对应优惠券" + sourceOrderCode);
        return couponReturnBo;
    }

    public static CouponReturnBo expired(Coupon coupon) {
        CouponReturnBo couponReturnBo = new CouponReturnBo();
        couponReturnBo.setSourceOrderCode(coupon.getOrderNo());
        couponReturnBo.setCouponCode(coupon.getCouponCode());
        couponReturnBo.setStatus(4);
        couponReturnBo.setRemark("优惠券已过期，不退还" + coupon.getCouponCode());
        return couponReturnBo;
    }
}
