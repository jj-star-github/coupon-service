package com.qmfresh.coupon.services.platform.infrastructure.http.promotion;

import lombok.Data;

import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
@Data
public class PromotionProtocol {

    private Area area;

    private User user;

    /**
     * 渠道
     */
    private Integer channel;

    private List<PromotionSsu> promotionSsuList;

    /**
     * 优惠券来源订单号
     */
    private String sourceOrderNo;

}
