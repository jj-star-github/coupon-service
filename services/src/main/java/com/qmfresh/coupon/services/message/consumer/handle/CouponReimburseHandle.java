/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/9/6
 * Description:CouponReimburseHandle.java
 */
package com.qmfresh.coupon.services.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponRemunerateRequestBean;
import com.qmfresh.coupon.interfaces.dto.coupon.UserIdParamBean;
import com.qmfresh.coupon.services.service.ICouponConsumerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lxc
 */
@Service
public class CouponReimburseHandle {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponReimburseHandle.class);

    @Autowired
    private ICouponConsumerService sendCouponService;

    public void handleMessage(CouponRemunerateRequestBean bean) {
        LOGGER.info("CouponReimburseHandle handleMessage begin....参数:" + JSON.toJSONString(bean));
        sendCouponService.couponReimburse(bean);
        LOGGER.info("CouponReimburseHandle handleMessage end ....");
    }
}
