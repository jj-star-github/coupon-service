package com.qmfresh.coupon.services.platform.domain.model.template;

import lombok.Data;

import java.io.Serializable;

@Data
public class StartEndTime implements Serializable {
    Integer timeStart;
    Integer timeEnd;
}
