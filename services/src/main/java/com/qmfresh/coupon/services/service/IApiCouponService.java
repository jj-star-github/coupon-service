/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/26
 * Description:ICouponApiService.java
 */
package com.qmfresh.coupon.services.service;


import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.services.platform.application.coupon.vo.CouponCampOnReturnBean;
import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.PromotionProtocol;
import com.qmfresh.coupon.services.entity.bo.CouponReturnBo;
import com.qmfresh.coupon.services.entity.bo.CouponReturnParamBo;
import com.qmfresh.coupon.services.entity.command.CouponPrintCheckCommand;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;

import java.util.List;

/**
 * @author lxc
 */
public interface IApiCouponService {

    /**
     * 会员个人中心显示可用优惠券个数
     *
     * @param bean
     * @return
     */
    Integer queryCouponNumCanUse(UserIdParamBean bean);

    /**
     * 会员查询我的优惠券
     *
     * @param bean
     * @return
     */
    ReturnAppBean queryMemCoupon(UserIdParamBean bean);

    /**
     * 促销查询可用的优惠券活动
     *
     * @param bean
     * @return
     */
    List<ProQueryCouponReturnBean> queryCouponActivityCanUse(ProQueryCouponRequestBean bean);

    /**
     * 收银触发发放优惠券
     *
     * @param bean
     * @return
     */
    List<CashQuerySendCouponReturnBean> cashTriggerSendCoupon(CashQuerySendCouponRequestBean bean, Boolean cashTriggerSendCoupon);

    /**
     * 根据优惠券ID查询优惠券信息 （云POS使用）
     * 会生成优优惠券码
     *
     * @param activityId
     * @return
     */
    CouponCampOnReturnBean queryCouponInfo(Integer activityId, Integer shopId, Integer sendNum);

    /**
     * 结算页查询所有未过期的优惠券
     *
     * @param bean
     * @return
     */
    ReturnAppBean settQueryMemCoupon(SettQueryCouponRequestBean bean);

    /**
     * 结算页查询所有未过期的优惠券(分页)
     *
     * @param bean
     * @return
     */
    ListWithPage<UnusedCouponBean> queryMyUnusedCouponWithPage(SettQueryCouponRequestBean bean);

    /**
     * 提交订单前再次查询优惠券
     *
     * @param bean
     * @return
     */
    QueryAgainReturnBean queryMemCouponAgain(QueryAgainRequestBean bean);

    /**
     * 优惠券使用
     *
     * @param bean
     * @return
     */
    CouponUseReturnBean useCoupon(CouponUseRequestBean bean);

//    /**
//     * 优惠券使用
//     * @param bean
//     * @return
//     */
//    CouponUseReturnBean useCouponNew(CouponUseRequestBean bean);

    /**
     * 优惠券补偿
     *
     * @param bean
     * @return
     */
    Boolean remunerateCoupon(CouponRemunerateRequestBean bean);


    /**
     * 优惠券补打检查
     *
     * @return 返回接口
     */
    List<CouponCheckReturnBean> printCheck(CouponPrintCheckCommand bean);

    /**
     * 优惠券使用校验（收银使用）
     *
     * @param bean
     * @return
     */
    CouponCheckReturnBean checkCouponByCouponInfo(CouponCheckRequestBean bean);

    /**
     * 补打优惠券
     *
     * @param param
     * @return
     */
    List<CashQuerySendCouponReturnBean> regainCoupon(PromotionProtocol param);

    /**
     * 查询发放到用户手中的优惠券
     *
     * @param bean
     * @return
     */
    List<CashQuerySendCouponReturnBean> querySentCouponList(QuerySentCouponListRequestBean bean);

    /**
     * 查询发放到用户手中的优惠券
     *
     * @param bean
     * @return
     */
    ListWithPage<CashQuerySendCouponReturnBean> querySentCouponListWithPage(QuerySentCouponListRequestBean bean);

    /**
     * 查询发放到用户手中的优惠券
     * @param bean
     * @return
     */
    ListWithPage<CashQuerySendCouponReturnBean> querySentCouponListWithPageForMall(QuerySentCouponListRequestBean bean);


    /**
     * 根据useID查询优惠券信息（线上商城用）
     */
    List<CouponForAppReturnBean> queryCouponByUserId(CouponForAppRequestBean bean);

    /**
     * 促销查询可用的优惠券活动(线上商城用)
     */
    ListWithPage<ProQueryCouponReturnBean> queryUseingCoupon(QueryCouponInfoBean bean);


    CouponReturnBo returnCoupon(CouponReturnParamBo couponReturnParamBo);


}
