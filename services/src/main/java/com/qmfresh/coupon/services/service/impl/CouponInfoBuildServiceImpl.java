/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:CouponInfoBuildServiceImpl.java
 */
package com.qmfresh.coupon.services.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.services.common.utils.CouponUtil;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.common.utils.SnowflakeIdWorker;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.service.ICouponInfoBuildService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * @author lxc
 */
@Service
public class CouponInfoBuildServiceImpl implements ICouponInfoBuildService {

    public static final SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(0, 0);

    @Override
    public Coupon buildCouponIfo(Long userId, CouponTemplate activity, String sourceOrderNo, Integer sourceShopId, Integer limitChannel, Integer cashSend, String couponCode,Integer promotionActivityId) {
        int now = DateUtil.getCurrentTimeIntValue();
        Coupon coupon = new Coupon();
        try {
            //设置优惠券限定渠道
            coupon.setLimitChannel(activity.getLimitChannel());
            //绑定发券门店id
            coupon.setSourceShopId(sourceShopId == null ? 0 : sourceShopId);
            //关联优惠券绑定的订单id
            coupon.setSourceOrderNo(StringUtils.isEmpty(sourceOrderNo) ? "" : sourceOrderNo);
            // 关联的优惠券活动的id
            coupon.setOriginalId(activity.getId());
            // 券类型
            Integer couponType = activity.getCouponType();
            String couponName = "优惠券";
            // 券码 cashSend,1收银端发放，0系统自动发放
            if (StringUtils.isEmpty(couponCode)) {
                if(cashSend == 1){
                    coupon.setCouponCode(getCouponCodeByshop(sourceOrderNo));
                }else {
                    coupon.setCouponCode(getCouponCode());
                }
            } else {
                coupon.setCouponCode(couponCode);
            }
            // 门槛
            Integer couponCondition = activity.getCouponCondition();
            coupon.setCouponCondition(couponCondition);
            // 满减所需金额
            coupon.setNeedMoney(activity.getNeedMoney());
            // 券类型
            coupon.setCouponType(couponType);
            // 解析活动规则
            JSONObject couponRule = JSONObject.parseObject(activity.getCouponRule());
            // 得到规则
            ActivityConditionRule conditionRule = (ActivityConditionRule) JSONObject.toJavaObject(couponRule, ActivityConditionRule.class);
            if (couponCondition.equals(CouponTypeEnums.NO_USE_CONDITION.getCode())) {
                // 无门槛使用
                couponName = "无门槛券";
                if (couponType.equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())) {
                    // 满减
                    coupon.setSubMoney(new BigDecimal(conditionRule.getSubMoney()));
                } else if (couponType.equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
                    // 折扣
                    coupon.setCouponDiscount(conditionRule.getDiscount());
                    coupon.setSubMoney(conditionRule.getMaxDiscountMoney());
                } else if (couponType.equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
                    // 得到随机金额
                    BigDecimal randomSubMoney = createRandomMoney(conditionRule.getRandomMoneyStart(), conditionRule.getRandomMoneyEnd());
                    coupon.setSubMoney(randomSubMoney);
                }
            } else {
                if (couponType.equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())) {
                    couponName = "满减券";
                    // 满减
                    coupon.setSubMoney(new BigDecimal(conditionRule.getSubMoney()));
                } else if (couponType.equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
                    couponName = "折扣券";
                    // 折扣
                    coupon.setCouponDiscount(conditionRule.getDiscount());
                    coupon.setSubMoney(conditionRule.getMaxDiscountMoney());
                } else if (couponType.equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
                    couponName = "立减券";
                    // 得到随机金额
                    BigDecimal randomSubMoney = createRandomMoney(conditionRule.getRandomMoneyStart(), conditionRule.getRandomMoneyEnd());
                    coupon.setSubMoney(randomSubMoney);
                }
            }
            // 券名称
            coupon.setCouponName(couponName);
            // 解析时间规则
            JSONObject useTimeRule = JSONObject.parseObject(activity.getUseTimeRule());
            // 时间规则
            ActivityTimeRule timeRule = (ActivityTimeRule) JSONObject.toJavaObject(useTimeRule, ActivityTimeRule.class);
            // 时间规则类型
            Integer useTimeType = activity.getUseTimeType();
            // 格式化时间
            SimpleDateFormat formatTime = new SimpleDateFormat("yyyy-MM-dd");
            // 有效期起始时间
            String timeStart = "0000-00-00";
            // 有效期截止时间
            String timeEnd = "9999-99-99";
            if (useTimeType.equals(CouponTypeEnums.TIME_USE_FROM_TO.getCode())) {
                // 从...到...
                coupon.setUseTimeStart(timeRule.getTimeStart());
                coupon.setUseTimeEnd(timeRule.getTimeEnd());

            } else if (useTimeType.equals(CouponTypeEnums.BEGIN_FROM_GET.getCode())) {
                // 领券当日起
                Integer beginTime = DateUtil.getMorningTimeByTimeStamp(now);
                Date date = new Date(beginTime * 1000L);
                Calendar ca = Calendar.getInstance();
                ca.setTime(date);
                ca.add(Calendar.DATE, timeRule.getTodayCanUse());
                date = ca.getTime();
                // 起始时间
                coupon.setUseTimeStart(beginTime);
                coupon.setUseTimeEnd((int) (date.getTime() / 1000) - 1);
            } else if (useTimeType.equals(CouponTypeEnums.BEGIN_FROM_TOMORROW.getCode())) {
                // 领券次日起
                Integer beginTime = DateUtil.getMorningTimeByTimeStamp(now);
                Date date = new Date(beginTime * 1000L);

                Calendar caStart = Calendar.getInstance();
                caStart.setTime(date);
                caStart.add(Calendar.DATE, 1);
                Date dateStart = caStart.getTime();

                Calendar caEnd = Calendar.getInstance();
                caEnd.setTime(date);
                caEnd.add(Calendar.DATE, timeRule.getTomorrowCanUse() + 1);
                Date dateEnd = caEnd.getTime();

                coupon.setUseTimeStart((int) (dateStart.getTime() / 1000));
                coupon.setUseTimeEnd((int) (dateEnd.getTime() / 1000) - 1);
            }
            // 适用商品规则类型
            Integer applicableGoodsType = activity.getApplicableGoodsType();
            coupon.setApplicableGoodsType(applicableGoodsType);
            // 使用说明
            coupon.setUseInstruction(activity.getUseInstruction());
            // 创建时间
            coupon.setCT(now);
            // 更新时间
            coupon.setUT(now);
            // 有效性
            coupon.setIsDeleted(CouponTypeEnums.IS_DELETED_DEFAULT.getCode());
            // 绑定用户id
            coupon.setUserId(userId);
            // 使用状态，默认未使用0
            coupon.setStatus(CouponTypeEnums.COUPON_UNUSED.getCode());
            //促销活动id
            coupon.setPromotionActivityId(promotionActivityId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("组装优惠券数据发生异常");
        }
        return coupon;
    }

    @Override
    public boolean isExpiredActivity(int now, CouponTemplate couponTemplate) {
        boolean flag = false;
        if (couponTemplate.getUseTimeType().equals(CouponTypeEnums.TIME_USE_FROM_TO.getCode())) {
            // 解析时间规则
            JSONObject timeRule = JSONObject.parseObject(couponTemplate.getUseTimeRule());
            // 时间规则
            ActivityTimeRule activityTimeRule = (ActivityTimeRule) JSONObject.toJavaObject(timeRule, ActivityTimeRule.class);
            if (now > activityTimeRule.getTimeEnd()) {
                // 表示现在时间已经大于券活动的最后使用时间,过期了就不查出来了
                flag = true;
                couponTemplate.setStatus(CouponTypeEnums.COUPON_ACTIVITY_EXPIRED.getCode());
            }
        }
        return flag;
    }

    /**
     * 根据shopId 生成优惠券码
     * @param shopId
     * @return
     */
    @Override
    public String getCouponCode(Integer shopId) {
        return getCouponCode2(shopId);
    }

    private String getCouponCode2(Integer shopId) {
        String stringId = String.valueOf(snowflakeIdWorker.nextId());
        String shopIdStr = "";
        if(null == shopId){
            shopIdStr = CouponUtil.getNumSequence(3);
        }else {
            shopIdStr = shopId.toString();
            if (shopIdStr.length() == 1) {
                shopIdStr = "00"+shopIdStr;
            } else if (shopIdStr.length() == 2) {
                shopIdStr = "0"+shopIdStr;
            }
        }

        return shopIdStr + stringId;
    }



    /**
     * 生成券码13位
     * <p>
     * todo 优化
     *
     * @return
     */
    private  String getCouponCode() {
        //使用snowflake生成的id的后9位
        String stringId = String.valueOf(snowflakeIdWorker.nextId());
//        String couponCode = "";
//        if(stringId.length() < 9){
//            StringBuilder sb = new StringBuilder();
//            for (int i = 1; i <= 9 - stringId.length(); i++) {
//                sb.append("8");
//            }
//            sb.append(stringId);
//            couponCode = sb.toString();
//        } else {
//            couponCode = stringId.substring(stringId.length() - 9);
//        }

        return stringId;
    }

    /**
     * @Author zbr
     * @Description shopId + 雪花
     * @Date 15:12 2020/7/30
     */
    private String getCouponCodeByshop(String sourceOrderNo) {
        String stringId = String.valueOf(snowflakeIdWorker.nextId());
        String shopId = "";
        if(StringUtils.isEmpty(sourceOrderNo)){
            shopId = CouponUtil.getNumSequence(3);
        }else {
            shopId = sourceOrderNo.substring(2,5);
        }
        return shopId + stringId;
    }

    /**
     * 生成随机立减金额
     *
     * @param min
     * @param max
     * @return
     */
    private BigDecimal createRandomMoney(int min, int max) {
        Random r = new Random();
        int startMoney = (int) ((max - min) * Math.random());
        return new BigDecimal(startMoney + min);
    }
}
