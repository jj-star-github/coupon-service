package com.qmfresh.coupon.services.platform.domain.model.coupon;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.SendLog;

/**
 * 发券日志
 */
public interface SendLogManager extends IService<SendLog> {

    /**
     * 根据业务标识查询发券记录
     * @param businessCode
     * @param businessType
     * @return
     */
    SendLog queryByBusiness(String businessCode, Integer businessType);
}
