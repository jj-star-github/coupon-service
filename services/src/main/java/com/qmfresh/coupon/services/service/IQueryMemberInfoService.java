/*
 * Copyright (C) 2018-2021 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.coupon.services.service;

import com.qmfresh.coupon.services.platform.infrastructure.http.member.GradeInfoQuery;
import com.qmfresh.coupon.services.platform.infrastructure.http.member.GradeInfoReturn;

/**
 * @author lxc
 * @Date 2020/3/27 0027 20:20
 */
public interface IQueryMemberInfoService {

    /**
     * 根据会员等级查询会员id信息
     *
     * @param param
     * @return
     */
    GradeInfoReturn queryGradeInfoList(GradeInfoQuery param);
}
