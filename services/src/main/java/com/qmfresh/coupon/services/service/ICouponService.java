/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/26
 * Description:ICouponApiService.java
 */
package com.qmfresh.coupon.services.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponInfoDto;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponSendDetailDto;
import com.qmfresh.coupon.interfaces.dto.coupon.CreateActivityBean;
import com.qmfresh.coupon.interfaces.dto.coupon.IdParamBean;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponCount;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponInfoBean;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponListBean;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponReturnBean;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvailableCouponQuery;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvilableCoupon;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetail;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetailQuery;
import com.qmfresh.coupon.interfaces.dto.member.MemberSendCouponParam;

import java.util.List;

/**
 * @author lxc
 */
public interface ICouponService {

    /**
     * 创建优惠券活动
     *
     * @param bean
     * @return
     */
    Boolean createCouponActivity(CreateActivityBean bean);

    /**
     * 查询已审批通过的优惠券信息
     *
     * @param bean
     * @return
     */
    ListWithPage<QueryCouponReturnBean> queryCouponInfoList(QueryCouponInfoBean bean);

    /**
     * 查询优惠券明细信息
     *
     * @param bean
     * @return
     */
    CreateActivityBean queryCouponDetailInfo(IdParamBean bean);

    /**
     * 优惠券修改
     *
     * @param bean
     * @return
     */
    Boolean modifyCouponInfo(CreateActivityBean bean);

    /**
     * 优惠券删除
     *
     * @param bean
     * @return
     */
    Boolean delCouponActivity(IdParamBean bean);

    /**
     * 查询可发放券列表
     * @param param
     * @return
     */
    ListWithPage<MemberAvilableCoupon> queryMemberAvilableCouponList(MemberAvailableCouponQuery param);
    /**
     * 发放详情页面查询优惠券信息
     * @param couponInfoDto
     * @return
     */
    List<CouponInfoDto> queryCouponInfoByCondition(CouponInfoDto couponInfoDto);

    /**
     * 查询会员优惠券明细
     * @param param
     * @return
     */
    ListWithPage<MemberCouponDetail> queryMemberCouponDetail(MemberCouponDetailQuery param);

    /**
     * 会员中台定向发放优惠券
     * @param param
     * @return
     */
    Boolean sendCouponToMember(MemberSendCouponParam param);
    /**
     * 查询发放列表详情
     * @param bean
     * @return
     */
    CouponSendDetailDto queryCouponSendById(IdParamBean bean);


    /**
     * 优惠券后台管理需要查询展示的详情信息
     * @param countBean
     * @return
     */
    Page<QueryCouponListBean> queryCouponListDetail(QueryCouponCount countBean);
}
