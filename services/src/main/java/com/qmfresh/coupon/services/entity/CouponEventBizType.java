package com.qmfresh.coupon.services.entity;

import lombok.Getter;
import lombok.ToString;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@ToString
public enum CouponEventBizType {
    /**
     * 活动发送
     */
    ACTIVITY_SEND(11, "活动发送"),
    SEND_ACTIVITY(12, "发送活动"),
    /**
     * 归还
     */
    CASH_RETURN(31, "收银归还"),
    MINIAPP_RETURN(32, "小程序归还"),
    MUNUAL_RETURN(33, "客服人工退"),
    /**
     * 使用
     */
    CASH_USE(21, "收银使用"),
    MINIAPP_USE(22, "小程序使用"),
    /**
     * 赠送失效
     */
    CASH_GIVEN_EXPIRED(42, "收银退单失效"),
    MANUAL_GIVEN_EXPIRED(42, "客服人工失效"),
    /**
     * 过期
     */
    SYSTEM_EXPIRED(51, "系统自动过期");

    private Integer code;
    private String msg;

    CouponEventBizType(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
