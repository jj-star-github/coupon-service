package com.qmfresh.coupon.services.platform.infrastructure.job;

import com.qmfresh.coupon.services.platform.application.coupon.SendCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;


/**
 * @ClassName SendCouponHandler
 * @Description TODO
 * @Author xbb
 * @Date 2020/4/9 20:28
 */
@JobHandler(value = "sendCouponHandler")
@Service
public class SendCouponHandler extends IJobHandler {

    @Autowired
    private SendCouponService sendCouponService;
    @Override
    public ReturnT<String> execute(String userInfo) throws Exception {
        XxlJobLogger.log("开始执行给特定用户发券的定时任务。。。。。。。。。。。。。。");
        SendCouponDto sendCouponDto = JSON.parseObject(userInfo,SendCouponDto.class);
        sendCouponService.hmSendByType(sendCouponDto);
        XxlJobLogger.log("给特定用户发券的定时任务完成。。。。。。。。。。。。。。");
        return ReturnT.SUCCESS;
    }

}
