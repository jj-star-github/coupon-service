package com.qmfresh.coupon.services.platform.domain.shared;

import com.qmfresh.coupon.services.common.utils.DateUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class BaseUtil {

    /**
     * 构造优惠券使用有效期
     *
     * @param timeStart
     * @param timeEnd
     * @return
     */
    public static String buildValidityPeriod(Integer timeStart, Integer timeEnd) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String startTime = "";
        String endTime = "";
        try {
            startTime = format.format(new Date(timeStart * 1000L));
            endTime = format.format(new Date(timeEnd * 1000L));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("查询有效期信息有误");
        }
        return "有效期:" + startTime + " 至 " + endTime;
    }

    /**
     * 判断是否已过期，明日过期，后日过期,两天后过期
     *
     * @param timeEnd
     * @return
     */
    public static Integer buildExpiredFlag(Integer timeEnd) {
        Integer expiredFlag = 0;
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 1);
        Calendar calendar2 = new GregorianCalendar();
        calendar2.setTime(new Date());
        calendar2.add(Calendar.DATE, 2);
        // 当前时间等于优惠券截止时间，表示今天截止
        if (DateUtil.getStartTimeStamp(new Date(now * 1000L)) >= timeEnd) {
            expiredFlag = -1;
        } else if (DateUtil.getStartTimeStamp(new Date(now * 1000L)) == DateUtil.getStartTimeStamp(new Date(timeEnd * 1000L))) {
            expiredFlag = 1;
        } else if (DateUtil.getStartTimeStamp(calendar.getTime()) == DateUtil.getStartTimeStamp(new Date(timeEnd * 1000L))) {
            expiredFlag = 2;
        } else if (DateUtil.getStartTimeStamp(calendar2.getTime()) == DateUtil.getStartTimeStamp(new Date(timeEnd * 1000L))) {
            expiredFlag = 3;
        }
        return expiredFlag;
    }

}
