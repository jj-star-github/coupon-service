package com.qmfresh.coupon.services.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.services.entity.CouponSendIds;
import com.qmfresh.coupon.interfaces.dto.coupon.ModifyRelationBean;
import com.qmfresh.coupon.services.manager.ICouponSendIdsManager;
import com.qmfresh.coupon.services.mapper.CouponSendIdsMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxc
 * @since 2019-08-02
 */
@Service
@Slf4j
public class CouponSendIdsManagerImpl extends ServiceImpl<CouponSendIdsMapper, CouponSendIds> implements ICouponSendIdsManager {

    @Autowired
    private CouponSendIdsMapper idsMapper;

    @Override
    public int modifySendRelationForCouponId(ModifyRelationBean bean) {
        return idsMapper.modifySendRelationForCouponId(bean);
    }

    @Override
    public int modifySendRelationForSendId(ModifyRelationBean bean) {
        return idsMapper.modifySendRelationForSendId(bean);
    }

    @Override
    public int delCouponIds(List<Integer> ids) {
        return idsMapper.delCouponIds(ids);
    }
}
