package com.qmfresh.coupon.services.platform.application.coupon.context;

import lombok.Data;

@Data
public class SendBySendActivityContext {

    private String businessCode;
    private Integer businessType;
    private Integer activityType;
    private Integer userId;
    private Integer shopId;
    private Integer sendNum;
    private Integer sourceSystem;

    public SendBySendActivityContext () {}

    public SendBySendActivityContext(String businessCode, Integer businessType, Integer activityType,
                                      Integer userId, Integer shopId, Integer sendNum, Integer sourceSystem) {
        this.businessCode = businessCode;
        this.businessType = businessType;
        this.activityType = activityType;
        this.userId = userId;
        this.shopId = shopId;
        this.sendNum = sendNum;
        this.sourceSystem = sourceSystem;
    }

}
