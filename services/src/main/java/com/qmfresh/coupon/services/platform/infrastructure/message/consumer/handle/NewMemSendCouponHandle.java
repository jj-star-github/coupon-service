/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/7
 * Description:NewMemSendCouponHandle.java
 */
package com.qmfresh.coupon.services.platform.infrastructure.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.coupon.UserIdParamBean;
import com.qmfresh.coupon.services.platform.application.coupon.SendCouponService;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendBySendActivityContext;
import com.qmfresh.coupon.services.platform.application.coupon.enums.SendBusinessTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityTypeEnums;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lxc
 */
@Service
public class NewMemSendCouponHandle {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(NewMemSendCouponHandle.class);

    @Autowired
    private SendCouponService sendCouponService;

    public void handleMessage(UserIdParamBean bean) {
        LOGGER.info("NewMemSendCouponHandle handleMessage begin....参数:" + JSON.toJSONString(bean));
        //封装发券上线文发券
        SendBySendActivityContext sendActivityContext  = new SendBySendActivityContext(bean.getUserId().toString(), SendBusinessTypeEnums.MEMBER_NEW_ACTIVITY.getType(),
                ActivityTypeEnums.NEW_USER.getCode(), bean.getUserId().intValue(), null, bean.getSendNum(), null);
        sendCouponService.sendCouponBySendActivity(sendActivityContext);
        LOGGER.info("NewMemSendCouponHandle handleMessage end ....");
    }
}
