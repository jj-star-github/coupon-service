package com.qmfresh.coupon.services.platform.domain.model.coupon;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.CouponUse;

import java.util.List;

/**
 * @ClassName ICouponUseManager
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/30 17:49
 */
public interface ICouponUseManager extends IService<CouponUse> {

    /**
     * 批量回退
     * @param couponIdList
     */
    void reimbursBatchByCouponId(List<Integer> couponIdList, Integer ut);

    /**
     * 根据订单号查询 订单使用优惠券信息
     * @param orderNo
     * @return
     */
    CouponUse queryByOrderNo(String orderNo);
}
