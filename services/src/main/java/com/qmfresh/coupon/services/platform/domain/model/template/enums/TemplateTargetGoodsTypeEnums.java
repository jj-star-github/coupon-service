package com.qmfresh.coupon.services.platform.domain.model.template.enums;

import lombok.Getter;

@Getter
public enum TemplateTargetGoodsTypeEnums {
    ALL_GOODS(1, "全品"),
    SKU_GOODS(2, "SKU"),
    CLASS1_GOODS(3, "CLASS1"),
    CLASS2_GOODS(4, "CLASS2"),
    SSU_GOODS(5, "SSU"),
    SALECLASS1_GOODS(6, "SALECLASS1"),
    SALECLASS2_GOODS(7, "SALECLASS2"),
    MEETING_GOODS(8, "会场品");

    private Integer code;

    private String remark;

    TemplateTargetGoodsTypeEnums(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
