package com.qmfresh.coupon.services.platform.infrastructure.dubbo;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.services.platform.infrastructure.dubbo.dto.ShopAreaDTO;
import com.qmgyl.rpc.result.DubboResult;
import com.qmgyl.shop.api.AreaApi;
import com.qmgyl.shop.dto.area.ShopAreaFullDto;
import com.qmgyl.shop.param.area.AreaConditionParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 门店大区服务
 */
@Service
@Slf4j
public class ShopAreaService {

    @Reference
    private AreaApi areaApi;

    public ShopAreaDTO queryArea(Integer shopId) {
        ShopAreaDTO shopAreaDTO = null;
        AreaConditionParam areaConditionParam = new AreaConditionParam();
        List<Integer> shopList = new ArrayList();
        shopList.add(shopId);
        areaConditionParam.setShopIds(shopList);
        log.info("查询门店所对应的大区信息，请求：param={}", JSON.toJSONString(areaConditionParam));
        DubboResult<List<ShopAreaFullDto>> result =  areaApi.queryFullArea(areaConditionParam);
        log.info("查询门店所对应的大区信息，响应：param={}", JSON.toJSONString(result));
        if (result.isOk() && null != result.getData()){
            List<ShopAreaFullDto> areaFullDtoList = result.getData();
            if (CollectionUtils.isNotEmpty(areaFullDtoList)) {
                shopAreaDTO = ShopAreaDTO.create(areaFullDtoList.get(0));
            }
        }
        return shopAreaDTO;
    }
}
