package com.qmfresh.coupon.services.platform.domain.model.activity.enums;

import lombok.Getter;

/**
 *
 */
@Getter
public enum ActivitySendUserTypeEnums {
    /**
     * 全部会员
     */
    ALLUSER(0, "全部会员"),
    /**
     * Excel导入
     */
    SOMEUSER(1, "Excel导入");

    private Integer code;
    private String name;

    ActivitySendUserTypeEnums(Integer code, String name) {
        this.code = code;
        this.name = name;
    }
}
