package com.qmfresh.coupon.services.platform.domain.model.member.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.qmfresh.coupon.interfaces.dto.member.GradeInfoRes;
import com.qmfresh.coupon.services.platform.domain.model.member.MemberServerManager;
import com.qmfresh.coupon.services.platform.infrastructure.http.member.GradeInfoQuery;
import com.qmfresh.coupon.services.platform.infrastructure.http.member.GradeInfoReturn;
import com.qmfresh.coupon.services.platform.infrastructure.http.member.MemberServerApi;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponInfoDto;
import com.qmfresh.coupon.interfaces.dto.coupon.QuerySentCouponListRequestBean;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetailQuery;
import com.qmfresh.coupon.services.entity.CouponStatus;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.SendCouponCommand;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.SendCouponVO;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.CouponMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.SendLogMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.SendLog;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
@Service
@Slf4j
public class MemberServerManagerImpl implements MemberServerManager {

    @Resource
    private MemberServerApi memberServerApi;

    @Override
    public List<Integer> getUserIdList(Integer memberType, Integer pageIndex, Integer pageSize) {
        GradeInfoQuery param = new GradeInfoQuery(memberType, pageSize, pageIndex);
        // 查询会员信息
        GradeInfoReturn gradeInfoReturn = memberServerApi.queryGradeInfoList(param);
        if (null == gradeInfoReturn || CollectionUtils.isEmpty(gradeInfoReturn.getListData())) {
            return null;
        }
        return gradeInfoReturn.getListData().stream().map(GradeInfoRes::getUserId).collect(Collectors.toList());
    }
}
