package com.qmfresh.coupon.services.platform.domain.model.activity;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class SendActivityVO {
    /**
     * 活动ID
     */
    private Long id;

    /**
     * 活动名
     */
    private String activityName;

    /**
     * 活动状态
     */
    private Integer status;

    /**
     * 发券规则
     */
    private SendActivityRule sendRule;

    /**
     * 描述
     */
    private String remark;

    /**
     * 最后操作人
     */
    private String lastOperatorName;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 发放类型
     */
    private Integer sendType;

    /**
     * 活动类型
     */
    private Integer activityType;

    /**
     * 绑定优惠券名
     */
    private List<String> couponNameList;

    /**
     * 绑定优惠描述
     */
    private List<String> remarkList;
}
