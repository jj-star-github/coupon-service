/*
 * Copyright (C) 2018-2021 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.coupon.services.platform.infrastructure.http.member;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lxc
 * @Date 2020/3/27 0027 21:00
 */
@Data
public class GradeInfoQuery implements Serializable {

    private static final long serialVersionUID = -1346796670585681379L;

    /**
     * 会员等级
     */
    private Integer gradeId;

    private Integer pageSize;
    private Integer pageIndex;

    public GradeInfoQuery () {}
    public GradeInfoQuery(Integer gradeId, Integer pageSize, Integer pageIndex) {
        this.gradeId = gradeId;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

}
