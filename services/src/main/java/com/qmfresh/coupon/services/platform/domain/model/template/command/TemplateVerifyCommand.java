package com.qmfresh.coupon.services.platform.domain.model.template.command;

import com.qmfresh.coupon.interfaces.dto.coupon.CouponCheckRequestBean;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
public class TemplateVerifyCommand {
    private Integer templateId;
    private Integer limitChannel;
    private Integer shopId;
    private List<Sku> skuList;

    public TemplateVerifyCommand() {}

    public TemplateVerifyCommand(CouponCheckRequestBean checkRequestBean, Integer templateId) {
        this.templateId = templateId;
        this.shopId = checkRequestBean.getShopId();
        this.limitChannel = null != checkRequestBean.getBizChannel() && checkRequestBean.getBizChannel() == 5 ? 2 : 1;
        List<Sku> skus = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(checkRequestBean.getGoodsList())) {
            checkRequestBean.getGoodsList().forEach(goodsInfo -> {
                skus.add(new Sku(goodsInfo));
            });
            this.skuList = skus;
        }

    }
}
