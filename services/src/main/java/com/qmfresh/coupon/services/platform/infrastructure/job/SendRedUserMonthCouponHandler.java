package com.qmfresh.coupon.services.platform.infrastructure.job;

import com.qmfresh.coupon.interfaces.enums.MemberFinalEnum;
import com.qmfresh.coupon.services.platform.application.coupon.SendCouponService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 红番茄月礼券
 */
@JobHandler(value = "SendRedUserMonthCouponHandler")
@Service
public class SendRedUserMonthCouponHandler extends IJobHandler {

    @Autowired
    private SendCouponService sendCouponService;
    
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("SendRedUserMonthCoupon-Begin。。。。。。。。。。。。。。");
        sendCouponService.sendMonth(MemberFinalEnum.RED_MEMBER);
        XxlJobLogger.log("SendRedUserMonthCoupon-End。。。。。。。。。。。。。。");
        return ReturnT.SUCCESS;
    }
}
