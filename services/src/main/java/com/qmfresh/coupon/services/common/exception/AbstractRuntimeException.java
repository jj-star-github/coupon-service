package com.qmfresh.coupon.services.common.exception;

/**
 * 自定义基础异常类
 *
 * @author Ivan
 */
public class AbstractRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public AbstractRuntimeException() {
    }

    public AbstractRuntimeException(String msg) {
        super(msg);
    }

    public AbstractRuntimeException(String msg, Throwable t) {
        super(msg, t);
    }
}
