/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/7
 * Description:NewMemSendCouponConsumer.java
 */
package com.qmfresh.coupon.services.platform.infrastructure.message.consumer;

import javax.annotation.Resource;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.interfaces.dto.coupon.UserIdParamBean;
import com.qmfresh.coupon.services.platform.infrastructure.message.consumer.handle.NewMemSendCouponHandle;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lxc
 * 新用户注册送券消费
 */
@Service
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}NewMemSendConsumer", topic = "${mq.topic.newUserRegister}")
public class NewMemSendCouponConsumer extends AbstractRocketMQListener {
    @Resource
    private NewMemSendCouponHandle handle;

    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        UserIdParamBean bean = JSON.parseObject(content,
                new TypeReference<UserIdParamBean>() {
                });
        handle.handleMessage(bean);
    }
}
