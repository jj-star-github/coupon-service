package com.qmfresh.coupon.services.platform.domain.model.activity.command;

import lombok.Data;

import java.util.Date;

/**
 * 活动查询
 */
@Data
public class QuerySendActivityCommand {
    private Integer sendType;
    private Integer activityType;
    private Integer activityStatus;
    private String activityNameKey;
    private Integer pageNum;
    private Integer pageSize;
    private Date beginTime;
    private Date endTime;
    private String createName;

    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }

    public QuerySendActivityCommand() {}

    public QuerySendActivityCommand(Integer sendType, Integer activityType, Integer activityStatus,
                                    String activityNameKey, Integer pageNum, Integer pageSize,Date beginTime, Date endTime, String createName) {
        this.sendType = sendType;
        this.activityType = activityType;
        this.activityStatus = activityStatus;
        this.activityNameKey = activityNameKey;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.createName = createName;
    }
}
