package com.qmfresh.coupon.services.platform.domain.model.coupon.command;

import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.platform.domain.model.template.CouponSendByTemplate;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 发送优惠券
 */
@Data
public class SendCouponVO {
    /**
     * 模板ID
     */
    private Integer templateId;
    /**
     * 全名称
     */
    private String couponCame;
    /**
     * 券门槛
     */
    private Integer couponCondition;
    /**
     * 订单需要金额
     */
    private Integer needMoney;
    /**
     * 优惠券类型
     */
    private Integer couponType;
    /**
     * 优惠金额
     */
    private BigDecimal subMoney;
    /**
     * 折扣
     */
    private Integer couponDiscount;
    /**
     * 使用券开始时间
     */
    private Integer useTimeStart;
    /**
     * 使用券的结束时间
     */
    private Integer useTimeEnd;
    /**
     * 适用渠道
     */
    private Integer limitChannel;
    /**
     * 使用规则
     */
    private String useInstruction;

    private Integer goodsType;

    private List<String> couponCode;

    /**
     * 剩余可领张数
     */
    private Integer residueNum;

    public SendCouponVO() {}

    public SendCouponVO(CouponSendByTemplate couponSendByTemplate, List<String> couponCode) {
        this.templateId = couponSendByTemplate.getTemplateId();
        this.couponCame = couponSendByTemplate.getTemplateName();
        this.couponCondition = couponSendByTemplate.getCouponCondition();
        this.needMoney = couponSendByTemplate.getNeedMoney();
        this.couponType = couponSendByTemplate.getCouponType();
        this.subMoney = couponSendByTemplate.getSubMoney();
        this.couponDiscount = couponSendByTemplate.getCouponDiscount();
        this.useTimeStart = couponSendByTemplate.getUseTimeStart();
        this.useTimeEnd = couponSendByTemplate.getUseTimeEnd();
        this.limitChannel = couponSendByTemplate.getLimitChannel();
        this.useInstruction = couponSendByTemplate.getUseInstruction();
        this.goodsType = couponSendByTemplate.getGoodsType();
        this.couponCode = couponCode;
        this.residueNum = couponSendByTemplate.getNotSendNumber();
    }

    public List<Coupon> createCoupon(Integer sourceShopId, String sourceOrderNo, Integer userId, Integer promotionId){
        List<Coupon> couponList = new ArrayList<>();
        for(String couponCode : this.getCouponCode()) {
            Coupon coupon = new Coupon();
            coupon.setLimitChannel(this.getLimitChannel());
            coupon.setSourceShopId(sourceShopId);
            coupon.setSourceOrderNo(sourceOrderNo);
            coupon.setOriginalId(this.getTemplateId());
            coupon.setCouponCondition(this.getCouponCondition());
            coupon.setNeedMoney(this.getNeedMoney());
            coupon.setCouponType(this.getCouponType());
            coupon.setSubMoney(this.getSubMoney());
            coupon.setCouponDiscount(this.getCouponDiscount());
            coupon.setCouponName(this.getCouponCame());
            coupon.setUseTimeStart(this.getUseTimeStart());
            coupon.setUseTimeEnd(this.getUseTimeEnd());
            coupon.setApplicableGoodsType(this.getGoodsType());
            coupon.setUseInstruction(this.getUseInstruction());
            coupon.setUserId(userId.longValue());
            coupon.setIsDeleted(CouponTypeEnums.IS_DELETED_DEFAULT.getCode());
            coupon.setStatus(CouponTypeEnums.COUPON_UNUSED.getCode());
            int now = DateUtil.getCurrentTimeIntValue();
            coupon.setCT(now);
            coupon.setUT(now);
            coupon.setCouponCode(couponCode);
            coupon.setResidueNum(this.getResidueNum());
            coupon.setPromotionActivityId(promotionId);
            couponList.add(coupon);
        }
        return couponList;
    }
}
