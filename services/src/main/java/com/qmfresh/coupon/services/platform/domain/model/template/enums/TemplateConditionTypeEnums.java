package com.qmfresh.coupon.services.platform.domain.model.template.enums;

import lombok.Getter;

@Getter
public enum TemplateConditionTypeEnums {

    NO_USE_CONDITION(0, "无门槛使用"),
    MONEY_OF_ORDER_AVAILABLE(1, "订单满多少金额可用");

    private Integer code;

    private String remark;

    TemplateConditionTypeEnums(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
