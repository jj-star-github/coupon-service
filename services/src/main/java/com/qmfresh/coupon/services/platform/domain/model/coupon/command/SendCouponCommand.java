package com.qmfresh.coupon.services.platform.domain.model.coupon.command;

import java.util.List;

import com.qmfresh.coupon.services.platform.application.coupon.context.SendBySendActivityContext;
import com.qmfresh.coupon.services.platform.application.coupon.context.SendContext;
import lombok.Data;

/**
 * 发送优惠券
 */
@Data
public class SendCouponCommand {
    private String businessCode;
    private Integer businessType;
    private Integer sourceShopId;
    private String sourceOrderNo;
    private Integer userId;
    private Integer sourceSystem;
    private List<SendCouponVO> sendCouponVO;
    private Integer promotionId;

    public SendCouponCommand() {}

    public SendCouponCommand(SendContext sendContext, List<SendCouponVO> sendCouponVO) {
        this.businessCode = sendContext.getBusinessCode();
        this.businessType = sendContext.getBusinessType();
        this.sourceShopId = sendContext.getShopId();
        this.sourceOrderNo = sendContext.getSourceOrderCode();
        this.userId = sendContext.getUserId();
        this.sendCouponVO = sendCouponVO;
        this.sourceSystem = sendContext.getSourceSystem();
        this.promotionId = sendContext.getPromotionId();
    }

    public SendCouponCommand(SendBySendActivityContext sendContext, List<SendCouponVO> sendCouponVO) {
        this.businessCode = sendContext.getBusinessCode();
        this.businessType = sendContext.getBusinessType();
        this.sourceShopId = sendContext.getShopId();
        this.userId = sendContext.getUserId();
        this.sendCouponVO = sendCouponVO;
        this.sourceSystem = sendContext.getSourceSystem();
    }
}
