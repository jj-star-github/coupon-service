package com.qmfresh.coupon.services.platform.domain.model.template;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateIsAllDayTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateUseTimeTypeEnums;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetGoods;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetShop;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class CouponTemplateInfo implements Serializable {
    private Integer id;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    private Integer couponCondition;
    /**
     * 订单所需金额
     */
    private Integer needMoney;
    /**
     * 券类型
     */
    private Integer couponType;
    /**
     * 券规则
     */
    private TemplateConditionRule conditionRule;
    /**
     * 使用时间类型
     */
    private Integer useTimeType;
    /**
     * 使用时间规则
     */
    private TemplateTimeRule timeRule;
    /**
     * 有效时间
     */
    private String effectiveTime;
    /**
     * 有效时间段
     */
    private String hmsTime;
    /**
     * 适用商品类型，1.全品，2.sku,3.class1,4.class2,5.ssu,6.saleClass1,7.saleClass2,8.会场
     */
    private Integer applicableGoodsType;
    /**
     * 券适用商品规则
     */
    private String applicableGoodsRule;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 状态2:生效中3:已过期
     */
    private Integer status;
    /**
     * 创建人id
     */
    private Integer createdId;
    /**
     * 创建人名称
     */
    private String createdName;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtModified;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;
    /**
     * 适用门店类型
     */
    private Integer applicableShopType;
    /**
     * 券模板剩余数量
     */
    private Integer couponNumber;
    /**
     * 券模板目标品
     */
    private List<TargetGoodsInfo> targetGoodsInfos;
    private List<String> goodsIdList;
    /**
     * 券模板目标店
     */
    private List<TargetShopInfo> targetShopInfos;
    private List<Integer> ShopIdList;

    public CouponTemplateInfo(CouponTemplate couponTemplate,
                              List<CouponTemplateTargetGoods> goodsList,
                              List<CouponTemplateTargetShop> shopList){
        this.setCouponName(couponTemplate.getCouponName());
        this.setCouponCondition(couponTemplate.getCouponCondition());
        this.setNeedMoney(couponTemplate.getNeedMoney());
        this.setCouponType(couponTemplate.getCouponType());
        TemplateConditionRule conditionRule = new TemplateConditionRule();
        TemplateConditionRule couponRule = JSON.parseObject(couponTemplate.getCouponRule(),new TypeReference<TemplateConditionRule>(){});
        if(couponRule.getCouponMaxSendNum() != null){
            conditionRule.setCouponMaxSendNum(couponRule.getCouponMaxSendNum());
        }
        if(couponRule.getDiscount() != null){
            conditionRule.setDiscount(couponRule.getDiscount());
        }
        if(couponRule.getMaxDiscountMoney() != null){
            conditionRule.setMaxDiscountMoney(couponRule.getMaxDiscountMoney());
        }
        if(couponRule.getRandomMoneyEnd() != null){
            conditionRule.setRandomMoneyEnd(couponRule.getRandomMoneyEnd());
        }
        if(couponRule.getRandomMoneyStart() != null){
            conditionRule.setRandomMoneyStart(couponRule.getRandomMoneyStart());
        }
        if(couponRule.getShopInstruction() != null){
            conditionRule.setShopInstruction(couponRule.getShopInstruction());
        }
        if(couponRule.getSubMoney() != null){
            conditionRule.setSubMoney(couponRule.getSubMoney());
        }
        if(couponRule.getUseReceiveNum() != null){
            conditionRule.setUseReceiveNum(couponRule.getUseReceiveNum());
        }
        this.setConditionRule(conditionRule);
        this.setUseTimeType(couponTemplate.getUseTimeType());
        TemplateTimeRule time = new TemplateTimeRule();
        TemplateTimeRule timeRule = JSON.parseObject(couponTemplate.getUseTimeRule(),new TypeReference<TemplateTimeRule>(){});
        this.setEffectiveTime(buildTime(couponTemplate.getUseTimeType(),timeRule));
        this.setHmsTime(buildHms(timeRule));
        if(timeRule.getHmsEnd() != null){
            time.setHmsEnd(timeRule.getHmsEnd());
        }
        if(timeRule.getHmsStart() != null){
            time.setHmsStart(timeRule.getHmsStart());
        }
        if(timeRule.getIsAllDay() != null){
            time.setIsAllDay(timeRule.getIsAllDay());
        }
        if(timeRule.getTimeEnd() != null){
            time.setTimeEnd(timeRule.getTimeEnd());
        }
        if(timeRule.getTimeStart() != null){
            time.setTimeStart(timeRule.getTimeStart());
        }
        if(timeRule.getTodayCanUse() != null){
            time.setTodayCanUse(timeRule.getTodayCanUse());
        }
        if(timeRule.getTomorrowCanUse() != null){
            time.setTomorrowCanUse(timeRule.getTomorrowCanUse());
        }
        this.setTimeRule(time);
        this.setApplicableGoodsType(couponTemplate.getApplicableGoodsType());
        this.setUseInstruction(couponTemplate.getUseInstruction());
        this.setStatus(couponTemplate.getStatus());
        this.setCreatedId(couponTemplate.getCreatedId());
        this.setCreatedName(couponTemplate.getCreatedName());
        this.setGmtCreate(couponTemplate.getGmtCreate());
        this.setGmtModified(couponTemplate.getGmtModified());
        this.setLimitChannel(couponTemplate.getLimitChannel());
        this.setApplicableShopType(couponTemplate.getApplicableShopType());
        this.setCouponNumber(couponTemplate.getCouponNumber());
        List<Integer> shopIdList = shopList.stream().map(CouponTemplateTargetShop::getShopId).collect(Collectors.toList());
        this.setShopIdList(shopIdList);
        List<String> goodsIdList = goodsList.stream().map(CouponTemplateTargetGoods::getGoodsId).collect(Collectors.toList());
        this.setGoodsIdList(goodsIdList);
        if(CollectionUtils.isNotEmpty(goodsList)){
            List<TargetGoodsInfo> goodsInfoList = new ArrayList<>();
            goodsList.forEach(
                    couponTemplateTargetGoods -> {
                        TargetGoodsInfo goodsInfo = new TargetGoodsInfo();
                        goodsInfo.setGoodsId(couponTemplateTargetGoods.getGoodsId());
                        goodsInfo.setGoodsName(couponTemplateTargetGoods.getGoodsName());
                        goodsInfo.setGoodsType(couponTemplateTargetGoods.getGoodsType());
                        goodsInfo.setTemplateId(couponTemplateTargetGoods.getTemplateId());
                        goodsInfoList.add(goodsInfo);
                    }
            );
            this.setTargetGoodsInfos(goodsInfoList);
        }
        if(CollectionUtils.isNotEmpty(shopList)){
            List<TargetShopInfo> shopInfoList = new ArrayList<>();
            shopList.forEach(
                    couponTemplateTargetShop -> {
                        TargetShopInfo shopInfo = new TargetShopInfo();
                        shopInfo.setShopId(couponTemplateTargetShop.getShopId());
                        shopInfo.setShopName(couponTemplateTargetShop.getShopName());
                        shopInfo.setShopType(couponTemplateTargetShop.getShopType());
                        shopInfo.setTemplateId(couponTemplateTargetShop.getTemplateId());
                        shopInfoList.add(shopInfo);
                    }
            );
            this.setTargetShopInfos(shopInfoList);
        }
    }

    public static String buildTime(Integer useTimeType, TemplateTimeRule timeRule){
        String effectiveTime = "";
        if(useTimeType.equals(TemplateUseTimeTypeEnums.TIME_USE_FROM_TO.getCode())){
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            effectiveTime = "有效期:"+ format.format(new Date(timeRule.getTimeStart() * 1000L))+"至"+format.format(new Date(timeRule.getTimeEnd() * 1000L));
        }else if(useTimeType.equals(TemplateUseTimeTypeEnums.BEGIN_FROM_GET.getCode())){
            effectiveTime = "领券当日起" + timeRule.getTodayCanUse() + "天内可用";
        }else if(useTimeType.equals(TemplateUseTimeTypeEnums.BEGIN_FROM_TOMORROW.getCode())){
            effectiveTime = "领券当日起" + timeRule.getTomorrowCanUse() + "天内可用";
        }
        return effectiveTime;
    }

    public static String buildHms(TemplateTimeRule timeRule){
        String hmsTime = "";
        if(timeRule != null && timeRule.getIsAllDay() != null && timeRule.getIsAllDay().equals(TemplateIsAllDayTypeEnums.IS_HMS.getCode())){
            hmsTime = timeRule.getHmsStart()+"--"+timeRule.getHmsEnd();
        }else {
            hmsTime = "全天可用";
        }
        return hmsTime;
    }

    /**
     * 验证优惠券小时段，默认true，不在小时段内false
     */
    public static boolean inAvailableRuleTime(TemplateTimeRule timeRule){
        if(timeRule.getIsAllDay() != null && timeRule.getIsAllDay().equals(1)){
            //设置日期格式
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            try {
                Date nowTime = df.parse(df.format(new Date()));
                //有效时间段
                Date hmsStart = df.parse(timeRule.getHmsStart());
                Date hmsEnd = df.parse(timeRule.getHmsEnd());
                if(nowTime.after(hmsStart) && nowTime.before(hmsEnd)){
                    return true;
                }else {
                    return false;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

}
