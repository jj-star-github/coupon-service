package com.qmfresh.coupon.services.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.services.entity.CouponEventLog;
import org.apache.ibatis.annotations.Param;

/**
 */
public interface CouponEventLogMapper extends BaseMapper<CouponEventLog> {

    CouponEventLog queryByBiz(@Param("bizType")Integer bizType, @Param("bizCode")String bizCode, @Param("sourceBizCode")String sourceBizCode);
}
