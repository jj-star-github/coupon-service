package com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author xbb
 * @since 2020-03-30
 */
@TableName("t_coupon_use")
@Data
public class CouponUse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增长
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 优惠券id
     */
	@TableField("coupon_id")
	private Integer couponId;
    /**
     * 订单号
     */
	@TableField("order_no")
	private String orderNo;
    /**
     * 订单金额
     */
	@TableField("order_amount")
	private BigDecimal orderAmount;
    /**
     * 用这张券购买的商品数量
     */
	@TableField("goods_num")
	private Integer goodsNum;
    /**
     * 绑定的用户id
     */
	@TableField("user_id")
	private Long userId;

    /**
     * 使用优惠券的门店id
     */
	@TableField("use_shop_id")
	private Integer useShopId;
    /**
     * 优惠券抵扣金额
     */
	@TableField("coupon_price")
	private BigDecimal couponPrice;
    /**
     * 优惠券使用时间
     */
	@TableField("use_time")
	private Integer useTime;
	/**
	 * 优惠券回退标记 0：未回退，1：已回退
	 */
	@TableField("is_reimburse")
	private Integer isReimburse;
    /**
     * 创建时间
     */
	@TableField("c_t")
	private Integer cT;
    /**
     * 更新时间
     */
	@TableField("u_t")
	private Integer uT;
    /**
     * 有效性
     */
	@TableField("is_deleted")
	private Integer isDeleted;

    /**
     * 封装使用记录
     * @return
     */
    public static CouponUse buildInfo () {
        CouponUse couponUse = new CouponUse();
        return couponUse;
    }

}
