package com.qmfresh.coupon.services.platform.domain.model.template.command;

import com.qmfresh.coupon.services.platform.domain.model.template.TemplateConditionRule;
import com.qmfresh.coupon.services.platform.domain.model.template.TemplateTimeRule;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class CouponTemplateCreateCommand implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 使用门槛
     */
    private Integer useCondition;
    /**
     * 订单满足条件的金额
     */
    private Integer needMoney;
    /**
     * 券类型
     */
    private Integer couponType;
    /**
     * 活动条件规则
     */
    private TemplateConditionRule conditionRule;
    /**
     * 用券时间类型
     */
    private Integer useTimeType;
    /**
     * 活动时间规则
     */
    private TemplateTimeRule timeRule;
    /**
     * 适用商品类型
     */
    private Integer applicableGoodsType;
    /**
     * 使用说明
     */
    private String useInstruction;
    /**
     * 创建人id
     */
    private Integer createId;
    /**
     * 创建人名称
     */
    private String createName;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;
    /**
     * 适用门店类型
     */
    private Integer applicableShopType;
    /**
     * 目标品
     */
    private List<TemplateTargetGoodsCommand> goodsList;
    /**
     * 目标店
     */
    private List<TemplateTargetShopsCommand> shopList;

    public static CouponTemplateCreateCommand create(String activityName,Integer useCondition,Integer needMoney,
                                                     Integer couponType,TemplateConditionRule conditionRule,
                                                     Integer useTimeType,TemplateTimeRule timeRule,
                                                     Integer applicableGoodsType,String useInstruction,
                                                     Integer limitChannel,Integer applicableShopType,
                                                     Integer createId,String createName,
                                                     List<TemplateTargetGoodsCommand> goodsList,
                                                     List<TemplateTargetShopsCommand> shopList){
        if(StringUtils.isBlank(activityName)){
            throw new BusinessException("券模板名称不可为空");
        }
        if (null == useCondition){
            throw new BusinessException("券门槛条件不可为空");
        }
        if (null == couponType){
            throw new BusinessException("券类型不可为空");
        }
        if (null == conditionRule){
            throw new BusinessException("券规则不可为空");
        }
        if (null == useTimeType){
            throw new BusinessException("券使用时间类型不可为空");
        }
        if (null == timeRule){
            throw new BusinessException("券使用时间规则不可为空");
        }
        if (null == applicableGoodsType){
            throw new BusinessException("券适用商品类型不可为空");
        }
        if (null == useInstruction){
            throw new BusinessException("券说明不可为空");
        }
        CouponTemplateCreateCommand command = CouponTemplateCreateCommand.builder()
                .activityName(activityName)
                .useCondition(useCondition)
                .needMoney(needMoney)
                .couponType(couponType)
                .conditionRule(conditionRule)
                .useTimeType(useTimeType)
                .timeRule(timeRule)
                .applicableGoodsType(applicableGoodsType)
                .useInstruction(useInstruction)
                .createId(createId)
                .createName(createName)
                .limitChannel(limitChannel)
                .applicableShopType(applicableShopType)
                .goodsList(goodsList)
                .shopList(shopList)
                .build();
        return command;
    }

}
