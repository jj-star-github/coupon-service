package com.qmfresh.coupon.services.common.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UtilMethods {

    /**
     * 连接url的两部分
     *
     * @param urlSegmen1
     * @param urlSegmen2
     * @return
     */
    public static String concatUrl(String urlSegmen1, String urlSegmen2) {
        String url = null;
        if (urlSegmen1 != null && urlSegmen2 != null) {
            if (urlSegmen1.endsWith("/")) {
                url = urlSegmen1 + urlSegmen2;
            } else {
                url = urlSegmen1 + "/" + urlSegmen2;
            }
        }
        return url;
    }
}
