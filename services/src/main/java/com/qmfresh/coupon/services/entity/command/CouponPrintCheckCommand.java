package com.qmfresh.coupon.services.entity.command;

import com.qmfresh.coupon.services.common.exception.BusinessException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Slf4j
public class CouponPrintCheckCommand implements Serializable {
    /**
     * 优惠券编码
     */
    private String couponCode;
    /**
     * 订单编码
     */
    private String orderCode;

    /**
     * 优惠券打印检查命令
     *
     * @param couponCode 优惠券编码
     * @param orderCode  订单编码
     * @return 优惠券打印检查命令
     */
    public static CouponPrintCheckCommand create(String couponCode, String orderCode) {

        if (StringUtils.isBlank(orderCode)) {
            throw new BusinessException("必须设置订单号");
        }

        CouponPrintCheckCommand printCheckCommand = new CouponPrintCheckCommand();
        printCheckCommand.setCouponCode(couponCode);
        printCheckCommand.setOrderCode(orderCode);
        return printCheckCommand;
    }
}
