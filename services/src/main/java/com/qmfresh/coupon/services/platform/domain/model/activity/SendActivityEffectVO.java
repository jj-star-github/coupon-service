package com.qmfresh.coupon.services.platform.domain.model.activity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SendActivityEffectVO {
    /**
     * 券模板ID
     */
    private Integer templateId;
    /**
     * 券名称
     */
    private String templateName;
    /**
     * 券渠道
     */
    private Integer limitChannel;
    /**
     * 券类型
     */
    private Integer couponType;
    /**
     * 券内容
     */
    private String useInstruction;
    /**
     * 用券时间
     */
    private String couponTime;
    /**
     * 有效时间段
     */
    private String couponHms;
    /**
     * 适用商品
     */
    private String applicableGoods;
    /**
     * 适用门店
     */
    private String applicableShop;
    /**
     * 计划发放张数
     */
    private Integer willSendCouponNum;
    /**
     * 计划发放人数
     */
    private Integer willSendUserNum;
    /**
     * 成功发放张数
     */
    private Integer sendCouponNum;
    /**
     * 成功发放人数
     */
    private Integer sendUserNum;
    /**
     * 使用张数
     */
    private Integer useCouponNum;
    /**
     * 使用人数
     */
    private Integer useUserNum;
    /**
     * 张数转化率
     */
    private BigDecimal couponConversion;
    /**
     * 人数转化率
     */
    private BigDecimal userConversion;

}
