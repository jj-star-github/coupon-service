package com.qmfresh.coupon.services.platform.domain.model.activity.impl;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityCouponManager;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityManager;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityCouponCacheKey;
import com.qmfresh.coupon.services.platform.domain.shared.Operator;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.SendActivityCouponMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivityCoupon;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 */
@Service
@Slf4j
public class SendActivityCouponManagerImpl extends ServiceImpl<SendActivityCouponMapper, SendActivityCoupon>
        implements SendActivityCouponManager {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    public void cleanActivityCache(Long activityId) {
        stringRedisTemplate.opsForValue().getOperations().delete(ActivityCouponCacheKey.BY_ACTIVITY_ID.key(activityId));
    }

    @Override
    public List<SendActivityCoupon> queryByActivityId(Long activityId) {
        String key = ActivityCouponCacheKey.BY_ACTIVITY_ID.key(activityId);
        String activityCouponStr = stringRedisTemplate.opsForValue().get(key);
        if (null != activityCouponStr) {
            return JSON.parseObject(activityCouponStr, new TypeReference<List<SendActivityCoupon>>() {});
        }
        activityCouponStr = "";
        List<SendActivityCoupon> activityCouponList = baseMapper.queryByActivityId(activityId);
        if (!CollectionUtils.isEmpty(activityCouponList)) {
            activityCouponStr = JSON.toJSONString(activityCouponList);
        }
        stringRedisTemplate.opsForValue().set(key, activityCouponStr, 60*60*1000,TimeUnit.MILLISECONDS);
        return activityCouponList;
    }

    @Override
    public List<SendActivityCoupon> queryByActivityId(Long activityId, List<Integer> templateIdList) {
        return baseMapper.queryByActivityAndTemp(activityId, templateIdList);
    }

    public List<SendActivityCoupon> queryByActivityId(List<Long> activityIdList) {
        List<SendActivityCoupon> activityCouponList = new ArrayList<>();
        for (Long activityId : activityIdList) {
            activityCouponList.addAll(this.queryByActivityId(activityId));
        }
        return activityCouponList;
    }

    @Override
    public void delete(Long activityId, Long id, Operator operator) {
        baseMapper.delete(id, activityId, operator.getOperatorId(), operator.getOperatorName());
        this.cleanActivityCache(activityId);
    }

    @Override
    public void updateSendNum(Long activityId, Long id, Integer sendNum, Operator operator) {
        baseMapper.updateSendNum(id, sendNum, activityId, operator.getOperatorId(), operator.getOperatorName());
        this.cleanActivityCache(activityId);
    }

    @Override
    public void insertBatch(List<SendActivityCoupon> activityCouponList, Long activityId) {
        baseMapper.insertBatch(activityCouponList);
        this.cleanActivityCache(activityId);
    }

    @Override
    public void updateSendRule(Long id, String sendRule,Long activityId) {
        baseMapper.updateSendRule(id,sendRule);
        this.cleanActivityCache(activityId);
    }

}
