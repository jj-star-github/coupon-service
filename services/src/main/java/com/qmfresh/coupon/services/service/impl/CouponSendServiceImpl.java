/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/26
 * Description:CouponApiServiceImpl.java
 */
package com.qmfresh.coupon.services.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.interfaces.enums.CouponSendTypeEnums;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.interfaces.enums.IsDeleteEnums;
import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.common.utils.ListUtil;
import com.qmfresh.coupon.services.entity.CouponActivity;
import com.qmfresh.coupon.services.entity.CouponSend;
import com.qmfresh.coupon.services.entity.CouponSendIds;
import com.qmfresh.coupon.services.manager.ICouponActivityManager;
import com.qmfresh.coupon.services.manager.ICouponSendIdsManager;
import com.qmfresh.coupon.services.manager.ICouponSendManager;
import com.qmfresh.coupon.services.service.ICouponSendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lxc
 */
@Service
public class CouponSendServiceImpl implements ICouponSendService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponSendServiceImpl.class);

    @Autowired
    private ICouponActivityManager activityManager;
    @Autowired
    private ICouponSendManager sendManager;
    @Autowired
    private ICouponSendIdsManager idsManager;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean couponSend(CreateCouponSendBean bean) {
        // 查询优惠券活动状态，防止多渠道同时提交，有的已被占用
        List<CouponActivity> couponActivityList = activityManager.list(new QueryWrapper<CouponActivity>()
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
                .in("id", bean.getNeedSendCouponIds()));
        for (CouponActivity activity : couponActivityList) {
            if (!activity.getStatus().equals(CouponTypeEnums.COUPON_ACTIVITY_NOT_SEND.getCode())) {
                throw new BusinessException("券被占用，请重新配置");
            }
        }
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        // 插入数据
        Integer id = insertIntoCouponSend(bean, now);

        // 插入关联的存id的表t_coupon_send_ids
        List<Integer> couponIds = bean.getNeedSendCouponIds();
        if (ListUtil.isNullOrEmpty(couponIds)) {
            throw new BusinessException("券发放活动对应的优惠券信息不能为空");
        }
        // 保存ids数据
        insertIntoCouponIds(now, id, couponIds);
        // 插入完发放数据，更新优惠券活动表状态为1:审批中
        CouponActivityUpdate param = new CouponActivityUpdate();
        param.setModifyStatus(CouponTypeEnums.COUPON_ACTIVITY_APPROVAL_ING.getCode());
        param.setCouponIdList(couponIds);
        activityManager.updateActivityStatus(param);

        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean couponSendApproval(ApprovalCouponSendBean bean) {
        LOGGER.info("start into method couponSendApproval,request :" + JSON.toJSONString(bean));
        // 优惠券发放id
        Integer originalId = bean.getCouponSendId();
        Assert.notNull(originalId, "审批优惠券发放不能没有原优惠券id");
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        // 审批状态 1:通过 2 不通过
        Integer status = bean.getStatus();
        if ((!status.equals(CouponSendTypeEnums.APPROVAL_SUCCESS.getCode())) && (!status.equals(CouponSendTypeEnums.APPROVAL_FALSE.getCode()))) {
            throw new BusinessException("审批状态非法！");
        }
        CouponSendUpdate updateInfo = new CouponSendUpdate();
        // 得到这个优惠券发放的id，用于更新审批信息
        updateInfo.setCouponSendId(bean.getCouponSendId());
        updateInfo.setApprovalId(bean.getApprovalId());
        updateInfo.setApprovalName(bean.getApprovalName());
        updateInfo.setRemarks(bean.getRemarks());
        updateInfo.setStatus(status);
        updateInfo.setLastUpdTime(now);

        // 更新审批信息
        int flag = sendManager.updateApprovalInfo(updateInfo);

        // 查询原优惠券发放信息，取配置的优惠券活动ids
        List<CouponSendIds> couponSendIds = idsManager.list(new QueryWrapper<CouponSendIds>()
                .eq("send_id", originalId)
                .eq("is_deleted", 0));

        List<Integer> couponIdList = couponSendIds.stream().map(e -> e.getCouponId()).collect(Collectors.toList());

        CouponActivityUpdate param = new CouponActivityUpdate();
        // 优惠券活动id集合
        param.setCouponIdList(couponIdList);

        // 如果审批通过，则更新券活动状态为2
        if (status.equals(CouponSendTypeEnums.APPROVAL_SUCCESS.getCode())) {
            param.setModifyStatus(CouponTypeEnums.COUPON_ACTIVITY_USEING.getCode());

        } else if (status.equals(CouponSendTypeEnums.APPROVAL_FALSE.getCode())) {
            // 如果审批不通过，则更新券活动状态为4
            param.setModifyStatus(CouponTypeEnums.COUPON_ACTIVITY_NOT_APPROVED.getCode());
        }
        // 更新优惠券活动的status
        int modifyFlag = activityManager.updateActivityStatus(param);

        if (flag <= 0 || modifyFlag <= 0) {
            return false;
        }
        return true;
    }

    @Override
    public ListWithPage<QueryCouponSendReturnBean> queryCouponSendList(QueryCouponSendListBean bean) {
        LOGGER.info("start into method queryCouponSendList,request :" + JSON.toJSONString(bean));
        ListWithPage<QueryCouponSendReturnBean> page = new ListWithPage<>();
        // 查询数据
        List<QueryCouponSendReturnBean> couponSendList = sendManager.queryInfoList(bean);
        // 查询个数
        int count = sendManager.sendInfoCount(bean);
        page.setTotalCount(count);
        page.setListData(couponSendList);
        return page;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CouponModifyReturnBean modifyCouponSendInfo(CreateCouponSendBean bean) {
        LOGGER.info("start into method modifyCouponSendInfo,request :" + JSON.toJSONString(bean));
        CouponModifyReturnBean returnBean = new CouponModifyReturnBean();
        int now = DateUtil.getCurrentTimeIntValue();
        // 原优惠券发放活动的id
        Integer id = bean.getId();
        // 新的活动id集合
        List<Integer> couponIds = bean.getNeedSendCouponIds();
        // 查询新进来的活动状态是不是0，不是0，返回错误信息，已经在审核中，不能关联
        List<CouponActivity> activityList = activityManager.list(new QueryWrapper<CouponActivity>()
                .in("id", couponIds)
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
                .eq("status", CouponTypeEnums.COUPON_ACTIVITY_APPROVAL_ING.getCode()));
        if (!ListUtil.isNullOrEmpty(activityList)) {

            List<String> alreadyUsed = activityList.stream()
                    .map(activity -> activity.getCouponName()).collect(Collectors.toList());

            returnBean.setFlag(false);
            returnBean.setAlreadyUsed(alreadyUsed);
            return returnBean;
        }
        // 软删除原来的数据
        int delCount = sendManager.delCouponSendActivity(id);
        // 重新新建一个活动
        int newId = insertIntoCouponSend(bean, now);
        // 如果券发放关联表有id这条数据，则更新为新的id
        // 查询券发放关联表,可能查询出多个
        List<CouponSendIds> couponSendIdsList = idsManager.list(new QueryWrapper<CouponSendIds>()
                .eq("send_id", id)
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));

        List<Integer> idsList = new ArrayList<>();
        // 查到数据代表这个发放活动已经提交审核了,要变更信息
        if (!ListUtil.isNullOrEmpty(couponSendIdsList)) {
            List<Integer> oldCouponIds = new ArrayList<>();
            for (CouponSendIds sendIds : couponSendIdsList) {
                idsList.add(sendIds.getId());
                oldCouponIds.add(sendIds.getCouponId());
            }
            // 软删除原来的关联
            idsManager.delCouponIds(idsList);
            // 重新插入
            insertIntoCouponIds(now, newId, couponIds);
        }

        // 插入完发放数据，更新优惠券活动表状态为1:审批中
        CouponActivityUpdate param = new CouponActivityUpdate();
        param.setModifyStatus(CouponTypeEnums.COUPON_ACTIVITY_APPROVAL_ING.getCode());
        param.setCouponIdList(couponIds);
        activityManager.updateActivityStatus(param);
        returnBean.setFlag(true);

        return returnBean;
    }

    /**
     * 保存ids数据
     *
     * @param now
     * @param id
     * @param couponIds
     */
    private void insertIntoCouponIds(int now, Integer id, List<Integer> couponIds) {
        List<CouponSendIds> couponIdsList = new ArrayList<>();
        for (Integer couponId : couponIds) {
            CouponSendIds sendIds = new CouponSendIds();
            sendIds.setCT(now);
            sendIds.setUT(now);
            sendIds.setSendId(id);
            sendIds.setCouponId(couponId);
            couponIdsList.add(sendIds);
        }
        idsManager.saveBatch(couponIdsList);
    }

    @Override
    public Boolean delCouponSendActivity(IdParamBean bean) {
        // 软删除原来的数据
        int delCount = sendManager.delCouponSendActivity(bean.getId());
        //软删除对应的ids记录
        this.deleteIds(bean.getId());
        if (delCount <= 0) {
            return false;
        }
        return true;
    }

    @Override
    public Boolean pauseSendActivity(IdParamBean bean) {
        // 暂停活动
        int pauseCount = sendManager.pauseSendActivity(bean.getId());

        if (pauseCount <= 0) {
            return false;
        }
        return true;
    }

    @Override
    public Boolean recoverPauseSendActivity(IdParamBean bean) {
        // 恢复暂停活动
        int recoverPauseCount = sendManager.recoverPauseSendActivity(bean.getId());

        if (recoverPauseCount <= 0) {
            return false;
        }
        return true;
    }

    @Override
    public CouponSendDetailReturnBean querySendDetailInfo(IdParamBean bean) {
        Integer id = bean.getId();
        // 查询数据
        List<CouponSend> couponSendList = sendManager.list(new QueryWrapper<CouponSend>()
                .eq("id", id)
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
        // 查询关联的ids表
        List<CouponSendIds> couponSendIdsList = idsManager.list(new QueryWrapper<CouponSendIds>()
                .eq("send_id", id)
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));

        List<Integer> idsList = new ArrayList<>();

        CouponSendDetailReturnBean detailReturnBean = new CouponSendDetailReturnBean();
        // 得到所有的关联活动的id
        if (!ListUtil.isNullOrEmpty(couponSendIdsList)) {
            for (CouponSendIds sendIds : couponSendIdsList) {
                idsList.add(sendIds.getCouponId());
            }
            // 查询券活动表
            List<CouponActivity> activityList = activityManager.list(new QueryWrapper<CouponActivity>()
                    .in("id", idsList)
                    .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));

            List<CouponDetailRelationBean> detailRelationBeanList = new ArrayList<>();
            for (CouponActivity activity : activityList) {
                CouponDetailRelationBean detailRelationBean = new CouponDetailRelationBean();
                detailRelationBean.setCouponName(activity.getCouponName());
                detailRelationBean.setCouponType(activity.getCouponType());
                detailRelationBean.setCouponContent(activity.getUseInstruction());
                detailRelationBean.setId(activity.getId());
                detailRelationBeanList.add(detailRelationBean);
            }
            // 
            detailReturnBean.setDetailRelationBean(detailRelationBeanList);
        }
        // 一个id只对应一条数据
        CouponSend couponSend = couponSendList.get(0);
        // id
        detailReturnBean.setId(couponSend.getId());
        // 发放规则
        CouponSendRule couponSendRule = new CouponSendRule();
        // 发放方式
        couponSendRule.setSendType(couponSend.getSendType());
        // 发放数量
        couponSendRule.setSendNum(couponSend.getSendNum());
        // 规则时间类型
        couponSendRule.setTimeLimitType(couponSend.getTimeLimitType());
        // 限制时间起始
        couponSendRule.setTimeLimitStart(couponSend.getTimeLimitStart());
        // 限制时间结束
        couponSendRule.setTimeLimitEnd(couponSend.getTimeLimitEnd());
        // 触发条件
        couponSendRule.setTriggerCondition(couponSend.getTriggerCondition());
        //每次发放数量(会员用)
        couponSendRule.setSendNumPerTime(couponSend.getSendNumPerTime());
        detailReturnBean.setCouponSendRule(couponSendRule);
        // 创建人id
        detailReturnBean.setCreatedId(couponSend.getCreatedId());
        // 创建人名称
        detailReturnBean.setCreatedName(couponSend.getCreatedName());
        // 审核人id
        detailReturnBean.setApprovalId(couponSend.getApprovalId());
        // 审核人名称
        detailReturnBean.setApprovalName(couponSend.getApprovalName());
        // 审批状态
        detailReturnBean.setStatus(couponSend.getStatus());
        // 备注
        detailReturnBean.setRemarks(couponSend.getRemarks());

        return detailReturnBean;
    }


    private void deleteIds(Integer sendId) {
        CouponSendIds couponSendIds = new CouponSendIds();
        couponSendIds.setIsDeleted(IsDeleteEnums.YES.getCode());
        idsManager.update(couponSendIds, new QueryWrapper<CouponSendIds>()
                .eq("send_id", sendId)
                .eq("is_deleted", IsDeleteEnums.NO.getCode()));
    }

    /**
     * 保存优惠券发放信息
     *
     * @param bean
     * @param now
     * @return
     */
    private Integer insertIntoCouponSend(CreateCouponSendBean bean, int now) {

        CouponSendRule couponSendRule = bean.getCouponSendRule();
        if(bean.getCouponSendRule().getSendType().equals(1)){
            if(couponSendRule.getSendNum() == null){
                couponSendRule.setSendNum(1000000L);
            }
        }
        if (couponSendRule.getSendNum() == null) {
            throw new BusinessException("必须设置发送数量");
        }

        // 优惠券发放规则
        Assert.notNull(couponSendRule, "券发放规则不能为空");
        CouponSend couponSend = new CouponSend();
        Integer sendType = couponSendRule.getSendType();
        // 发放方式
        couponSend.setSendType(sendType);
        if (sendType.equals(CouponSendTypeEnums.SYSTEM_AUTO_SEND.getCode())) {
            // 系统自动触发
            Integer timeLimitType = couponSendRule.getTimeLimitType();
            // 有效期类型
            couponSend.setTimeLimitType(timeLimitType);
            if (timeLimitType.equals(CouponSendTypeEnums.TIME_HAVA_LIMIT.getCode())) {
                //有时间限制
                // 时间起始
                couponSend.setTimeLimitStart(couponSendRule.getTimeLimitStart());
                // 时间截止
                couponSend.setTimeLimitEnd(couponSendRule.getTimeLimitEnd());
            }
            // 触发条件
            couponSend.setTriggerCondition(couponSendRule.getTriggerCondition());
        } else if (sendType.equals(CouponSendTypeEnums.ONLINE_SEND.getCode())) {
            // 线上促销
            couponSend.setSendNum(couponSendRule.getSendNum());
        } else if (sendType.equals(CouponSendTypeEnums.OFFLINE_SEND.getCode())) {
            // 线下促销
            couponSend.setSendNum(couponSendRule.getSendNum());
        }
        // 创建人id
        couponSend.setCreatedId(bean.getOperatorId());
        // 创建人名称
        couponSend.setCreatedName(bean.getOperatorName());
        // 只要创建了活动,默认的状态就是0
        couponSend.setStatus(CouponSendTypeEnums.SEND_STATUS_DEFAULT.getCode());
        // 创建时间
        couponSend.setCT(now);
        // 更新时间
        couponSend.setUT(now);
        // 有效性
        couponSend.setIsDeleted(CouponTypeEnums.IS_DELETED_DEFAULT.getCode());

        //每次发放张数（会员使用）
        if (couponSendRule.getSendNumPerTime() != null) {
            couponSend.setSendNumPerTime(couponSendRule.getSendNumPerTime());
        }
        // 插入优惠券发放表
        sendManager.save(couponSend);

        return couponSend.getId();
    }
}
