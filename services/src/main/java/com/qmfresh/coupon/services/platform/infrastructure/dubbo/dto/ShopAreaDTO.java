package com.qmfresh.coupon.services.platform.infrastructure.dubbo.dto;

import com.qmgyl.shop.dto.area.ShopAreaFullDto;
import lombok.Data;

import java.io.Serializable;

/**
 * 门店区域信息
 */
@Data
public class ShopAreaDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer shopId;
    /**
     * 大区
     */
    private AreaDTO dqDto;
    /**
     * 区域
     */
    private AreaDTO qyDto;
    /**
     * 片区
     */
    private AreaDTO pqDto;

    public static ShopAreaDTO create(ShopAreaFullDto shopAreaFullDto) {
        ShopAreaDTO shopAreaDTO = new ShopAreaDTO();
        shopAreaDTO.setShopId(shopAreaFullDto.getShopId());
        shopAreaDTO.setDqDto(AreaDTO.create(shopAreaFullDto.getArea1Dto()));
        shopAreaDTO.setQyDto(AreaDTO.create(shopAreaFullDto.getArea2Dto()));
        shopAreaDTO.setPqDto(AreaDTO.create(shopAreaFullDto.getArea3Dto()));
        return shopAreaDTO;
    }
}
