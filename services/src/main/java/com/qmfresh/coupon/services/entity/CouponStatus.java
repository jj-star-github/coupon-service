package com.qmfresh.coupon.services.entity;

import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 * @see CouponTypeEnums
 */
@Getter
public enum CouponStatus {


    /**
     * 未使用
     */
    UNUSED(0),
    /**
     * 已使用
     */
    USED(1),
    /**
     * 已过期
     */
    EXPIRED(2),
    /**
     * 已失效
     */
    RETURN(4);

    private Integer code;

    CouponStatus(Integer code) {
        this.code = code;
    }
}
