package com.qmfresh.coupon.services.platform.domain.model.template.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateShopVo implements Serializable {
    private Long id;
    private Integer templateId;
    private Integer shopId;
    private String shopName;
    private Integer shopType;
}
