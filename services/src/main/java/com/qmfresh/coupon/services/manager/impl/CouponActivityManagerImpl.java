package com.qmfresh.coupon.services.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.services.entity.CouponActivity;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponActivityUpdate;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponInfoBean;
import com.qmfresh.coupon.services.entity.command.CouponActivityQueryCommand;
import com.qmfresh.coupon.services.manager.ICouponActivityManager;
import com.qmfresh.coupon.services.mapper.CouponActivityMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
@Service
@Slf4j
public class CouponActivityManagerImpl extends ServiceImpl<CouponActivityMapper, CouponActivity> implements ICouponActivityManager {

    @Resource
    CouponActivityMapper activityMapper;

    @Override
    public List<CouponActivity> queryCouponActivityInfo(QueryCouponInfoBean bean) {
        return activityMapper.queryCouponActivityInfo(bean);
    }

    /**
     * 分页查询优惠活动
     *
     * @param couponActivityQueryCommand
     * @return
     */
    @Override
    public Page<CouponActivity> pageCouponActivity(CouponActivityQueryCommand couponActivityQueryCommand) {
        return activityMapper.selectPage(new Page<>(couponActivityQueryCommand.getPageNum(), couponActivityQueryCommand.getPageSize()),
                new LambdaQueryWrapper<CouponActivity>().eq(CouponActivity::getIsDeleted, 0)
                        .eq(couponActivityQueryCommand.getCouponStatus() != null, CouponActivity::getStatus, couponActivityQueryCommand.getCouponStatus())
                        .eq(couponActivityQueryCommand.getCouponType() != null, CouponActivity::getCouponType, couponActivityQueryCommand.getCouponType())
                        .orderByDesc(CouponActivity::getId)
        );
    }

    @Override
    public int queryCouponActivityCount(QueryCouponInfoBean bean) {
        return activityMapper.queryCouponActivityCount(bean);
    }

    @Override
    public int updateActivityStatus(CouponActivityUpdate bean) {
        return activityMapper.updateActivityStatus(bean);
    }

    @Override
    public int delActivity(Integer id) {
        List<Integer> ids = new ArrayList<>();
        ids.add(id);
        return activityMapper.delActivityBatch(ids);
    }

    @Override
    public CouponActivity queryInfoById(Integer id) {
        return activityMapper.queryInfoById(id);
    }

    /**
     * 活动列表
     *
     * @param originalIds 活动ids
     * @return 活动列表
     */
    @Override
    public List<CouponActivity> couponActivities(List<Integer> originalIds) {
        if (CollectionUtils.isEmpty(originalIds)) {
            return new ArrayList<>();
        }

        List<CouponActivity> activities = activityMapper.selectList(new LambdaQueryWrapper<CouponActivity>()
                .in(CouponActivity::getId, originalIds)
                .eq(CouponActivity::getIsDeleted, 0)
        );
        if (CollectionUtils.isEmpty(originalIds)) {
            log.warn("未查询到优惠活动：{}", JSON.toJSONString(originalIds));
        }

        return activities;
    }
}
