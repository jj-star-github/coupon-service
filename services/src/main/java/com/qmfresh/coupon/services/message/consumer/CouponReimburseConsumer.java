/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/9/6
 * Description:CouponReimburse.java
 */
package com.qmfresh.coupon.services.message.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponRemunerateRequestBean;
import com.qmfresh.coupon.services.message.consumer.handle.CouponReimburseHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author lxc
 * 优惠券补偿
 */
@Service
@Slf4j

@RocketMQMessageListener(consumerGroup = "${spring.application.name}ReimburseConsumer", topic = "${mq.topic.couponFallBack}")
public class CouponReimburseConsumer extends AbstractRocketMQListener {

    @Resource
    private CouponReimburseHandle handle;

    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        CouponRemunerateRequestBean bean = JSON.parseObject(content,
                new TypeReference<CouponRemunerateRequestBean>() {
                });
        handle.handleMessage(bean);

    }
}
