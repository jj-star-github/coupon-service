package com.qmfresh.coupon.services.platform.domain.model.coupon;

/**
 * 调用promotion服务
 */
public interface PromotionManager {

    Double getPromotionDetailNew(Long activityId);


}
