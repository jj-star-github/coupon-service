package com.qmfresh.coupon.services.platform.domain.model.activity;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.services.platform.domain.model.activity.command.CreateSendActivityCommand;
import com.qmfresh.coupon.services.platform.domain.model.activity.command.QuerySendActivityCommand;
import com.qmfresh.coupon.services.platform.domain.shared.Operator;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivity;

import java.util.List;

/**
 * 发券日志
 */
public interface SendActivityManager extends IService<SendActivity> {

    /**
     * 根据活动类型查询活动信息
     * @param activityType
     * @return
     */
    List<SendActivity> query(Integer activityType);

    /**
     * 状态更新
     * @param id
     * @param status
     */
    void updateStatus(Long id, Integer status, Operator operator);

    /**
     * 活动删除
     * @param id
     */
    void delete(Long id, Operator operator);

    /**
     * 创建活动
     */
    void createActivity(CreateSendActivityCommand createSendActivityCommand);

    /**
     * 分页查询
     * @param querySendActivityCommand
     * @return
     */
    Page<SendActivityVO> pageActivity(QuerySendActivityCommand querySendActivityCommand);

    /**
     * 创建定向发放
     */
    void createSendToUse(CreateSendActivityCommand createSendActivityCommand);

    List<SendActivity> queryBySendType(Integer sendType,Integer status);

    void updateFinishedStatus(Long id, Integer status, Operator operator);
}