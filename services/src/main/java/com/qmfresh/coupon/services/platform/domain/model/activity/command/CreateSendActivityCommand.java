package com.qmfresh.coupon.services.platform.domain.model.activity.command;

import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityCouponVO;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityRule;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.domain.shared.Operator;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivityCoupon;
import lombok.Data;
import org.eclipse.jetty.util.StringUtil;

import java.util.List;

/**
 * 活动创建
 */
@Data
public class CreateSendActivityCommand {
    /**
     * 活动名
     */
    private String activityName;

    /**
     * 发放类型
     */
    private Integer sendType;

    /**
     * 活动类型
     */
    private Integer activityType;

    /**
     * 发放规则
     */
    private SendActivityRule sendRule;

    /**
     * 描述
     */
    private String remark;

    /**
     * 操作人
     */
    private Operator operator;

    /**
     * 商品信息
     */
    private List<SendActivityCouponVO> sendActivityCouponVOList;

    /**
     * 商品信息
     */
    private List<SendActivityCoupon> sendActivityCouponList;

    public CreateSendActivityCommand () {}

    public CreateSendActivityCommand (String activityName, Integer sendType, Integer activityType, SendActivityRule sendRule, String remark, Operator operator) {
        if (StringUtil.isEmpty(activityName)) {
            throw new BusinessException("活动名称不可为空");
        }
        if (null == sendType){
            throw new BusinessException("请选择有效的发放类型");
        }
        if (null == activityType) {
            throw new BusinessException("请选择有效的活动类型");
        }
        this.activityName = activityName;
        this.sendType = sendType;
        this.activityType = activityType;
        this.sendRule = sendRule;
        this.operator = operator;
        this.remark = remark;
    }
}
