package com.qmfresh.coupon.services.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvailableCouponQuery;
import com.qmfresh.coupon.services.entity.CouponSend;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
public interface CouponSendMapper extends BaseMapper<CouponSend> {

    /**
     * 更新审批信息
     *
     * @param bean
     * @return
     */
    int updateApprovalInfo(@Param("condition") CouponSendUpdate bean);

    /**
     * 查询券发放列表
     *
     * @param bean
     * @return
     */
    List<QueryCouponSendReturnBean> queryInfoList(@Param("condition") QueryCouponSendListBean bean);

    /**
     * 查询列表个数
     *
     * @param bean
     * @return
     */
    int sendInfoCount(@Param("condition") QueryCouponSendListBean bean);

    /**
     * 优惠券发放删除
     *
     * @param id
     * @return
     */
    int delCouponSendActivity(@Param("id") Integer id);

    /**
     * 暂停优惠券发放活动
     *
     * @param id
     * @return
     */
    int pauseSendActivity(@Param("id") Integer id);

    /**
     * 恢复暂停优惠券发放活动
     *
     * @param id
     * @return
     */
    int recoverPauseSendActivity(@Param("id") Integer id);

    List<CouponSend>  queryAvailableSendCouponList(@Param("condition") MemberAvailableCouponQuery param);

    int queryAvailableSendCouponCount(@Param("condition")MemberAvailableCouponQuery param);
}