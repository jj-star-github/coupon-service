package com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetailQuery;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponCount;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
public interface CouponMapper extends BaseMapper<Coupon> {

    /**
     * 分页查询会员的优惠券信息，区分状态
     * @param memberCouponDetailQuery
     * @return
     */
    List<Coupon>  queryCouponInfoByStatus(@Param("condition") MemberCouponDetailQuery memberCouponDetailQuery);

    /**
     * 查询订单使用的优惠信息
     * @param orderNo
     * @param couponCodes
     * @return
     */
    List<Coupon> queryUseCouponByOrderNo(@Param("orderNo")String orderNo, @Param("couponCodeList")List<String> couponCodes);

    /**
     * 批量跟新
     * @param idList
     * @param coupon
     */
    void updateBatch(@Param("idList") List<Integer> idList,@Param("coupon") Coupon coupon);

    /**
     * 根据优惠券码查询
     * @param couponCode
     * @return
     */
    Coupon queryCouponByCode(@Param("couponCode") String couponCode);

    /**
     *
     * @param couponList
     */
    void insertBatch(@Param("couponList") List<Coupon> couponList);

    /**
     * 根据会员id优惠券id和促销活动id查询
     * @param userId
     * @param couponId
     * @param activityId
     * @param start
     * @param end
     * @return
     */
    Coupon queryCouponByUserId(@Param("userId") Integer userId, @Param("couponId") Integer couponId, @Param("activityId") Integer activityId, @Param("start") Integer start, @Param("end") Integer end);


    /**
     * 查询个数
     * @param bean
     * @return
     */
    Integer queryCouponListDetail(@Param("condition") QueryCouponCount bean);

    /**
     * 根据Id批量查询
     * @param idList
     * @return
     */
    List<Coupon> queryByIdList(@Param("idList")List<Integer> idList);

    /**
     * 根据用户和券模板ID 查询
     * @param userList
     * @param originalList
     * @return
     */
    List<Coupon> queryByUserAndOriginal(@Param("userList")List<Integer> userList, @Param("originalList") List<Integer> originalList);


    /**
     * 根据用户查询指定状态券
     *
     * @return
     */
    List<Coupon> queryByUser(@Param("userId")Integer userId, @Param("status") Integer status, @Param("channelList")List<Integer> channelList, @Param("useTimeEnd") Integer useTimeEnd);


    /**
     * 根据useId和templateId查询数量
     * @param userId
     * @param userId
     * @return
     */
    int countByUseId(@Param("userId")Integer userId,@Param("templateId")Integer templateId);

    /**
     *
     * @param id
     * @param couponCode
     * @param userId
     * @return
     */
    Coupon queryByIdOrCode(@Param("id") Integer id, @Param("couponCode")String couponCode, @Param("userId")Long userId);

    /**
     * 发放使用张数
     * @param templateId
     * @param status
     * @return
     */
    int sendNum(@Param("templateId")Integer templateId,@Param("status")Integer status);

    /**
     * 发放使用人数
     * @param templateId
     * @param status
     * @return
     */
    int useNum(@Param("templateId")Integer templateId,@Param("status")Integer status);

    Integer countCouponDetail(@Param("templateId")Integer templateId,@Param("userId")Long userId,@Param("couponCode")String couponCode);

    List<Coupon> queryCouponDetail(@Param("templateId")Integer templateId,@Param("userId")Long userId,@Param("couponCode")String couponCode,@Param("start")Integer start, @Param("length")Integer length);
}
