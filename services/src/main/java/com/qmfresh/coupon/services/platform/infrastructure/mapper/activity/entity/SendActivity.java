package com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityRule;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityVO;
import com.qmfresh.coupon.services.platform.domain.model.activity.command.CreateSendActivityCommand;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivitySendTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityStatusEnums;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

/**
 * 发放活动
 */
@TableName("t_send_activity")
@Data
public class SendActivity implements Serializable {
    public static final String BASE_URL = "https://qm-oss.oss-cn-hangzhou.aliyuncs.com/";
    public static final String FILE_URL = "coupon/sendToUser/";
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String activityName;
    private Integer sendType;
    private Integer activityType;
    private Integer sendStatus;
    private String sendRule;
    private String remark;
    private Integer lastOperatorId;
    private String lastOperatorName;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

    public SendActivity(){}

    public SendActivity(CreateSendActivityCommand createSendActivityCommand) {
        this.activityName = createSendActivityCommand.getActivityName();
        this.sendType = createSendActivityCommand.getSendType();
        this.activityType = createSendActivityCommand.getActivityType();
        this.sendRule = JSON.toJSONString(createSendActivityCommand.getSendRule());
        this.sendStatus = ActivityStatusEnums.CREATE.getCode();
        if (ActivitySendTypeEnums.SYSTEM.getCode() == createSendActivityCommand.getSendType().intValue()) {
            this.sendStatus = ActivityStatusEnums.RUNNING.getCode();
        }else if(ActivitySendTypeEnums.APPOINT.getCode() == createSendActivityCommand.getSendType().intValue()){
            this.sendStatus = ActivityStatusEnums.CREATE.getCode();
        }
        this.setRemark(createSendActivityCommand.getRemark());
        Date now = new Date();
        this.gmtCreate = now;
        this.gmtModified = now;
        this.lastOperatorId = createSendActivityCommand.getOperator().getOperatorId();
        this.lastOperatorName = createSendActivityCommand.getOperator().getOperatorName();
    }

    public SendActivityVO createSendActivityVO(List<SendActivityCoupon> sendActivityCouponList) {
        SendActivityVO sendActivityVO = new SendActivityVO();
        sendActivityVO.setId(this.getId());
        sendActivityVO.setActivityName(this.getActivityName());
        sendActivityVO.setStatus(this.getSendStatus());
        SendActivityRule sendActivityRule = new SendActivityRule();
        if(ActivitySendTypeEnums.APPOINT.getCode().equals(this.getSendType())){
            SendActivityRule sendRule = JSON.parseObject(this.getSendRule(),new TypeReference<SendActivityRule>(){});
            sendActivityRule.setSendTime(sendRule.getSendTime()!=null ? sendRule.getSendTime() : null );
            sendActivityRule.setSendUseType(sendRule.getSendUseType()!=null ? sendRule.getSendUseType() : null);
            sendActivityRule.setFileName(sendRule.getFileName()!=null ? BASE_URL + FILE_URL +sendRule.getFileName() : null);
        }
        sendActivityVO.setSendRule(sendActivityRule);
        sendActivityVO.setRemark(this.getRemark());
        sendActivityVO.setSendType(this.getSendType());
        sendActivityVO.setLastOperatorName(this.getLastOperatorName());
        sendActivityVO.setGmtModified(this.getGmtModified());
        sendActivityVO.setActivityType(this.getActivityType());
        if (CollectionUtils.isNotEmpty(sendActivityCouponList)) {
            sendActivityVO.setCouponNameList(sendActivityCouponList.stream().map(SendActivityCoupon :: getCouponName).collect(Collectors.toList()));
            sendActivityVO.setRemarkList(sendActivityCouponList.stream().map(SendActivityCoupon :: getRemark).collect(Collectors.toList()));
        }
        return sendActivityVO;
    }
}
