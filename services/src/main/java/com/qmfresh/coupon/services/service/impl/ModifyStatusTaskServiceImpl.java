/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:ModifyStatusTaskServiceImpl.java
 */
package com.qmfresh.coupon.services.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.manager.IModifyStatusTaskManager;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.CouponMapper;
import com.qmfresh.coupon.services.service.IModifyStatusTaskService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author lxc
 */
@Service
@Slf4j
public class ModifyStatusTaskServiceImpl implements IModifyStatusTaskService {


    @Resource
    private IModifyStatusTaskManager manager;
    @Resource
    private ICouponManager couponManager;
    @Resource
    private CouponMapper mapper;

    @Override
    public void modifyStatus() {
        manager.modifyStatus();
    }

    @Override
    public void modifyCouponStatus() {
        QueryWrapper<Coupon> entityWrapper = new QueryWrapper<>();
        entityWrapper.eq("status", CouponTypeEnums.COUPON_UNUSED.getCode())
                .le("use_time_end", DateUtil.getCurrentTimeIntValue())
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
                .orderByDesc("id");
        //查询500条，根据id排序
        Page<Coupon> page = new Page<>(1, 500);
        IPage<Coupon> couponList = mapper.selectPage(page,entityWrapper);
        while (CollectionUtils.isNotEmpty(couponList.getRecords()) && couponList.getRecords().size() == 500){
            try{
                List<Coupon> coupon = couponList.getRecords().stream().map(o -> {
                    o.setStatus(CouponTypeEnums.COUPON_EXPIRED.getCode());
                    return o;
                }).collect(Collectors.toList());
                couponManager.updateBatchById(coupon);
            }catch (Exception e){
                log.warn("业务处理失败：{}", JSON.toJSONString(couponList));
            }
            int size = couponList.getRecords().size();
            entityWrapper.ge("id",couponList.getRecords().get(size-1).getId());
            couponList = mapper.selectPage(page,entityWrapper);
        }
        //没有满足500，最后处理一遍
        if(CollectionUtils.isNotEmpty(couponList.getRecords())){
            try{
                List<Coupon> couponUp = couponList.getRecords().stream().map(o -> {
                    o.setStatus(CouponTypeEnums.COUPON_EXPIRED.getCode());
                    return o;
                }).collect(Collectors.toList());
                couponManager.updateBatchById(couponUp);
            }catch (Exception e){
                log.warn("业务处理失败：{}", JSON.toJSONString(couponList));
            }
        }
    }

    @Override
    public void modifyCouponSendStatus() {
        manager.modifyCouponSendStatus();
    }
}
