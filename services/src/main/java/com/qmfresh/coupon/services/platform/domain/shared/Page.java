package com.qmfresh.coupon.services.platform.domain.shared;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import com.baomidou.mybatisplus.core.metadata.IPage;

import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class Page<T> implements Serializable {

    /**
     * 页码
     */
    private Integer pageNum;
    /**
     * 页大小
     */
    private Integer pageSize;
    /**
     * 总页数
     */
    private Long totalCount;
    /**
     * 记录列表
     */
    private List<T> records;

}
