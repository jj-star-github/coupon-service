package com.qmfresh.coupon.services.platform.domain.model.template.enums;

import lombok.Getter;

@Getter
public enum TemplateUseTimeTypeEnums {
    TIME_USE_FROM_TO(1, "从。。。到什么"),
    BEGIN_FROM_GET(2, "领券当天起"),
    BEGIN_FROM_TOMORROW(3, "领券此日起");

    private Integer code;

    private String remark;

    TemplateUseTimeTypeEnums(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
