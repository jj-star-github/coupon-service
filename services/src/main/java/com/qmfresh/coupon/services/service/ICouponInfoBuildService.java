/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:CouponInfoBuildService.java
 */
package com.qmfresh.coupon.services.service;

import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;

/**
 * @author lxc
 */
public interface ICouponInfoBuildService {

    /**
     * 组装保存优惠券信息
     * @param userId
//     * @param now
     * @param activity
     * @param sourceOrderNo
     * @param sourceShopId
     * @param limitChannel
     * @return
     */
    Coupon buildCouponIfo(Long userId, CouponTemplate activity,String sourceOrderNo,Integer sourceShopId,Integer limitChannel,Integer cashSend, String couponCode,Integer promotionActivityId);

    /**
     * 判断是否是过期活动，虽然有定时任务，但是双保险
     * @param now
     * @param couponTemplate
     * @return
     */
    boolean isExpiredActivity(int now, CouponTemplate couponTemplate);

    /**
     * 获取优惠券码
     * @return
     */
    String getCouponCode(Integer shopId);
}
