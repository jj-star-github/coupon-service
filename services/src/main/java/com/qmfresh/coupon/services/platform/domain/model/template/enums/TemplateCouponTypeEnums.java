package com.qmfresh.coupon.services.platform.domain.model.template.enums;

import lombok.Getter;

@Getter
public enum TemplateCouponTypeEnums {

    COUPON_MANJIAN(1, "满减"),
    COUPON_DISCOUNT(2, "折扣"),
    COUPON_RANDOM_REDUCE(3, "金额随机立减");

    private Integer code;

    private String remark;

    TemplateCouponTypeEnums(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
