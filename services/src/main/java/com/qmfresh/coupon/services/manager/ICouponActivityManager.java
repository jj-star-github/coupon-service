package com.qmfresh.coupon.services.manager;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponActivityUpdate;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponInfoBean;
import com.qmfresh.coupon.services.entity.CouponActivity;
import com.qmfresh.coupon.services.entity.command.CouponActivityQueryCommand;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
public interface ICouponActivityManager extends IService<CouponActivity> {

    /**
     * 查询已审批通过的优惠券信息
     *
     * @param bean
     * @return
     */
    List<CouponActivity> queryCouponActivityInfo(QueryCouponInfoBean bean);

    /**
     * 分页查询优惠券活动
     *
     * @return
     */
    Page<CouponActivity> pageCouponActivity(CouponActivityQueryCommand couponActivityQueryCommand);

    /**
     * 查询已审批通过的优惠券的个数
     *
     * @param bean
     * @return
     */
    int queryCouponActivityCount(QueryCouponInfoBean bean);

    /**
     * 更新活动的状态
     *
     * @param bean
     * @return
     */
    int updateActivityStatus(CouponActivityUpdate bean);

    /**
     * 删除活动
     *
     * @param id
     * @return
     */
    int delActivity(Integer id);

    /**
     * 根据id查询优惠券活动信息
     *
     * @param id
     * @return
     */
    CouponActivity queryInfoById(Integer id);

    List<CouponActivity> couponActivities(List<Integer> originalIds);
}
