package com.qmfresh.coupon.services.platform.domain.model.activity.enums;

/**
 * @Description:
 * @Version: V1.0
 */
public enum SendCouponTagEnums {

    SEND_COUPON_TAG(1,"发送优惠券业务标识");


    private SendCouponTagEnums(Integer code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    private String detail;
}
