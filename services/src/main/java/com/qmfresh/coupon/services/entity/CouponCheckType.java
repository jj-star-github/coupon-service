package com.qmfresh.coupon.services.entity;

import lombok.Getter;

/**
 * 1.优惠券已失效 2.优惠券已被使用 3. 补打失败，优惠券失败 4.已退货退款
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public enum CouponCheckType {
    /**
     * 优惠券已失效
     */
    EXPIRED(1),
    /**
     * 已使用
     */
    USED(2),
    /**
     * 补打失败
     */
    RECHECK(3),
    /**
     * 已退货退款
     */
    RETURNED(4);

    private Integer code;

    CouponCheckType(Integer code) {
        this.code = code;
    }
}
