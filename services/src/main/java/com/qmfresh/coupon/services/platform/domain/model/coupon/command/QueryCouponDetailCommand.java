package com.qmfresh.coupon.services.platform.domain.model.coupon.command;

import lombok.Data;

@Data
public class QueryCouponDetailCommand {

    private Integer templateId;
    private Long userId;
    private String couponCode;
    private Integer pageNum;
    private Integer pageSize;
    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }

    public QueryCouponDetailCommand(){}

    public QueryCouponDetailCommand(Integer templateId,Long userId,String couponCode,Integer pageNum,Integer pageSize){
        this.templateId = templateId;
        this.userId = userId;
        this.couponCode = couponCode;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }
}
