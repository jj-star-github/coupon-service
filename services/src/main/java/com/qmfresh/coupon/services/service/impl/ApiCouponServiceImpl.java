/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/26
 * Description:CouponApiServiceImpl.java
 */
package com.qmfresh.coupon.services.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.interfaces.dto.promotion.CouponInfo;
import com.qmfresh.coupon.services.platform.application.coupon.vo.CouponCampOnReturnBean;
import com.qmfresh.coupon.services.platform.domain.model.template.*;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateConditionTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateCouponTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateStatusTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateTargetGoodsTypeEnums;
import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.MzContext;
import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.PromotionContext;
import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.PromotionProtocol;
import com.qmfresh.coupon.interfaces.enums.CouponSendTypeEnums;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.interfaces.enums.RequestSourceEnum;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.entity.*;
import com.qmfresh.coupon.services.entity.bo.CouponReturnBo;
import com.qmfresh.coupon.services.entity.bo.CouponReturnParamBo;
import com.qmfresh.coupon.services.entity.command.CouponPrintCheckCommand;
import com.qmfresh.coupon.services.external.service.IPromotionExternalApiService;
import com.qmfresh.coupon.services.manager.*;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponUseManager;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.CouponUse;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.CouponTemplateMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetGoods;
import com.qmfresh.coupon.services.service.DLock;
import com.qmfresh.coupon.services.message.RocketMQSender;
import com.qmfresh.coupon.services.service.IApiCouponService;
import com.qmfresh.coupon.services.service.ICouponInfoBuildService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.eclipse.jetty.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lxc
 */
@Service
@Slf4j
public class ApiCouponServiceImpl implements IApiCouponService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiCouponServiceImpl.class);

    private static final Integer GOODS_CAN_USE = 0;

    private static final Integer GOODS_CAN_NOT_USE = 1;

    @Autowired
    private ICouponManager couponManager;
    @Autowired
    private ICouponSendManager sendManager;
    @Autowired
    private ICouponSendIdsManager idsManager;
    @Autowired
    private ICouponActivityManager activityManager;
    @Autowired
    private ICouponInfoBuildService buildService;
    @Autowired
    private IPromotionExternalApiService promotionExternalApiService;
    @Autowired
    private ICouponUseManager couponUseManager;
    @Resource
    private DLock dLock;
    @Resource
    private DefaultMQProducer defaultMQProducer;
    @Resource
    private ICouponTemplateManager iCouponTemplateManager;
    @Resource
    private ICouponTemplateTargetGoodsManager targetGoodsManager;
    @Resource
    private CouponTemplateMapper couponTemplateMapper;

    @Override
    public Integer queryCouponNumCanUse(UserIdParamBean bean) {
        LOGGER.info("start into method queryCouponNumCanUse,userId:" + JSON.toJSONString(bean.getUserId()));
        // 查询优惠券表，查出此用户的数据
        int couponCount = couponManager.count(new QueryWrapper<Coupon>()
                .eq("is_deleted", 0)
                .eq("user_id", bean.getUserId())
                .notIn("limit_channel", 1)
                .eq("status", CouponTypeEnums.COUPON_UNUSED.getCode()));
        return couponCount;
    }

    @Override
    public ReturnAppBean queryMemCoupon(UserIdParamBean bean) {
        LOGGER.info("start into method queryMemCoupon,userId:" + JSON.toJSONString(bean.getUserId()));
        ReturnAppBean returnAppBean = new ReturnAppBean();
        // 查询优惠券表，查出此用户的数据
        List<Coupon> couponList = couponManager.list(new QueryWrapper<Coupon>()
                .eq("is_deleted", 0)
                .notIn("limit_channel", 1)
                .eq("user_id", bean.getUserId())
                .orderByDesc("use_time_end"));
        // 构造可用列表
        List<ReturnAppCanUseBean> canUseList = new ArrayList<>();
        // 构造不可用列表
        List<ReturnAppCanNotUseBean> unUseList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(couponList)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                for (Coupon coupon : couponList) {
                    // 可用
                    ReturnAppCanUseBean canUseBean = new ReturnAppCanUseBean();
                    // 不可用
                    ReturnAppCanNotUseBean notUseBean = new ReturnAppCanNotUseBean();
                    // 起始时间
                    String timeStart = format.format(new Date(coupon.getUseTimeStart() * 1000L));
                    // 截止时间
                    String timeEnd = format.format(new Date(coupon.getUseTimeEnd() * 1000L));
                    // 现在时间
                    String nowTime = format.format(new Date(DateUtil.getCurrentTimeIntValue() * 1000L));
                    // 有效期展示
                    String periodTime = buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd());
                    CouponTemplate couponTemplate = iCouponTemplateManager.selectById(coupon.getOriginalId());
                    if (coupon.getStatus().equals(CouponStatus.EXPIRED.getCode())
                            || coupon.getStatus().equals(CouponStatus.RETURN.getCode())) {
                        // 状态2的
                        buildUnUseCommonInfo(coupon, notUseBean, periodTime, couponTemplate);
                        notUseBean.setUnUseReason("优惠券已过期");
                        unUseList.add(notUseBean);
                    } else {
                        // 状态0的
                        if ((format.parse(nowTime).getTime() < format.parse(timeStart).getTime())) {
                            // 当前时间小于券开始时间，不能用,或者，券结束时间小于当前时间，不能用
                            buildUnUseCommonInfo(coupon, notUseBean, periodTime, couponTemplate);
                            notUseBean.setUnUseReason("未到使用时间");
                            unUseList.add(notUseBean);
                        } else {
                            Integer expiredFlag = buildExpiredFlag(coupon.getUseTimeEnd());
                            canUseBean.setId(coupon.getId());
                            canUseBean.setCouponName(coupon.getCouponName());
                            canUseBean.setUseInstruction(coupon.getUseInstruction());
                            canUseBean.setValidityPeriod(periodTime);
                            canUseBean.setCouponActivityName(couponTemplate.getCouponName());
                            if (coupon.getCouponType().equals(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode())) {
                                canUseBean.setDiscount(coupon.getCouponDiscount());
                                canUseBean.setPreferentialType(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode());
                            } else {
                                canUseBean.setPreferentialType(CouponTypeEnums.FIXED_AMOUNT_TYPE.getCode());
                            }
                            // 优惠金额
                            canUseBean.setPreferentialMoney(coupon.getSubMoney());
                            // 过期标识
                            canUseBean.setExpiredFlag(expiredFlag);
                            canUseList.add(canUseBean);
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.error("queryMemCoupon something is wrong:" + JSON.toJSONString(bean.getUserId()));
                e.printStackTrace();
                throw new RuntimeException("组装优惠券查询数据发生错误");
            }
        }
        returnAppBean.setCanUseList(canUseList);
        returnAppBean.setUnUseList(unUseList);
        LOGGER.info("return List:" + JSON.toJSONString(returnAppBean));
        return returnAppBean;
    }

    /**
     * 构造不可用对象
     *
     * @param coupon
     * @param notUseBean
     * @param periodTime
     * @param couponTemplate
     */
    private void buildUnUseCommonInfo(Coupon coupon, ReturnAppCanNotUseBean notUseBean, String periodTime, CouponTemplate couponTemplate) {
        notUseBean.setId(coupon.getId());
        notUseBean.setCouponName(coupon.getCouponName());
        notUseBean.setUseInstruction(coupon.getUseInstruction());
        notUseBean.setValidityPeriod(periodTime);
        notUseBean.setCouponActivityName(couponTemplate.getCouponName());
        if (coupon.getCouponType().equals(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode())) {
            notUseBean.setDiscount(coupon.getCouponDiscount());
            notUseBean.setPreferentialType(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode());
        } else {
            notUseBean.setPreferentialType(CouponTypeEnums.FIXED_AMOUNT_TYPE.getCode());
        }
        // 优惠金额
        notUseBean.setPreferentialMoney(coupon.getSubMoney());
    }

    @Override
    public List<ProQueryCouponReturnBean> queryCouponActivityCanUse(ProQueryCouponRequestBean bean) {
        LOGGER.info("start into method queryCouponActivityCanUse,request:" + JSON.toJSONString(bean));
        List<ProQueryCouponReturnBean> returnBeanList = new ArrayList<>();
        int now = DateUtil.getCurrentTimeIntValue();

        // 查询couponTemplate表，得到数据，返回
        List<CouponTemplate> templateList = iCouponTemplateManager.list(new QueryWrapper<CouponTemplate>()
                .notIn("limit_channel", 2)
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
                .eq("status", CouponTypeEnums.COUPON_ACTIVITY_USEING.getCode())
                .orderByDesc("id"));
        if (CollectionUtils.isNotEmpty(templateList)) {
            try {
                for (CouponTemplate template : templateList) {
                    // 判断此活动是否过期，双保险
                    boolean flag = buildService.isExpiredActivity(now, template);
                    if (flag) {
                        continue;
                    }
                    ProQueryCouponReturnBean returnBean = new ProQueryCouponReturnBean();
                    returnBean.setCouponActivityName(template.getCouponName());
                    // 门槛
                    Integer couponCondition = template.getCouponCondition();
                    String couponName = "";
                    // 券类型
                    Integer couponType = template.getCouponType();
                    // 解析活动规则
                    JSONObject couponRule = JSONObject.parseObject(template.getCouponRule());
                    // 得到规则
                    TemplateConditionRule conditionRule = JSONObject.toJavaObject(couponRule, TemplateConditionRule.class);
                    if (couponType.equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())) {
                        couponName = conditionRule.getSubMoney() + "元券";
                    } else if (couponType.equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
                        BigDecimal discount = new BigDecimal(conditionRule.getDiscount());
                        couponName = discount.divide(new BigDecimal(10), 1, RoundingMode.HALF_DOWN) + "折券";
                    } else if (couponType.equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
                        couponName = "随机金额券";
                    }
                    returnBean.setCouponName(couponName);
                    returnBean.setCouponType(couponType);
                    returnBean.setId(template.getId());
                    returnBean.setUseInstruction(template.getUseInstruction());
                    returnBean.setTotalNum(conditionRule.getCouponMaxSendNum().longValue());
                    returnBean.setStillHas(template.getCouponNumber().longValue());
                    returnBean.setCreatedName(template.getCreatedName());
                    returnBean.setCreatedTime((int)template.getGmtCreate().getTime());
                    String validityPeriod = "";
                    JSONObject timeRule = JSONObject.parseObject(template.getUseTimeRule());
                    // 时间规则
                    ActivityTimeRule activityTimeRule = JSONObject.toJavaObject(timeRule, ActivityTimeRule.class);
                    if (template.getUseTimeType().equals(CouponTypeEnums.TIME_USE_FROM_TO.getCode())) {
                        validityPeriod = buildValidityPeriod(activityTimeRule.getTimeStart(), activityTimeRule.getTimeEnd());
                    } else if (template.getUseTimeType().equals(CouponTypeEnums.BEGIN_FROM_GET.getCode())) {
                        validityPeriod = "领券当日起" + activityTimeRule.getTodayCanUse() + "天内可用";
                    } else if (template.getUseTimeType().equals(CouponTypeEnums.BEGIN_FROM_TOMORROW.getCode())) {
                        validityPeriod = "领券次日起" + activityTimeRule.getTomorrowCanUse() + "天内可用";
                    }
                    returnBean.setValidityPeriod(validityPeriod);
                    returnBeanList.add(returnBean);
                }
            } catch (Exception e) {
                log.error("", e);
                throw new RuntimeException("组装返回促销优惠券信息失败");
            }
        }

        return returnBeanList;
    }

    @Override
    public List<CashQuerySendCouponReturnBean> cashTriggerSendCoupon(CashQuerySendCouponRequestBean bean, Boolean cashTriggerSendCoupon) {
        LOGGER.info("start into method cashTriggerSendCoupon,request:{} {}", JSON.toJSONString(bean), JSON.toJSONString(cashTriggerSendCoupon));
        bean.setUserId(0L);
        // 返回
        List<CashQuerySendCouponReturnBean> returnBeanList = new ArrayList<>();
        // 查询券活动表的id,是否是已审批通过，是保存优惠券并返回给收银端，否则返回空
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        // 拟发放数量
        Integer sendNum = bean.getSendNum();
        // 优惠券活动id
        Integer activityId = bean.getActivityId();
        // 促销活动id
        Integer promotionId = bean.getPromotionId();
        //优惠券绑定的订单id
        String sourceOrderNo = bean.getSourceOrderNo();
        //校验优惠券是否可以发券
        //根据sourceOrderNo查询该券是否已发放
        if (StringUtils.isNotEmpty(sourceOrderNo) && StringUtils.isNotBlank(sourceOrderNo)) {
            List<Coupon> existCoupon = couponManager.list(new QueryWrapper<Coupon>()
                    .eq("source_order_no", sourceOrderNo)
                    .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
            if (CollectionUtils.isNotEmpty(existCoupon)) {
                LOGGER.info("{}已发过优惠券", JSON.toJSONString(bean));
                throw new BusinessException("该订单已经发放了优惠券");
            }
        }

        CouponTemplate activity = couponTemplateMapper.queryRunInById(activityId);

        //未匹配到活动
        if (activity == null) {
            LOGGER.info("活动不存在 {}", bean.getActivityId());
            return returnBeanList;
        }
        if (cashTriggerSendCoupon) {
            if (promotionId == null) {
                LOGGER.info("促销活动不存在 sourceOrderNo={} promotionId={}", sourceOrderNo, bean.getPromotionId());
                return returnBeanList;
            }
            String meetMoney = this.getMeetMoneyNew(promotionId.longValue());
            if (StringUtils.isBlank(meetMoney)) {
                LOGGER.info("促销价格不存在 sourceOrderNo={}, promotionId={}", sourceOrderNo, promotionId);
                return returnBeanList;
            }
            double meetMoney2 = new Double(meetMoney);
            if (bean.getRealAmount() != null && bean.getRealAmount().doubleValue() < meetMoney2) {
                LOGGER.info("活动金额不满足：{} {} {} {}", sourceOrderNo, promotionId, meetMoney2, bean.getRealAmount());
                return returnBeanList;
            }
        }

        if (CollectionUtils.isNotEmpty(bean.getCouponCode())) {
            sendNum = bean.getCouponCode().size();
        }
        for (int i = 0; i < sendNum; i++) {
            String couponCode = null;
            if (CollectionUtils.isNotEmpty(bean.getCouponCode())) {
                couponCode = bean.getCouponCode().get(i);
            }
            Coupon coupon = buildService.buildCouponIfo(bean.getUserId(), activity, sourceOrderNo, bean.getSourceShopId(), bean.getLimitChannel(), 1, couponCode, null);
//            LOGGER.info("组装优惠券信息，源:{},组装后:{}", JSON.toJSONString(bean), JSON.toJSONString(coupon));
            // 保存优惠券
            couponManager.save(coupon);
            //                //拆表后插入逻辑
            // 组装返回信息
            CashQuerySendCouponReturnBean returnBean = new CashQuerySendCouponReturnBean();
            returnBean.setCouponId(coupon.getId());
            returnBean.setCouponName(coupon.getCouponName());
            returnBean.setCouponCode(coupon.getCouponCode());
            returnBean.setUseInstruction(coupon.getUseInstruction());
            // 有效期
            returnBean.setValidityPeriod(buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd()));
            //来源订单号
            returnBean.setSourceOrderNo(coupon.getSourceOrderNo());
            returnBeanList.add(returnBean);
        }
        return returnBeanList;
    }

    @Override
    public List<CouponForAppReturnBean> queryCouponByUserId(CouponForAppRequestBean bean) {
        log.info("商城查询优惠券， param={}", JSON.toJSONString(bean));
        List<CouponForAppReturnBean> couponForAppReturnBeans = new ArrayList<>();
        QueryWrapper<Coupon> entityWrapper = new QueryWrapper<>();
        entityWrapper.eq("user_id", bean.getUserId());
        entityWrapper.eq("is_deleted", 0);
        entityWrapper.eq("status", 0);
        entityWrapper.notIn("limit_channel", 1);
        int now = DateUtil.getCurrentTimeIntValue();
        entityWrapper.le("use_time_start", now);
        entityWrapper.ge("use_time_end", now);
        List<Coupon> couponList = couponManager.list(entityWrapper);
        if (couponList == null) {
            return couponForAppReturnBeans;
        }
        couponList.forEach(
                coupon -> {
                    CouponForAppReturnBean couponForApp = new CouponForAppReturnBean();
                    couponForApp.setCouponId(coupon.getId());
                    couponForApp.setCouponName(coupon.getCouponName());
                    couponForApp.setCouponCode(coupon.getCouponCode());
                    couponForApp.setCouponCondition(coupon.getCouponCondition());
                    couponForApp.setNeedMoney(coupon.getNeedMoney());
                    couponForApp.setCouponType(coupon.getCouponType());
                    couponForApp.setSubMoney(coupon.getSubMoney());
                    couponForApp.setCouponDiscount(coupon.getCouponDiscount());
                    if (coupon.getCouponType().equals(3)) {
                        CouponTemplate couponTemplate = iCouponTemplateManager.selectById(coupon.getOriginalId());
                        ActivityConditionRule conditionRule = JSON.parseObject(couponTemplate.getCouponRule(), new TypeReference<ActivityConditionRule>() {
                        });
                        couponForApp.setRandomMoneyStart(conditionRule.getRandomMoneyStart());
                        couponForApp.setRandomMoneyEnd(conditionRule.getRandomMoneyEnd());
                        if (coupon.getCouponCondition().equals(1)) {
                            couponForApp.setCouponRemark("满" + coupon.getNeedMoney() + "减" + conditionRule.getRandomMoneyStart() + "-" + conditionRule.getRandomMoneyEnd());
                        } else if (coupon.getCouponCondition().equals(0)) {
                            couponForApp.setCouponRemark(conditionRule.getRandomMoneyStart() + "-" + conditionRule.getRandomMoneyEnd() + "元随机抵扣");
                        }
                    }
                    if (coupon.getCouponCondition().equals(1)) {
                        if (coupon.getCouponType().equals(1)) {
                            couponForApp.setCouponRemark("满" + coupon.getNeedMoney() + "减" + coupon.getSubMoney().setScale(0, BigDecimal.ROUND_DOWN));
                        } else if (coupon.getCouponType().equals(2)) {
                            couponForApp.setCouponRemark("满" + coupon.getNeedMoney() + "享" + coupon.getCouponDiscount() / 10 + "折");
                        }
                    } else if (coupon.getCouponCondition().equals(0)) {
                        if (coupon.getCouponType().equals(1)) {
                            couponForApp.setCouponRemark(coupon.getSubMoney().setScale(0, BigDecimal.ROUND_DOWN) + "元抵扣");
                        } else if (coupon.getCouponType().equals(2)) {
                            couponForApp.setCouponRemark(coupon.getCouponDiscount() / 10 + "折");
                        }
                    }
                    couponForApp.setApplicableGoodsType(coupon.getApplicableGoodsType());
                    couponForApp.setClass1Ids(coupon.getClass1Ids());
                    couponForApp.setClass2Ids(coupon.getClass2Ids());
                    couponForAppReturnBeans.add(couponForApp);
                }
        );
        return couponForAppReturnBeans;
    }

    @Override
    public ListWithPage<ProQueryCouponReturnBean> queryUseingCoupon(QueryCouponInfoBean bean) {
        log.info("into method queryUseingCoupon,入参:" + JSON.toJSONString(bean));
        ListWithPage<ProQueryCouponReturnBean> page = new ListWithPage<ProQueryCouponReturnBean>();
        List<ProQueryCouponReturnBean> returnBeanList = new ArrayList<>();
        // 查询个数
        bean.setCouponStatus(2);
        int count = couponTemplateMapper.queryCount(bean.getCouponStatus(),bean.getCouponType(),null,null,null,null,null);
        if (0 >= count) {
            return page;
        }
        page.setTotalCount(count);
        List<CouponTemplate> templateList = couponTemplateMapper.queryByCondition(bean.getCouponStatus(),bean.getCouponType(),null,null,null,null,null,bean.getStart(),bean.getEnd());
        if (CollectionUtils.isEmpty(templateList)) {
            return page;
        }
        try {
            for (CouponTemplate couponTemplate : templateList) {
                ProQueryCouponReturnBean returnBean = new ProQueryCouponReturnBean();
                returnBean.setCouponActivityName(couponTemplate.getCouponName());
                // 门槛
                String couponName = "";
                // 券类型
                Integer couponType = couponTemplate.getCouponType();
                // 解析活动规则
                JSONObject couponRule = JSONObject.parseObject(couponTemplate.getCouponRule());
                // 得到规则
                ActivityConditionRule conditionRule = JSONObject.toJavaObject(couponRule, ActivityConditionRule.class);
                if (couponType.equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())) {
                    couponName = conditionRule.getSubMoney() + "元券";
                } else if (couponType.equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
                    BigDecimal discount = new BigDecimal(conditionRule.getDiscount());
                    couponName = discount.divide(new BigDecimal(10), 1, RoundingMode.HALF_DOWN) + "折券";
                } else if (couponType.equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
                    couponName = "随机金额券";
                }
                returnBean.setCouponName(couponName);
                returnBean.setCouponType(couponType);
                returnBean.setId(couponTemplate.getId());
                returnBean.setUseInstruction(couponTemplate.getUseInstruction());
                returnBean.setCreatedName(couponTemplate.getCreatedName());
                returnBean.setCreatedTime((int)couponTemplate.getGmtCreate().getTime());
                String validityPeriod = "";
                JSONObject timeRule = JSONObject.parseObject(couponTemplate.getUseTimeRule());
                // 时间规则
                ActivityTimeRule activityTimeRule = JSONObject.toJavaObject(timeRule, ActivityTimeRule.class);
                if (couponTemplate.getUseTimeType().equals(CouponTypeEnums.TIME_USE_FROM_TO.getCode())) {
                    validityPeriod = buildValidityPeriod(activityTimeRule.getTimeStart(), activityTimeRule.getTimeEnd());
                } else if (couponTemplate.getUseTimeType().equals(CouponTypeEnums.BEGIN_FROM_GET.getCode())) {
                    validityPeriod = "领券当日起" + activityTimeRule.getTodayCanUse() + "天内可用";
                } else if (couponTemplate.getUseTimeType().equals(CouponTypeEnums.BEGIN_FROM_TOMORROW.getCode())) {
                    validityPeriod = "领券次日起" + activityTimeRule.getTomorrowCanUse() + "天内可用";
                }
                returnBean.setValidityPeriod(validityPeriod);
                //商城拉新 新增字段
                returnBean.setStatus(couponTemplate.getStatus());
                returnBean.setCouponCondition(couponTemplate.getCouponCondition());
                returnBean.setLimitChannel(couponTemplate.getLimitChannel());
                ActivityConditionRule rule = JSON.parseObject(couponTemplate.getCouponRule(), new TypeReference<ActivityConditionRule>() {
                });
                if (couponTemplate.getCouponType().equals(2)) {
                    returnBean.setDiscount(rule.getDiscount());
                } else if (couponTemplate.getCouponType().equals(3)) {
                    returnBean.setRandomMoneyStart(rule.getRandomMoneyStart());
                    returnBean.setRandomMoneyEnd(rule.getRandomMoneyEnd());
                } else if (couponTemplate.getCouponType().equals(1)) {
                    returnBean.setSubMoney(rule.getSubMoney());
                }
                if (couponTemplate.getCouponCondition().equals(1)) {
                    returnBean.setNeedMoney(couponTemplate.getNeedMoney());
                }
                returnBeanList.add(returnBean);
            }
        } catch (Exception e) {
            log.error("", e);
            throw new RuntimeException("组装返回促销优惠券信息失败");
        }
        page.setListData(returnBeanList);
        return page;
    }

    @Override
    public CouponReturnBo returnCoupon(CouponReturnParamBo couponReturnParamBo) {

        Coupon coupon = couponManager.queryUseCoupon(couponReturnParamBo.getSourceOrderCode());
        if (coupon == null) {
            return CouponReturnBo.notUsed(couponReturnParamBo.getSourceOrderCode());
        }

        log.info("开始退还优惠券:couponCode= {}  sourceOrderCode={}", coupon.getCouponCode(), couponReturnParamBo.getSourceOrderCode());
        int i = couponManager.returnCoupon(coupon, couponReturnParamBo.getSourceOrderCode());

        if (i == 1) {
            return CouponReturnBo.success(coupon);
        }
        return CouponReturnBo.expired(coupon);
    }

    private String getMeetMoneyNew(Long promotionId) {
        ServiceResult<String> result = promotionExternalApiService.getPromotionDetailNew(promotionId);
        if (null == result || !result.getSuccess()) {
            return null;
        }
        LOGGER.info("促销返回信息:{} {}", promotionId, JSON.toJSONString(result));
        JSONObject promotionInfo = JSONObject.parseObject(result.getBody());
        return promotionInfo.getString("meetMoney");
    }


    @Override
    public CouponCampOnReturnBean queryCouponInfo(Integer activityId, Integer shopId, Integer sendNum) {
        CouponCampOnReturnBean returnBean = new CouponCampOnReturnBean();
        CouponActivity activity = activityManager.getOne(new QueryWrapper<CouponActivity>()
                .eq("id", activityId)
                .eq("status", CouponTypeEnums.COUPON_ACTIVITY_USEING.getCode())
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
        if (null == activity) {
            LOGGER.info("优惠券查询失败 activityId={}", activityId);
            return returnBean;
        }
        // 券类型
        Integer couponType = activity.getCouponType();
        // 门槛
        Integer couponCondition = activity.getCouponCondition();
        String couponName = "优惠券";
        if (couponCondition.equals(CouponTypeEnums.NO_USE_CONDITION.getCode())) {
            // 无门槛使用
            couponName = "无门槛券";
        } else {
            if (couponType.equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())) {
                couponName = "满减券";
            } else if (couponType.equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
                couponName = "折扣券";
            } else if (couponType.equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
                couponName = "立减券";
            }
        }
        // 组装返回信息
        returnBean.setCouponId(activityId);
        returnBean.setCouponName(couponName);
        List<String> couponCodeList = new ArrayList<>();
        for (int i = 0; i < sendNum; i++) {
            couponCodeList.add(buildService.getCouponCode(shopId));
        }
        returnBean.setCouponCodeList(couponCodeList);
        returnBean.setUseInstruction(activity.getUseInstruction());

        // 解析时间规则
        TemplateTimeRule timeRule = JSON.parseObject(activity.getUseTimeRule(),new TypeReference<TemplateTimeRule>(){});
        StartEndTime startEndTime = CouponSendByTemplate.buildStartEndTime(timeRule,activity.getUseTimeType());
        returnBean.setValidityPeriod(buildValidityPeriod(startEndTime.getTimeStart(), startEndTime.getTimeEnd()));
        return returnBean;
    }

    /**
     * 已分表
     *
     * @param bean
     * @return
     */
    @Override
    public ReturnAppBean settQueryMemCoupon(SettQueryCouponRequestBean bean) {
        LOGGER.info("start into method settQueryMemCoupon,request:" + JSON.toJSONString(bean));
        ReturnAppBean returnAppBean = new ReturnAppBean();
        // 当前时间 00:00:00
        int now = DateUtil.getCurrentTimeIntValue();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // 查询这个会员所有未使用的优惠券
            List<Coupon> couponList = couponManager.list(new QueryWrapper<Coupon>()
                    .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
                    .eq("user_id", bean.getUserId())
                    .eq("status", CouponTypeEnums.COUPON_UNUSED.getCode())
                    .notIn("limit_channel", 1)
                    .ge("use_time_end", now)
                    .le("use_time_start", now)
                    .orderByDesc("use_time_end")
                    .orderByAsc("c_t"));

            // 可用列表
            List<ReturnAppCanUseBean> canUseList = new ArrayList<>();
            // 不可用列表
            List<ReturnAppCanNotUseBean> unUseList = new ArrayList<>();
            // 看适用商品
            if (CollectionUtils.isNotEmpty(couponList)) {
                Map<Integer, CouponTemplate> couponTemplateMap = new HashMap<>();
                List<Integer> templateIds = couponList.stream().map(Coupon::getOriginalId).collect(Collectors.toList());
        // 查询券活动
        List<CouponTemplate> templateList =
            iCouponTemplateManager.queryByIdList(
                templateIds, TemplateStatusTypeEnums.IN_USE_STATUS.getCode());
                templateList.forEach(
                        couponTemplate -> {
                            couponTemplateMap.put(couponTemplate.getId(), couponTemplate);
                        }
                );
                for (Coupon coupon : couponList) {
                    // 获取券活动
                    CouponTemplate couponTemplate = couponTemplateMap.get(coupon.getOriginalId());
                    //验证小时段
                    boolean availableResult = CouponActivity.inAvailableRuleTime(couponTemplate);
                    if (!availableResult) {
                        continue;
                    }
                    ReturnAppCanUseBean canUseBean = new ReturnAppCanUseBean();
                    ReturnAppCanNotUseBean notUseBean = new ReturnAppCanNotUseBean();
                    // 得到适用商品类型
                    Integer applicableType = couponTemplate.getApplicableGoodsType();
                    Integer couponCondition = couponTemplate.getCouponCondition();
                    // 1级分类
                    List<SettlementClass1Bean> class1BeanList = bean.getClass1Ids();
                    // 2级分类
                    List<SettlementClass2Bean> class2BeanList = bean.getClass2Ids();
                    // 券类型
                    Integer couponType = coupon.getCouponType();
                    // 所有商品可用，则判断金额是否满足
                    buildInfoByApplicableGoods(couponTemplate, bean, coupon, canUseBean, notUseBean, applicableType, couponCondition, class1BeanList, class2BeanList, couponType,
                            canUseList, unUseList);
                }
            }
            returnAppBean.setCanUseList(canUseList);
            returnAppBean.setUnUseList(unUseList);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("组装结算页查询优惠券数据发生错误");
        }
        return returnAppBean;
    }

    @Override
    public ListWithPage<UnusedCouponBean> queryMyUnusedCouponWithPage(SettQueryCouponRequestBean bean) {
        ListWithPage<UnusedCouponBean> resultWithPage = new ListWithPage<>();
        ReturnAppBean rawCoupon = this.settQueryMemCoupon(bean);
        if (rawCoupon == null) {
            return new ListWithPage<>();
        }
        Integer canuseCount = rawCoupon.getCanUseList() == null ? 0 : rawCoupon.getCanUseList().size();
        Integer cannotCount = rawCoupon.getUnUseList() == null ? 0 : rawCoupon.getUnUseList().size();
        Integer totalCount = cannotCount + canuseCount;
        List<UnusedCouponBean> unusedCouponBeanList = new ArrayList<>();
        for (ReturnAppCanUseBean returnAppCanUseBean : rawCoupon.getCanUseList()) {
            UnusedCouponBean unusedCouponBean = new UnusedCouponBean();
            BeanUtils.copyProperties(returnAppCanUseBean, unusedCouponBean);
            unusedCouponBeanList.add(unusedCouponBean);
        }
        for (ReturnAppCanNotUseBean returnAppCanNotUseBean : rawCoupon.getUnUseList()) {
            UnusedCouponBean unusedCouponBean = new UnusedCouponBean();
            BeanUtils.copyProperties(returnAppCanNotUseBean, unusedCouponBean);
            unusedCouponBeanList.add(unusedCouponBean);
        }
        if (CollectionUtils.isEmpty(unusedCouponBeanList)) {
            return new ListWithPage<>();
        }
        List<UnusedCouponBean> currentPageList = unusedCouponBeanList.subList((bean.getPageIndex() - 1) * bean.getPageSize()
                , bean.getPageIndex() * bean.getPageSize() > unusedCouponBeanList.size() ? unusedCouponBeanList.size() : bean.getPageIndex() * bean.getPageSize());
        resultWithPage.setTotalCount(totalCount);
        resultWithPage.setListData(currentPageList);
        return resultWithPage;
    }

    /**
     * 根据优惠券满足商品的类型判断
     *
     * @param bean
     * @param coupon
     * @param canUseBean
     * @param notUseBean
     * @param applicableType
     * @param couponCondition
     * @param class1BeanList
     * @param class2BeanList
     * @param couponType
     */
    private void buildInfoByApplicableGoods(CouponTemplate couponTemplate, SettQueryCouponRequestBean bean, Coupon coupon, ReturnAppCanUseBean canUseBean,
                                            ReturnAppCanNotUseBean notUseBean, Integer applicableType, Integer couponCondition, List<SettlementClass1Bean> class1BeanList, List<SettlementClass2Bean> class2BeanList,
                                            Integer couponType, List<ReturnAppCanUseBean> canUseList, List<ReturnAppCanNotUseBean> unUseList) {
        Integer id = coupon.getId();
        String validityPeriod = buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd());
        String useInstruction = coupon.getUseInstruction();
        String activityName = couponTemplate.getCouponName();
        String couponName = coupon.getCouponName();
        Integer expiredFlag = buildExpiredFlag(coupon.getUseTimeEnd());

        if (applicableType.equals(TemplateTargetGoodsTypeEnums.ALL_GOODS.getCode())) {
            buildAllGoodsThatCanUse(couponTemplate, couponCondition, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, notUseBean, expiredFlag, unUseList, bean);

        } else if(applicableType.equals(TemplateTargetGoodsTypeEnums.CLASS1_GOODS.getCode()) || applicableType.equals(TemplateTargetGoodsTypeEnums.CLASS2_GOODS.getCode())){
            buildSomeGoodsThatCanUse(couponTemplate, couponCondition, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, notUseBean, expiredFlag, unUseList,
                    class1BeanList, class2BeanList);

        }
    }

    /**
     * 所有商品可用
     *
     * @param couponCondition
     * @param id
     * @param validityPeriod
     * @param useInstruction
     * @param activityName
     * @param couponName
     * @param coupon
     * @param canUseBean
     * @param couponType
     * @param canUseList
     * @param notUseBean
     * @param expiredFlag
     * @param unUseList
     * @param bean
     */
    private void buildAllGoodsThatCanUse(CouponTemplate couponTemplate, Integer couponCondition, Integer id, String validityPeriod, String useInstruction, String activityName, String couponName, Coupon coupon, ReturnAppCanUseBean canUseBean,
                                         Integer couponType, List<ReturnAppCanUseBean> canUseList, ReturnAppCanNotUseBean notUseBean, Integer expiredFlag, List<ReturnAppCanNotUseBean> unUseList,
                                         SettQueryCouponRequestBean bean) {
        // 判断使用门槛
        // 无门槛
        if (couponCondition.equals(TemplateConditionTypeEnums.NO_USE_CONDITION.getCode())) {
            buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
        } else {
            // 查出需要满足的金额，看看订单总金额满足与否
            // 金额
            BigDecimal needMoney = new BigDecimal(coupon.getNeedMoney());
            if (bean.getTotalAmount().compareTo(needMoney) >= 0) {
                // 满足条件
                buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
            } else {
                buildCanNotUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, notUseBean, couponType, "金额不满足使用条件", unUseList);
            }
        }
    }

    /**
     * 指定商品可用
     *
     * @param couponCondition
     * @param id
     * @param validityPeriod
     * @param useInstruction
     * @param activityName
     * @param couponName
     * @param coupon
     * @param canUseBean
     * @param couponType
     * @param canUseList
     * @param notUseBean
     * @param expiredFlag
     * @param unUseList
     * @param class1BeanList
     * @param class2BeanList
     */
    private void buildSomeGoodsThatCanUse(CouponTemplate couponTemplate, Integer couponCondition, Integer id, String validityPeriod, String useInstruction, String activityName, String couponName, Coupon coupon, ReturnAppCanUseBean canUseBean,
                                          Integer couponType, List<ReturnAppCanUseBean> canUseList, ReturnAppCanNotUseBean notUseBean, Integer expiredFlag, List<ReturnAppCanNotUseBean> unUseList,
                                          List<SettlementClass1Bean> class1BeanList, List<SettlementClass2Bean> class2BeanList) {
        // 指定商品可用
        // 无门槛
        // 取出数据库的一级分类
        List<CouponTemplateTargetGoods> goodsList = targetGoodsManager.queryByTemplateId(couponTemplate.getId());
        Integer goodsType = couponTemplate.getApplicableGoodsType();
        List<String> goodsIdList = goodsList.stream().map(CouponTemplateTargetGoods::getGoodsId).collect(Collectors.toList());
//        List<Integer> class1IdList = JSON.parseArray(coupon.getClass1Ids(), Integer.class);
//        //  取出数据库的二级分类
//        List<Integer> class2IdList = JSON.parseArray(coupon.getClass2Ids(), Integer.class);
        if (couponCondition.equals(TemplateConditionTypeEnums.NO_USE_CONDITION.getCode())) {
            // 看看商品是否满足条件
            if (CollectionUtils.isNotEmpty(goodsIdList) && goodsType.equals(TemplateTargetGoodsTypeEnums.CLASS2_GOODS.getCode())) {
                boolean flag = false;
                for (SettlementClass2Bean class2Bean : class2BeanList) {
                    if (goodsIdList.contains(class2Bean.getClass2Id().toString())) {
                        flag = true;
                        break;
                    }
                }
                // 如果存在满足条件的二级分类
                if (flag) {
                    // can
                    buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
                } else {
                    // can not
                    buildCanNotUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, notUseBean, couponType, "没有满足条件的商品类型", unUseList);
                }
            } else if (CollectionUtils.isNotEmpty(goodsIdList) && goodsType.equals(TemplateTargetGoodsTypeEnums.CLASS1_GOODS.getCode())){
                boolean flag = false;
                for (SettlementClass1Bean class1Bean : class1BeanList) {
                    if (goodsIdList.contains(class1Bean.getClass1Id().toString())) {
                        flag = true;
                        break;
                    }
                }
                // 如果存在满足条件的一级分类
                if (flag) {
                    // can
                    buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
                } else {
                    // can not
                    buildCanNotUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, notUseBean, couponType, "没有满足条件的商品类型", unUseList);
                }
            }
        } else {
            // 需要满足金额才能用
            Integer needMoney = coupon.getNeedMoney();
            if (CollectionUtils.isNotEmpty(goodsIdList) && goodsType.equals(TemplateTargetGoodsTypeEnums.CLASS2_GOODS.getCode())) {
                // 二级分类不为空，按二级分类算
                BigDecimal class2Amount = BigDecimal.ZERO;
                for (SettlementClass2Bean class2Bean : class2BeanList) {
                    // 把满足条件的二级分类的金额取出来
                    if (goodsIdList.contains(class2Bean.getClass2Id().toString())) {
                        class2Amount = class2Amount.add(class2Bean.getClass2Amount());
                    }
                }
                if (class2Amount.compareTo(new BigDecimal(needMoney)) >= 0) {
                    // 金额够，可以用
                    buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
                } else {
                    // 金额不够不能用
                    buildCanNotUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, notUseBean, couponType, "满足条件的商品总金额不足", unUseList);
                }
            } else if (CollectionUtils.isNotEmpty(goodsIdList) && goodsType.equals(TemplateTargetGoodsTypeEnums.CLASS1_GOODS.getCode())){
                // 按一级分类算
                BigDecimal class1Amount = BigDecimal.ZERO;
                for (SettlementClass1Bean class1Bean : class1BeanList) {
                    // 把满足条件的二级分类的金额取出来
                    if (goodsIdList.contains(class1Bean.getClass1Id().toString())) {
                        class1Amount = class1Amount.add(class1Bean.getClass1Amount());
                    }
                }
                if (class1Amount.compareTo(new BigDecimal(needMoney)) >= 0) {
                    // 金额够，可以用
                    buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
                } else {
                    // 金额不够不能用
                    buildCanNotUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, notUseBean, couponType, "满足条件的商品总金额不足", unUseList);
                }
            }
        }
    }

    /**
     * 指定商品不可用
     *
     * @param couponCondition
     * @param id
     * @param validityPeriod
     * @param useInstruction
     * @param activityName
     * @param couponName
     * @param coupon
     * @param canUseBean
     * @param couponType
     * @param canUseList
     * @param notUseBean
     * @param expiredFlag
     * @param unUseList
     * @param class1BeanList
     * @param class2BeanList
     */
    private void buildSomeGoodsThatCanNotUse(CouponTemplate couponTemplate, Integer couponCondition, Integer id, String validityPeriod, String useInstruction, String activityName, String couponName, Coupon coupon, ReturnAppCanUseBean canUseBean,
                                             Integer couponType, List<ReturnAppCanUseBean> canUseList, ReturnAppCanNotUseBean notUseBean, Integer expiredFlag, List<ReturnAppCanNotUseBean> unUseList,
                                             List<SettlementClass1Bean> class1BeanList, List<SettlementClass2Bean> class2BeanList) {
        // 指定商品不可用
        // 无门槛，如果list里面不包含，则可用
        // 取出数据库的一级分类
        List<Integer> class1IdList = JSON.parseArray(coupon.getClass1Ids(), Integer.class);
        //  取出数据库的二级分类
        List<Integer> class2IdList = JSON.parseArray(coupon.getClass2Ids(), Integer.class);
        if (couponCondition.equals(CouponTypeEnums.NO_USE_CONDITION.getCode())) {
            // 看看商品是否满足条件
            if (StringUtil.isNotBlank(coupon.getClass2Ids())) {
                boolean flag = false;
                for (SettlementClass2Bean class2Bean : class2BeanList) {
                    if (!class2IdList.contains(class2Bean.getClass2Id())) {
                        flag = true;
                        break;
                    }
                }
                // 如果存在满足条件的二级分类
                if (flag) {
                    // can
                    buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
                } else {
                    // can not
                    buildCanNotUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, notUseBean, couponType, "没有满足条件的商品类型", unUseList);
                }
            } else {
                boolean flag = false;
                for (SettlementClass1Bean class1Bean : class1BeanList) {
                    if (!class1IdList.contains(class1Bean.getClass1Id())) {
                        flag = true;
                        break;
                    }
                }
                // 如果存在满足条件的一级分类
                if (flag) {
                    // can
                    buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
                } else {
                    // can not
                    buildCanNotUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, notUseBean, couponType, "没有满足条件的商品类型", unUseList);
                }
            }
        } else {
            // 需要满足金额才能用
            Integer needMoney = coupon.getNeedMoney();
            if (StringUtils.isNotBlank(coupon.getClass2Ids())) {
                // 二级分类不为空，按二级分类算
                BigDecimal class2Amount = BigDecimal.ZERO;
                for (SettlementClass2Bean class2Bean : class2BeanList) {
                    // 把满足条件的二级分类的金额取出来
                    if (!class2IdList.contains(class2Bean.getClass2Id())) {
                        class2Amount = class2Amount.add(class2Bean.getClass2Amount());
                    }
                }
                if (class2Amount.compareTo(new BigDecimal(needMoney)) >= 0) {
                    // 金额够，可以用
                    buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
                } else {
                    // 金额不够不能用
                    buildCanNotUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, notUseBean, couponType, "满足条件的商品总金额不足", unUseList);
                }
            } else {
                // 按一级分类算
                BigDecimal class1Amount = BigDecimal.ZERO;
                for (SettlementClass1Bean class1Bean : class1BeanList) {
                    // 把满足条件的二级分类的金额取出来
                    if (!class1IdList.contains(class1Bean.getClass1Id())) {
                        class1Amount = class1Amount.add(class1Bean.getClass1Amount());
                    }
                }
                if (class1Amount.compareTo(new BigDecimal(needMoney)) >= 0) {
                    // 金额够，可以用
                    buildCanUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, canUseBean, couponType, canUseList, expiredFlag);
                } else {
                    // 金额不够不能用
                    buildCanNotUseInfo(couponTemplate, id, validityPeriod, useInstruction, activityName, couponName, coupon, notUseBean, couponType, "满足条件的商品总金额不足", unUseList);
                }
            }
        }
    }

    /**
     * 不能用的券信息
     *
     * @param coupon
     * @param notUseBean
     * @param couponType
     * @param reason
     */
    private void buildCanNotUseInfo(CouponTemplate couponTemplate, Integer id, String validityPeriod, String useInstruction, String activityName, String couponName, Coupon coupon,
                                    ReturnAppCanNotUseBean notUseBean, Integer couponType, String reason, List<ReturnAppCanNotUseBean> unUseList) {
        if (couponType.equals(TemplateCouponTypeEnums.COUPON_MANJIAN.getCode()) || couponType.equals(TemplateCouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
            // 满减或者随机金额
            // 前端展示固定金额样式
            notUseBean.setPreferentialType(CouponTypeEnums.FIXED_AMOUNT_TYPE.getCode());
        } else if (couponType.equals(TemplateCouponTypeEnums.COUPON_DISCOUNT.getCode())) {
            // 折扣类型
            notUseBean.setPreferentialType(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode());
            notUseBean.setDiscount(coupon.getCouponDiscount());
        }
        notUseBean.setApplicableGoodsType(coupon.getApplicableGoodsType());
        notUseBean.setPreferentialMoney(coupon.getSubMoney());
        notUseBean.setUnUseReason(reason);
        notUseBean.setId(id);
        notUseBean.setValidityPeriod(validityPeriod);
        notUseBean.setUseInstruction(useInstruction);
        notUseBean.setCouponActivityName(activityName);
        notUseBean.setCouponName(couponName);

        notUseBean.setCouponCode(coupon.getCouponCode());

        notUseBean.setCouponId(coupon.getId());

        notUseBean.setStatus(coupon.getStatus());

        notUseBean.setSourceShopId(coupon.getSourceShopId());

        notUseBean.setUseShopId(coupon.getUseShopId());

        notUseBean.setLimitChannel(coupon.getLimitChannel());

        ActivityTimeRule timeRule = JSON.parseObject(couponTemplate.getUseTimeRule(), new TypeReference<ActivityTimeRule>() {
        });
        if (timeRule.getIsAllDay() != null && timeRule.getIsAllDay().equals(1)) {
            notUseBean.setUseHMSRule(timeRule.getHmsStart() + "--" + timeRule.getHmsEnd());
        } else {
            notUseBean.setUseHMSRule("全天可用");
        }
        // 不可用
        unUseList.add(notUseBean);
    }

    /**
     * 能用的券信息
     *
     * @param coupon
     * @param canUseBean
     * @param couponType
     */
    private void buildCanUseInfo(CouponTemplate couponTemplate, Integer id, String validityPeriod, String useInstruction, String activityName, String couponName, Coupon coupon, ReturnAppCanUseBean canUseBean,
                                 Integer couponType, List<ReturnAppCanUseBean> canUseList, Integer expiredFlag) {
        if (couponType.equals(TemplateCouponTypeEnums.COUPON_MANJIAN.getCode()) || couponType.equals(TemplateCouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
            // 满减或者随机金额
            // 前端展示固定金额样式
            canUseBean.setPreferentialType(CouponTypeEnums.FIXED_AMOUNT_TYPE.getCode());
        } else if (couponType.equals(TemplateCouponTypeEnums.COUPON_DISCOUNT.getCode())) {
            // 折扣类型
            canUseBean.setPreferentialType(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode());
            canUseBean.setDiscount(coupon.getCouponDiscount());
        }
        canUseBean.setApplicableGoodsType(coupon.getApplicableGoodsType());
        canUseBean.setPreferentialMoney(coupon.getSubMoney());
        canUseBean.setId(id);
        // 有效期
        canUseBean.setValidityPeriod(validityPeriod);
        // 使用说明
        canUseBean.setUseInstruction(useInstruction);
        // 券活动名称
        canUseBean.setCouponActivityName(activityName);
        // 券名称
        canUseBean.setCouponName(couponName);
        // 过期标识
        canUseBean.setExpiredFlag(expiredFlag);

        canUseBean.setCouponCode(coupon.getCouponCode());

        canUseBean.setCouponId(coupon.getId());

        canUseBean.setStatus(coupon.getStatus());

        canUseBean.setSourceShopId(coupon.getSourceShopId());

        canUseBean.setUseShopId(coupon.getUseShopId());

        canUseBean.setLimitChannel(coupon.getLimitChannel());

        ActivityTimeRule timeRule = JSON.parseObject(couponTemplate.getUseTimeRule(), new TypeReference<ActivityTimeRule>() {
        });
        if (timeRule.getIsAllDay() != null && timeRule.getIsAllDay().equals(1)) {
            canUseBean.setUseHMSRule(timeRule.getHmsStart() + "--" + timeRule.getHmsEnd());
        } else {
            canUseBean.setUseHMSRule("全天可用");
        }

        // id
        canUseList.add(canUseBean);

    }

    @Override
    public QueryAgainReturnBean queryMemCouponAgain(QueryAgainRequestBean bean) {
        //        return this.queryMemCouponNewAgain(bean);
        // 查询用户的优惠券，如果状态是0，就返回，否则返回个空
        QueryAgainReturnBean returnBean = new QueryAgainReturnBean();
        Coupon coupon = couponManager.getOne(new QueryWrapper<Coupon>()
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
                .eq("user_id", bean.getUserId())
                .eq("status", CouponTypeEnums.COUPON_UNUSED.getCode())
                .eq("id", bean.getCouponId()));
        if (coupon != null) {
            CouponTemplate couponTemplate = iCouponTemplateManager.selectById(coupon.getOriginalId());
            if (couponTemplate != null) {
                returnBean.setCouponActivityName(couponTemplate.getCouponName());
            }
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            returnBean.setCouponName(coupon.getCouponName());
            returnBean.setId(coupon.getId());
            returnBean.setUseInstruction(coupon.getUseInstruction());
            Integer couponType = coupon.getCouponType();
            if (couponType.equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode()) || couponType.equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
                // 满减或者随机金额
                // 前端展示固定金额样式
                returnBean.setPreferentialType(CouponTypeEnums.FIXED_AMOUNT_TYPE.getCode());
            } else if (couponType.equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
                // 折扣类型
                returnBean.setPreferentialType(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode());
                returnBean.setDiscount(coupon.getCouponDiscount());
            }
            returnBean.setPreferentialMoney(coupon.getSubMoney());
            returnBean.setValidityPeriod(buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd()));
        }
        return returnBean;
    }

    @Override
    public CouponUseReturnBean useCoupon(CouponUseRequestBean bean) {
        CouponUseReturnBean returnBean = useCouponNew(bean);
        try {
            //使用成功后发送MQ
            if (returnBean.isSuccessFlag()) {
                Coupon coupon = couponManager.getOne(new QueryWrapper<Coupon>()
                        .eq("id", bean.getCouponId())
                        .eq("is_deleted", CouponSendTypeEnums.IS_DELETED_DEFAULT.getCode()));
                UseCouponSendMQDTO useCoupon = new UseCouponSendMQDTO();
                useCoupon.setCouponId(coupon.getId());
                useCoupon.setOriginalId(coupon.getOriginalId());
                useCoupon.setCouponCode(coupon.getCouponCode());
                useCoupon.setCouponCondition(coupon.getCouponCondition());
                useCoupon.setNeedMoney(coupon.getNeedMoney());
                useCoupon.setCouponType(coupon.getCouponType());
                useCoupon.setSubMoney(coupon.getSubMoney());
                useCoupon.setCouponDiscount(coupon.getCouponDiscount());
                useCoupon.setOrderNo(coupon.getOrderNo());
                useCoupon.setOrderAmount(coupon.getOrderAmount());
                useCoupon.setSourceShopId(coupon.getSourceShopId());
                useCoupon.setUseShopId(coupon.getUseShopId());
                useCoupon.setCouponPrice(coupon.getCouponPrice());
                useCoupon.setUseTime(coupon.getUseTime());
                log.info("使用优惠券-通知MQ：", JSON.toJSONString(useCoupon));
                RocketMQSender.doSend(defaultMQProducer, RocketMQSender.COUPON_COUPON_USED, useCoupon);
            }
        } catch (Exception e) {
            throw new BusinessException("使用优惠券MQ有误");
        }
        return returnBean;
    }

    @Transactional(rollbackFor = Exception.class)
    public CouponUseReturnBean useCouponNew(CouponUseRequestBean bean) {
        int now = DateUtil.getCurrentTimeIntValue();
        CouponUseReturnBean returnBean = new CouponUseReturnBean();
        // 防重查询，如果优惠券状态，如果已经使用过，返回错误
        //        Coupon couponQuery = couponManager.selectById(bean.getCouponId());
        Coupon couponQuery = couponManager.getOne(new QueryWrapper<Coupon>()
                .eq("id", bean.getCouponId())
                .eq("is_deleted", CouponSendTypeEnums.IS_DELETED_DEFAULT.getCode()));
        if (couponQuery == null) {
            throw new BusinessException("优惠券不存在");
        }
        //保持线上老逻辑
        if (null == bean.getBizChannel() && couponQuery.getUserId().compareTo(bean.getUserId()) != 0) {
            throw new BusinessException("该用户：" + bean.getUserId() + "无法使用优惠券：" + bean.getCouponId());
        }
        // 优惠券状态
        Integer couponStatus = couponQuery.getStatus();
        // 优惠券状态未使用,则可更新状态,否则返回错误
        if (couponStatus.equals(CouponTypeEnums.COUPON_UNUSED.getCode())) {
            // 更新优惠券的状态
            Coupon coupon = new Coupon();
//            coupon.setUserId(bean.getUserId());
            coupon.setSourceSystem(bean.getSourceSystem());
            coupon.setOrderNo(StringUtils.isEmpty(bean.getOrderNo()) ? "" : bean.getOrderNo());
            coupon.setOrderAmount(bean.getTotalAmount());
            coupon.setGoodsNum(bean.getGoodsNum());
            coupon.setCouponPrice(bean.getCouponPrice());
            coupon.setUseTime(now);
            coupon.setStatus(CouponTypeEnums.COUPON_USED.getCode());
            coupon.setUseShopId(bean.getShopId() == null ? 0 : bean.getShopId());
            //            coupon.setIsDeleted(CouponTypeEnums.COUPON_USED.getCode());
            coupon.setUT(now);
            couponManager.update(coupon, new QueryWrapper<Coupon>()
                    .eq("id", bean.getCouponId())
                    .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));

            CouponUse couponUse = new CouponUse();
            BeanUtils.copyProperties(couponQuery, couponUse);
            couponUse.setCouponId(bean.getCouponId());
            couponUse.setOrderNo(StringUtils.isEmpty(bean.getOrderNo()) ? "" : bean.getOrderNo());
            couponUse.setOrderAmount(bean.getTotalAmount());
            couponUse.setGoodsNum(bean.getGoodsNum());
            couponUse.setUserId(bean.getUserId());
            couponUse.setUseShopId(bean.getShopId() == null ? 0 : bean.getShopId());
            couponUse.setCouponPrice(bean.getCouponPrice());
            couponUse.setUseTime(now);
            couponUse.setUT(now);
            couponUseManager.save(couponUse);

            returnBean.setSuccessFlag(true);
        } else {
            // 券已经用过了
            if (couponStatus.equals(CouponTypeEnums.COUPON_USED.getCode())) {
                returnBean.setReason("此优惠券已经被使用,不能重复使用");
            } else if (couponQuery.getStatus().equals(CouponTypeEnums.COUPON_EXPIRED.getCode())) {
                // 券过期了
                returnBean.setReason("此优惠券已经过期,不能再使用");
            }
            returnBean.setSuccessFlag(false);
        }
        return returnBean;
    }


    @Override
    public Boolean remunerateCoupon(CouponRemunerateRequestBean bean) {
        //        return this.remunerateCouponNew(bean);
        int now = DateUtil.getCurrentTimeIntValue();
        Coupon coupon = new Coupon();
        coupon.setStatus(CouponTypeEnums.COUPON_UNUSED.getCode());
        coupon.setUT(now);
        couponManager.update(coupon, new QueryWrapper<Coupon>()
                .eq("id", bean.getCouponId())
                .eq("user_id", bean.getUserId())
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));

        return true;
    }

    @Override
    public List<CouponCheckReturnBean> printCheck(CouponPrintCheckCommand bean) {
        String orderCode = bean.getOrderCode();
        List<Coupon> coupons = couponManager.coupon(orderCode);
        List<CouponCheckReturnBean> returnBeans = new ArrayList<>();
        if (CollectionUtils.isEmpty(coupons)) {
            return new ArrayList<>();
        }
        List<Integer> activityIds = coupons.stream().map(Coupon::getOriginalId).collect(Collectors.toList());
        List<CouponTemplate> activities = iCouponTemplateManager.queryByIdList(activityIds,TemplateStatusTypeEnums.IN_USE_STATUS.getCode());
        Map<Integer, Coupon> couponMap = coupons.stream().collect(Collectors.toMap(Coupon::getOriginalId, coupon -> coupon));
        Map<Integer, CouponTemplate> couponActivityMap = activities.stream().collect(Collectors.toMap(CouponTemplate::getId, couponActivity -> couponActivity));

        for (Coupon c : coupons) {
            returnBeans.add(Coupon.check(c, true));
        }
        List<CouponCheckReturnBean> tmp = new ArrayList<>();

        for (CouponCheckReturnBean checkReturnBean : returnBeans) {
            if (checkReturnBean.getSuccessFlag()) {
                CouponTemplate activity = couponActivityMap.get(checkReturnBean.getOriginalId());
                Coupon coupon = couponMap.get(checkReturnBean.getOriginalId());
                checkReturnBean = Coupon.buildInfo(activity, coupon, checkReturnBean);
                tmp.add(checkReturnBean);
            } else {
                tmp.add(checkReturnBean);
            }
        }
        return returnBeans;
    }

    @Override
    public CouponCheckReturnBean checkCouponByCouponInfo(CouponCheckRequestBean bean) {
        QueryWrapper<Coupon> entityWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(bean.getCouponId())) {
            entityWrapper.eq("id", bean.getCouponId());
        } else {
            throw new BusinessException("优惠券id不能为空");
        }
        entityWrapper.eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode());
        Coupon coupon = couponManager.getOne(entityWrapper);
        return Coupon.check(coupon, false);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<CashQuerySendCouponReturnBean> regainCoupon(PromotionProtocol param) {
        LOGGER.info("regainCoupon param:{}", JSON.toJSONString(param));
        List<CashQuerySendCouponReturnBean> returnBeanList = new ArrayList<>();
        if (!StringUtils.isEmpty(param.getSourceOrderNo())) {
            List<Coupon> couponList = couponManager.list(new QueryWrapper<Coupon>()
                    .eq("source_order_no", param.getSourceOrderNo())
                    .eq("status", CouponTypeEnums.COUPON_UNUSED.getCode())
                    .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
            //            List<CouponNew> couponList = this.regainCouponNew(param);
            //如果有数据直接返回
            if (CollectionUtils.isNotEmpty(couponList)) {
                returnBeanList = couponList.stream().map(coupon -> {
                    CashQuerySendCouponReturnBean rb = new CashQuerySendCouponReturnBean();
                    rb.setCouponId(coupon.getId());
                    rb.setSourceOrderNo(coupon.getSourceOrderNo());
                    rb.setCouponCode(coupon.getCouponCode());
                    rb.setCouponName(coupon.getCouponName());
                    rb.setUseInstruction(coupon.getUseInstruction());
                    rb.setValidityPeriod(buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd()));
                    return rb;
                }).collect(Collectors.toList());
                return returnBeanList;
            }
        }
        //如果没有数据，重新生成
        PromotionContext promotionContext = promotionExternalApiService.executeStrategy(param);
        if (promotionContext != null && CollectionUtils.isNotEmpty(promotionContext.getMzContexts())) {
            for (MzContext mzContext : promotionContext.getMzContexts()) {
                CouponInfo couponInfo = mzContext.getCouponInfo();
                if (couponInfo == null
                        || couponInfo.getGivingNum() == null
                        || couponInfo.getGivingNum().compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                } else {
                    CashQuerySendCouponRequestBean bean = new CashQuerySendCouponRequestBean();
                    if (param.getUser() != null && param.getUser().getUserId() != null && param.getUser().getUserId() != 0) {
                        bean.setUserId(param.getUser().getUserId().longValue());
                    }
                    //此处的activityId指的是优惠券活动id，要注意
                    bean.setActivityId(couponInfo.getCouponId().intValue());
                    bean.setSendNum(couponInfo.getGivingNum().intValue());
                    bean.setSourceOrderNo(param.getSourceOrderNo());
                    returnBeanList = this.cashTriggerSendCoupon(bean, false);
                    break;
                }
            }
        }
        return returnBeanList;
    }

    @Override
    public List<CashQuerySendCouponReturnBean> querySentCouponList(QuerySentCouponListRequestBean bean) {
        LOGGER.info("start into method querySentCouponList,bean:" + JSON.toJSONString(bean));
        List<Coupon> couponList = null;
        couponList = couponManager.querySendCouponByCondition(bean);
        LOGGER.info("return querySentCouponList,couponList:" + JSON.toJSONString(couponList));
        return buildReturnResult(couponList);
    }

    @Override
    public ListWithPage<CashQuerySendCouponReturnBean> querySentCouponListWithPage(QuerySentCouponListRequestBean bean) {
        ListWithPage<CashQuerySendCouponReturnBean> finalResult = new ListWithPage<>();
        if (bean == null || bean.getUserId() == null || bean.getUserId().longValue() == 0) {
            LOGGER.warn("用户id不能为空");
            return finalResult;
        }
        Page<Coupon> couponPage = new Page<>(bean.getPageIndex(), bean.getPageSize());
        QueryWrapper<Coupon> entityWrapper = new QueryWrapper<>();
        //优惠券来源订单
        if (StringUtils.isNotEmpty(bean.getSourceOrderNo())) {
            entityWrapper.eq("source_order_no", bean.getSourceOrderNo());
        }
        //优惠券码
        if (StringUtils.isNotEmpty(bean.getCouponCode())) {
            entityWrapper.eq("coupon_code", bean.getCouponCode());
        }
        //发券门店id
        if (bean.getSourceShopId() != null) {
            entityWrapper.eq("source_shop_id", bean.getSourceShopId());
        }
        if (bean.getUserId() != null) {
            entityWrapper.eq("user_id", bean.getUserId());
        }
        if (bean.getStatus() != null) {
            entityWrapper.eq("status", bean.getStatus());
            if (bean.getStatus() == CouponTypeEnums.COUPON_UNUSED.getCode()) {
                if (RequestSourceEnum.MY_COUPON_INFO.getCode().equals(bean.getRequestSource())) {
                    entityWrapper.orderByAsc("use_time_end");
                    entityWrapper.orderByAsc("c_t");
                } else if (RequestSourceEnum.FRONT_PAGE.getCode().equals(bean.getRequestSource())) {
                    entityWrapper.orderByDesc("c_t");
                }

            } else if (bean.getStatus() == CouponTypeEnums.COUPON_USED.getCode()) {
                entityWrapper.orderByDesc("u_t");
            } else if (bean.getStatus() == CouponTypeEnums.COUPON_EXPIRED.getCode()) {
                entityWrapper.orderByAsc("use_time_end");
            }
        }
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        if (bean.getStatus() == CouponTypeEnums.COUPON_UNUSED.getCode()) {
            entityWrapper.le("use_time_start", now);
            entityWrapper.ge("use_time_end", now);
        }
        if (bean.getCouponUseTimeStart() != null) {
            entityWrapper.ge("use_time", bean.getCouponUseTimeStart());
            entityWrapper.ge("status", CouponTypeEnums.COUPON_USED.getCode());
        }
        if (bean.getCouponUseTimeEnd() != null) {
            entityWrapper.le("use_time", bean.getCouponUseTimeEnd());
            entityWrapper.ge("status", CouponTypeEnums.COUPON_USED.getCode());
        }
        //优惠券渠道
        if (bean.getLimitChannel() != null) {
            if (bean.getLimitChannel() == 1) {
                entityWrapper.notIn("limit_channel", 2);
            } else if (bean.getLimitChannel() == 2) {
                entityWrapper.notIn("limit_channel", 1);
            }
        }

        entityWrapper.eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode());
        Page<Coupon> couponPageList = couponManager.page(couponPage, entityWrapper);
        if (couponPageList == null) {
            return finalResult;
        }
        finalResult.setListData(buildReturnResult(couponPageList.getRecords()));
        finalResult.setTotalCount((int) couponPageList.getTotal());
        return finalResult;
    }

    @Override
    public ListWithPage<CashQuerySendCouponReturnBean> querySentCouponListWithPageForMall(QuerySentCouponListRequestBean bean) {
        ListWithPage<CashQuerySendCouponReturnBean> finalResult = new ListWithPage<>();
        if (bean == null || bean.getUserId() == null || bean.getUserId().longValue() == 0) {
            LOGGER.warn("用户id不能为空");
            return finalResult;
        }
        Page<Coupon> couponPage = new Page<>(bean.getPageIndex(), bean.getPageSize());
        QueryWrapper<Coupon> entityWrapper = new QueryWrapper<>();
        //优惠券来源订单
        if (StringUtils.isNotEmpty(bean.getSourceOrderNo())) {
            entityWrapper.eq("source_order_no", bean.getSourceOrderNo());
        }
        //优惠券码
        if (StringUtils.isNotEmpty(bean.getCouponCode())) {
            entityWrapper.eq("coupon_code", bean.getCouponCode());
        }
        //发券门店id
        if (bean.getSourceShopId() != null) {
            entityWrapper.eq("source_shop_id", bean.getSourceShopId());
        }
        if (bean.getUserId() != null) {
            entityWrapper.eq("user_id", bean.getUserId());
        }
        if (bean.getStatus() != null) {
            entityWrapper.eq("status", bean.getStatus());
            if (bean.getStatus() == CouponTypeEnums.COUPON_UNUSED.getCode()) {
                if (RequestSourceEnum.MY_COUPON_INFO.getCode().equals(bean.getRequestSource())) {
                    entityWrapper.orderByAsc("use_time_end");
                    entityWrapper.orderByAsc("c_t");
                } else if (RequestSourceEnum.FRONT_PAGE.getCode().equals(bean.getRequestSource())) {
                    entityWrapper.orderByDesc("c_t");
                }

            } else if (bean.getStatus() == CouponTypeEnums.COUPON_USED.getCode()) {
                entityWrapper.orderByDesc("u_t");
            } else if (bean.getStatus() == CouponTypeEnums.COUPON_EXPIRED.getCode()) {
                entityWrapper.orderByAsc("use_time_end");
            }
        }
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        if (bean.getStatus() == CouponTypeEnums.COUPON_UNUSED.getCode()) {
            entityWrapper.ge("use_time_end", now);
        }
        if (bean.getCouponUseTimeStart() != null) {
            entityWrapper.ge("use_time", bean.getCouponUseTimeStart());
            entityWrapper.ge("status", CouponTypeEnums.COUPON_USED.getCode());
        }
        if (bean.getCouponUseTimeEnd() != null) {
            entityWrapper.le("use_time", bean.getCouponUseTimeEnd());
            entityWrapper.ge("status", CouponTypeEnums.COUPON_USED.getCode());
        }

        entityWrapper.eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode());
        Page<Coupon> couponPageList = couponManager.page(couponPage, entityWrapper);
        if (couponPageList == null) {
            return finalResult;
        }
        finalResult.setListData(buildReturnResult(couponPageList.getRecords()));
        finalResult.setTotalCount((int) couponPageList.getTotal());
        return finalResult;
    }

    /**
     * 所用商品可用
     *
     * @param couponCondition
     * @param coupon
     * @param activity
     * @param bean
     * @param returnBean
     * @return
     */
    private CouponCheckReturnBean buildAllGoodsCanUse(Integer couponCondition, Coupon coupon, CouponActivity activity, CouponCheckRequestBean bean, CouponCheckReturnBean returnBean) {
        //分析券门槛couponCondition
        if (couponCondition.equals(CouponTypeEnums.NO_USE_CONDITION.getCode())) {
            //返回可用信息
            buildUsefulCouponInfo(coupon, activity, bean.getGoodsList(), returnBean);
            return returnBean;
        }
        if (CollectionUtils.isEmpty(bean.getGoodsList())) {
            //返回不可用信息
            buildUselessCouponInfo(returnBean, "商品总金额不足，无法用券。");
            return returnBean;
        }
        //计算每个商品的总价，看是否满足可用条件
        // 金额
        BigDecimal needMoney = new BigDecimal(coupon.getNeedMoney());
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (GoodsInfo goodsInfo : bean.getGoodsList()) {
            totalPrice = totalPrice.add(goodsInfo.getPrice().multiply(goodsInfo.getAmount()));
        }
        if (totalPrice.compareTo(needMoney) >= 0) {
            //返回可用信息
            buildUsefulCouponInfo(coupon, activity, bean.getGoodsList(), returnBean);
        } else {
            //返回不可用信息
            buildUselessCouponInfo(returnBean, "商品总金额不足，无法用券。");
        }
        return returnBean;
    }

    private CouponCheckReturnBean buildSomeGoodsCanUse(Integer couponCondition, Coupon coupon, CouponActivity activity, CouponCheckRequestBean bean, CouponCheckReturnBean returnBean) {
        // 指定商品可用
        // 无门槛
        // 取出数据库的一级分类
        List<Integer> class1IdList = JSON.parseArray(coupon.getClass1Ids(), Integer.class);
        //  取出数据库的二级分类
        List<Integer> class2IdList = JSON.parseArray(coupon.getClass2Ids(), Integer.class);
        if (couponCondition.equals(CouponTypeEnums.NO_USE_CONDITION.getCode())) {
            List<GoodsInfo> effectiveGoodsList = buildGoodsInfo(bean, coupon, class1IdList, class2IdList, GOODS_CAN_USE);
            if (CollectionUtils.isEmpty(effectiveGoodsList)) {
                //返回不可用信息
                buildUselessCouponInfo(returnBean, "此类商品不能使用该券。");
            } else {
                //返回可用信息
                buildUsefulCouponInfo(coupon, activity, effectiveGoodsList, returnBean);
            }
        } else {
            if (CollectionUtils.isNotEmpty(bean.getGoodsList())) {
                BigDecimal needMoney = new BigDecimal(coupon.getNeedMoney());
                if (StringUtils.isNotEmpty(coupon.getClass1Ids()) || StringUtils.isNotEmpty(coupon.getClass2Ids())) {
                    List<GoodsInfo> effectiveGoodsList = buildGoodsInfo(bean, coupon, class1IdList, class2IdList, GOODS_CAN_USE);
                    buildCouponInfo(effectiveGoodsList, needMoney, coupon, activity, returnBean);
                }
            } else {
                //返回不可用信息
                buildUselessCouponInfo(returnBean, "商品总金额不足，无法用券。");
            }
        }
        return returnBean;
    }

    private CouponCheckReturnBean buildGoodsCanNotUse(Integer couponCondition, Coupon coupon, CouponCheckRequestBean bean, CouponActivity activity, CouponCheckReturnBean returnBean) {
        // 无门槛，如果list里面不包含，则可用
        // 取出数据库的一级分类
        List<Integer> class1IdList = JSON.parseArray(coupon.getClass1Ids(), Integer.class);
        //  取出数据库的二级分类
        List<Integer> class2IdList = JSON.parseArray(coupon.getClass2Ids(), Integer.class);
        if (couponCondition.equals(CouponTypeEnums.NO_USE_CONDITION.getCode())) {
            List<GoodsInfo> effectiveGoodsList = buildGoodsInfo(bean, coupon, class1IdList, class2IdList, GOODS_CAN_NOT_USE);

            if (CollectionUtils.isNotEmpty(effectiveGoodsList)) {
                //返回可用信息
                buildUsefulCouponInfo(coupon, activity, effectiveGoodsList, returnBean);
            } else {
                //返回不可用信息
                buildUselessCouponInfo(returnBean, "此类商品不能使用该券。");
            }
        } else {
            if (CollectionUtils.isEmpty(bean.getGoodsList())) {
                //返回不可用信息
                buildUselessCouponInfo(returnBean, "商品总金额不足，无法用券。");
            } else {
                if (StringUtils.isNotEmpty(coupon.getClass1Ids()) || StringUtils.isNotEmpty(coupon.getClass2Ids())) {
                    //满足条件限额
                    BigDecimal needMoney = new BigDecimal(coupon.getNeedMoney());
                    List<GoodsInfo> effectiveGoods = buildGoodsInfo(bean, coupon, class1IdList, class2IdList, GOODS_CAN_NOT_USE);
                    buildCouponInfo(effectiveGoods, needMoney, coupon, activity, returnBean);
                }
            }
        }
        return returnBean;
    }

    private void buildCouponInfo(List<GoodsInfo> effectiveGoods, BigDecimal needMoney, Coupon coupon, CouponActivity activity, CouponCheckReturnBean returnBean) {
        //如果存在满足条件的商品
        if (CollectionUtils.isNotEmpty(effectiveGoods)) {
            BigDecimal totalPrice = BigDecimal.ZERO;
            for (GoodsInfo goodsInfo : effectiveGoods) {
                totalPrice = totalPrice.add(goodsInfo.getPrice().multiply(goodsInfo.getAmount()));
            }
            if (totalPrice.compareTo(needMoney) >= 0) {
                //返回可用信息
                buildUsefulCouponInfo(coupon, activity, effectiveGoods, returnBean);
            } else {
                //返回不可用信息
                buildUselessCouponInfo(returnBean, "此类商品总金额不满足使用条件。");
            }
        } else {
            //返回不可用信息
            buildUselessCouponInfo(returnBean, "此类商品不能使用该券。");
        }
    }

    private List<GoodsInfo> buildGoodsInfo(CouponCheckRequestBean bean, Coupon coupon, List<Integer> class1IdList, List<Integer> class2IdList, Integer flag) {
        List<GoodsInfo> effectiveGoodsList = new ArrayList<>();
        if (GOODS_CAN_USE.equals(flag)) {
            //结合goodsList,看是否有商品符合券的使用标准
            for (GoodsInfo goodsInfo : bean.getGoodsList()) {
                if (StringUtils.isNotEmpty(coupon.getClass1Ids()) && class1IdList.contains(goodsInfo.getClass1Id())) {
                    effectiveGoodsList.add(goodsInfo);
                    continue;
                }
                if (StringUtils.isNotEmpty(coupon.getClass2Ids()) && class2IdList.contains(goodsInfo.getClass2Id())) {
                    effectiveGoodsList.add(goodsInfo);
                    continue;
                }
            }
        } else {
            for (GoodsInfo goodsInfo : bean.getGoodsList()) {
                if (StringUtils.isNotEmpty(coupon.getClass1Ids()) && class1IdList.contains(goodsInfo.getClass1Id())) {
                    continue;
                }
                if (StringUtils.isNotEmpty(coupon.getClass2Ids()) && class2IdList.contains(goodsInfo.getClass2Id())) {
                    continue;
                }
                effectiveGoodsList.add(goodsInfo);
            }
        }
        return effectiveGoodsList;
    }

    private List<CashQuerySendCouponReturnBean> buildReturnResult(List<Coupon> couponList) {
        if (CollectionUtils.isEmpty(couponList)) {
            return new ArrayList<>();
        }
        return couponList.stream().map(coupon -> {
            CashQuerySendCouponReturnBean rb = new CashQuerySendCouponReturnBean();
            CouponTemplate couponTemplate = iCouponTemplateManager.selectById(coupon.getOriginalId());
            ActivityTimeRule timeRule = JSON.parseObject(couponTemplate.getUseTimeRule(), new TypeReference<ActivityTimeRule>() {
            });
            if (timeRule.getIsAllDay() != null && timeRule.getIsAllDay().equals(1)) {
                rb.setUseHMSRule(timeRule.getHmsStart() + "--" + timeRule.getHmsEnd());
            } else {
                rb.setUseHMSRule("全天可用");
            }
            rb.setCouponActivityName(couponTemplate.getCouponName());
            rb.setCouponId(coupon.getId());
            rb.setSourceOrderNo(coupon.getSourceOrderNo());
            rb.setCouponCode(coupon.getCouponCode());
            rb.setCouponName(coupon.getCouponName());
            rb.setUseInstruction(coupon.getUseInstruction());
            rb.setValidityPeriod(buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd()));
            rb.setCouponPrice(coupon.getCouponPrice());
            rb.setSourceShopId(coupon.getSourceShopId());
            rb.setUseShopId(coupon.getUseShopId());
            rb.setCouponType(coupon.getCouponType());
            rb.setSubMoney(coupon.getSubMoney());
            rb.setCouponDiscount(coupon.getCouponDiscount());
            rb.setApplicableGoodsType(coupon.getApplicableGoodsType());
            rb.setExpiredFlag(buildExpiredFlag(coupon.getUseTimeEnd()));
            rb.setCreateTime(coupon.getCT());
            rb.setCouponCondition(coupon.getCouponCondition());
            rb.setLimitChannel(coupon.getLimitChannel());
            rb.setUseTime(coupon.getUseTime());
            return rb;
        }).collect(Collectors.toList());
    }

    private void buildUsefulCouponInfo(Coupon coupon, CouponActivity couponActivity, List<GoodsInfo> effectiveGoods, CouponCheckReturnBean returnBean) {
        Integer couponId = coupon.getId();
        String validityPeriod = buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd());
        String useInstruction = coupon.getUseInstruction();
        String activityName = couponActivity.getCouponName();
        String couponName = coupon.getCouponName();
        Integer expiredFlag = buildExpiredFlag(coupon.getUseTimeEnd());
        Integer couponType = coupon.getCouponType();
        String sourceOrderNo = coupon.getSourceOrderNo();

        //组织返回实体
        if (couponType.equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode()) || couponType.equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
            // 满减或者随机金额
            // 前端展示固定金额样式
            returnBean.setPreferentialType(CouponTypeEnums.FIXED_AMOUNT_TYPE.getCode());
        } else if (couponType.equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
            // 折扣类型
            returnBean.setPreferentialType(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode());
            returnBean.setDiscount(coupon.getCouponDiscount());
        }
        returnBean.setPreferentialMoney(coupon.getSubMoney());
        returnBean.setCouponId(couponId);
        // 有效期
        returnBean.setValidityPeriod(validityPeriod);
        // 使用说明
        returnBean.setUseInstruction(useInstruction);
        // 券活动名称
        returnBean.setCouponActivityName(activityName);
        // 券名称
        returnBean.setCouponName(couponName);
        // 过期标识
        returnBean.setExpiredFlag(expiredFlag);
        //可用商品列表
        returnBean.setUsefulGoodsList(effectiveGoods);
        //优惠券关联的来源订单
        returnBean.setSourceOrderNo(sourceOrderNo);
        returnBean.setSuccessFlag(true);
    }

    private void buildUselessCouponInfo(CouponCheckReturnBean returnBean, String reason) {
        returnBean.setReason(reason);
        returnBean.setSuccessFlag(false);
    }

    /**
     * 构造优惠券使用有效期
     *
     * @param timeStart
     * @param timeEnd
     * @return
     */
    private String buildValidityPeriod(Integer timeStart, Integer timeEnd) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String startTime = "";
        String endTime = "";
        try {
            startTime = format.format(new Date(timeStart * 1000L));
            endTime = format.format(new Date(timeEnd * 1000L));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("查询有效期信息有误");
        }
        return "有效期:" + startTime + " 至 " + endTime;
    }

    /**
     * 判断是否已过期，明日过期，后日过期,两天后过期
     *
     * @param timeEnd
     * @return
     */
    private Integer buildExpiredFlag(Integer timeEnd) {
        Integer expiredFlag = 0;
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 1);
        Calendar calendar2 = new GregorianCalendar();
        calendar2.setTime(new Date());
        calendar2.add(Calendar.DATE, 2);
        // 当前时间等于优惠券截止时间，表示今天截止
        if (DateUtil.getStartTimeStamp(new Date(now * 1000L)) >= timeEnd) {
            expiredFlag = -1;
        } else if (DateUtil.getStartTimeStamp(new Date(now * 1000L)) == DateUtil.getStartTimeStamp(new Date(timeEnd * 1000L))) {
            expiredFlag = 1;
        } else if (DateUtil.getStartTimeStamp(calendar.getTime()) == DateUtil.getStartTimeStamp(new Date(timeEnd * 1000L))) {
            expiredFlag = 2;
        } else if (DateUtil.getStartTimeStamp(calendar2.getTime()) == DateUtil.getStartTimeStamp(new Date(timeEnd * 1000L))) {
            expiredFlag = 3;
        }
        return expiredFlag;
    }

    /**
     * 校验优惠券信息是否可以发券
     *
     * @param bean
     */
    private void checkOrderInfo(CashQuerySendCouponRequestBean bean, Boolean checkSourceOrderNo) {
        // 优惠券活动id
        Integer activityId = bean.getActivityId();
        //优惠券绑定的订单id
        String sourceOrderNo = bean.getSourceOrderNo();
        //先查询该券是否发放
        CouponSendIds couponSendIdsBean = idsManager.getOne(new QueryWrapper<CouponSendIds>()
                .eq("coupon_id", activityId)
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
        );
        if (couponSendIdsBean == null) {
            throw new BusinessException(CouponTypeEnums.COUPON_ACTIVITY_NOT_SEND.getDetail());
        }
        //查询券是否审核通过
        CouponSend couponSend = sendManager.getOne(new QueryWrapper<CouponSend>()
                .eq("id", couponSendIdsBean.getSendId())
                .eq("status", CouponTypeEnums.COUPON_SEND_APPROVED.getCode())
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
        );
        if (couponSend == null) {
            throw new BusinessException(CouponTypeEnums.COUPON_SEND_APPROVING.getDetail());
        }
        //查询券是否达到发放最大值
        int couponSentNum = couponManager.count(new QueryWrapper<Coupon>().eq("original_id", activityId));
        if (couponSentNum + bean.getSendNum() > couponSend.getSendNum()) {
            LOGGER.info("{}已超过最大发送量,需要发：{}，已发:{},最大:{}", JSON.toJSONString(bean), couponSentNum, bean.getSendNum(), couponSend.getSendNum());
            throw new BusinessException(CouponTypeEnums.COUPON_SEND_LIMITED.getDetail());
        }
        //根据sourceOrderNo查询该券是否已发放
        if (checkSourceOrderNo && StringUtils.isNotEmpty(sourceOrderNo) && StringUtils.isNotBlank(sourceOrderNo)) {
            List<Coupon> existCoupon = couponManager.list(new QueryWrapper<Coupon>()
                    .eq("source_order_no", sourceOrderNo)
                    .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
            if (CollectionUtils.isNotEmpty(existCoupon)) {
                LOGGER.info("{}已发过优惠券", JSON.toJSONString(bean));

                throw new BusinessException("该订单已经发放了优惠券");
            }
        }
    }

    private CouponCheckReturnBean checkCouponInfo(Coupon coupon) {
        CouponCheckReturnBean returnBean = new CouponCheckReturnBean();
        int now = DateUtil.getCurrentTimeIntValue();
        if (coupon == null) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("没有该优惠券信息,无法使用");
            return returnBean;
        } else if (CouponTypeEnums.COUPON_USED.getCode() == coupon.getStatus()) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("优惠券已使用");
            return returnBean;
        } else if (coupon.getUseTimeStart() == null || coupon.getUseTimeEnd() == null) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("优惠券信息异常，无法使用");
            return returnBean;
        } else if (CouponTypeEnums.COUPON_EXPIRED.getCode() == coupon.getStatus()
                || coupon.getUseTimeEnd() < now) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("优惠券已过期");
            return returnBean;
        } else if (coupon.getUseTimeStart() > now) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("活动未开始，该券还不能使用");
            return returnBean;
        }
        returnBean.setSuccessFlag(true);
        returnBean.setReason("优惠券可以使用");
        return returnBean;
    }

    public CouponActivity verificationCouponActivity(Integer couponActivityId) {
        //验证活动是否存在
        CouponActivity activity = activityManager.getOne(new QueryWrapper<CouponActivity>()
                .eq("id", couponActivityId)
                .eq("status", CouponTypeEnums.COUPON_ACTIVITY_USEING.getCode())
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
        //未匹配到活动
        if (activity == null) {
            log.info("领取优惠券 - 活动不存在 {}", couponActivityId);
            throw new BusinessException("活动不存在");
        }
        //查询该券是否发放
        CouponSendIds couponSendIdsBean = idsManager.getOne(new QueryWrapper<CouponSendIds>()
                .eq("coupon_id", couponActivityId)
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
        );
        if (couponSendIdsBean == null) {
            log.info("领取优惠券 - 券未发放 {}", couponActivityId);
            throw new BusinessException(CouponTypeEnums.COUPON_ACTIVITY_NOT_SEND.getDetail());
        }
        //查询券是否审核通过
        CouponSend couponSend = sendManager.getOne(new QueryWrapper<CouponSend>()
                .eq("id", couponSendIdsBean.getSendId())
                .eq("status", CouponTypeEnums.COUPON_SEND_APPROVED.getCode())
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
        );
        if (couponSend == null) {
            log.info("领取优惠券 - 券活动未审核通过 {}", couponActivityId);
            throw new BusinessException(CouponTypeEnums.COUPON_SEND_APPROVING.getDetail());
        }
        return activity;
    }

}
