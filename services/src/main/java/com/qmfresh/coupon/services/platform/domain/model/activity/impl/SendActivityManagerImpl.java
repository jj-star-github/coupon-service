package com.qmfresh.coupon.services.platform.domain.model.activity.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityManager;
import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityVO;
import com.qmfresh.coupon.services.platform.domain.model.activity.command.CreateSendActivityCommand;
import com.qmfresh.coupon.services.platform.domain.model.activity.command.QuerySendActivityCommand;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityCacheKey;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivitySendTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityStatusEnums;
import com.qmfresh.coupon.services.platform.domain.shared.BaseUtil;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.domain.shared.Operator;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.SendActivityCouponMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.SendActivityMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivity;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivityCoupon;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 *
 */
@Service
@Slf4j
public class SendActivityManagerImpl extends ServiceImpl<SendActivityMapper, SendActivity>
        implements SendActivityManager {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private SendActivityCouponMapper sendActivityCouponMapper;

    public void cleanActivityCache(SendActivity activityPO) {
        stringRedisTemplate.opsForValue().getOperations().delete(ActivityCacheKey.BY_ACTIVITY_TYPE.key(activityPO.getActivityType()));
    }

    @Override
    public List<SendActivity> query(Integer activityType) {
        String key = ActivityCacheKey.BY_ACTIVITY_TYPE.key(activityType);
        String activityStr = stringRedisTemplate.opsForValue().get(key);
        if (null != activityStr) {
            return JSON.parseObject(activityStr, new TypeReference<List<SendActivity>>() {});
        }
        activityStr = "";
        List<SendActivity> activityList = baseMapper.queryByActivityType(activityType);
        if (!CollectionUtils.isEmpty(activityList)) {
            activityStr = JSON.toJSONString(activityList);
        }
        stringRedisTemplate.opsForValue().set(key, activityStr, 60*60*1000,TimeUnit.MILLISECONDS);
        return activityList;
    }

    @Override
    public void updateStatus(Long id, Integer status, Operator operator) {
        SendActivity sendActivity = baseMapper.queryById(id);
        if (null == sendActivity){
            return;
        }
        if (ActivitySendTypeEnums.APPOINT.getCode().equals(sendActivity.getSendType())
            && ! ActivityStatusEnums.CREATE.getCode().equals(sendActivity.getSendStatus())) {
            throw new BusinessException("定向活动正在发放中");
        }

        baseMapper.updateStatus(id, status, operator.getOperatorId(), operator.getOperatorName());
        this.cleanActivityCache(sendActivity);
    }

    @Override
    public void delete(Long id, Operator operator) {
        SendActivity sendActivity = baseMapper.queryById(id);
        if (null == sendActivity){
            return;
        }
        baseMapper.delete(id, operator.getOperatorId(), operator.getOperatorName());
        this.cleanActivityCache(sendActivity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createActivity(CreateSendActivityCommand createSendActivityCommand) {
        //插入活动信息
        SendActivity sendActivity = new SendActivity(createSendActivityCommand);
        baseMapper.insert(sendActivity);
        //插入目标优惠券信息
        List<SendActivityCoupon> activityCoupon = createSendActivityCommand.getSendActivityCouponVOList().stream().map(vo->{
            return new SendActivityCoupon(vo, sendActivity.getId(), createSendActivityCommand.getOperator());
        }).collect(Collectors.toList());
        sendActivityCouponMapper.insertBatch(activityCoupon);
    }

    @Override
    public Page<SendActivityVO> pageActivity(QuerySendActivityCommand querySendActivityCommand) {
        Page<SendActivityVO> sendActivityVOPage = new Page<>();
        //查询总数
        Integer totalCount = baseMapper.queryCount(querySendActivityCommand.getActivityNameKey(), querySendActivityCommand.getSendType(),
                querySendActivityCommand.getActivityType(), querySendActivityCommand.getActivityStatus(),
                querySendActivityCommand.getBeginTime(),querySendActivityCommand.getEndTime(),querySendActivityCommand.getCreateName());
        if (null == totalCount || 0 >= totalCount) {
            return sendActivityVOPage;
        }
        sendActivityVOPage.setTotalCount(totalCount.longValue());
        //查询明细
        List<SendActivityVO> voList = new ArrayList<>();
        List<SendActivity> sendActivityList = baseMapper.queryByPage(querySendActivityCommand.getActivityNameKey(), querySendActivityCommand.getSendType(), querySendActivityCommand.getActivityType(),
                querySendActivityCommand.getActivityStatus(), querySendActivityCommand.getStart(), querySendActivityCommand.getPageSize(),
                querySendActivityCommand.getBeginTime(),querySendActivityCommand.getEndTime(),querySendActivityCommand.getCreateName());
        if (!CollectionUtils.isEmpty(sendActivityList)) {
            //获取关联的优惠券信息
            List<Long> activityIdList = sendActivityList.stream().map(SendActivity::getId).collect(Collectors.toList());
            List<SendActivityCoupon> activityCouponList = sendActivityCouponMapper.queryByActivityIdList(activityIdList);
            if (!CollectionUtils.isEmpty(activityCouponList)) {
                Map<Long, List<SendActivityCoupon>> actCouponMap = activityCouponList.stream().collect(Collectors.groupingBy(SendActivityCoupon::getActivityId));
                voList= sendActivityList.stream().map(activity->{
                    return activity.createSendActivityVO(actCouponMap.get(activity.getId()));
                }).collect(Collectors.toList());
            } else {
                voList= sendActivityList.stream().map(activity->{
                    return activity.createSendActivityVO(null);
                }).collect(Collectors.toList());
            }
        }
        sendActivityVOPage.setRecords(voList);
        return sendActivityVOPage;
    }

    @Override
    public void createSendToUse(CreateSendActivityCommand createSendActivityCommand) {
        //插入活动信息
        SendActivity sendActivity = new SendActivity(createSendActivityCommand);
        baseMapper.insert(sendActivity);
        //插入目标优惠券信息
        List<SendActivityCoupon> activityCoupon = createSendActivityCommand.getSendActivityCouponList().stream().map(vo->{
            return new SendActivityCoupon(vo, sendActivity.getId(), createSendActivityCommand.getOperator());
        }).collect(Collectors.toList());
        sendActivityCouponMapper.insertBatch(activityCoupon);
    }

    @Override
    public List<SendActivity> queryBySendType(Integer sendType, Integer status) {
        return baseMapper.queryBySendType(sendType,status);
    }

    @Override
    public void updateFinishedStatus(Long id, Integer status, Operator operator) {
        SendActivity sendActivity = baseMapper.queryById(id);
        if (null == sendActivity){
            return;
        }
        baseMapper.updateStatus(id, status, operator.getOperatorId(), operator.getOperatorName());
        this.cleanActivityCache(sendActivity);
    }
}
