package com.qmfresh.coupon.services.platform.domain.model.template.enums;

import lombok.Getter;

@Getter
public enum TemplateShopTypeEnums {
    ALL_SHOP(1, "全部可用"),
    CAN_SHOP(2, "指定可用"),
    NO_SHOP(3, "指定不可用");

    private Integer code;

    private String remark;

    TemplateShopTypeEnums(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
