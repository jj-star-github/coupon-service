package com.qmfresh.coupon.services.platform.domain.model.template.command;

import lombok.Data;

@Data
public class CouponTemplateQueryCommand {
    private String couponName;
    private Integer couponType;
    private Integer limitChannel;
    private Integer status;
    private Integer goodsType;
    private Integer shopType;
    private Integer bingTemplate;
    private Integer pageNum;
    private Integer pageSize;
    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }

    public CouponTemplateQueryCommand(){}

    public CouponTemplateQueryCommand(String couponName,Integer couponType,Integer limitChannel,
                                      Integer status,Integer goodsType,Integer shopTyp,Integer bingTemplate,
                                      Integer pageNum,Integer pageSize){
        this.couponName = couponName;
        this.couponType = couponType;
        this.limitChannel = limitChannel;
        this.status = status;
        this.goodsType = goodsType;
        this.shopType = shopTyp;
        this.bingTemplate = bingTemplate;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }
}
