package com.qmfresh.coupon.services.platform.infrastructure.http.member;


public interface MemberServerApi {

    /**
     * 根据会员等级查询会员id信息
     *
     * @param param
     * @return
     */
    GradeInfoReturn queryGradeInfoList(GradeInfoQuery param);

}
