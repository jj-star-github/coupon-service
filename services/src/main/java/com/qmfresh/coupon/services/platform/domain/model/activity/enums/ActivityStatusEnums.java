package com.qmfresh.coupon.services.platform.domain.model.activity.enums;

import lombok.Getter;

/**
 *
 */
@Getter
public enum ActivityStatusEnums {
    /**
     * 待发放
     */
    CREATE(1, "待发放"),
    /**
     * 发放中/活动中
     */
    RUNNING(2, "发放中/活动中"),

    /**
     * 已发放/已结束
     */
    FINISHED(3, "已发放/已结束"),

    CLOSED(4, "关闭"),
    ;

    private Integer code;
    private String name;

    ActivityStatusEnums(Integer code, String name) {
        this.code = code;
        this.name = name;
    }
}
