package com.qmfresh.coupon.services.platform.domain.model.template.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.ActivityCouponCacheKey;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateTargetGoodsManager;
import com.qmfresh.coupon.services.platform.domain.model.template.command.GoodsQueryCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetGoodsCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateGoodsVo;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateInfoVo;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.CouponTemplateTargetGoodsMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetGoods;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetShop;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ICouponTemplateTargetGoodsManagerImpl extends ServiceImpl<CouponTemplateTargetGoodsMapper,CouponTemplateTargetGoods> implements ICouponTemplateTargetGoodsManager {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void insertBatch(Integer templateId, List<TemplateTargetGoodsCommand> goodsList) {
        List<CouponTemplateTargetGoods> targetList = new ArrayList<>();
        goodsList.forEach(
                templateTargetGoodsCommand -> {
                    CouponTemplateTargetGoods targetGoods = new CouponTemplateTargetGoods(templateTargetGoodsCommand);
                    targetGoods.setTemplateId(templateId);
                    targetList.add(targetGoods);
                }
        );
        if(!targetList.isEmpty()){
            super.saveBatch(targetList);
        }
    }

    @Override
    public List<CouponTemplateTargetGoods> queryByTemplateId(Integer templateId) {
        //缓存获取
//        String key = ActivityCouponCacheKey.GOODS_BY_TEMPLATE_ID.key(templateId);
//        String activityStr = stringRedisTemplate.opsForValue().get(key);
//        if (StringUtils.isNotEmpty(activityStr)) {
//            return JSON.parseObject(activityStr, new TypeReference<List<CouponTemplateTargetGoods>>() {});
//        }
        List<CouponTemplateTargetGoods> targetGoodsList = baseMapper.queryByTemplateId(templateId,null);
//        activityStr = CollectionUtils.isEmpty(targetGoodsList) ? "" : JSON.toJSONString(targetGoodsList);
//        stringRedisTemplate.opsForValue().set(key, activityStr, 24*60*60*1000, TimeUnit.MILLISECONDS);
        return targetGoodsList;
    }

    @Override
    public Page<TemplateGoodsVo> queryByCondition(GoodsQueryCommand command){
        Page<TemplateGoodsVo> goodsVoPage = new Page<>();
        Integer totalCount = baseMapper.queryCount(command.getTemplateId(),command.getGoodsName(),command.getGoodsId());
        if(null == totalCount || 0 >= totalCount){
            return goodsVoPage;
        }
        List<CouponTemplateTargetGoods> goodsVoList = baseMapper.queryByPage(command.getTemplateId(),command.getGoodsName(),command.getGoodsId(),command.getStart(),command.getPageSize());
        List<TemplateGoodsVo> goodsInfoVoList = goodsVoList.stream().map(CouponTemplateTargetGoods::createGoodsVo).collect(Collectors.toList());
        goodsVoPage.setTotalCount(totalCount.longValue());
        goodsVoPage.setRecords(goodsInfoVoList);
        return goodsVoPage;
    }
}
