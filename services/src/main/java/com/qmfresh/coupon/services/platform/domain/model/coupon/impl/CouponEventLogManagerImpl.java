package com.qmfresh.coupon.services.platform.domain.model.coupon.impl;



import com.qmfresh.coupon.services.platform.domain.model.coupon.CouponEventLogManager;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.services.entity.CouponEventLog;
import com.qmfresh.coupon.services.mapper.CouponEventLogMapper;

import lombok.extern.slf4j.Slf4j;

/**
 */
@Service
@Slf4j
public class CouponEventLogManagerImpl extends ServiceImpl<CouponEventLogMapper, CouponEventLog> implements CouponEventLogManager {

    @Override
    public CouponEventLog queryEventLog(Integer bizType, String bizCode, String sourceBizCode) {
        return baseMapper.queryByBiz(bizType, bizCode, sourceBizCode);
    }
}
