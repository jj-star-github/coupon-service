package com.qmfresh.coupon.services.platform.domain.model.template.enums;

import lombok.Getter;

@Getter
public enum TemplateStatusTypeEnums {
    IN_USE_STATUS(2, "生效中"),
    EXPIRED_STATUS(3, "已过期");

    private Integer code;

    private String remark;

    TemplateStatusTypeEnums(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
