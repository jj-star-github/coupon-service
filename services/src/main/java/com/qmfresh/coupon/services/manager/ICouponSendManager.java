package com.qmfresh.coupon.services.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvailableCouponQuery;
import com.qmfresh.coupon.services.entity.CouponSend;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
public interface ICouponSendManager extends IService<CouponSend> {

    /**
     * 更新审批信息
     * @param bean
     * @return
     */
    int updateApprovalInfo(CouponSendUpdate bean);

    /**
     * 查询券发放列表
     * @param bean
     * @return
     */
    List<QueryCouponSendReturnBean> queryInfoList(QueryCouponSendListBean bean);

    /**
     * 查询列表个数
     * @param bean
     * @return
     */
    int sendInfoCount(QueryCouponSendListBean bean);


    /**
     * 软删除优惠券发放活动 
     * @param id
     * @return
     */
    int delCouponSendActivity(Integer id);

    /**
     * 暂停优惠券发放活动 
     * @param id
     * @return
     */
    int pauseSendActivity(Integer id);

    /**
     * 恢复暂停优惠券发放活动
     *
     * @param id
     * @return
     */
    int recoverPauseSendActivity(Integer id);

    /**
     * 查询会员中台可发放优惠券
     * @param param
     * @return
     */
    List<CouponSend>  queryAvailableSendCouponList(MemberAvailableCouponQuery param);

    /**
     * 查询会员中台可发放优惠券总数
     * @param param
     * @return
     */
    int queryAvailableSendCouponCount(MemberAvailableCouponQuery param);

    CouponSendDetailDto queryCouponSendById(Integer id);


}
