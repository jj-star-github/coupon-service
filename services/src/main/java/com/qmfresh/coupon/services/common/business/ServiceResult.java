package com.qmfresh.coupon.services.common.business;

import com.qmfresh.coupon.services.platform.domain.shared.BizCode;
import lombok.Data;

import java.io.Serializable;

/**
 * 通用返回类
 *
 * @param <T>
 * @author wyh
 */
@Data
public final class ServiceResult<T> implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    private Boolean success = true;
    private int errorCode;
    private String message;
    private T body;

    public ServiceResult() {
    }

    public ServiceResult(T body) {
        this.body = body;
    }

    public ServiceResult(int errorCode, String message) {
        this.success = false;
        this.errorCode = errorCode;
        this.message = message;
    }

    /**
     * 创建成功
     *
     * @param body
     * @param <T>
     * @return
     */
    public static <T> ServiceResult<T> success(T body) {
        return create(true, BizCode.SUCCESS.getCode(), BizCode.SUCCESS.name(), body);
    }

    /**
     * 创建失败
     *
     * @param bizCode 错误码
     * @param message   消息内容
     * @param <T>       泛型
     * @return ServiceResultDTO
     */
    public static <T> ServiceResult<T> fail(BizCode bizCode, String message) {
        return create(false, bizCode.getCode(), message, null);
    }

    /**
     * 创建失败
     *
     * @param errorCode 错误码
     * @param message   消息内容
     * @param <T>       泛型
     * @return ServiceResultDTO
     */
    public static <T> ServiceResult<T> fail(Integer errorCode, String message) {
        return create(false, errorCode, message, null);
    }

    /**
     * 创建
     *
     * @param success   是否成功
     * @param errorCode 错误码
     * @param message   错误消息
     * @param body      内容
     * @param <T>       类型
     * @return ServiceResult的对象
     */
    private static <T> ServiceResult<T> create(Boolean success, Integer errorCode, String message, T body) {
        ServiceResult<T> serviceResultDTO = new ServiceResult<>();
        serviceResultDTO.setSuccess(success);
        serviceResultDTO.setErrorCode(errorCode);
        serviceResultDTO.setMessage(message);
        serviceResultDTO.setBody(body);
        return serviceResultDTO;
    }
}