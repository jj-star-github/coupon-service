package com.qmfresh.coupon.services.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.PromotionContext;
import com.qmfresh.coupon.services.platform.infrastructure.http.promotion.PromotionProtocol;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.services.common.business.ServiceResultUtil;
import com.qmfresh.coupon.services.common.utils.RequestUtil;
import com.qmfresh.coupon.services.external.config.PromotionUrlManager;
import com.qmfresh.coupon.services.external.service.IPromotionExternalApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @program: coupon-service
 * @description:
 * @author: xbb
 * @create: 2019-12-12 18:27
 **/
@Service
@Slf4j
public class PromotionExternalApiServiceImpl implements IPromotionExternalApiService {


    @Resource
    private PromotionUrlManager promotionUrlManager;

    @Override
    public PromotionContext executeStrategy(PromotionProtocol param) {
        ServiceResult<PromotionContext> result;
        try {
            result = RequestUtil.postJson(
                    promotionUrlManager.getExecuteStrategyUrl(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<PromotionContext>>() {
                    });
        } catch (Exception e) {
            String message = "url:" + promotionUrlManager.getExecuteStrategyUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(result);
    }

    @Override
    public ServiceResult<String> getPromotionDetailNew(Long activityId) {
        ServiceResult<String> result;
        try {
            result = RequestUtil.postJson(
                    promotionUrlManager.getPromotionDetailNew(),
                    JSON.toJSONString(activityId),
                    new TypeReference<ServiceResult<String>>(){});
            log.info(" getPromotionDetailNew result={}", result);
        } catch (Exception e) {
            String message = "url:" + promotionUrlManager.getPromotionDetailNew() + ",params:" + JSON.toJSONString(activityId);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return result;
    }
}
