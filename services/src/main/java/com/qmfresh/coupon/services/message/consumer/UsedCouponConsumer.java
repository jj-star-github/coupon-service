package com.qmfresh.coupon.services.message.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponUseRequestBean;
import com.qmfresh.coupon.services.message.consumer.handle.UsedCouponHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: coupon-service
 * @description:
 * @author: xbb
 * @create: 2019-11-26 17:10
 **/
@Service
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}UsedCouponConsumer", topic = "${mq.topic.consumeUsedCoupon}")
public class UsedCouponConsumer extends AbstractRocketMQListener {

    @Resource
    private UsedCouponHandle handle;


    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        List<CouponUseRequestBean> list = JSON.parseObject(content,
                new TypeReference<List<CouponUseRequestBean>>() {
                });
        handle.handleMessage(list);
    }
}
