package com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.qmfresh.coupon.services.platform.domain.model.activity.SendActivityCouponVO;
import com.qmfresh.coupon.services.platform.domain.shared.Operator;
import lombok.Data;
import lombok.ToString;


/**
 * 发放活动对应优惠券信息
 */
@TableName("t_send_activity_coupon")
@Data
@ToString
public class SendActivityCoupon implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Long activityId;
    private Integer templateId;
    private String couponName;
    private Integer couponType;
    private Integer sendNum;
    private String remark;
    private Integer lastOperatorId;
    private String lastOperatorName;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;
    private String sendRule;

    public SendActivityCouponVO createVO() {
        SendActivityCouponVO couponVO = new SendActivityCouponVO();
        couponVO.setId(this.getId());
        couponVO.setActivityId(this.getActivityId());
        couponVO.setTemplateId(this.getTemplateId());
        couponVO.setCouponName(this.getCouponName());
        couponVO.setCouponType(this.getCouponType());
        couponVO.setSendNum(this.getSendNum());
        couponVO.setLastOperatorName(this.getLastOperatorName());
        couponVO.setGmtModified(this.getGmtModified());
        couponVO.setRemark(this.getRemark());
        return couponVO;
    }

    public SendActivityCoupon() {}

    public SendActivityCoupon(SendActivityCouponVO activityCouponVO, Long activityId, Operator operator) {
        this.activityId = activityId;
        this.couponName = activityCouponVO.getCouponName();
        this.couponType = activityCouponVO.getCouponType();
        this.sendNum = activityCouponVO.getSendNum();
        this.templateId = activityCouponVO.getTemplateId();
        Date now = new Date();
        this.gmtCreate = now;
        this.gmtModified = now;
        this.lastOperatorId = operator.getOperatorId();
        this.lastOperatorName = operator.getOperatorName();
        this.isDeleted = 0;
        this.remark = activityCouponVO.getRemark();
    }

    public SendActivityCoupon(SendActivityCoupon sendActivityCoupon,Long activityId, Operator operator){
        this.activityId = activityId;
        this.couponName = sendActivityCoupon.getCouponName();
        this.couponType = sendActivityCoupon.getCouponType();
        this.sendNum = sendActivityCoupon.getSendNum();
        this.templateId = sendActivityCoupon.getTemplateId();
        Date now = new Date();
        this.gmtCreate = now;
        this.gmtModified = now;
        this.lastOperatorId = operator.getOperatorId();
        this.lastOperatorName = operator.getOperatorName();
        this.isDeleted = 0;
        this.remark = sendActivityCoupon.getRemark();
        this.sendRule = sendActivityCoupon.getSendRule();
    }
}