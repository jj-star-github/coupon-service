package com.qmfresh.coupon.services.entity.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class CouponReturnParamBo implements Serializable {

    /**
     * 订单
     */
    private String sourceOrderCode;
    /**
     * 当前门店id
     */
    private Integer currentShopId;
    /**
     * 操作者id
     */
    private Integer operatorId;
    /**
     * 操作者名称
     */
    private String operatorName;
}
