package com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.eclipse.jetty.util.StringUtil;

/**
 * 发券日志
 */
@TableName("t_send_log")
@Data
public class SendLog implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String businessCode;
    private Integer businessType;
    private String couponIds;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

    public SendLog () {}

    public SendLog (String businessCode, Integer businessType, List<String> couponIdList) {
        this.setBusinessCode(businessCode);
        this.setBusinessType(businessType);
        String couponIds = "";
        for(String couponId : couponIdList){
            couponIds = couponId+",";
        }
        if (!StringUtil.isEmpty(couponIds)) {
            couponIds = couponIds.substring(0, couponIds.length()-1);
        }
        this.setCouponIds(couponIds);
        Date now = new Date();
        this.setGmtCreate(now);
        this.setGmtModified(now);
    }

}
