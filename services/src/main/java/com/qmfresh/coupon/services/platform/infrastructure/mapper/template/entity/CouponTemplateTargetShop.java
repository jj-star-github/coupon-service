package com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetShopsCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateShopVo;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 券模板目标店
 */
@TableName("t_coupon_template_target_shop")
@Data
@ToString
public class CouponTemplateTargetShop implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 券模板id
     */
    @TableField("template_id")
    private Integer templateId;
    /**
     * 门店id
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 门店名
     */
    @TableField("shop_name")
    private String shopName;
    /**
     * 门店类型,1.大区,2.片区,3.门店，4.仓
     */
    @TableField("shop_type")
    private Integer shopType;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 有效性
     */
    @TableField("is_deleted")
    private Integer isDeleted;

    public CouponTemplateTargetShop(){}

    public CouponTemplateTargetShop(TemplateTargetShopsCommand command){
        this.setShopId(command.getShopId());
        this.setShopName(command.getShopName());
        this.setShopType(command.getShopType());
        this.setGmtCreate(new Date());
        this.setGmtModified(new Date());
    }

    public TemplateShopVo createShopVo(){
        TemplateShopVo shopVo = new TemplateShopVo();
        shopVo.setId(this.id);
        shopVo.setTemplateId(this.templateId);
        shopVo.setShopId(this.shopId);
        shopVo.setShopName(this.shopName);
        shopVo.setShopType(this.shopType);
        return shopVo;
    }
}
