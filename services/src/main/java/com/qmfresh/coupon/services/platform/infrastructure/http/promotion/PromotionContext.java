package com.qmfresh.coupon.services.platform.infrastructure.http.promotion;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/5/25.
 *
 * @author wyh
 */
@Data
public class PromotionContext implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private List<BzContext> bzContexts;

    private List<MzContext> mzContexts;

    private List<MjContext> mjContexts;

    private List<PromotionSsuContext> ssuContexts;
}
