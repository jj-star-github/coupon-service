package com.qmfresh.coupon.services.platform.infrastructure.job;

import com.qmfresh.coupon.services.platform.application.template.CouponTemplateService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 修改券模板状态
 */
@JobHandler(value = "modifyCouponTemplateStatusHandler")
@Component
public class ModifyCouponActivityStatusHandler extends IJobHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModifyCouponActivityStatusHandler.class);
    
    @Autowired
    private CouponTemplateService templateService;
    
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        LOGGER.info("修改券模板状态 modifyCouponTemplateStatusHandler 开始。。。。。。。。。。。。。。");
        templateService.modifyStatus();
        LOGGER.info("修改券模板状态 modifyCouponTemplateStatusHandler 结束。。。。。。。。。。。。。。");
        return ReturnT.SUCCESS;
    }
}
