package com.qmfresh.coupon.services.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@TableName("t_coupon_event_log")
public class CouponEventLog implements Serializable {

    private Long id;
    /**
     * 券码
     */
    private String couponCode;
    /**
     * @see CouponEventBizType
     */
    private Integer bizType;
    /**
     * 业务编码
     */
    private String bizCode;
    /**
     * 业务源编码
     */
    private String sourceBizCode;
    /**
     * 业务描述
     */
    private String bizDesc;
    private String remark;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer operatorId;
    private String operatorName;
    private Integer currentShopId;


    public static CouponEventLog create(CouponEventBizType bizType,
                                        String couponCode,
                                        String bizCode,
                                        String bizSourceCode,
                                        String remark,
                                        Operator operator,
                                        Integer currentShopId
    ) {
        CouponEventLog couponEventLog = new CouponEventLog();
        couponEventLog.setBizType(bizType.getCode());
        couponEventLog.setBizDesc(bizType.getMsg());
        couponEventLog.setCouponCode(couponCode);
        couponEventLog.setBizCode(bizCode);
        couponEventLog.setSourceBizCode(bizSourceCode);
        couponEventLog.setRemark(remark);
        couponEventLog.setOperatorId(operator.getOperatorId());
        couponEventLog.setOperatorName(operator.getOperatorName());
        couponEventLog.setGmtCreate(new Date());
        couponEventLog.setGmtModified(new Date());
        couponEventLog.setCurrentShopId(currentShopId);
        return couponEventLog;

    }

    @Data
    public static class Remark {
        private String from;
        private String to;
        private String reason;

        public static String create(CouponStatus from, CouponStatus to, String reason) {
            Remark remark = new Remark();
            remark.setFrom(from.name());
            remark.setTo(to.name());
            remark.setReason(reason);
            return JSON.toJSONString(remark);
        }
    }
}
