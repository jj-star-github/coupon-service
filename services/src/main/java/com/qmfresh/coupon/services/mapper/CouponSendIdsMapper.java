package com.qmfresh.coupon.services.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.services.entity.CouponSendIds;
import com.qmfresh.coupon.interfaces.dto.coupon.ModifyRelationBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxc
 * @since 2019-08-02
 */
public interface CouponSendIdsMapper extends BaseMapper<CouponSendIds> {

    /**
     * 修改券发放关联的依赖关系
     *
     * @param bean
     * @return
     */
    int modifySendRelationForCouponId(@Param("condition") ModifyRelationBean bean);

    /**
     * 修改券发放关联的依赖关系
     *
     * @param bean
     * @return
     */
    int modifySendRelationForSendId(@Param("condition") ModifyRelationBean bean);

    /**
     * 软删除修改券发放关联表
     *
     * @param ids
     * @return
     */
    int delCouponIds(@Param("idList") List<Integer> ids);
}