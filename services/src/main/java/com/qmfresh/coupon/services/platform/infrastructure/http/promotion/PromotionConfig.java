package com.qmfresh.coupon.services.platform.infrastructure.http.promotion;

import com.qmfresh.coupon.services.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PromotionConfig {

    private String callbackUrl;

    @Value("${url.service.promotion}")
    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public String getExecuteStrategyUrl(){
        return UtilMethods.concatUrl(callbackUrl,"promotion/executeStrategy");
    }

    public String getPromotionDetail(){
        return UtilMethods.concatUrl(callbackUrl,"promotion/getPromotionDetail");
    }

    public String getPromotionDetailNew(){
        return UtilMethods.concatUrl(callbackUrl,"/prom/activity/getPromActivityRule");
    }
}
