package com.qmfresh.coupon.services.platform.infrastructure.dubbo.dto;

import com.qmgyl.shop.dto.area.BaseAreaDto;
import lombok.Data;

import java.io.Serializable;

/**
 * 区域信息
 */
@Data
public class AreaDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务区id
     */
    private Integer id;
    /**
     * 父级id
     */
    private Integer pid;
    /**
     * 层级
     */
    private Integer level;
    /**
     * 区域名
     */
    private String areaName;
    /**
     * 区域码
     */
    private String areaCode;

    public static AreaDTO create(BaseAreaDto baseAreaDto) {
        if (null == baseAreaDto) {
            return null;
        }
        AreaDTO areaDTO = new AreaDTO();
        areaDTO.setId(baseAreaDto.getId());
        areaDTO.setAreaName(baseAreaDto.getAreaName());
        areaDTO.setAreaCode(baseAreaDto.getAreaCode());
        areaDTO.setLevel(baseAreaDto.getLevel());
        areaDTO.setPid(baseAreaDto.getPid());
        return areaDTO;
    }
}
