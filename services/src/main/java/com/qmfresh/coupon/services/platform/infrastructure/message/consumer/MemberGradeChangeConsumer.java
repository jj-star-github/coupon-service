package com.qmfresh.coupon.services.platform.infrastructure.message.consumer;

import javax.annotation.Resource;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.interfaces.dto.vip.MemberGradeChangeEventDto;
import com.qmfresh.coupon.services.platform.infrastructure.message.consumer.handle.MemberGradeChangeHandler;

import lombok.extern.slf4j.Slf4j;

/**
 * @className MemberGradeChangeConsumer
 * @description TODO
 * @author xbb
 * @date 2020/3/27 10:54
 */
@Service
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}GradeChangeConsumer", topic = "${mq.topic.memberGradeChange}")
public class MemberGradeChangeConsumer extends AbstractRocketMQListener {
    @Resource
    private MemberGradeChangeHandler handler;
    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        MemberGradeChangeEventDto bean = JSON.parseObject(content,
                new TypeReference<MemberGradeChangeEventDto>() {
                });
        handler.handleMessage(bean);
    }
}
