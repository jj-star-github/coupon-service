package com.qmfresh.coupon.services.schedule;

import com.qmfresh.coupon.services.service.IModifyStatusTaskService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时改优惠券状态定时任务
 * @author lxc
 */
@JobHandler(value = "modifyCouponStatusHandler")
@Component
public class ModifyCouponStatusHandler extends IJobHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModifyCouponStatusHandler.class);
    
    @Autowired
    private IModifyStatusTaskService taskService;
    
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        LOGGER.info("开始执行更新优惠券状态定时任务。。。。。。。。。。。。。。");
        taskService.modifyCouponStatus();
        LOGGER.info("更新优惠券状态定时任务完成。。。。。。。。。。。。。。");
        return ReturnT.SUCCESS;
    }
}
