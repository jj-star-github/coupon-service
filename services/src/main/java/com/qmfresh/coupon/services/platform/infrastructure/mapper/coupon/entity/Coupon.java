package com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.qmfresh.coupon.client.coupon.QueryCouponReturnDTO;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityConditionRule;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityTimeRule;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponCheckReturnBean;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.entity.CouponCheckType;
import com.qmfresh.coupon.services.entity.CouponStatus;
import com.qmfresh.coupon.services.platform.domain.model.coupon.CouponDetailVO;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * <p>
 *
 * </p>
 *
 * @author lxc
 * @since 2019-08-09
 */
@TableName("t_coupon")
@Data
@Slf4j
public class Coupon extends Model<Coupon> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增长
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 原优惠券活动的id
     */
    @TableField("original_id")
    private Integer originalId;
    /**
     * 券码
     */
    @TableField("coupon_code")
    private String couponCode;
    /**
     * 券名称
     */
    @TableField("coupon_name")
    private String couponName;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    @TableField("coupon_condition")
    private Integer couponCondition;
    /**
     * 订单所需金额
     */
    @TableField("need_money")
    private Integer needMoney;
    /**
     * 券类型(1:满减,2:折扣,3:随机金额)
     */
    @TableField("coupon_type")
    private Integer couponType;
    /**
     * 优惠金额
     */
    @TableField("sub_money")
    private BigDecimal subMoney;
    /**
     * 折扣
     */
    @TableField("coupon_discount")
    private Integer couponDiscount;
    /**
     * 券使用开始时间
     */
    @TableField("use_time_start")
    private Integer useTimeStart;
    /**
     * 券使用结束时间
     */
    @TableField("use_time_end")
    private Integer useTimeEnd;
    /**
     * 券适用商品类型(1:全部可用2:指定可用3:指定不可用)
     */
    @TableField("applicable_goods_type")
    private Integer applicableGoodsType;
    /**
     * 1级分类
     */
    @TableField("class1_ids")
    private String class1Ids;
    /**
     * 2级分类
     */
    @TableField("class2_ids")
    private String class2Ids;
    /**
     * 订单号
     */
    @TableField("order_no")
    private String orderNo;
    /**
     * 订单金额
     */
    @TableField("order_amount")
    private BigDecimal orderAmount;
    /**
     * 用这张券购买的商品数量
     */
    @TableField("goods_num")
    private Integer goodsNum;
    /**
     * 绑定的用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 来源系统
     */
    @TableField("source_system")
    private Integer sourceSystem;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    @TableField("limit_channel")
    private Integer limitChannel;
    /**
     * 使用说明
     */
    @TableField("use_instruction")
    private String useInstruction;
    /**
     * 优惠券绑定的订单号
     */
    @TableField("source_order_no")
    private String sourceOrderNo;
    /**
     * 发券的门店id
     */
    @TableField("source_shop_id")
    private Integer sourceShopId;
    /**
     * 用券的门店id
     */
    @TableField("use_shop_id")
    private Integer useShopId;
    /**
     * 优惠券抵扣金额
     */
    @TableField("coupon_price")
    private BigDecimal couponPrice;
    /**
     * 优惠券使用时间
     */
    @TableField("use_time")
    private Integer useTime;
    /**
     * 使用状态(0:未使用1:已使用2:已过期)
     */
    private Integer status;
    /**
     * 创建时间
     */
    @TableField("c_t")
    private Integer cT;
    /**
     * 更新时间
     */
    @TableField("u_t")
    private Integer uT;
    /**
     * 有效性
     */
    @TableField("is_deleted")
    private Integer isDeleted;
    /**
     * 促销活动id
     */
    @TableField("promotion_activity_id")
    private Integer promotionActivityId;


    @TableField(exist = false)
    private Integer couponCount;
    @TableField(exist = false)
    private BigDecimal payAmount;
    @TableField(exist = false)
    private Integer residueNum;


    public String getIdStr() {
        return this.getId().toString();
    }

    @TableField(exist = false)
    private String validityPeriod;

    /**
     * 构建优惠券信息
     *
     * @param activity        优惠活动
     * @param coupon          优惠券
     * @param checkReturnBean 返回值
     * @return 促销检查
     */
    public static CouponCheckReturnBean buildInfo(CouponTemplate activity, Coupon coupon, CouponCheckReturnBean checkReturnBean) {
        if (activity == null || coupon == null) {
            log.info("优惠券活动和优惠券缺失");
            return checkReturnBean;
        }
        Integer couponId = coupon.getId();
        String validityPeriod = buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd());
        String useInstruction = coupon.getUseInstruction();
        String activityName = activity.getCouponName();
        String couponName = coupon.getCouponName();
        Integer expiredFlag = buildExpiredFlag(coupon.getUseTimeEnd());
        Integer couponType = coupon.getCouponType();
        String sourceOrderNo = coupon.getSourceOrderNo();

        //组织返回实体
        if (couponType.equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode()) || couponType.equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
            // 满减或者随机金额
            // 前端展示固定金额样式
            checkReturnBean.setPreferentialType(CouponTypeEnums.FIXED_AMOUNT_TYPE.getCode());
        } else if (couponType.equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
            // 折扣类型
            checkReturnBean.setPreferentialType(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode());
            checkReturnBean.setDiscount(coupon.getCouponDiscount());
        }
        checkReturnBean.setPreferentialMoney(coupon.getSubMoney());
        checkReturnBean.setCouponId(couponId);
        // 有效期
        checkReturnBean.setValidityPeriod(validityPeriod);
        // 使用说明
        checkReturnBean.setUseInstruction(useInstruction);
        // 券活动名称
        checkReturnBean.setCouponActivityName(activityName);
        // 券名称
        checkReturnBean.setCouponName(couponName);
        // 过期标识
        checkReturnBean.setExpiredFlag(expiredFlag);
        //可用商品列表
        //优惠券关联的来源订单
        checkReturnBean.setSourceOrderNo(sourceOrderNo);
        checkReturnBean.setSuccessFlag(true);

        return checkReturnBean;
    }


    public static String buildValidityPeriod(Integer timeStart, Integer timeEnd) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String startTime = "";
        String endTime = "";
        try {
            startTime = format.format(new Date(timeStart * 1000L));
            endTime = format.format(new Date(timeEnd * 1000L));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("查询有效期信息有误");
        }
        return "有效期:" + startTime + " 至 " + endTime;
    }

    /**
     * 判断是否已过期，明日过期，后日过期,两天后过期
     *
     * @param timeEnd
     * @return
     */
    private static Integer buildExpiredFlag(Integer timeEnd) {
        Integer expiredFlag = 0;
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 1);
        Calendar calendar2 = new GregorianCalendar();
        calendar2.setTime(new Date());
        calendar2.add(Calendar.DATE, 2);
        // 当前时间等于优惠券截止时间，表示今天截止
        if (DateUtil.getStartTimeStamp(new Date(now * 1000L)) >= timeEnd) {
            expiredFlag = -1;
        } else if (DateUtil.getStartTimeStamp(new Date(now * 1000L)) == DateUtil.getStartTimeStamp(new Date(timeEnd * 1000L))) {
            expiredFlag = 1;
        } else if (DateUtil.getStartTimeStamp(calendar.getTime()) == DateUtil.getStartTimeStamp(new Date(timeEnd * 1000L))) {
            expiredFlag = 2;
        } else if (DateUtil.getStartTimeStamp(calendar2.getTime()) == DateUtil.getStartTimeStamp(new Date(timeEnd * 1000L))) {
            expiredFlag = 3;
        }
        return expiredFlag;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    /**
     * 检查coupon是否可以逻辑
     *
     * @param coupon  coupon对象
     * @param isPrint
     * @return CouponCheckReturnBean
     */
    public static CouponCheckReturnBean check(Coupon coupon, Boolean isPrint) {
        CouponCheckReturnBean returnBean = new CouponCheckReturnBean();
        int now = DateUtil.getCurrentTimeIntValue();
        if (coupon == null) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("没有该优惠券信息,无法使用");
            return returnBean;
        }
        returnBean.setCouponCode(coupon.getCouponCode());
        if (CouponStatus.USED.getCode().equals(coupon.getStatus())) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("优惠券已使用");

            returnBean.setType(CouponCheckType.USED.getCode());
            returnBean.setUseCouponTime(coupon.getUseTime() * 1000L);
            returnBean.setUseShopId(coupon.getUseShopId());
            returnBean.setBindUserId(coupon.getUserId().intValue());
            returnBean.setSourceOrderNo(coupon.getSourceOrderNo());
            returnBean.setUseOrderCode(coupon.getOrderNo());

            return returnBean;
        }

        if (coupon.getUseTimeStart() == null || coupon.getUseTimeEnd() == null) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("优惠券信息异常，无法使用");
            return returnBean;
        }

        if (CouponStatus.EXPIRED.getCode().equals(coupon.getStatus())
                || coupon.getUseTimeEnd() < now) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("优惠券已过期");

            returnBean.setType(CouponCheckType.EXPIRED.getCode());
            returnBean.setBindUserId(coupon.getUserId().intValue());
            returnBean.setUseShopId(coupon.getUseShopId());
            returnBean.setUseCouponTime(coupon.getUT() * 1000L);
            returnBean.setSourceOrderNo(coupon.getSourceOrderNo());

            return returnBean;
        }

        if (CouponStatus.RETURN.getCode().equals(coupon.getStatus())) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("赠送优惠券已退货");
            returnBean.setType(CouponCheckType.RETURNED.getCode());
            returnBean.setUseOrderCode(coupon.getOrderNo());
            returnBean.setUseCouponTime(coupon.getUT() * 1000L);
            returnBean.setUseShopId(coupon.getUseShopId());
            returnBean.setBindUserId(coupon.getUserId().intValue());
            returnBean.setSourceOrderNo(coupon.getSourceOrderNo());
            return returnBean;
        }

        if (coupon.getUseTimeStart() > now && !isPrint) {
            returnBean.setSuccessFlag(false);
            returnBean.setReason("活动未开始，该券还不能使用");
            return returnBean;
        }

        returnBean.setSuccessFlag(true);
        returnBean.setReason("优惠券可以使用");
        returnBean.setCouponCode(coupon.getCouponCode());
        returnBean.setOriginalId(coupon.getOriginalId());
        returnBean.setCouponId(coupon.getId());

        return returnBean;
    }

    public Integer getPromotionActivityId() {
        return promotionActivityId;
    }

    public void setPromotionActivityId(Integer promotionActivityId) {
        this.promotionActivityId = promotionActivityId;
    }

    public QueryCouponReturnDTO createQueryCouponReturnDTO(CouponTemplate couponTemplate) {
        QueryCouponReturnDTO returnDTO = new QueryCouponReturnDTO();
        returnDTO.setId(this.getId());
        returnDTO.setCouponCode(this.getCouponCode());
        returnDTO.setCouponName(this.getCouponName());
        returnDTO.setUseInstruction(this.getUseInstruction());
        returnDTO.setValidityPeriod(buildValidityPeriod(this.getUseTimeStart(), this.getUseTimeEnd()));
        returnDTO.setCouponType(this.getCouponType());
        returnDTO.setDiscount(this.getCouponDiscount());
        returnDTO.setPreferentialMoney(this.getSubMoney());
        returnDTO.setNeedMoney(this.getNeedMoney() == null ? null : BigDecimal.valueOf(this.getNeedMoney()));
        returnDTO.setSubMoney(this.getSubMoney());
        returnDTO.setCouponCondition(this.getCouponCondition());
        String tag = "";
        if (this.getCouponType().equals(3)) {
            ActivityConditionRule conditionRule = JSON.parseObject(couponTemplate.getCouponRule(), new TypeReference<ActivityConditionRule>() {
            });
            if (this.getCouponCondition().equals(1)) {
                tag = "满" + this.getNeedMoney() + "减" + conditionRule.getRandomMoneyStart() + "-" + conditionRule.getRandomMoneyEnd();
            } else if (this.getCouponCondition().equals(0)) {
                tag = conditionRule.getRandomMoneyStart() + "-" + conditionRule.getRandomMoneyEnd() + "元随机抵扣";
            }
        }
        if (this.getCouponCondition().equals(1)) {
            if (this.getCouponType().equals(1)) {
                tag = "满" + this.getNeedMoney() + "减" + this.getSubMoney().setScale(0, BigDecimal.ROUND_DOWN);
            } else if (this.getCouponType().equals(2)) {
                tag = "满" + this.getNeedMoney() + "享" + new BigDecimal(this.getCouponDiscount()).divide(new BigDecimal("10")) + "折";
            }
        } else if (this.getCouponCondition().equals(0)) {
            if (this.getCouponType().equals(1)) {
                tag = this.getSubMoney().setScale(0, BigDecimal.ROUND_DOWN) + "元抵扣";
            } else if (this.getCouponType().equals(2)) {
                tag = new BigDecimal(this.getCouponDiscount()).divide(new BigDecimal("10")) + "折";
            }
        }
        returnDTO.setTag(tag);
        return returnDTO;
    }

    public CouponDetailVO createCouponDetailVo() {
        CouponDetailVO couponDetailVO = new CouponDetailVO();
        couponDetailVO.setCouponCode(this.couponCode);
        couponDetailVO.setCouponName(this.couponName);
        couponDetailVO.setOrderAmount(this.orderAmount);
        couponDetailVO.setOrderNo(this.orderNo);
        couponDetailVO.setSourceOrderNo(this.getSourceOrderNo());
        couponDetailVO.setSourceShopId(this.getSourceShopId());
        couponDetailVO.setStatus(this.status);
        couponDetailVO.setTemplateId(this.originalId);
        couponDetailVO.setUseInstruction(this.useInstruction);
        couponDetailVO.setUserId(this.userId);
        couponDetailVO.setUseShopId(this.useShopId);
        couponDetailVO.setUseTime(this.useTime == null ? null : this.useTime);
        couponDetailVO.setSendTime(this.cT);
        return couponDetailVO;
    }
}
