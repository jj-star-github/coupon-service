package com.qmfresh.coupon.services.platform.infrastructure.mapper.template;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetGoods;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetShop;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CouponTemplateTargetShopMapper extends BaseMapper<CouponTemplateTargetShop> {
    /**
     *根据券模板id查
     * @param templateId
     * @return
     */
    List<CouponTemplateTargetShop> queryByTemplateId(@Param("templateId")Integer templateId);

    /**
     *分页查
     * @param templateId
     * @return
     */
    List<CouponTemplateTargetShop> queryByPage(@Param("templateId")Integer templateId,@Param("shopName")String shopName,@Param("shopId")Integer shopId,@Param("start")Integer start, @Param("length")Integer length);

    Integer queryCount(@Param("templateId")Integer templateId,@Param("shopName")String shopName,@Param("shopId")Integer shopId);
}
