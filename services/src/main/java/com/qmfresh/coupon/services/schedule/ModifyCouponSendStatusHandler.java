package com.qmfresh.coupon.services.schedule;

import com.qmfresh.coupon.services.service.IModifyStatusTaskService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: coupon-service
 * @description: 定时改优惠券发放状态定时任务
 * @author: xbb
 * @create: 2019-12-05 11:37
 **/
@JobHandler(value = "modifyCouponSendStatusHandler")
@Component
public class ModifyCouponSendStatusHandler extends IJobHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModifyCouponSendStatusHandler.class);

    @Autowired
    private IModifyStatusTaskService taskService;

    @Override
    public ReturnT<String> execute(String s) throws Exception {
        LOGGER.info("开始执行更新优惠券发放状态定时任务。。。。。。。。。。。。。。");
        taskService.modifyCouponSendStatus();
        LOGGER.info("更新优惠券发放状态定时任务完成。。。。。。。。。。。。。。");
        return ReturnT.SUCCESS;
    }

}
