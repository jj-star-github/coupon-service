package com.qmfresh.coupon.services.platform.domain.shared;

import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.net.URL;

/**
 * @author xzw
 * @date ${date} mailto 741342093@qq.com
 */
public class Paths {

    public static String dirName(String resource) {

        try {
            URL url = ResourceUtils.getURL(resource);
            return url.getPath();
        } catch (FileNotFoundException e) {
            return null;
        }

    }

}
