package com.qmfresh.coupon.services.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponUseRequestBean;
import com.qmfresh.coupon.services.service.ICouponConsumerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: coupon-service
 * @description: 消费优惠券的消费者
 * @author: xbb
 * @create: 2019-11-26 16:34
 **/
@Service
public class UsedCouponHandle {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsedCouponHandle.class);

    @Autowired
    private ICouponConsumerService couponConsumerService;

    public void handleMessage(List<CouponUseRequestBean> list){
        LOGGER.info("UsedCouponHandle handleMessage begin....param:" + JSON.toJSONString(list));
        couponConsumerService.consumeUsedCoupon(list);
        LOGGER.info("UsedCouponHandle handleMessage end....");
    }
}
