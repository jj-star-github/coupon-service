package com.qmfresh.coupon.services.entity.command;

import com.qmfresh.coupon.services.common.exception.BusinessException;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
@Slf4j
public class CouponQueryCommand implements Serializable {

    //使用订单号

    private String useOrderCode;
    //赠送订单号
    private String givenOrderCode;

    private List<String> couponCodes;


    public static CouponQueryCommand create(String useOrderCode, String givenOrderCode) {

        if (StringUtils.isBlank(useOrderCode) && StringUtils.isBlank(givenOrderCode)) {
            throw new BusinessException("业务单号必须设置");
        }

        if (StringUtils.isNotBlank(useOrderCode) && StringUtils.isNotBlank(givenOrderCode)) {
            throw new BusinessException("只能设置一个单号");
        }


        CouponQueryCommand couponQueryCommand = new CouponQueryCommand();
        couponQueryCommand.setUseOrderCode(useOrderCode);
        couponQueryCommand.setGivenOrderCode(givenOrderCode);
        log.info("优惠券查询参数： {}", couponQueryCommand);
        return couponQueryCommand;

    }

    public static CouponQueryCommand create(List<String> couponCodes) {
        CouponQueryCommand c = new CouponQueryCommand();
        c.setCouponCodes(couponCodes);
        return c;
    }
}
