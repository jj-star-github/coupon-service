package com.qmfresh.coupon.services.platform.infrastructure.oss;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.google.common.io.Files;
import com.qmfresh.coupon.services.common.utils.UtilMethods;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.UUID;

@Slf4j
@Service
public class OssUtils {

    @Value("${oss.endpoint}")
    private String endpoint;
    @Value("${oss.accessKeyId}")
    private String accessKeyId;
    @Value("${oss.accessKeySecret}")
    private String accessKeySecret;
    @Value("${oss.bucket}")
    private String bucket;

    public OSS create() {
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        return ossClient;
    }

    public String getBucket() {
        return bucket;
    }

    public String upload(MultipartFile mFile) {
        OSS ossClient = this.create();
        if (!ossClient.doesBucketExist(bucket)) {
            log.error("您的Bucket不存在，创建Bucket：" + bucket + "。");
            throw new RuntimeException("文件上传失败");
        }
        File file = new File("temp");
        try {
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);
        } catch (IOException e) {
            throw new RuntimeException("文件流处理异常", e);
        }
        String originalFilename = mFile.getOriginalFilename();
        String prefix = Files.getFileExtension(originalFilename);

        String fileName = UUID.randomUUID().toString() + "." + prefix.toLowerCase();
        String path = "coupon/sendToUser";
        ossClient.putObject(bucket, UtilMethods.concatUrl(path, fileName), file);
        log.info("Object：" + "originalFilename:" + mFile.getOriginalFilename() + ",fileName:" + fileName + "上传成功。");
        return fileName;
    }
}
