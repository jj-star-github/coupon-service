/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:IModifyStatusTaskManager.java
 */
package com.qmfresh.coupon.services.manager;

/**
 * @author lxc
 */
public interface IModifyStatusTaskManager {

    /**
     * 修改优惠券活动使用时间类型是1的
     */
    void modifyStatus();

    /**
     * 修改优惠券发放状态
     */
    void modifyCouponSendStatus();
}
