package com.qmfresh.coupon.services.external.config;

import com.qmfresh.coupon.services.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @program: coupon-service
 * @description:
 * @author: xbb
 * @create: 2019-12-12 18:25
 **/
@Component
public class PromotionUrlManager {

    private String callbackUrl;

    @Value("${url.service.promotion}")
    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public String getExecuteStrategyUrl(){
        return UtilMethods.concatUrl(callbackUrl,"promotion/executeStrategy");
    }

    public String getPromotionDetailNew(){
        return UtilMethods.concatUrl(callbackUrl,"/prom/activity/getPromActivityRule");
    }
}
