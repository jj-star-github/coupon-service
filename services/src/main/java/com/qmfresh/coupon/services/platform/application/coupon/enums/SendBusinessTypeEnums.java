package com.qmfresh.coupon.services.platform.application.coupon.enums;

/**
 * 发券活动类型
 */
public enum SendBusinessTypeEnums {
	PROMOTION_ACTIVITY(101,"促销活动发券"),
	FLOW_ACTIVITY(102,"流量池活动"),
	MEMBER_ACTIVITY(103,"会员中心发券"),
	MEMBER_NEW_ACTIVITY(1031,"会员中心新用户"),
	MEMBER_UPDATE_ACTIVITY(1032,"会员中心升级"),
	UP_SHOP_NEW_USER_ACTIVITY(104,"线上商城信用户"),
	;
	private int type;
    private String detail;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	private SendBusinessTypeEnums(int type, String detail) {
		this.type = type;
		this.detail = detail;
	}
    
}
