/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:ModifyStatusTaskManagerImpl.java
 */
package com.qmfresh.coupon.services.manager.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityTimeRule;
import com.qmfresh.coupon.services.entity.CouponActivity;
import com.qmfresh.coupon.services.entity.CouponSend;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.manager.ICouponActivityManager;
import com.qmfresh.coupon.services.manager.ICouponSendIdsManager;
import com.qmfresh.coupon.services.manager.ICouponSendManager;
import com.qmfresh.coupon.services.manager.IModifyStatusTaskManager;
import com.qmfresh.coupon.services.mapper.CouponActivityMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lxc
 */
@Service
public class ModifyStatusTaskManagerImpl implements IModifyStatusTaskManager {

    @Autowired
    private CouponActivityMapper activityMapper;
    @Autowired
    private ICouponActivityManager activityManager;
    @Autowired
    private ICouponSendManager sendManager;
    @Autowired
    private ICouponSendIdsManager idsManager;

    @Override
    public void modifyStatus() {
        int now = DateUtil.getCurrentTimeIntValue();
        // 查优惠券活动表使用时间的类型是1的
        List<CouponActivity> activityList = activityManager.list(new QueryWrapper<CouponActivity>()
                .eq("use_time_type", CouponTypeEnums.TIME_USE_FROM_TO.getCode())
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
        List<Integer> ids = new ArrayList<>();
        for (CouponActivity activity : activityList) {
            // 解析时间规则
            JSONObject timeRule = JSONObject.parseObject(activity.getUseTimeRule());
            // 时间规则
            ActivityTimeRule activityTimeRule = (ActivityTimeRule) JSONObject.toJavaObject(timeRule, ActivityTimeRule.class);
            if (now > activityTimeRule.getTimeEnd()) {
                // 表示现在时间已经大于券活动的最后使用时间
                // 将满足条件的id放入list
                ids.add(activity.getId());
            }
        }
        if (CollectionUtils.isNotEmpty(ids)) {
            activityMapper.delActivityBatch(ids);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void modifyCouponSendStatus() {
        //1.查询couponSend记录
        //2.修改过期的记录状态值
        List<CouponSend> couponSendList = sendManager.list(new QueryWrapper<CouponSend>()
                .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode())
                .eq("send_type", CouponTypeEnums.COUPON_SEND_SENDTYPE_ONE.getCode())
                .eq("time_limit_type", CouponTypeEnums.COUPON_SEND_TIME_LIMITED.getCode())
                .eq("status", CouponTypeEnums.COUPON_SEND_APPROVED.getCode())
                .le("time_limit_end", DateUtil.getCurrentTimeIntValue()));

        if (CollectionUtils.isEmpty(couponSendList)) {
            return;
        }
        List<CouponSend> couponSends = couponSendList.stream().map(o -> {
            o.setStatus(CouponTypeEnums.COUPON_SEND_STOP.getCode());
            return o;
        }).collect(Collectors.toList());
        sendManager.updateBatchById(couponSends);

    }
}
