package com.qmfresh.coupon.services.entity.command;

import com.qmfresh.coupon.services.common.exception.BusinessException;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class CouponActivityQueryCommand implements Serializable {

    private Integer pageNum;
    private Integer pageSize;
    /**
     * 券类型1:满减,2:折扣,3:随机金额
     */
    private Integer couponType;
    /**
     * 优惠券状态
     */
    private Integer couponStatus;

    public static CouponActivityQueryCommand create(Integer pageNum, Integer pageSize,
                                                    Integer couponType, Integer couponStatus) {

        if (pageNum == null || pageNum <0) {
            throw new BusinessException("必须设置PageNum");
        }
        if (pageSize == null || pageSize < 0) {
            throw new BusinessException("必须设置PageNum");
        }

        CouponActivityQueryCommand couponActivityQueryCommand = new CouponActivityQueryCommand();
        couponActivityQueryCommand.setPageNum(pageNum);
        couponActivityQueryCommand.setPageSize(pageSize);
        couponActivityQueryCommand.setCouponStatus(couponStatus);
        couponActivityQueryCommand.setCouponType(couponType);

        return couponActivityQueryCommand;
    }
}
