package com.qmfresh.coupon.services.platform.domain.model.template;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateConditionTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateCouponTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateUseTimeTypeEnums;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

@Data
public class CouponSendByTemplate implements Serializable {
    /**
     * 券模板id
     */
    private Integer templateId;
    /**
     * 券模板名称
     */
    private String templateName;
    /**
     * 券模板门槛
     */
    private Integer couponCondition;
    /**
     * 订单所需金额
     */
    private Integer needMoney;
    /**
     * 券模板类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponType;
    /**
     * 券优惠金额
     */
    private BigDecimal subMoney;
    /**
     * 券折扣
     */
    private Integer couponDiscount;
    /**
     * 券开始时间
     */
    private Integer useTimeStart;
    /**
     * 券结束时间
     */
    private Integer useTimeEnd;
    /**
     * 用户本次已发放张数
     */
    private Integer alreadySendNumber;
    /**
     * 用户未发放张数
     */
    private Integer notSendNumber;
    /**
     * 渠道
     */
    private Integer limitChannel;
    /**
     * 使用说明
     */
    private String useInstruction;
    /**
     * 目标品类型
     */
    private Integer goodsType;


    public CouponSendByTemplate(){}

    public CouponSendByTemplate(CouponTemplate couponTemplate,int alreadySendNumber,int sendNumber,boolean checkSendNumber){
        this.setLimitChannel(couponTemplate.getLimitChannel());
        this.setTemplateId(couponTemplate.getId());
        this.setCouponCondition(couponTemplate.getCouponCondition());
        Integer couponType = couponTemplate.getCouponType();
        this.setCouponType(couponType);
        String couponName = "优惠券";
        TemplateConditionRule couponRule = JSON.parseObject(couponTemplate.getCouponRule(),new TypeReference<TemplateConditionRule>(){});
        if(couponType.equals(TemplateCouponTypeEnums.COUPON_MANJIAN.getCode())){
            couponName = "满减券";
            this.setSubMoney(new BigDecimal(couponRule.getSubMoney()));
        }else if(couponType.equals(TemplateCouponTypeEnums.COUPON_DISCOUNT.getCode())){
            couponName = "折扣券";
            this.setCouponDiscount(couponRule.getDiscount());
            this.setSubMoney(couponRule.getMaxDiscountMoney());
        }else if(couponType.equals(TemplateCouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())){
            couponName = "随机券";
            BigDecimal randomSubMoney = createRandomMoney(couponRule.getRandomMoneyStart(), couponRule.getRandomMoneyEnd());
            this.setSubMoney(randomSubMoney);
        }
        if(couponTemplate.getCouponCondition().equals(TemplateConditionTypeEnums.NO_USE_CONDITION.getCode())){
            couponName = "无门槛券";
        }
        this.setTemplateName(couponName);
        this.setNeedMoney(couponTemplate.getNeedMoney());
        TemplateTimeRule timeRule = JSON.parseObject(couponTemplate.getUseTimeRule(),new TypeReference<TemplateTimeRule>(){});
        StartEndTime startEndTime = buildStartEndTime(timeRule,couponTemplate.getUseTimeType());
        this.setUseTimeStart(startEndTime.getTimeStart());
        this.setUseTimeEnd(startEndTime.getTimeEnd());
        this.setUseInstruction(couponTemplate.getUseInstruction());
        this.setGoodsType(couponTemplate.getApplicableGoodsType());
        if(couponRule.getUseReceiveNum() != null || checkSendNumber){
            //用户剩余张数
            Integer notSend = (couponRule.getUseReceiveNum() - alreadySendNumber) > 0 ? (couponRule.getUseReceiveNum() - alreadySendNumber) : 0;
            //本次发放张数
            Integer alreadySend = (notSend - sendNumber) > 0 ? sendNumber : notSend;
            //发放后剩余张数
            Integer canSend = (notSend - sendNumber) > 0 ? (notSend - sendNumber) : 0;
            this.setAlreadySendNumber(alreadySend);
            this.setNotSendNumber(canSend);
        }else {
            this.setAlreadySendNumber(sendNumber);
            this.setNotSendNumber(9999);
        }
    }

    public BigDecimal createRandomMoney(int min, int max) {
        Random r = new Random();
        int startMoney = (int) ((max - min) * Math.random());
        return new BigDecimal(startMoney + min);
    }

    public static StartEndTime buildStartEndTime(TemplateTimeRule timeRule,Integer useTimeType){
        int now = DateUtil.getCurrentTimeIntValue();
        // 有效期起始时间
        Integer timeStart = 0000;
        // 有效期截止时间
        Integer timeEnd = 0000;
        if(useTimeType.equals(TemplateUseTimeTypeEnums.TIME_USE_FROM_TO.getCode())){
            if(timeRule.getTimeEnd() < now ){
                throw new BusinessException("券模板已过期");
            }
            timeStart = timeRule.getTimeStart();
            timeEnd = timeRule.getTimeEnd();
        }else if(useTimeType.equals(TemplateUseTimeTypeEnums.BEGIN_FROM_GET.getCode())){
            // 领券当日起
            Integer beginTime = DateUtil.getMorningTimeByTimeStamp(now);
            Date date = new Date(beginTime * 1000L);
            Calendar ca = Calendar.getInstance();
            ca.setTime(date);
            ca.add(Calendar.DATE, timeRule.getTodayCanUse());
            date = ca.getTime();
            timeStart = beginTime;
            timeEnd = (int) (date.getTime() / 1000) - 1;
        }else if(useTimeType.equals(TemplateUseTimeTypeEnums.BEGIN_FROM_TOMORROW.getCode())){
            // 领券次日起
            Integer beginTime = DateUtil.getMorningTimeByTimeStamp(now);
            Date date = new Date(beginTime * 1000L);
            Calendar caStart = Calendar.getInstance();
            caStart.setTime(date);
            caStart.add(Calendar.DATE, 1);
            Date dateStart = caStart.getTime();
            Calendar caEnd = Calendar.getInstance();
            caEnd.setTime(date);
            caEnd.add(Calendar.DATE, timeRule.getTomorrowCanUse() + 1);
            Date dateEnd = caEnd.getTime();
            timeStart = (int) (dateStart.getTime() / 1000);
            timeEnd = (int) (dateEnd.getTime() / 1000) - 1;
        }
        StartEndTime startEndTime = new StartEndTime();
        startEndTime.setTimeStart(timeStart);
        startEndTime.setTimeEnd(timeEnd);
        return startEndTime;
    }
}
