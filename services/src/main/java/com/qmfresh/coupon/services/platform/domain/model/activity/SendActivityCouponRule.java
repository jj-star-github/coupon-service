package com.qmfresh.coupon.services.platform.domain.model.activity;

import lombok.Data;

@Data
public class SendActivityCouponRule {

    /**
     * 预计发放张数
     */
    private Integer willSendCouponNum;
    /**
     * 预计发放人数
     */
    private Integer willSendUserNum;
}
