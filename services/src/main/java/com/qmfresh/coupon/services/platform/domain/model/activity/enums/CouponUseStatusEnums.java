package com.qmfresh.coupon.services.platform.domain.model.activity.enums;

/**
 * @Description:
 * @Version: V1.0
 */
public enum CouponUseStatusEnums {
    ALL_COUPON(-1,"显示全部优惠券"),
    NOTUSE_COUPON(0,"优惠券未使用"),
    ALREADYUSE_COUPON(1,"优惠券已经使用过了"),
    ALREADYEXPIRED_COUPON(2,"优惠券已经过期了");


    private Integer code;
    private String detail;

   private CouponUseStatusEnums(Integer code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
