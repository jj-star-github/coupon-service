package com.qmfresh.coupon.services.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
public class Operator implements Serializable {
    /**
     * 操作者id
     */
    private Integer operatorId;
    /**
     * 操作者名称
     */
    private String operatorName;

    public static Operator create(Integer operatorId, String operatorName) {
        Operator operator = new Operator();
        operator.setOperatorId(operatorId);
        operator.setOperatorName(operatorName);
        return operator;
    }
}
