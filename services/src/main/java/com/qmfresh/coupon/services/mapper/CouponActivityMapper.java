package com.qmfresh.coupon.services.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.services.entity.CouponActivity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
public interface CouponActivityMapper extends BaseMapper<CouponActivity> {

    /**
     * 查询已审批通过的优惠券信息
     *
     * @param bean
     * @return
     */
    List<CouponActivity> queryCouponActivityInfo(@Param("condition") QueryCouponInfoBean bean);

    /**
     * 查询已审批通过的优惠券的个数
     *
     * @param bean
     * @return
     */
    int queryCouponActivityCount(@Param("condition") QueryCouponInfoBean bean);

    /**
     * 更新活动的状态
     * @param bean
     * @return
     */
    int updateActivityStatus(@Param("condition") CouponActivityUpdate bean);
    
    /**
     * 批量删除活动
     * @param ids
     * @return
     */
    int delActivityBatch(@Param("ids") List<Integer> ids);

    /**
     * 分别查询出优惠券已使用,未使用，已过期的优惠券数量
     * @param couponSendDetailDto
     * @return
     */
    CouponSendDetailDto queryCouponUseCount(@Param("condition")CouponSendDetailDto couponSendDetailDto);

    /**
     * 根据id查询优惠券活动信息
     * @param id
     * @return
     */
    CouponActivity queryInfoById(@Param("id")Integer id);
}