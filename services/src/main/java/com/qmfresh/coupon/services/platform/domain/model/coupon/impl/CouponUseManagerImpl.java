package com.qmfresh.coupon.services.platform.domain.model.coupon.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponUseManager;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.CouponUseMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.CouponUse;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName CouponUseManagerImpl
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/30 17:50
 */
@Service
public class CouponUseManagerImpl extends ServiceImpl<CouponUseMapper, CouponUse> implements ICouponUseManager {

    @Resource
    private CouponUseMapper couponUseMapper;

    @Override
    public void reimbursBatchByCouponId(List<Integer> couponIdList, Integer ut) {
        couponUseMapper.reimbursBatchByCouponId(couponIdList, ut);
    }

    @Override
    public CouponUse queryByOrderNo(String orderNo) {
        return couponUseMapper.queryByOrderNo(orderNo);
    }
}
