package com.qmfresh.coupon.services.platform.infrastructure.dubbo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.client.template.CouponTemplateApiService;
import com.qmfresh.coupon.client.template.dto.*;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityTimeRule;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.services.entity.CouponActivity;
import com.qmfresh.coupon.services.entity.CouponStatus;
import com.qmfresh.coupon.services.manager.impl.CouponSendManagerImpl;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.domain.model.template.CouponTemplateInfo;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.model.template.TemplateConditionRule;
import com.qmfresh.coupon.services.platform.domain.model.template.TemplateTimeRule;
import com.qmfresh.coupon.services.platform.domain.model.template.command.Sku;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.ChannelEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateCouponTypeEnums;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateStatusTypeEnums;
import com.qmfresh.coupon.services.platform.domain.shared.BaseUtil;
import com.qmfresh.coupon.services.platform.domain.shared.BusinessException;
import com.qmfresh.coupon.services.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.CouponMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmgyl.rpc.result.CodeMsg;
import com.qmgyl.rpc.result.DubboResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CouponTemplateApiServiceImpl implements CouponTemplateApiService {

    @Resource
    private ICouponTemplateManager templateManager;
    @Resource
    private CouponMapper couponMapper;
    @Resource
    private ICouponManager couponManager;
    @Resource
    private CouponSendManagerImpl couponSendManagerImpl;

    @Override
    public DubboResult<TemplateBingActResultDTO> bingTemplate(TemplateBingMeetingDTO dto) {
        log.info("券模板绑定会场，入参："+ JSON.toJSONString(dto));
        DubboResult dubboResult = new DubboResult();
        if(dto.getActId() == null || CollectionUtils.isEmpty(dto.getTemplateIds())){
            throw new BusinessException("参数有误");
        }
        try{
            TemplateBingActResultDTO resultDTO = templateManager.bingTemplate(dto.getActId(),dto.getActName(),dto.getTemplateIds());
            dubboResult.success(resultDTO);
        }catch (Exception e){
            log.error("券模板绑定会场，接口异常 e={}",LogExceptionStackTrace.errorStackTrace(e));
            dubboResult.failure(new CodeMsg(20001001L, "系统错误"),e.getMessage());
        }
        log.info("券模板绑定会场，返回参："+ JSON.toJSONString(dubboResult));
        return dubboResult;
    }

    @Override
    public DubboResult<List<TemplateQueryByActResultDTO>> queryByAct(TemplateQueryByActDTO dto) {
        log.info("查询会场下的会场券，入参："+ JSON.toJSONString(dto));
        if(dto.getActId() == null){
            throw new BusinessException("参数会场id为空！");
        }
        DubboResult dubboResult = new DubboResult();
        try{
            List<TemplateQueryByActResultDTO> resultDTOList = buildActResult(dto.getActId(),dto.getUserId());
            dubboResult.success(resultDTOList);
        }catch (Exception e){
            log.error("查询会场下的会场券，接口异常 e={}",LogExceptionStackTrace.errorStackTrace(e));
            dubboResult.failure(new CodeMsg(20001001L, "系统错误"),e.getMessage());
        }
        log.info("查询会场下的会场券，返回参："+ JSON.toJSONString(dubboResult));
        return dubboResult;
    }

    @Override
    public DubboResult<ReturnAppDTO> newSettlementQuery(SettQueryCouponRequestDTO dto) {
        log.info("商城结算页查询优惠券，入参："+JSON.toJSONString(dto));
        if(dto.getUserId() == null){
            throw new BusinessException("参数UserId为空！");
        }
        if(CollectionUtils.isEmpty(dto.getGoodsList())){
            throw new BusinessException("参数GoodsList为空！");
        }
        DubboResult dubboResult = new DubboResult();
        try{
            ReturnAppDTO returnAppDTO = buildReturnApp(dto.getUserId(),dto.getShopId(),buildSkuList(dto.getGoodsList()));
            dubboResult.success(returnAppDTO);
        }catch (Exception e){
            log.error("商城结算页查询优惠券，接口异常 e={}",LogExceptionStackTrace.errorStackTrace(e));
            dubboResult.failure(new CodeMsg(20001001L, "系统错误"),e.getMessage());
        }
        log.info("商城结算页查询优惠券，返回参："+JSON.toJSONString(dubboResult));
        return dubboResult;
    }

    public List<TemplateQueryByActResultDTO> buildActResult(String actId,Integer userId){
        List<TemplateQueryByActResultDTO> resultDTOList = new ArrayList<>();
        List<CouponTemplate>  couponTemplateList = templateManager.queryByAct(actId);
        if(CollectionUtils.isEmpty(couponTemplateList)){
            return resultDTOList;
        }
        for (CouponTemplate couponTemplate : couponTemplateList){
            //已领数量
            int alreadySendNumber = 0;
            if(userId != null && userId > 0){
                alreadySendNumber = couponMapper.countByUseId(userId,couponTemplate.getId());
            }
            TemplateConditionRule couponRule = JSON.parseObject(couponTemplate.getCouponRule(),new TypeReference<TemplateConditionRule>(){});
            //每人限领数量
            Integer useMaxReceiveNum = couponRule.getUseReceiveNum() != null ? couponRule.getUseReceiveNum() : 9999;
            //用户还能领取数量
            Integer useCanReceiveNum = useMaxReceiveNum - alreadySendNumber > 0 ? useMaxReceiveNum - alreadySendNumber : 0;
            //库存为0时，用户领取量也为0
            if(couponTemplate.getCouponNumber()==0){
                useCanReceiveNum = 0;
            }
            TemplateQueryByActResultDTO resultDTO =CouponTemplate.creatByActResult(couponTemplate,useMaxReceiveNum,useCanReceiveNum);
            resultDTOList.add(resultDTO);
        }
        return resultDTOList;
    }

    public ReturnAppDTO buildReturnApp(Long userId,Integer shopId,List<Sku> skuList){
        ReturnAppDTO returnAppDTO = new ReturnAppDTO();
        // 查询用户可以使用的优惠券
        List<Coupon> couponList = couponManager.queryByUser(userId.intValue(), CouponStatus.UNUSED.getCode(), ChannelEnums.COUPON_RANDOM_REDUCE.getCode());
        if (CollectionUtils.isEmpty(couponList)) {
            return returnAppDTO;
        }
        List<Integer> templateIdList = couponList.stream().map(Coupon :: getOriginalId).distinct().collect(Collectors.toList());
    // 查询模板信息
    List<CouponTemplate> templateList =
        templateManager.queryByIdList(templateIdList, TemplateStatusTypeEnums.IN_USE_STATUS.getCode());
        if (CollectionUtils.isEmpty(templateList)) {
            return returnAppDTO;
        }
        Map<Integer, CouponTemplate> couponTemplateMap = new HashMap<>();
        templateList.forEach(
                couponTemplate -> {
                    couponTemplateMap.put(couponTemplate.getId(), couponTemplate);
                }
        );
        List<ReturnAppCanUseBean> canUseList = new ArrayList<>();
        List<ReturnAppCanNotUseBean> unUseList = new ArrayList<>();
        for (Coupon coupon : couponList){
            ReturnAppCanNotUseBean canNotUseBean = new ReturnAppCanNotUseBean();
            // 获取券活动
            CouponTemplate template = couponTemplateMap.get(coupon.getOriginalId());
            TemplateTimeRule timeRule = JSON.parseObject(template.getUseTimeRule(),new TypeReference<TemplateTimeRule>(){});
            //验证小时段
            boolean availableResult = CouponTemplateInfo.inAvailableRuleTime(timeRule);
            if (!availableResult) {
                canNotUseBean = buildCanNotUse("不在适用时间段内",coupon,template);
                unUseList.add(canNotUseBean);
                continue;
            }
            //验证门店
            boolean verifyShop= templateManager.verifyShop(shopId,template.getApplicableShopType(),template.getId());
            if(!verifyShop){
                canNotUseBean = buildCanNotUse("不在适用门店内",coupon,template);
                unUseList.add(canNotUseBean);
                continue;
            }
            //验证品
            List<Sku> canUseGoods = templateManager.buildGoodsInfo(skuList,template);
            boolean verifyGoods= templateManager.verifyGoods(canUseGoods,template);
            if(!verifyGoods){
                canNotUseBean = buildCanNotUse("商品总金额不足",coupon,template);
                unUseList.add(canNotUseBean);
                continue;
            }
            ReturnAppCanUseBean canUseBean = buildCanUse(coupon,template);
            canUseList.add(canUseBean);
        }
        returnAppDTO.setCanUseList(canUseList);
        returnAppDTO.setUnUseList(unUseList);
        return returnAppDTO;
    }

    public ReturnAppCanUseBean buildCanUse(Coupon coupon,CouponTemplate template){
        ReturnAppCanUseBean canUseBean = new ReturnAppCanUseBean();
        canUseBean.setId(coupon.getId());
        canUseBean.setApplicableGoodsType(coupon.getApplicableGoodsType());
        canUseBean.setCouponActivityName(template.getCouponName());
        canUseBean.setCouponCode(coupon.getCouponCode());
        canUseBean.setCouponId(coupon.getId());
        canUseBean.setCouponName(coupon.getCouponName());
        if (template.getCouponType().equals(TemplateCouponTypeEnums.COUPON_MANJIAN.getCode()) || template.getCouponType().equals(TemplateCouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
            // 满减或者随机金额
            // 前端展示固定金额样式
            canUseBean.setPreferentialType(CouponTypeEnums.FIXED_AMOUNT_TYPE.getCode());
        } else if (template.getCouponType().equals(TemplateCouponTypeEnums.COUPON_DISCOUNT.getCode())) {
            // 折扣类型
            canUseBean.setPreferentialType(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode());
            canUseBean.setDiscount(coupon.getCouponDiscount());
        }
        canUseBean.setExpiredFlag(BaseUtil.buildExpiredFlag(coupon.getUseTimeEnd()));
        canUseBean.setLimitChannel(coupon.getLimitChannel());
        canUseBean.setPreferentialMoney(coupon.getSubMoney());
        canUseBean.setSourceShopId(coupon.getSourceShopId());
        canUseBean.setStatus(coupon.getStatus());
        ActivityTimeRule timeRule = JSON.parseObject(template.getUseTimeRule(), new TypeReference<ActivityTimeRule>() {});
        if (timeRule.getIsAllDay() != null && timeRule.getIsAllDay().equals(1)) {
            canUseBean.setUseHMSRule(timeRule.getHmsStart() + "--" + timeRule.getHmsEnd());
        } else {
            canUseBean.setUseHMSRule("全天可用");
        }
        canUseBean.setUseInstruction(template.getUseInstruction());
        canUseBean.setUseShopId(coupon.getUseShopId());
        canUseBean.setValidityPeriod(BaseUtil.buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd()));
        return canUseBean;
    }

    public ReturnAppCanNotUseBean buildCanNotUse(String unUseReason,Coupon coupon,CouponTemplate template){
        ReturnAppCanNotUseBean canNotUseBean = new ReturnAppCanNotUseBean();
        canNotUseBean.setUnUseReason(unUseReason);
        canNotUseBean.setApplicableGoodsType(coupon.getApplicableGoodsType());
        canNotUseBean.setCouponActivityName(template.getCouponName());
        canNotUseBean.setCouponCode(coupon.getCouponCode());
        canNotUseBean.setCouponId(coupon.getId());
        canNotUseBean.setCouponName(coupon.getCouponName());
        if (template.getCouponType().equals(TemplateCouponTypeEnums.COUPON_MANJIAN.getCode()) || template.getCouponType().equals(TemplateCouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
            // 满减或者随机金额
            // 前端展示固定金额样式
            canNotUseBean.setPreferentialType(CouponTypeEnums.FIXED_AMOUNT_TYPE.getCode());
        } else if (template.getCouponType().equals(TemplateCouponTypeEnums.COUPON_DISCOUNT.getCode())) {
            // 折扣类型
            canNotUseBean.setPreferentialType(CouponTypeEnums.DISCOUNT_AMOUNT_TYPE.getCode());
            canNotUseBean.setDiscount(coupon.getCouponDiscount());
        }
        canNotUseBean.setPreferentialMoney(coupon.getSubMoney());
        canNotUseBean.setId(coupon.getId());
        canNotUseBean.setLimitChannel(coupon.getLimitChannel());
        canNotUseBean.setSourceShopId(coupon.getSourceShopId());
        canNotUseBean.setStatus(coupon.getStatus());
        ActivityTimeRule timeRule = JSON.parseObject(template.getUseTimeRule(), new TypeReference<ActivityTimeRule>() {});
        if (timeRule.getIsAllDay() != null && timeRule.getIsAllDay().equals(1)) {
            canNotUseBean.setUseHMSRule(timeRule.getHmsStart() + "--" + timeRule.getHmsEnd());
        } else {
            canNotUseBean.setUseHMSRule("全天可用");
        }
        canNotUseBean.setUseInstruction(template.getUseInstruction());
        canNotUseBean.setUseShopId(coupon.getUseShopId());
        canNotUseBean.setValidityPeriod(BaseUtil.buildValidityPeriod(coupon.getUseTimeStart(), coupon.getUseTimeEnd()));
        return canNotUseBean;
    }

    public List<Sku> buildSkuList(List<GoodsInfo> goodsInfoList){
        List<Sku> skuList = goodsInfoList.stream().map(e -> {
            Sku sku = new Sku();
            sku.setPrice(e.getPrice());
            sku.setSkuId(e.getSkuId());
            sku.setAmount(e.getAmount());
            sku.setClass1Id(e.getClass1Id());
            sku.setClass2Id(e.getClass2Id());
            sku.setGoodType(e.getGoodType());
            sku.setMeetingId(e.getMeetingId());
            sku.setSaleClass1Id(e.getSaleClass1Id());
            sku.setSaleClass2Id(e.getSaleClass2Id());
            sku.setSsuId(e.getSsuId());
            return sku; }).collect(Collectors.toList());
        return skuList;
    }

}
