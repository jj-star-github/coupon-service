package com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.CouponUse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author xbb
 * @since 2020-03-30
 */
public interface CouponUseMapper extends BaseMapper<CouponUse> {

    /**
     * 批量回退
     * @param couponIdList
     */
    void reimbursBatchByCouponId(@Param("couponIdList") List<Integer> couponIdList, @Param("ut") Integer ut);

    CouponUse queryByOrderNo(@Param("orderNo")String orderNo);

}