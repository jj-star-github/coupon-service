package com.qmfresh.coupon.services.platform.domain.model.template;

import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateTimeRule implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * type=1 的起始时间
     */
    private Integer timeStart;
    /**
     * type=1 的截止时间
     */
    private Integer timeEnd;
    /**
     * 领券当日起多少天可用
     */
    private Integer todayCanUse;
    /**
     * 领券次日起多少天可用
     */
    private Integer tomorrowCanUse;
    /**
     * 是否全天，0是1否
     */
    private Integer isAllDay;
    /**
     * isAllDay = 1 的小时段开始
     */
    private String hmsStart;
    /**
     * isAllDay = 1 的小时段结束
     */
    private String hmsEnd;
}
