package com.qmfresh.coupon.services.manager.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvailableCouponQuery;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.services.entity.CouponActivity;
import com.qmfresh.coupon.services.entity.CouponSend;
import com.qmfresh.coupon.services.entity.CouponSendIds;
import com.qmfresh.coupon.services.manager.ICouponSendManager;
import com.qmfresh.coupon.services.mapper.CouponActivityMapper;
import com.qmfresh.coupon.services.mapper.CouponSendIdsMapper;
import com.qmfresh.coupon.services.mapper.CouponSendMapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
@Service
@Slf4j
public class CouponSendManagerImpl extends ServiceImpl<CouponSendMapper, CouponSend> implements ICouponSendManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(CouponSendManagerImpl.class);

    @Resource
    private CouponSendMapper sendMapper;
    @Resource
    private CouponActivityMapper couponActivityMapper;
    @Resource
    private CouponSendIdsMapper couponSendIdsMapper;

    @Override
    public int updateApprovalInfo(CouponSendUpdate bean) {
        return sendMapper.updateApprovalInfo(bean);
    }

    @Override
    public List<QueryCouponSendReturnBean> queryInfoList(QueryCouponSendListBean bean) {
        return sendMapper.queryInfoList(bean);
    }

    @Override
    public int sendInfoCount(QueryCouponSendListBean bean) {
        return sendMapper.sendInfoCount(bean);
    }

    @Override
    public int delCouponSendActivity(Integer id) {
        return sendMapper.delCouponSendActivity(id);
    }

    @Override
    public int pauseSendActivity(Integer id) {
        return sendMapper.pauseSendActivity(id);
    }

    @Override
    public int recoverPauseSendActivity(Integer id) {
        return sendMapper.recoverPauseSendActivity(id);
    }

    @Override
    public List<CouponSend> queryAvailableSendCouponList(MemberAvailableCouponQuery param) {
        return sendMapper.queryAvailableSendCouponList(param);
    }

    @Override
    public int queryAvailableSendCouponCount(MemberAvailableCouponQuery param) {
        return sendMapper.queryAvailableSendCouponCount(param);
    }

    @Override
    public CouponSendDetailDto queryCouponSendById(Integer id) {
        CouponSend couponSend;
        CouponSendIds couponSendIds = new CouponSendIds();
        CouponActivity couponActivity = new CouponActivity();
        CouponSendDetailDto couponSendDetailDto = new CouponSendDetailDto();
        try {
            LambdaQueryWrapper<CouponSend> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(CouponSend::getIsDeleted, 0);
            queryWrapper.eq(CouponSend::getId, id);
            couponSend = sendMapper.selectOne(queryWrapper);

            //todo refactor
            if (couponSend != null) {
                LambdaQueryWrapper<CouponSendIds> idsQueryWrapper = new LambdaQueryWrapper<>();
                idsQueryWrapper.eq(CouponSendIds::getSendId, couponSend.getId());
                idsQueryWrapper.eq(CouponSendIds::getIsDeleted, 0);
                couponSendIds = couponSendIdsMapper.selectOne(idsQueryWrapper);
                if (couponSendIds != null && couponSendIds.getCouponId() != null) {
                    LambdaQueryWrapper<CouponActivity> activityQuery = new LambdaQueryWrapper<>();
                    activityQuery.eq(CouponActivity::getId, couponSendIds.getCouponId());
                    activityQuery.eq(CouponActivity::getIsDeleted, 0);
                    couponActivity = couponActivityMapper.selectOne(activityQuery);
                }
            }
            if (couponSend == null || couponSendIds == null || couponActivity == null) {
                throw new BusinessException("查询发放优惠券明细异常");
            }
            couponSendDetailDto.setCouponId(couponActivity.getId());
            couponSendDetailDto.setSendId(couponSend.getId());
            couponSendDetailDto.setCouponName(couponActivity.getCouponName());
            couponSendDetailDto.setSubMoney(couponActivity.getCouponRule());

            String couponType = "";
            String applicableGoodsType = "";
            String subMoney = "";
            String sendStatus = "";
            String effectiveTime = "";
            String couponCondition = "";
            String sendType = "";
            Integer sendNumPerTime = null;
            JSONObject couponRule = JSONObject.parseObject(couponActivity.getCouponRule());
            // 活动规则
            ActivityConditionRule rule = (ActivityConditionRule) JSONObject.toJavaObject(couponRule, ActivityConditionRule.class);
            if (couponActivity.getCouponType().equals(CouponTypeEnums.COUPON_MANJIAN.getCode())) {
                couponType = CouponTypeEnums.COUPON_MANJIAN.getDetail();
                subMoney = rule.getSubMoney().toString();
            } else if (couponActivity.getCouponType().equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
                couponType = CouponTypeEnums.COUPON_DISCOUNT.getDetail();
                if (rule.getDiscount() != null) {
                    if (rule.getMaxDiscountMoney() != null) {
                        subMoney = "打" + rule.getDiscount() / 10 + "折,最多优惠" + rule.getMaxDiscountMoney().toString() + "元";
                    } else {
                        subMoney = "打" + rule.getDiscount() / 10 + "折";
                    }
                }
            } else if (couponActivity.getCouponType().equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
                couponType = CouponTypeEnums.COUPON_RANDOM_REDUCE.getDetail();
                subMoney = rule.getRandomMoneyStart().toString() + "~" + rule.getRandomMoneyEnd();
            }
            if (couponActivity.getApplicableGoodsType().equals(CouponTypeEnums.ALL_GOODS_CAN_USE.getCode())) {
                applicableGoodsType = CouponTypeEnums.ALL_GOODS_CAN_USE.getDetail();
            } else if (couponActivity.getApplicableGoodsType().equals(CouponTypeEnums.GOODS_CAN_USE.getCode())) {
                applicableGoodsType = CouponTypeEnums.GOODS_CAN_USE.getDetail();
            } else if (couponActivity.getApplicableGoodsType().equals(CouponTypeEnums.GOODS_CAN_NOT_USE.getCode())) {
                applicableGoodsType = CouponTypeEnums.GOODS_CAN_NOT_USE.getDetail();
            }
            if (couponSend.getStatus().equals(CouponTypeEnums.COUPON_SEND_APPROVING.getCode())) {
                sendStatus = CouponTypeEnums.COUPON_SEND_APPROVING.getDetail();
            } else if (couponSend.getStatus().equals(CouponTypeEnums.COUPON_SEND_APPROVED.getCode())) {
                sendStatus = CouponTypeEnums.COUPON_SEND_APPROVED.getDetail();
            } else if (couponSend.getStatus().equals(CouponTypeEnums.COUPON_SEND_NOT_APPROVED.getCode())) {
                sendStatus = CouponTypeEnums.COUPON_SEND_NOT_APPROVED.getDetail();
            } else if (couponSend.getStatus().equals(CouponTypeEnums.COUPON_SEND_STOP.getCode())) {
                sendStatus = CouponTypeEnums.COUPON_SEND_STOP.getDetail();
            }
            if (couponSend.getSendType().equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_FOUR.getCode())) {
                sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_FOUR.getDetail();
            } else if (couponSend.getSendType().equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_FIVE.getCode())) {
                sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_FIVE.getDetail();
            } else if (couponSend.getSendType().equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_ONE.getCode())) {
                sendNumPerTime = couponSend.getSendNumPerTime();
                sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_ONE.getDetail();
            } else if (couponSend.getSendType().equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_TWO.getCode())) {
                sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_TWO.getDetail();
            } else if (couponSend.getSendType().equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_THREE.getCode())) {
                sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_THREE.getDetail();
            }

            JSONObject timeRule = JSONObject.parseObject(couponActivity.getUseTimeRule());
            // 时间规则
            ActivityTimeRule activityTimeRule = (ActivityTimeRule) JSONObject.toJavaObject(timeRule, ActivityTimeRule.class);
            if (couponActivity.getUseTimeType().equals(CouponTypeEnums.TIME_USE_FROM_TO.getCode())) {
                effectiveTime = buildValidityPeriod(activityTimeRule.getTimeStart(), activityTimeRule.getTimeEnd());
            } else if (couponActivity.getUseTimeType().equals(CouponTypeEnums.BEGIN_FROM_GET.getCode())) {
                effectiveTime = "领券当日起" + activityTimeRule.getTodayCanUse() + "天内可用";
            } else if (couponActivity.getUseTimeType().equals(CouponTypeEnums.BEGIN_FROM_TOMORROW.getCode())) {
                effectiveTime = "领券次日起" + activityTimeRule.getTodayCanUse() + "天内可用";
            }
            if (couponActivity.getCouponCondition().equals(CouponTypeEnums.NO_USE_CONDITION.getCode())) {
                couponCondition = CouponTypeEnums.NO_USE_CONDITION.getDetail();
            } else if (couponActivity.getCouponCondition().equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())) {
                couponCondition = "订单满" + couponActivity.getNeedMoney() + "元可用";
            }
            couponSendDetailDto.setCouponCondition(couponCondition);
            couponSendDetailDto.setCouponType(couponType);
            couponSendDetailDto.setApplicableGoodsType(applicableGoodsType);
            couponSendDetailDto.setSubMoney(subMoney);
            couponSendDetailDto.setSendStatus(sendStatus);
            couponSendDetailDto.setEffectiveTime(effectiveTime);
            //查询此活动已使用,未使用,已过期的数量
            CouponSendDetailDto countDto = couponActivityMapper.queryCouponUseCount(couponSendDetailDto);
            if (countDto != null) {
                couponSendDetailDto.setAlreadyUseNum(countDto.getAlreadyUseNum());
                couponSendDetailDto.setNotUseNum(countDto.getNotUseNum());
                couponSendDetailDto.setExpireNum(countDto.getExpireNum());
                Long alreadySendNum = countDto.getAlreadyUseNum() + countDto.getNotUseNum() + countDto.getExpireNum();
                couponSendDetailDto.setAlreadySendNum(alreadySendNum);
            }
            couponSendDetailDto.setRemark(couponSend.getRemarks());
            couponSendDetailDto.setSendNumPerTime(sendNumPerTime);
            couponSendDetailDto.setTotalSendNum(couponSend.getSendNum());
            couponSendDetailDto.setSendType(sendType);
        } catch (Exception e) {
            LOGGER.info("查询发放优惠券活动明细异常", e);
            throw new BusinessException("查询发放优惠券活动明细异常");
        }

        return couponSendDetailDto;
    }

    /**
     * 构造优惠券使用有效期
     *
     * @param timeStart
     * @param timeEnd
     * @return
     */
    private String buildValidityPeriod(Integer timeStart, Integer timeEnd) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String startTime = "";
        String endTime = "";
        try {
            startTime = format.format(new Date(timeStart * 1000L));
            endTime = format.format(new Date(timeEnd * 1000L));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("查询有效期信息有误");
        }
        return "有效期:" + startTime + " 至 " + endTime;
    }
}
