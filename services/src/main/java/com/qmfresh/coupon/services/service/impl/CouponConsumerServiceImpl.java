/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/7
 * Description:NewMemSendCouponServiceImpl.java
 */
package com.qmfresh.coupon.services.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.qmfresh.coupon.interfaces.dto.coupon.*;
import com.qmfresh.coupon.services.platform.infrastructure.http.member.GradeInfoQuery;
import com.qmfresh.coupon.interfaces.dto.member.GradeInfoRes;
import com.qmfresh.coupon.services.platform.infrastructure.http.member.GradeInfoReturn;
import com.qmfresh.coupon.interfaces.enums.*;
import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.common.utils.ListUtil;
import com.qmfresh.coupon.services.entity.*;
import com.qmfresh.coupon.services.manager.*;
import com.qmfresh.coupon.services.message.RocketMQSender;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponUseManager;
import com.qmfresh.coupon.services.platform.infrastructure.job.SendCouponDto;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.CouponUse;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.CouponTemplateMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.service.ICouponConsumerService;
import com.qmfresh.coupon.services.service.ICouponInfoBuildService;
import com.qmfresh.coupon.services.service.IQueryMemberInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.eclipse.jetty.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 发券逻辑
 * @author lxc
 */
@Service
@Slf4j
public class CouponConsumerServiceImpl implements ICouponConsumerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponConsumerServiceImpl.class);

    @Autowired
    private ICouponManager couponManager;
    @Autowired
    private ICouponUseManager couponUseManager;
    @Autowired
    private ICouponSendIdsManager idsManager;
    @Autowired
    private ICouponInfoBuildService buildService;
    @Autowired
    private IQueryMemberInfoService memberService;
    @Resource
    private DefaultMQProducer defaultMQProducer;
    @Resource
    private CouponTemplateMapper couponTemplateMapper;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void couponReimburse(CouponRemunerateRequestBean bean) {
        int now = DateUtil.getCurrentTimeIntValue();
        Coupon coupon = new Coupon();
        coupon.setStatus(CouponTypeEnums.COUPON_UNUSED.getCode());
        coupon.setUT(now);
        couponManager.update(coupon, new QueryWrapper<Coupon>()
            .eq("id", bean.getCouponId())
            .eq("user_id", bean.getUserId())
            .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
        CouponUse couponUse = new CouponUse();
        couponUse.setIsReimburse(ReimburseTypeEnum.IS_REIMBURSE.getCode());
        couponUse.setUT(now);
        couponUseManager.update(couponUse, new QueryWrapper<CouponUse>()
            .eq("coupon_id", bean.getCouponId())
            .eq("user_id", bean.getUserId())
            .eq("is_reimburse", ReimburseTypeEnum.NOT_REIMBURSE.getCode())
            .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
    }

    @Override
    public void onlineCouponSend(CashQuerySendCouponRequestBean bean) {
        LOGGER.info("start into method onlineCouponSend,request:" + JSON.toJSONString(bean));
        // 查询券活动表的id,是否是已审批通过，是保存优惠券并返回给收银端，否则返回空
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        // 拟发放数量
        Integer sendNum = bean.getSendNum();
        // 优惠券活动id
        Integer activityId = bean.getActivityId();
        CouponTemplate activity = couponTemplateMapper.queryRunInById(activityId);
        if (activity != null) {
            List<Coupon> couponList = new ArrayList<>();
            for (int i = 0; i < sendNum; i++) {
                Coupon coupon = buildService.buildCouponIfo(bean.getUserId(), activity, bean.getSourceOrderNo(), bean.getSourceShopId(), activity.getLimitChannel(),0, null,null);
                LOGGER.info("组装优惠券信息，源:{},组装后:{}",JSON.toJSONString(bean),JSON.toJSONString(coupon));
                couponList.add(coupon);
            }
            // 保存优惠券
            couponManager.saveBatch(couponList);
        }
    }

    @Override
    public void consumeUsedCoupon(List<CouponUseRequestBean> list) {
        boolean result = consumeUsedCouponNew(list);
        if(result){
            Coupon coupon = couponManager.getOne(new QueryWrapper<Coupon>()
                    .eq("id", list.get(0).getCouponId())
                    .eq("is_deleted", CouponSendTypeEnums.IS_DELETED_DEFAULT.getCode()));

            UseCouponSendMQDTO useCoupon = new UseCouponSendMQDTO();
            useCoupon.setCouponId(coupon.getId());
            useCoupon.setOriginalId(coupon.getOriginalId());
            useCoupon.setCouponCode(coupon.getCouponCode());
            useCoupon.setCouponCondition(coupon.getCouponCondition());
            useCoupon.setNeedMoney(coupon.getNeedMoney());
            useCoupon.setCouponType(coupon.getCouponType());
            useCoupon.setSubMoney(coupon.getSubMoney());
            useCoupon.setCouponDiscount(coupon.getCouponDiscount());
            useCoupon.setOrderNo(coupon.getOrderNo());
            useCoupon.setOrderAmount(coupon.getOrderAmount());
            useCoupon.setSourceShopId(coupon.getSourceShopId());
            useCoupon.setUseShopId(coupon.getUseShopId());
            useCoupon.setCouponPrice(coupon.getCouponPrice());
            useCoupon.setUseTime(coupon.getUseTime());
            log.info("使用优惠券-通知MQ：", JSON.toJSONString(useCoupon));
            RocketMQSender.doSend(defaultMQProducer,RocketMQSender.COUPON_COUPON_USED,useCoupon);
        }
    }
    @Transactional(rollbackFor = Exception.class)
    public boolean consumeUsedCouponNew(List<CouponUseRequestBean> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new BusinessException("券信息不能为空");
        }
        for (CouponUseRequestBean bean : list) {
            if (bean.getCouponId() == null) {
                throw new BusinessException("优惠券id不能为空");
            }
            if (bean.getOrderNo() == null) {
                throw new BusinessException("订单id不能为空");
            }
            int now = DateUtil.getCurrentTimeIntValue();
            // 防重查询，如果优惠券状态，如果已经使用过，返回错误
            Coupon couponQuery = couponManager.getById(bean.getCouponId());
            // 优惠券状态
            Integer couponStatus = couponQuery.getStatus();
            // 优惠券状态未使用,则可更新状态
            if (couponStatus.equals(CouponTypeEnums.COUPON_UNUSED.getCode())) {
                // 更新优惠券的状态
                Coupon coupon = new Coupon();
                //                coupon.setUserId(bean.getUserId());
                coupon.setSourceSystem(bean.getSourceSystem());
                coupon.setOrderNo(StringUtils.isEmpty(bean.getOrderNo()) ? "" : bean.getOrderNo());
                coupon.setOrderAmount(bean.getTotalAmount());
                coupon.setGoodsNum(bean.getGoodsNum());
                coupon.setStatus(CouponTypeEnums.COUPON_USED.getCode());
                coupon.setUseTime(now);
                coupon.setUT(now);
                coupon.setUseShopId(bean.getShopId() == null ? 0 : bean.getShopId());
                coupon.setCouponPrice(bean.getCouponPrice());
                couponManager.update(coupon, new QueryWrapper<Coupon>()
                    .eq("id", bean.getCouponId())
                    .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));

                CouponUse couponUse = new CouponUse();
                BeanUtils.copyProperties(couponQuery, couponUse);
                couponUse.setCouponId(bean.getCouponId());
                couponUse.setOrderNo(StringUtils.isEmpty(bean.getOrderNo()) ? "" : bean.getOrderNo());
                couponUse.setOrderAmount(bean.getTotalAmount());
                couponUse.setGoodsNum(bean.getGoodsNum());
                if (bean.getUserId() != null) {
                    couponUse.setUserId(bean.getUserId());
                }
                couponUse.setUseShopId(bean.getShopId());
                couponUse.setCouponPrice(bean.getCouponPrice());
                couponUse.setUseTime(bean.getUseTimestamp() == null ? now : BigDecimal.valueOf(bean.getUseTimestamp()).divide(BigDecimal.valueOf(1000), RoundingMode.HALF_UP).intValue());
                couponUse.setUT(now);
                return couponUseManager.save(couponUse);
            }
        }
        return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderCancel(OrderCancelRequestBean param) {
        if (StringUtil.isBlank(param.getOrderId()) || CollectionUtils.isEmpty(param.getCouponsCodes())) {
            return;
        }
        List<Coupon> useCouponList = couponManager.queryUseCouponByOrderNo(param.getOrderId(), param.getCouponsCodes());
        if (CollectionUtils.isEmpty(useCouponList)) {
            return;
        }
        int now = DateUtil.getCurrentTimeIntValue();
        List<Integer> couponIdList = useCouponList.stream().map(Coupon::getId).collect(Collectors.toList());
        Coupon coupon = new Coupon();
        coupon.setCouponPrice(BigDecimal.ZERO);
        coupon.setStatus(CouponTypeEnums.COUPON_UNUSED.getCode());
        coupon.setUT(now);
        couponManager.updateBatch(couponIdList, coupon);
        couponUseManager.reimbursBatchByCouponId(couponIdList, now);
    }

    /**
     * 获取不同类型会员需要发放优惠券的活动id
     *
     * @param greenSendList
     * @param redSendList
     * @param goldSendList
     * @return
     */
    private Map<String, List<Integer>> getActivityIdMap(List<Integer> greenSendList, List<Integer> redSendList, List<Integer> goldSendList) {
        Map<String, List<Integer>> typeActivityMap = new HashMap<>();

        List<Integer> allTypeList = new ArrayList<>();
        allTypeList.addAll(greenSendList);
        allTypeList.addAll(redSendList);
        allTypeList.addAll(goldSendList);
        if (CollectionUtils.isEmpty(allTypeList)) {
            return typeActivityMap;
        }
        List<CouponSendIds> sendIdList = idsManager.list(new QueryWrapper<CouponSendIds>()
            .in("send_id", allTypeList)
            .eq("is_deleted", IsDeleteEnums.NO.getCode())
        );
        if (CollectionUtils.isEmpty(sendIdList)) {
            return typeActivityMap;
        }
        return this.getActivityIdsMap(sendIdList, greenSendList, redSendList, goldSendList);
    }

    /**
     * 得到每种会员的活动id
     *
     * @param infoList
     * @param greenSendList
     * @param redSendList
     * @param goldSendList
     * @return
     */
    private Map<String, List<Integer>> getActivityIdsMap(List<CouponSendIds> infoList, List<Integer> greenSendList, List<Integer> redSendList, List<Integer> goldSendList) {

        Map<String, List<Integer>> listMap = new HashMap<>();
        // 青番茄
        Set<Integer> greenActivityList = new HashSet();
        // 红番茄
        Set<Integer> redActivityList = new HashSet();
        // 金番茄
        Set<Integer> goldActivityList = new HashSet();
        for (CouponSendIds couponSendIds : infoList) {
            if (greenSendList.contains(couponSendIds.getSendId())) {
                greenActivityList.add(couponSendIds.getCouponId());
            } else if (redSendList.contains(couponSendIds.getSendId())) {
                redActivityList.add(couponSendIds.getCouponId());
            } else if (goldSendList.contains(couponSendIds.getSendId())) {
                goldActivityList.add(couponSendIds.getCouponId());
            }
        }
        listMap.put("green", new ArrayList<>(greenActivityList));
        listMap.put("red", new ArrayList<>(redActivityList));
        listMap.put("gold", new ArrayList<>(goldActivityList));
        return listMap;
    }

    /**
     * 得到不同类别会员的发放id
     *
     * @param couponSend
     * @param greenList
     * @param redList
     * @param goldList
     */
    private void buildDifferentTypeList(CouponSend couponSend, List<Integer> greenList, List<Integer> redList, List<Integer> goldList) {
        if (couponSend.getTriggerCondition() == null) {
            return;
        }
        if (couponSend.getTriggerCondition().equals(MemberFinalEnum.GREEN_MEMBER.getTypeInCoupon())) {
            greenList.add(couponSend.getId());
        } else if (couponSend.getTriggerCondition().equals(MemberFinalEnum.RED_MEMBER.getTypeInCoupon())) {
            redList.add(couponSend.getId());
        } else if (couponSend.getTriggerCondition().equals(MemberFinalEnum.GOLD_MEMBER.getTypeInCoupon())) {
            goldList.add(couponSend.getId());
        }
    }

    /**
     * 用会员种类查询会员系统得到会员Id
     *
     * @param memberType
     * @return
     */
    private List<Integer> getUserIdList(Integer memberType) {
        List<GradeInfoRes> returnList = new ArrayList<>();
        for (int i = 1; ; i++) {
            GradeInfoQuery param = new GradeInfoQuery();
            param.setGradeId(memberType);
            param.setPageSize(200);
            param.setPageIndex(i);
            // 查询会员信息 
            GradeInfoReturn gradeInfoReturn = memberService.queryGradeInfoList(param);
            returnList.addAll(gradeInfoReturn.getListData());
            // 如果返回的数量小于每页大小，表示后面没有数据了,则需要跳出循环
            if (gradeInfoReturn.getListData().size() < 200) {
                break;
            }
        }
        if (CollectionUtils.isEmpty(returnList)) {
            return new ArrayList<>();
        }
        return returnList.stream().map(GradeInfoRes::getUserId).distinct().collect(Collectors.toList());
    }

}
