package com.qmfresh.coupon.services.platform.domain.model.template.command;

import lombok.Data;

@Data
public class ShopQueryCommand {
    private Integer templateId;
    private Integer shopId;
    private String shopName;
    private Integer pageNum;
    private Integer pageSize;
    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }

    public ShopQueryCommand(){}

    public ShopQueryCommand(Integer templateId,
                            Integer shopId,
                            String shopName,
                            Integer pageNum,
                            Integer pageSize){
        this.templateId=templateId;
        this.shopId=shopId;
        this.shopName=shopName;
        this.pageNum=pageNum;
        this.pageSize=pageSize;
    }
}
