package com.qmfresh.coupon.services.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxc
 * @since 2019-07-31
 */
@TableName("t_coupon_send")
public class CouponSend extends Model<CouponSend> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增长
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 发放方式(1:系统自动触发2:运营手动触发3:用户自主领取4:线上促销5:线下促销)
     */
	@TableField("send_type")
	private Integer sendType;
    /**
     * 有效期类型(1:长期有效2:有时间限制)
     */
	@TableField("time_limit_type")
	private Integer timeLimitType;
    /**
     * 有效期起始时间
     */
	@TableField("time_limit_start")
	private Integer timeLimitStart;
    /**
     * 有效期截止时间
     */
	@TableField("time_limit_end")
	private Integer timeLimitEnd;
    /**
     * 触发条件(1:新用户注册,2.会员升级：[201：青番茄升红番茄，202：红番茄升金番茄，203：青番茄升金番茄],3.会员定时发放：[301:青番茄会员，302:红番茄会员，303：金番茄会员])
     */
	@TableField("trigger_condition")
	private Integer triggerCondition;
    /**
     * 发放数量
     */
	@TableField("send_num")
	private Long sendNum;
    /**
     * 创建人id
     */
	@TableField("created_id")
	private Integer createdId;
    /**
     * 创建人名称
     */
	@TableField("created_name")
	private String createdName;
    /**
     * 审批人id
     */
	@TableField("approval_id")
	private Integer approvalId;
    /**
     * 审批人姓名
     */
	@TableField("approval_name")
	private String approvalName;
    /**
     * 审批情况(0:未审批,1:审批通过,2:审批不通过)
     */
	@TableField("status")
	private Integer status;
	/**
	 * 每次发放的张数
	 */
	@TableField("send_num_per_time")
	private Integer sendNumPerTime;
    /**
     * 备注
     */
	private String remarks;
    /**
     * 创建时间
     */
	@TableField("c_t")
	private Integer cT;
    /**
     * 更新时间
     */
	@TableField("u_t")
	private Integer uT;
    /**
     * 有效性
     */
	@TableField("is_deleted")
	private Integer isDeleted;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSendType() {
		return sendType;
	}

	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}

	public Integer getTimeLimitType() {
		return timeLimitType;
	}

	public void setTimeLimitType(Integer timeLimitType) {
		this.timeLimitType = timeLimitType;
	}

	public Integer getTimeLimitStart() {
		return timeLimitStart;
	}

	public void setTimeLimitStart(Integer timeLimitStart) {
		this.timeLimitStart = timeLimitStart;
	}

	public Integer getTimeLimitEnd() {
		return timeLimitEnd;
	}

	public void setTimeLimitEnd(Integer timeLimitEnd) {
		this.timeLimitEnd = timeLimitEnd;
	}

	public Integer getTriggerCondition() {
		return triggerCondition;
	}

	public void setTriggerCondition(Integer triggerCondition) {
		this.triggerCondition = triggerCondition;
	}

	public Long getSendNum() {
		return sendNum;
	}

	public void setSendNum(Long sendNum) {
		this.sendNum = sendNum;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(Integer approvalId) {
		this.approvalId = approvalId;
	}

	public String getApprovalName() {
		return approvalName;
	}

	public void setApprovalName(String approvalName) {
		this.approvalName = approvalName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getSendNumPerTime() {
		return sendNumPerTime;
	}

	public void setSendNumPerTime(Integer sendNumPerTime) {
		this.sendNumPerTime = sendNumPerTime;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getCT() {
		return cT;
	}

	public void setCT(Integer cT) {
		this.cT = cT;
	}

	public Integer getUT() {
		return uT;
	}

	public void setUT(Integer uT) {
		this.uT = uT;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
