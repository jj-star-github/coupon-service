/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/26
 * Description:CouponApiServiceImpl.java
 */
package com.qmfresh.coupon.services.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qmfresh.coupon.interfaces.dto.base.ListWithPage;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityConditionRule;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityGoodsRule;
import com.qmfresh.coupon.interfaces.dto.coupon.ActivityTimeRule;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponInfoDto;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponSendDetailDto;
import com.qmfresh.coupon.interfaces.dto.coupon.CreateActivityBean;
import com.qmfresh.coupon.interfaces.dto.coupon.IdParamBean;
import com.qmfresh.coupon.interfaces.dto.coupon.ModifyRelationBean;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponInfoBean;
import com.qmfresh.coupon.interfaces.dto.coupon.QueryCouponReturnBean;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvailableCouponQuery;
import com.qmfresh.coupon.interfaces.dto.member.MemberAvilableCoupon;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetail;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetailQuery;
import com.qmfresh.coupon.interfaces.dto.member.MemberSendCouponParam;
import com.qmfresh.coupon.interfaces.enums.CouponSendTypeEnums;
import com.qmfresh.coupon.interfaces.enums.CouponTypeEnums;
import com.qmfresh.coupon.services.common.exception.BusinessException;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.common.utils.ListUtil;
import com.qmfresh.coupon.services.entity.CouponActivity;
import com.qmfresh.coupon.services.entity.CouponSend;
import com.qmfresh.coupon.services.entity.CouponSendIds;
import com.qmfresh.coupon.services.entity.command.CouponActivityQueryCommand;
import com.qmfresh.coupon.services.entity.command.CouponQueryListCommand;
import com.qmfresh.coupon.services.manager.ICouponActivityManager;
import com.qmfresh.coupon.services.manager.ICouponSendIdsManager;
import com.qmfresh.coupon.services.manager.ICouponSendManager;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.CouponUseStatusEnums;
import com.qmfresh.coupon.services.platform.domain.model.activity.enums.SendCouponTagEnums;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponCount;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponListBean;
import com.qmfresh.coupon.services.platform.domain.model.template.ICouponTemplateManager;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.TemplateStatusTypeEnums;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.CouponTemplateMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import com.qmfresh.coupon.services.service.ICouponConsumerService;
import com.qmfresh.coupon.services.service.ICouponService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lxc
 */
@Service
@Slf4j
public class CouponServiceImpl implements ICouponService {

    @Autowired
    private ICouponActivityManager activityManager;
    @Autowired
    private ICouponManager couponManager;
    @Autowired
    private ICouponSendManager sendManager;
    @Autowired
    private ICouponSendIdsManager idsManager;
    @Autowired
    private ICouponConsumerService consumerService;
    @Resource
    private ICouponTemplateManager iCouponTemplateManager;
    @Resource
    private CouponTemplateMapper couponTemplateMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean createCouponActivity(CreateActivityBean bean) {
        insertIntoCouponActivity(bean);
        return true;
    }

    @Override
    public ListWithPage<QueryCouponReturnBean> queryCouponInfoList(QueryCouponInfoBean bean) {
        log.info("start into queryCouponInfoList,request:" + JSON.toJSONString(bean));
        ListWithPage<QueryCouponReturnBean> page = new ListWithPage<>();
        int now = DateUtil.getCurrentTimeIntValue();
        // 查询信息
        Page<CouponActivity> activityList = activityManager.pageCouponActivity(CouponActivityQueryCommand.create(bean.getPageIndex(), bean.getPageSize(), bean.getCouponType(), bean.getCouponStatus()));
        // 返回集合
        List<QueryCouponReturnBean> couponReturnBeanList = new ArrayList<>();
        int expiredNum = 0;

        if (CollectionUtils.isEmpty(activityList.getRecords())) {
            page.setTotalCount(0);
            page.setListData(couponReturnBeanList);
            return page;
        }

        List<Integer> activityIds = new ArrayList<>();
        for (CouponActivity activity : activityList.getRecords()) {
            activityIds.add(activity.getId());
        }

        //优惠券 todo  add  the index avoid the db back
        QueryWrapper<Coupon> query = new QueryWrapper<>();
        query.in("original_id", activityIds).eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()).groupBy("original_id").select("original_id,count(id) as couponCount");


        List<Coupon> couponList = couponManager.list(query);
        Map<Integer, Coupon> alreadyGetCountMap = new HashMap<>(48);
        for (Coupon c : couponList) {
            alreadyGetCountMap.put(c.getOriginalId(), c);
        }

        //活动已发送券数量和剩余发送券数量
        Map<Integer, CouponSend> sendMap = new HashMap<>(48);
        QueryWrapper<CouponSendIds> couponSendIdsQuery = new QueryWrapper<>();
        couponSendIdsQuery.eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()).in("coupon_id", activityIds);
        List<CouponSendIds> couponSendIds = idsManager.list(couponSendIdsQuery);
        if (CollectionUtils.isNotEmpty(couponSendIds)) {
            QueryWrapper<CouponSend> sendQuery = new QueryWrapper<>();
            sendQuery.in("id", couponSendIds.stream().map(CouponSendIds::getSendId).collect(Collectors.toList()));
            sendQuery.eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode());
            List<CouponSend> couponSends = sendManager.list(sendQuery);
            Map<Integer, Integer> activitySendIdMap = new HashMap<>(48);
            for (CouponSendIds sendId : couponSendIds) {
                activitySendIdMap.put(sendId.getSendId(), sendId.getCouponId());
            }
            for (CouponSend send : couponSends) {
                sendMap.put(activitySendIdMap.get(send.getId()), send);
            }
        }

//        //活动已使用券的金额和平均金额
        QueryWrapper<Coupon> couponQuery = new QueryWrapper<>();
        couponQuery.in("original_id", activityIds);
        couponQuery.eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()).eq("status", CouponTypeEnums.COUPON_USED.getCode()).groupBy("original_id").select("original_id,count(original_id) as couponCount");
        List<Coupon> usedCouponCount = couponManager.list(couponQuery);
        Map<Integer, Coupon> usedCouponAmountMap = new HashMap<>(48);
        if (CollectionUtils.isNotEmpty(usedCouponCount)) {
            for (Coupon c : usedCouponCount) {
                usedCouponAmountMap.put(c.getOriginalId(), c);
            }
        }


        // 遍历出有用信息
        for (CouponActivity activity : activityList.getRecords()) {
            QueryCouponReturnBean returnBean = new QueryCouponReturnBean();
            Integer activityId = activity.getId();
            // 券名称
            returnBean.setCouponName(activity.getCouponName());
            // 券类型
            Integer couponType = activity.getCouponType();
            returnBean.setCouponType(couponType);
            // 券内容
            returnBean.setCouponContent(activity.getUseInstruction());
            // 查询券表
            Coupon alreadyGetCoupon = alreadyGetCountMap.get(activityId);
            int alreadyGetCountCount = alreadyGetCoupon == null ? 0 : alreadyGetCoupon.getCouponCount();
            returnBean.setAlreadyGet((long) alreadyGetCountCount);


            // 剩余(只有线上和线下促销才有值)
            // 查询券发放关联活动id表
            CouponSend couponSend = sendMap.get(activityId);
            if (couponSend != null) {
                Integer sendType = couponSend.getSendType();
                // 只有线上和线下促销才会有发放数量
                if (sendType.equals(CouponSendTypeEnums.ONLINE_SEND.getCode()) || sendType.equals(CouponSendTypeEnums.OFFLINE_SEND.getCode())) {
                    // 发放数量
                    Long sendNum = couponSend.getSendNum() == null ? 0 : couponSend.getSendNum();
                    Long stillHas = sendNum - (long) alreadyGetCountCount;
                    returnBean.setStillHas(stillHas);
                }
            }

            Coupon coupon = usedCouponAmountMap.get(activityId);
            if (coupon != null) {
                int alreadyUsedCount = coupon.getCouponCount() == null ? 0 : coupon.getCouponCount();
                BigDecimal payAmount = coupon.getPayAmount() == null ? BigDecimal.ZERO : coupon.getPayAmount();
                returnBean.setAlreadyUsed((long) alreadyUsedCount);
                returnBean.setPayAmount(payAmount);
                if (alreadyGetCountCount != 0) {
                    returnBean.setAvgAmount(payAmount.divide(new BigDecimal(alreadyGetCountCount + ""), 2, RoundingMode.HALF_UP));
                }

            }
            // 状态(券活动表的status字段)
            returnBean.setStatus(activity.getStatus());
            returnBean.setId(activityId);
            couponReturnBeanList.add(returnBean);
        }
        // 查询个数
        int count = activityManager.queryCouponActivityCount(bean);
        page.setTotalCount(count - expiredNum);
        page.setListData(couponReturnBeanList);
        return page;
    }

    @Override
    public CreateActivityBean queryCouponDetailInfo(IdParamBean bean) {
        log.info("start into method queryCouponDetailInfo,request:" + JSON.toJSONString(bean));
        // 一个id只对应一条数据
        CreateActivityBean activityBean = new CreateActivityBean();
        try {
            // 查询数据
            CouponActivity couponActivity = activityManager.getOne(new QueryWrapper<CouponActivity>().eq("id", bean.getId()).eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
            CouponTemplate couponTemplate = iCouponTemplateManager.selectById(bean.getId());
//            CouponActivity couponActivity = activityManager.getOne(new QueryWrapper<CouponActivity>()
//                    .eq("id", bean.getId())
//                    .eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));
            // id
            activityBean.setOriginalId(couponTemplate.getId());
            // 券名称
            activityBean.setActivityName(couponTemplate.getCouponName());
            // 门槛
            activityBean.setUseCondition(couponTemplate.getCouponCondition());
            // 满足条件的金额
            activityBean.setNeedMoney(couponTemplate.getNeedMoney());
            // 券类型
            activityBean.setCouponType(couponTemplate.getCouponType());
            // 解析活动规则
            JSONObject couponRule = JSONObject.parseObject(couponTemplate.getCouponRule());
            // 活动规则
            activityBean.setConditionRule((ActivityConditionRule) JSONObject.toJavaObject(couponRule, ActivityConditionRule.class));
            // 时间类型
            activityBean.setUseTimeType(couponTemplate.getUseTimeType());
            // 解析时间规则
            JSONObject timeRule = JSONObject.parseObject(couponTemplate.getUseTimeRule());
            // 时间规则
            activityBean.setTimeRule((ActivityTimeRule) JSONObject.toJavaObject(timeRule, ActivityTimeRule.class));
            // 商品类型
            activityBean.setApplicableGoodsType(couponTemplate.getApplicableGoodsType());
            // 解析商品规则
            JSONObject goodsRule = JSONObject.parseObject(couponTemplate.getApplicableGoodsRule());
            // 商品规则
            activityBean.setGoodsRule((ActivityGoodsRule) JSONObject.toJavaObject(goodsRule, ActivityGoodsRule.class));
            // 使用说明
            activityBean.setUseInstruction(couponTemplate.getUseInstruction());
            // 创建人id
            activityBean.setCreateId(couponTemplate.getCreatedId());
            // 创建人名称
            activityBean.setCreateName(couponTemplate.getCreatedName());
            //限定渠道
            activityBean.setLimitChannel(couponTemplate.getLimitChannel());
            activityBean.setApplicableShopType(couponTemplate.getApplicableShopType());
        } catch (Exception e) {
            log.error("queryCouponDetailInfo is something wrong:" + JSON.toJSONString(bean));
            e.printStackTrace();
        }
        return activityBean;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean modifyCouponInfo(CreateActivityBean bean) {
        log.info("start into method modifyCouponInfo,request:" + JSON.toJSONString(bean));
        // 原优惠券活动的id
        Integer id = bean.getOriginalId();
        // 软删除原来的数据
        int delCount = activityManager.delActivity(id);
        // 重新新建一个活动
        int newCouponId = insertIntoCouponActivity(bean);
        // 如果券发放关联表有id这条数据，则更新为新的id
        // 查询券发放关联表
        List<CouponSendIds> couponSendIds = idsManager.list(new QueryWrapper<CouponSendIds>().eq("coupon_id", id).eq("is_deleted", CouponTypeEnums.IS_DELETED_DEFAULT.getCode()));

        int modifyCount = 0;

        if (CollectionUtils.isNotEmpty(couponSendIds)) {
            ModifyRelationBean modifyRelationBean = new ModifyRelationBean();
            modifyRelationBean.setNewId(newCouponId);
            modifyRelationBean.setOriginalId(couponSendIds.get(0).getId());
            modifyCount = idsManager.modifySendRelationForCouponId(modifyRelationBean);
        }

        boolean flag = false;
        if (delCount > 0 && newCouponId > 0) {
            flag = true;
            if (!ListUtil.isNullOrEmpty(couponSendIds) && modifyCount <= 0) {
                flag = false;
            }
        }
        return flag;
    }

    @Override
    public Boolean delCouponActivity(IdParamBean bean) {
        // 软删除原来的数据
        int delCount = activityManager.delActivity(bean.getId());

        if (delCount <= 0) {
            return false;
        }
        return true;
    }

    /**
     * 保存优惠券活动信息
     *
     * @param bean
     * @return
     */
    private int insertIntoCouponActivity(CreateActivityBean bean) {
        // 当前时间
        int now = DateUtil.getCurrentTimeIntValue();
        // 券名称
        String couponName = bean.getActivityName();
        // 使用门槛
        Integer useCondition = bean.getUseCondition();
        // 有门槛使用的满足金额
        Integer needMoney = bean.getNeedMoney();
        // 使用说明
        String useInstruction = bean.getUseInstruction();
        // 券表
        Coupon coupon = new Coupon();
        // 活动表
        CouponActivity activity = new CouponActivity();

        activity.setCouponName(couponName);
        activity.setCouponCondition(useCondition);

        if (useCondition.equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())) {
            Assert.notNull(needMoney, "有限制使用的情况,满足金额不能为空");
            // 订单所需金额
            activity.setNeedMoney(needMoney);
            coupon.setNeedMoney(needMoney);
        }
        // 券类型
        activity.setCouponType(bean.getCouponType());
        // 券规则
        activity.setCouponRule(JSON.toJSONString(bean.getConditionRule()));
        // 使用时间类型
        activity.setUseTimeType(bean.getUseTimeType());
        // 使用时间规则
        activity.setUseTimeRule(JSON.toJSONString(bean.getTimeRule()));
        // 适用商品类型
        activity.setApplicableGoodsType(bean.getApplicableGoodsType());
        // 适用商品规则
        activity.setApplicableGoodsRule(JSON.toJSONString(bean.getGoodsRule()));
        // 使用说明
        activity.setUseInstruction(useInstruction);
        // 发放状态,初始未发放
        activity.setStatus(CouponSendTypeEnums.SEND_STATUS_DEFAULT.getCode());
        // 创建人id
        activity.setCreatedId(bean.getOperatorId());
        // 创建人名称
        activity.setCreatedName(bean.getOperatorName());
        // 创建时间
        activity.setCT(now);
        // 更新时间
        activity.setUT(now);
        // 有效性
        activity.setIsDeleted(CouponTypeEnums.IS_DELETED_DEFAULT.getCode());
        //限定渠道
        activity.setLimitChannel(bean.getLimitChannel());
        // 插入券活动表
        activityManager.save(activity);

        return activity.getId();
    }

    @Override
    public ListWithPage<MemberAvilableCoupon> queryMemberAvilableCouponList(MemberAvailableCouponQuery param) {
        ListWithPage<MemberAvilableCoupon> page = new ListWithPage<MemberAvilableCoupon>();
        List<MemberAvilableCoupon> memberAvilableCoupons = new ArrayList<MemberAvilableCoupon>();
        try {
      Integer totalCount =
          couponTemplateMapper.queryCount(
              TemplateStatusTypeEnums.IN_USE_STATUS.getCode(), null, null, null, null, null, null);
            if(null == totalCount || 0 >= totalCount){
                return page;
            }
            List<CouponTemplate> couponTemplateList = couponTemplateMapper.queryByCondition(TemplateStatusTypeEnums.IN_USE_STATUS.getCode(),null,
                    null,null,null,
                    null,null,param.getStart(),param.getEnd());
            for(CouponTemplate couponTemplate : couponTemplateList){
                MemberAvilableCoupon memberAvilableCoupon = new MemberAvilableCoupon();
                memberAvilableCoupon.setCouponId(couponTemplate.getId());
                memberAvilableCoupon.setCouponName(couponTemplate.getCouponName());
                memberAvilableCoupon.setUseInstruction(couponTemplate.getUseInstruction());
                String useTime = "";
                String applicableGoodsType = "";
                String couponType = "";
                String couponCondition = "";
                if (couponTemplate.getCouponType().equals(CouponTypeEnums.COUPON_MANJIAN.getCode())) {
                    couponType = CouponTypeEnums.COUPON_MANJIAN.getDetail();
                } else if (couponTemplate.getCouponType().equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
                    couponType = CouponTypeEnums.COUPON_DISCOUNT.getDetail();
                } else if (couponTemplate.getCouponType().equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
                    couponType = CouponTypeEnums.COUPON_RANDOM_REDUCE.getDetail();
                }
                if (couponTemplate.getCouponCondition().equals(CouponTypeEnums.NO_USE_CONDITION.getCode())) {
                    couponCondition = "无门槛";
                } else if (couponTemplate.getCouponCondition().equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())) {
                    couponCondition = "订单满" + couponTemplate.getNeedMoney() + "可用";
                }
                if (couponTemplate.getApplicableGoodsType().equals(CouponTypeEnums.ALL_GOODS_CAN_USE.getCode())) {
                    applicableGoodsType = CouponTypeEnums.ALL_GOODS_CAN_USE.getDetail();
                } else if (couponTemplate.getApplicableGoodsType().equals(CouponTypeEnums.GOODS_CAN_USE.getCode())) {
                    applicableGoodsType = CouponTypeEnums.GOODS_CAN_USE.getDetail();
                }
                JSONObject timeRule = JSONObject.parseObject(couponTemplate.getUseTimeRule());
                // 时间规则
                ActivityTimeRule activityTimeRule = (ActivityTimeRule) JSONObject.toJavaObject(timeRule, ActivityTimeRule.class);
                if (couponTemplate.getUseTimeType().equals(CouponTypeEnums.TIME_USE_FROM_TO.getCode())) {
                    if (activityTimeRule.getTimeStart() != null && activityTimeRule.getTimeEnd() != null) {
                        String useTimeStart = DateUtil.dateToStr(DateUtil.getDateByTimeStamp(activityTimeRule.getTimeStart(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
                        String useTimeEnd = DateUtil.dateToStr(DateUtil.getDateByTimeStamp(activityTimeRule.getTimeEnd(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
                        useTime = useTimeStart + "-" + useTimeEnd;
                    }
                } else if (couponTemplate.getUseTimeType().equals(CouponTypeEnums.BEGIN_FROM_GET.getCode())) {
                    useTime = "领券当日起" + activityTimeRule.getTodayCanUse() + "天内可用";
                } else if (couponTemplate.getUseTimeType().equals(CouponTypeEnums.BEGIN_FROM_TOMORROW.getCode())) {
                    useTime = "领券次日起" + activityTimeRule.getTodayCanUse() + "天内可用";
                }
                memberAvilableCoupon.setUseTime(useTime);
                memberAvilableCoupon.setApplicableGoodsType(applicableGoodsType);
                memberAvilableCoupon.setCouponType(couponType);
                memberAvilableCoupon.setCouponCondition(couponCondition);
                memberAvilableCoupons.add(memberAvilableCoupon);
            }
            page.setTotalCount(totalCount);
            page.setListData(memberAvilableCoupons);
        } catch (Exception e) {
            log.info("查询可发放优惠券异常", e);
            throw new BusinessException("查询可发放优惠券异常");
        }
        return page;
    }

    @Override
    public ListWithPage<MemberCouponDetail> queryMemberCouponDetail(MemberCouponDetailQuery param) {
        Assert.notNull(param.getUserId());
        Assert.notNull(param.getStatus());
        ListWithPage<MemberCouponDetail> page = new ListWithPage<>();
        try {
            List<MemberCouponDetail> memberCouponDetails = new ArrayList<>();
            //List<Coupon> couponList = couponManager.queryCouponInfoByStatus(param);
            List<Coupon> couponList = couponManager.list(new QueryWrapper<Coupon>().eq("is_deleted", 0)
                    .eq("user_id", param.getUserId())
                    .eq("status", param.getStatus()));
            if (!CollectionUtils.isEmpty(couponList)) {
                //券id
                List<Integer> idList = couponList.stream().map(Coupon::getOriginalId).collect(Collectors.toList());
                //券id关联中间表查询发放表
                List<CouponSendIds> conponIdsList = idsManager.list(new QueryWrapper<CouponSendIds>().in("coupon_id", idList).eq("is_deleted", 0));
                List<Integer> couponSendIds = conponIdsList.stream().map(CouponSendIds::getSendId).collect(Collectors.toList());
                Map<Integer, Integer> couponSendMap = new HashMap<Integer, Integer>();
                List<CouponSend> couponSendList = sendManager.list(new QueryWrapper<CouponSend>().in("id", couponSendIds).eq("is_deleted", 0));
                for (CouponSendIds sendIds : conponIdsList) {
                    for (CouponSend couponSend : couponSendList) {
                        if (sendIds.getSendId().equals(couponSend.getId())) {
                            couponSendMap.put(sendIds.getCouponId(), couponSend.getSendType());
                            break;
                        }
                    }
                }
                for (Coupon coupon : couponList) {
                    MemberCouponDetail memberCouponDetail = new MemberCouponDetail();
                    memberCouponDetail.setCouponId(coupon.getId());
                    memberCouponDetail.setCouponName(coupon.getCouponName());
                    memberCouponDetail.setSourceShopId(coupon.getSourceShopId());
                    memberCouponDetail.setUseInstruction(coupon.getUseInstruction());
                    memberCouponDetail.setCouponCode(coupon.getCouponCode());
                    String useTime = "";
                    String applicableGoodsType = "";
                    String sendType = "";
                    String couponType = "";
                    String couponCondition = "";
                    if (coupon.getCouponCondition().equals(CouponTypeEnums.NO_USE_CONDITION.getCode())) {
                        couponCondition = "无门槛";
                    } else if (coupon.getCouponCondition().equals(CouponTypeEnums.MONEY_OF_ORDER_AVAILABLE.getCode())) {
                        couponCondition = "订单满" + coupon.getNeedMoney() + "可用";
                    }
                    if (coupon.getSourceOrderNo() != null) {
                        memberCouponDetail.setSourceOrderNo(coupon.getSourceOrderNo());
                        memberCouponDetail.setCouponUseTime(DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getUseTime(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"));
                    }
                    memberCouponDetail.setcT(DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getCT(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"));
                    if (coupon.getCouponType().equals(CouponTypeEnums.COUPON_MANJIAN.getCode())) {
                        couponType = CouponTypeEnums.COUPON_MANJIAN.getDetail();
                    } else if (coupon.getCouponType().equals(CouponTypeEnums.COUPON_DISCOUNT.getCode())) {
                        couponType = CouponTypeEnums.COUPON_DISCOUNT.getDetail();
                    } else if (coupon.getCouponType().equals(CouponTypeEnums.COUPON_RANDOM_REDUCE.getCode())) {
                        couponType = CouponTypeEnums.COUPON_RANDOM_REDUCE.getDetail();
                    }
                    if (coupon.getApplicableGoodsType().equals(CouponTypeEnums.ALL_GOODS_CAN_USE.getCode())) {
                        applicableGoodsType = CouponTypeEnums.ALL_GOODS_CAN_USE.getDetail();
                    } else if (coupon.getApplicableGoodsType().equals(CouponTypeEnums.GOODS_CAN_USE.getCode())) {
                        applicableGoodsType = CouponTypeEnums.GOODS_CAN_USE.getDetail();
                    } else if (coupon.getApplicableGoodsType().equals(CouponTypeEnums.GOODS_CAN_NOT_USE.getCode())) {
                        applicableGoodsType = CouponTypeEnums.GOODS_CAN_NOT_USE.getDetail();
                    }
                    if (coupon.getUseTimeStart() != null && coupon.getUseTimeEnd() != null) {
                        String useTimeStart = DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getUseTimeStart(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
                        String useTimeEnd = DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getUseTimeEnd(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
                        useTime = useTimeStart + "-" + useTimeEnd;
                    }
                    if (couponSendMap.get(coupon.getOriginalId()).equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_FOUR.getCode())) {
                        sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_FOUR.getDetail();
                    } else if (couponSendMap.get(coupon.getOriginalId()).equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_FIVE.getCode())) {
                        sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_FIVE.getDetail();
                    } else if (couponSendMap.get(coupon.getOriginalId()).equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_ONE.getCode())) {
                        sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_ONE.getDetail();
                    } else if (couponSendMap.get(coupon.getOriginalId()).equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_TWO.getCode())) {
                        sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_TWO.getDetail();
                    } else if (couponSendMap.get(coupon.getOriginalId()).equals(CouponTypeEnums.COUPON_SEND_SENDTYPE_THREE.getCode())) {
                        sendType = CouponTypeEnums.COUPON_SEND_SENDTYPE_THREE.getDetail();
                    }
                    memberCouponDetail.setSendType(sendType);
                    memberCouponDetail.setUseTime(useTime);
                    memberCouponDetail.setApplicableGoodsType(applicableGoodsType);
                    memberCouponDetail.setCouponType(couponType);
                    memberCouponDetail.setCouponCondition(couponCondition);
                    memberCouponDetails.add(memberCouponDetail);
                }
                int totalCount = couponManager.queryTotalCountByStatus(param);
                page.setListData(memberCouponDetails);
                page.setTotalCount(totalCount);
            }
        } catch (Exception e) {
            log.info("查询优惠券会员明细异常", e);
            throw new BusinessException("查询优惠券会员明细异常");
        }

        return page;
    }

    @Override
    public Boolean sendCouponToMember(MemberSendCouponParam param) {
        log.info("会员中台发放券参数:" + JSON.toJSON(param));
        Assert.notNull(param.getUserId(), "发券人不可为空");
        Assert.notNull(param.getCouponId(), "发放券id不可为空");
        Assert.notNull(param.getUserIdList(), "未选中会员用户");
        Assert.notNull(param.getSendNum(), "发放数量不可为空");
        List<Integer> userIdList = param.getUserIdList();
        if (userIdList.size() > 500) {
            throw new BusinessException("会员定向发送优惠券人数不能超过500");
        }

        return true;
    }

    @Override
    public List<CouponInfoDto> queryCouponInfoByCondition(CouponInfoDto couponInfoDto) {
        if (!StringUtils.isNotEmpty(couponInfoDto.getCouponCode()) && !StringUtils.isNotEmpty(couponInfoDto.getOrderNo()) && couponInfoDto.getMemberId() == null) {
            throw new BusinessException("会员id,订单号,券码必输一个");
        }
        CouponSendIds couponSendIds = idsManager.getOne(new QueryWrapper<CouponSendIds>().eq("send_id", couponInfoDto.getCouponSendId()).eq("is_deleted", 0));
        if (couponSendIds == null) {
            throw new BusinessException("无关联发放优惠券活动");
        }
        List<CouponInfoDto> couponInfoDtos = new ArrayList<>();
        try {
            List<Coupon> couponList = couponManager.queryCouponInfoByCondition(couponInfoDto, couponSendIds.getCouponId());
            if (CollectionUtils.isEmpty(couponList)) {
                return couponInfoDtos;
            }
            for (Coupon coupon : couponList) {
                CouponInfoDto couponInfo = new CouponInfoDto();
                couponInfo.setCouponCode(coupon.getCouponCode());
                couponInfo.setOrderNo(coupon.getOrderNo());
                couponInfo.setOriginalId(coupon.getOriginalId());
                couponInfo.setStatus(coupon.getStatus());
                couponInfo.setMemberId(coupon.getUserId());
                couponInfo.setUseShopId(coupon.getUseShopId());
                couponInfo.setCouponSendId(couponInfoDto.getCouponSendId());
                couponInfo.setcT(DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getCT(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"));
                if (coupon.getStatus().equals(CouponTypeEnums.COUPON_UNUSED.getCode()) || coupon.getStatus().equals(CouponTypeEnums.COUPON_EXPIRED.getCode())) {
                    couponInfo.setUseTime("");
                } else if (coupon.getStatus().equals(CouponTypeEnums.COUPON_USED.getCode())) {
                    if (coupon.getUseTime() != null) {
                        String useTime = DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getUseTime(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
                        couponInfo.setUseTime(useTime);
                    }
                }
                couponInfoDtos.add(couponInfo);
            }
        } catch (Exception e) {
            throw new BusinessException("根据条件查询优惠券异常");
        }
        return couponInfoDtos;
    }

    @Override
    public CouponSendDetailDto queryCouponSendById(IdParamBean bean) {
        Assert.notNull(bean.getId(), "优惠券发放id不为空");
        return sendManager.queryCouponSendById(bean.getId());
    }


    /**
     * 优惠券后台管理需要查询展示的详情信息
     * @author dingjunjun
     * @param countBean
     * @return
     */
    @Override
    public Page<QueryCouponListBean> queryCouponListDetail(QueryCouponCount countBean) {
        log.info("优惠券后台管理需要查询展示的详情信息参数：" + JSON.toJSONString(countBean));
        //查询返回的集合
       Page<QueryCouponListBean> page = new Page<>();

        //查询个数
        Integer count = couponManager.queryCouponListDetailCount(countBean);
        if (null == count) {
            throw new BusinessException("暂无数据");
        }
        page.setTotal(count);
        // 查询信息
        Page<Coupon> couponList = couponManager.pageCoupon(CouponQueryListCommand.create(countBean.getPageNum(), countBean.getPageSize(), countBean.getOriginalId(), countBean.getCouponCode(),countBean.getSourceOrderNo(),countBean.getCouponType(),countBean.getStartTime(),countBean.getEndTime(),countBean.getStatus()));
        List<QueryCouponListBean> queryCouponListReturnBean = new ArrayList<>();
        if (CollectionUtils.isEmpty(couponList.getRecords())) {
            page.setTotal(0);
            page.setRecords(queryCouponListReturnBean);
        }
        for (Coupon coupon : couponList.getRecords()) {
            QueryCouponListBean bean = new QueryCouponListBean();
            bean.setCouponUseShop(coupon.getSourceShopId());
            bean.setCouponUseOrderId(coupon.getOrderNo());
            bean.setQueryCouponUseRule(coupon.getCouponType());
            bean.setQueryCouponUseStatus(coupon.getStatus());
            //bean.setCouponCode(coupon.getCouponCode());
            bean.setSourceOrderNo(coupon.getSourceOrderNo());
            bean.setCouponName(coupon.getCouponName());
            bean.setOrderAmount(coupon.getOrderAmount());
            bean.setUseInstruction(coupon.getUseInstruction());
            bean.setUserId(coupon.getUserId());
            bean.setUseShopId(coupon.getUseShopId());
            try {
                if ((countBean.getStartTime() != null && countBean.getEndTime() == null) && coupon.getCT() <= countBean.getStartTime() ) {
                    bean.setCreatTime((DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getCT(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss")));
                }
                else if ((countBean.getStartTime() == null && countBean.getEndTime() != null) &&  coupon.getCT() <= countBean.getEndTime()){
                    bean.setCreatTime((DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getCT(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss")));
                }

                if ( countBean.getStartTime() != null && countBean.getEndTime() != null && (countBean.getStartTime() <= coupon.getCT() && coupon.getCT() <= countBean.getEndTime())) {
                    bean.setCreatTime((DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getCT(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss")));
                } else {
                    bean.setCreatTime((DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getCT(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss")));
                }
                bean.setUseTimeEnd((DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getUseTimeEnd(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss")));
                bean.setUseTimeStart((DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getUseTimeStart(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss")));
                if (coupon.getStatus().equals(CouponUseStatusEnums.NOTUSE_COUPON.getCode()) || coupon.getStatus().equals(CouponUseStatusEnums.ALREADYEXPIRED_COUPON.getCode())) {
                    bean.setSendCouponUseTime(null);
                } else if (coupon.getStatus().equals(CouponUseStatusEnums.ALREADYUSE_COUPON.getCode())) {
                    if (coupon.getUseTime() != null) {
                        String useTime = DateUtil.dateToStr(DateUtil.getDateByTimeStamp(coupon.getUseTime(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
                        bean.setSendCouponUseTime(useTime);
                    }
                }
            } catch (ParseException e) {
                log.info("查询优惠券列表异常！", e);
                throw new BusinessException("查询优惠券列表异常！");
            }
            //设置发券的业务标识
            bean.setSendCouponTag(SendCouponTagEnums.SEND_COUPON_TAG.getCode());
            queryCouponListReturnBean.add(bean);
        }
        page.setRecords(queryCouponListReturnBean);
        return page;
    }
}
