package com.qmfresh.coupon.services.platform.domain.model.activity;

import lombok.Data;

import java.util.Date;

@Data
public class SendActivityCouponVO {
    /**
     * ID
     */
    private Long id;

    /**
     * 券模板ID
     *
     */
    private Integer templateId;

    /**
     * 活动ID
     */
    private Long activityId;

    /**
     * 券名
     */
    private String couponName;

    /**
     * 优惠券类型  1:满减,2:折扣,3:随机金额
     */
    private Integer couponType;

    /**
     * 发放张数
     */
    private Integer sendNum;

    private String remark;

    /**
     * 最后操作人
     */
    private String lastOperatorName;

    /**
     * 最近修改时间
     */
    private Date gmtModified;

}
