package com.qmfresh.coupon.services.platform.domain.model.template;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.services.platform.domain.model.template.command.ShopQueryCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetShopsCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateShopVo;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetShop;

import java.util.List;

public interface ICouponTemplateTargetShopManager extends IService<CouponTemplateTargetShop> {

    /**
     * 批量插入目标商店信息
     * @param templateId
     * @param shopList
     */
    void insertBatch(Integer templateId,List<TemplateTargetShopsCommand> shopList);

    /**
     *根据券模板id查目标店
     * @param templateId
     * @return
     */
    List<CouponTemplateTargetShop> queryByTemplateId(Integer templateId);

    /**
     *根据券模板idL查目标店
     * @param templateIdList
     * @return
     */
    List<CouponTemplateTargetShop> queryByTemplateId(List<Integer> templateIdList);

    Page<TemplateShopVo> queryByCondition(ShopQueryCommand command);
}
