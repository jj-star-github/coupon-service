package com.qmfresh.coupon.services.platform.domain.model.activity.enums;

/**
 *
 */
public enum ActivityCacheKey {
    BY_ACTIVITY_TYPE, //
    ;
    private final String value;

    ActivityCacheKey() {
        this.value = "coupon:activity" + name();
    }

    public String key() {
        return value;
    }

    public String key(Object... params) {
        StringBuilder key = new StringBuilder(value);
        if (params != null && params.length > 0) {
            for (Object param : params) {
                if (null != param) {
                    key.append(':');
                    key.append(String.valueOf(param));
                }
            }
        }
        return key.toString();
    }
}
