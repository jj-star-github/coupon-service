package com.qmfresh.coupon.services.platform.domain.model.template;

import lombok.Data;

import java.io.Serializable;


@Data
public class TargetGoodsInfo implements Serializable {
    /**
     * 券模板id
     */
    private Integer templateId;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 商品名
     */
    private String goodsName;
    /**
     * 商品类型，1.全品，2.sku,3.class1,4.class2,5.ssu,6.saleClass1,7.saleClass2,8.会场
     */
    private Integer goodsType;
}
