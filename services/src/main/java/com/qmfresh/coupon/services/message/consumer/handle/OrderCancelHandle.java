package com.qmfresh.coupon.services.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.coupon.interfaces.dto.coupon.OrderCancelRequestBean;
import com.qmfresh.coupon.services.service.ICouponConsumerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 订单取消 优惠券回退
 */
@Service
public class OrderCancelHandle {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderCancelHandle.class);

    @Autowired
    private ICouponConsumerService sendCouponService;

    public void handleMessage(OrderCancelRequestBean bean) {
        LOGGER.info("OrderCancelHandle handleMessage begin....参数:" + JSON.toJSONString(bean));
        sendCouponService.orderCancel(bean);
        LOGGER.info("OrderCancelHandle handleMessage end ....");
    }
}
