package com.qmfresh.coupon.services.platform.domain.model.template;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.services.platform.domain.model.template.command.GoodsQueryCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetGoodsCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateGoodsVo;
import com.qmfresh.coupon.services.platform.domain.shared.Page;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplateTargetGoods;

import java.util.List;

public interface ICouponTemplateTargetGoodsManager extends IService<CouponTemplateTargetGoods> {

    /**
     * 批量插入目标商品信息
     * @param templateId
     * @param goodsList
     */
    void insertBatch(Integer templateId,List<TemplateTargetGoodsCommand> goodsList);

    /**
     *根据券模板id查目标品
     * @param templateId
     * @return
     */
    List<CouponTemplateTargetGoods> queryByTemplateId(Integer templateId);

    Page<TemplateGoodsVo> queryByCondition(GoodsQueryCommand command);
}
