package com.qmfresh.coupon.services.platform.infrastructure.job;

import com.qmfresh.coupon.interfaces.enums.MemberFinalEnum;
import com.qmfresh.coupon.services.common.utils.DateUtil;
import com.qmfresh.coupon.services.platform.application.coupon.SendCouponService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 给特定用户发放
 */
@JobHandler(value = "SendToUserCouponHandler")
@Service
@Slf4j
public class SendToUserCouponHandler extends IJobHandler {

    @Autowired
    private SendCouponService sendCouponService;
    
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        int beginTime = DateUtil.getCurrentTimeIntValue();
        log.info("定向发放-Begin:" + beginTime);
        sendCouponService.sendToUser();
        int endTime = DateUtil.getCurrentTimeIntValue();
        log.info("定向发放-End:" + endTime);
        log.info("用时:" + (endTime - beginTime));
        return ReturnT.SUCCESS;
    }
}
