package com.qmfresh.coupon.services.platform.domain.model.template.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateGoodsVo implements Serializable {
    private Long id;
    private Integer templateId;
    private String goodsId;
    private String goodsName;
    private Integer goodsType;
}
