/*
 * Copyright (C) 2018-2021 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.coupon.services.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.services.platform.infrastructure.http.member.GradeInfoQuery;
import com.qmfresh.coupon.services.platform.infrastructure.http.member.GradeInfoReturn;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.services.common.business.ServiceResultUtil;
import com.qmfresh.coupon.services.common.utils.RequestUtil;
import com.qmfresh.coupon.services.platform.infrastructure.http.member.MemberUrlManager;
import com.qmfresh.coupon.services.service.IQueryMemberInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lxc
 * @Date 2020/3/27 0027 21:02
 */
@Service
public class QueryMemberInfoServiceImpl implements IQueryMemberInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryMemberInfoServiceImpl.class);
    
    @Autowired
    private MemberUrlManager urlManager;

    @Override
    public GradeInfoReturn queryGradeInfoList(GradeInfoQuery param) {
        ServiceResult<GradeInfoReturn> result;
        try {
            result = RequestUtil.postJson(
                    urlManager.getMemberInfoByGradeId(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<GradeInfoReturn>>() {
                    });
        } catch (Exception e) {
            String message = "url:" + urlManager.getMemberInfoByGradeId() + ",params:" + JSON.toJSONString(param);
            LOGGER.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(result);
    }
}
