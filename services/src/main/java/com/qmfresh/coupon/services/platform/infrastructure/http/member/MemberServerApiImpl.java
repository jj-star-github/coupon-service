package com.qmfresh.coupon.services.platform.infrastructure.http.member;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.coupon.services.common.business.ServiceResult;
import com.qmfresh.coupon.services.common.business.ServiceResultUtil;
import com.qmfresh.coupon.services.common.utils.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MemberServerApiImpl implements MemberServerApi{

    @Autowired
    private MemberUrlManager urlManager;

    @Override
    public GradeInfoReturn queryGradeInfoList(GradeInfoQuery param) {
        ServiceResult<GradeInfoReturn> result;
        try {
            result = RequestUtil.postJson(
                    urlManager.getMemberInfoByGradeId(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<GradeInfoReturn>>() {
                    });
        } catch (Exception e) {
            String message = "url:" + urlManager.getMemberInfoByGradeId() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(result);
    }
}
