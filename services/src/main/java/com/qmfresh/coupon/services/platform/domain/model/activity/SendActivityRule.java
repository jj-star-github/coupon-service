package com.qmfresh.coupon.services.platform.domain.model.activity;

import lombok.Data;

import java.util.Date;

@Data
public class SendActivityRule {
    private String sendTime;
    private Integer sendUseType;
    private String fileName;
}
