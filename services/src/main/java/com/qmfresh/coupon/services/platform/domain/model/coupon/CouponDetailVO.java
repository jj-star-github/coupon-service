package com.qmfresh.coupon.services.platform.domain.model.coupon;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CouponDetailVO {
    /**
     * 券模板ID
     */
    private Integer templateId;
    /**
     * userId
     */
    private Long userId;
    /**
     * 券名
     */
    private String couponName;
    /**
     * 券优惠说明
     */
    private String useInstruction;
    /**
     * 券发放时间
     */
    private Integer sendTime;
    /**
     * 券状态
     */
    private Integer status;
    /**
     * 券使用时间
     */
    private Integer useTime;
    /**
     * 使用券订单号
     */
    private String orderNo;
    /**
     * 使用券订单金额
     */
    private BigDecimal orderAmount;
    /**
     * 发放券订单号
     */
    private String sourceOrderNo;
    /**
     * 发放券门店
     */
    private Integer sourceShopId;
    /**
     * 使用券门店
     */
    private Integer useShopId;
    /**
     * 券码
     */
    private String couponCode;

}
