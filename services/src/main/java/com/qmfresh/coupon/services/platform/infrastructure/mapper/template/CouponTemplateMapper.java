package com.qmfresh.coupon.services.platform.infrastructure.mapper.template;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity.CouponTemplate;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface CouponTemplateMapper extends BaseMapper<CouponTemplate> {

    /**
     *根据id查
     * @param id
     * @return
     */
    CouponTemplate queryById(@Param("id")Integer id);

    /**
     *根据id查
     * @param id
     * @return
     */
    CouponTemplate queryRunInById(@Param("id")Integer id);


    /**
     *更新券模板剩余数量
     * @param id
     * @param sendNumber
     * @param gmtModified
     * @return
     */
    void updateNumber(@Param("id")Integer id,@Param("sendNumber")Integer sendNumber,@Param("gmtModified")Date gmtModified);

    /**
     *分页查
     *  @param status
     */
    List<CouponTemplate> queryByCondition(@Param("status")Integer status,@Param("couponType")Integer couponType,
                                          @Param("limitChannel")Integer limitChannel,@Param("goodsType")Integer goodsType,
                                          @Param("shopType")Integer shopType,@Param("couponName")String couponName,@Param("bingTemplate")Integer bingTemplate,
                                          @Param("start")Integer start, @Param("length")Integer length);

    Integer queryCount(@Param("status")Integer status,@Param("couponType")Integer couponType,
                                          @Param("limitChannel")Integer limitChannel,@Param("goodsType")Integer goodsType,
                                          @Param("shopType")Integer shopType,@Param("couponName")String couponName,@Param("bingTemplate")Integer bingTemplate);

    /**
     *更新券模板绑定会场
     * @param templateIds
     * @param gmtModified
     * @return
     */
    void updateBatch(@Param("templateIds")List<Integer> templateIds,@Param("gmtModified")Date gmtModified);

    /**
     * 根据使用时间类型查询
     * @param useTimeType
     * @return
     */
    List<CouponTemplate> queryByTimeType(@Param("useTimeType")Integer useTimeType);

    /**
     * 批量修改过期券模板
     * @param idList
     */
    void updateOutTime(@Param("idList")List<Integer> idList, @Param("gmtModified")Date gmtModified);

    /**
     * 未绑定会场的券模板
     * @return
     */
    List<CouponTemplate> queryUnBing();

    /**
     * 根据Id批量查询
     * @param idList
     * @return
     */
    List<CouponTemplate> queryByIdList(@Param("idList")List<Integer> idList,@Param("status")Integer status);


}
