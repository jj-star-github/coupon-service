package com.qmfresh.coupon.services.platform.domain.model.activity.enums;

import lombok.Getter;

/**
 *
 */
@Getter
public enum ActivityTypeEnums {
    /**
     * 实时发放
     */
    PROMPTLY(101, "实时生效"),
    /**
     * 指定时间
     */
    APPOINT_TIME(102, "指定生效"),

    /**
     * 新用户
     */
    NEW_USER(201, "新用户"),
    /**
     * 青番茄升级红番茄
     */
    VIP_UP_G_1(202, "青番茄升级红番茄"),
    /**
     * 青番茄升级金番茄
     */
    VIP_UP_G_2(203, "青番茄升级金番茄"),
    /**
     * 红番茄升级金番茄
     */
    VIP_UP_R_2(204, "红番茄升级金番茄"),
    /**
     * 金番茄月礼券
     */
    USER_MONTH_GOLD(303, "金番茄月礼券"),
    /**
     * 红番茄月礼券
     */
    USER_MONTH_RED(302, "红番茄月礼券"),
    /**
     * 青番茄月礼券
     */
    USER_MONTH_GREEN(301, "青番茄月礼券"),
    ;

    private Integer code;
    private String name;

    ActivityTypeEnums(Integer code, String name) {
        this.code = code;
        this.name = name;
    }
}
