package com.qmfresh.coupon.services.platform.domain.model.coupon;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponInfoDto;
import com.qmfresh.coupon.interfaces.dto.coupon.QuerySentCouponListRequestBean;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetailQuery;
import com.qmfresh.coupon.services.entity.Operator;
import com.qmfresh.coupon.services.entity.command.CouponQueryCommand;
import com.qmfresh.coupon.services.entity.command.CouponQueryListCommand;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.QueryCouponDetailCommand;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.SendCouponCommand;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponCount;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
public interface ICouponManager extends IService<Coupon> {

    List<Coupon> querySendCouponByCondition(QuerySentCouponListRequestBean bean);

    /**
     * 查询使用订单coupon
     *
     * @param orderCode 订单号
     * @return 优惠券
     */
    Coupon queryUseCoupon(String orderCode);

    /**
     * 分页查询会员优惠券
     *
     * @param memberCouponDetailQuery
     * @return
     */
    List<Coupon> queryCouponInfoByStatus(MemberCouponDetailQuery memberCouponDetailQuery);

    /**
     * 查询会员优惠券某个状态总数量
     *
     * @param memberCouponDetailQuery
     * @return
     */
    int queryTotalCountByStatus(MemberCouponDetailQuery memberCouponDetailQuery);

    /**
     * 发放优惠券活动根据条件查询优惠券使用信息
     *
     * @param couponInfoDto
     * @return
     */
    List<Coupon> queryCouponInfoByCondition(CouponInfoDto couponInfoDto, Integer originalId);

    /**
     * 根据订单号查询已使用得优惠券
     *
     * @param orderNo
     * @param couponCodes
     * @return
     */
    List<Coupon> queryUseCouponByOrderNo(String orderNo, List<String> couponCodes);

    void updateBatch(List<Integer> idList, Coupon coupon);

    /**
     * 根据券码查询
     *
     * @param couponCode
     * @return
     */
    @Deprecated
    Coupon queryCouponByCode(String couponCode);

    /**
     * 根据券码查询优惠券
     *
     * @param sourceOrderCode 优惠券券码
     * @return 优惠券对象
     */
    List<Coupon> coupon(String sourceOrderCode);


    List<Coupon> list(CouponQueryCommand couponQueryCommand);

    int returnCoupon(Coupon coupon, String sourceOrderCode);

    /**
     * 失效优惠券
     *
     * @param coupons         优惠券列表
     * @param operator        操作者id
     * @param refundCode      退货单号
     * @param sourceOrderCode 原订单号
     * @param currentShopId   当前门店id
     * @return 失效
     */
    int expiredCoupon(List<Coupon> coupons, Operator operator, String refundCode, String sourceOrderCode, Integer currentShopId);

    /**
     * 批量插入
     */
    void batchInsert(List<Coupon> couponList);

    /**
     * 根据ID或者券码查询优惠券信息
     * @param id
     * @param couponCode
     * @param userId
     * @return
     */
    Coupon queryByIdOrCode(Integer id, String couponCode, Long userId);

    /**
     * 根据券码查询
     *
     * @param userId
     * @param couponId
     * @param activityId
     * @param start
     * @param end
     * @return
     */
    Coupon queryCouponByUserId(Integer userId, Integer couponId, Integer activityId, Integer start, Integer end);


    /**
     * 查询个数
     * @param bean
     * @return
     */
    Integer queryCouponListDetailCount(QueryCouponCount bean);

    /**
     * 分页查询优惠券管理后台查询详情页
     * @param couponQueryListCommand
     * @return
     */
    Page<Coupon> pageCoupon(CouponQueryListCommand couponQueryListCommand);
    //==================================================== 以下为新版促销使用 ====================================================

    /**
     * 发放优惠券 支持返回
     * @return
     */
    List<Coupon> newSendCoupon(SendCouponCommand sendCouponCommand);

    /**
     *
     * @param idList
     * @return
     */
    List<Coupon> queryById(List<Integer> idList);

    /**
     * 批量查询
     * @param userList
     * @param templateList
     * @return
     */
    List<Coupon> queryByUserAndTemplate(List<Integer> userList, List<Integer> templateList);

    /**
     * 根据用户查询优惠券列表
     * @param userId
     * @param status
     * @return
     */
    List<Coupon> queryByUser(Integer userId, Integer status, Integer channelId);

    /**
     * 根据条件查询优惠券列表
     * @param couponDetailCommand
     * @return
     */
    com.qmfresh.coupon.services.platform.domain.shared.Page<CouponDetailVO> queryCouponDetail(QueryCouponDetailCommand couponDetailCommand);

    int sendNum(Integer templateId,Integer status);

    int useNum(Integer templateId,Integer status);
}
