package com.qmfresh.coupon.services.platform.infrastructure.mapper.template.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.coupon.services.platform.domain.model.template.command.TemplateTargetGoodsCommand;
import com.qmfresh.coupon.services.platform.domain.model.template.vo.TemplateGoodsVo;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 券模板目标品
 */
@TableName("t_coupon_template_target_goods")
@Data
@ToString
public class CouponTemplateTargetGoods implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 券模板id
     */
    @TableField("template_id")
    private Integer templateId;
    /**
     * 商品id
     */
    @TableField("goods_id")
    private String goodsId;
    /**
     * 商品名
     */
    @TableField("goods_name")
    private String goodsName;
    /**
     * 商品类型，1.全品，2.sku,3.class1,4.class2,5.ssu,6.saleClass1,7.saleClass2,8.会场
     */
    @TableField("goods_type")
    private Integer goodsType;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 有效性
     */
    @TableField("is_deleted")
    private Integer isDeleted;

    public CouponTemplateTargetGoods(){}

    public CouponTemplateTargetGoods(TemplateTargetGoodsCommand command){
        this.setGoodsId(command.getGoodsId());
        this.setGoodsName(command.getGoodsName());
        this.setGoodsType(command.getGoodsType());
        this.setGmtCreate(new Date());
        this.setGmtModified(new Date());
    }

    public CouponTemplateTargetGoods(Integer templateId,String goodsId,String goodsName,Integer goodsType){
        this.setTemplateId(templateId);
        this.setGoodsId(goodsId);
        this.setGoodsName(goodsName);
        this.setGoodsType(goodsType);
        this.setGmtCreate(new Date());
        this.setGmtModified(new Date());
    }

    public TemplateGoodsVo createGoodsVo(){
        TemplateGoodsVo goodsVo = new TemplateGoodsVo();
        goodsVo.setId(this.id);
        goodsVo.setTemplateId(this.templateId);
        goodsVo.setGoodsId(this.goodsId);
        goodsVo.setGoodsName(this.goodsName);
        goodsVo.setGoodsType(this.goodsType);
        return goodsVo;
    }
}
