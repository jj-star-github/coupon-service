package com.qmfresh.coupon.services.platform.infrastructure.http.promotion;

import com.qmfresh.coupon.services.common.business.ServiceResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PromotionServerApiImpl implements PromotionServerApi{


    @Override
    public PromotionContext executeStrategy(PromotionProtocol param) {
        return null;
    }

    @Override
    public ServiceResult<String> getPromotionDetailNew(Long activityId) {
        return null;
    }
}
