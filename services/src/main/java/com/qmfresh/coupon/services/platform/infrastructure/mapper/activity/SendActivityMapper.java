package com.qmfresh.coupon.services.platform.infrastructure.mapper.activity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.activity.entity.SendActivity;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 发放活动
 */
public interface SendActivityMapper extends BaseMapper<SendActivity> {

    /**
     * 根据活动类型查询
     * @param activityType
     * @return
     */
    List<SendActivity> queryByActivityType(@Param("activityType") Integer activityType);

    /**
     * 根据id拆线呢
     * @param id
     * @return
     */
    SendActivity queryById(@Param("id")Long id);

    /**
     * 更新发放状态
     * @param id
     * @param sendStatus
     */
    void updateStatus(@Param("id")Long id , @Param("sendStatus")Integer sendStatus, @Param("operatorId")Integer operatorId, @Param("operatorName")String operatorName);

    /**
     * 删除发放活动
     * @param id
     */
    void delete(@Param("id")Long id, @Param("operatorId")Integer operatorId, @Param("operatorName")String operatorName);

    //###  分页查询
    Integer queryCount(@Param("activityName")String activityName, @Param("sendType")Integer sendType,
                       @Param("activityType")Integer activityType, @Param("status")Integer status,
                       @Param("beginTime")Date beginTime,@Param("endTime")Date endTime,@Param("createName")String createName);

    List<SendActivity> queryByPage(@Param("activityName")String activityName, @Param("sendType")Integer sendType,
                       @Param("activityType")Integer activityType, @Param("status")Integer status, @Param("start")Integer start, @Param("length")Integer length,
                       @Param("beginTime")Date beginTime,@Param("endTime")Date endTime,@Param("createName")String createName);

    /**
     * 根据发放类型查询
     * @param sendType
     * @param status
     * @return
     */
    List<SendActivity> queryBySendType(@Param("sendType") Integer sendType,@Param("status") Integer status);
}