package com.qmfresh.coupon.services.platform.domain.model.coupon.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.coupon.interfaces.dto.coupon.CouponInfoDto;
import com.qmfresh.coupon.interfaces.dto.coupon.QuerySentCouponListRequestBean;
import com.qmfresh.coupon.interfaces.dto.member.MemberCouponDetailQuery;
import com.qmfresh.coupon.services.common.utils.TimeUtil;
import com.qmfresh.coupon.services.entity.CouponEventLog;
import com.qmfresh.coupon.services.entity.CouponStatus;
import com.qmfresh.coupon.services.entity.Operator;
import com.qmfresh.coupon.services.entity.command.CouponQueryCommand;
import com.qmfresh.coupon.services.entity.command.CouponQueryListCommand;
import com.qmfresh.coupon.services.mapper.CouponEventLogMapper;
import com.qmfresh.coupon.services.platform.domain.model.coupon.CouponDetailVO;
import com.qmfresh.coupon.services.platform.domain.model.coupon.ICouponManager;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.QueryCouponDetailCommand;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.SendCouponCommand;
import com.qmfresh.coupon.services.platform.domain.model.coupon.command.SendCouponVO;
import com.qmfresh.coupon.services.platform.domain.model.entity.QueryCouponCount;
import com.qmfresh.coupon.services.platform.domain.model.template.enums.ChannelEnums;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.CouponMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.SendLogMapper;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.Coupon;
import com.qmfresh.coupon.services.platform.infrastructure.mapper.coupon.entity.SendLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.mapping.ResultSetType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.qmfresh.coupon.services.entity.CouponEventBizType.CASH_GIVEN_EXPIRED;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxc
 * @since 2019-07-29
 */
@Service
@Slf4j
public class CouponManagerImpl extends ServiceImpl<CouponMapper, Coupon> implements ICouponManager {

    @Resource
    private CouponMapper couponMapper;
    @Resource
    private SendLogMapper sendLogMapper;

    @Resource
    private CouponEventLogMapper couponEventLogMapper;

    @Override
    public List<Coupon> querySendCouponByCondition(QuerySentCouponListRequestBean bean) {
        QueryWrapper<Coupon> entityWrapper = new QueryWrapper<>();
        if (bean.getUseShopId() != null) {
            entityWrapper.eq("use_shop_id", bean.getUseShopId());
        } else if (bean.getSourceShopId() != null) {
            entityWrapper.eq("source_shop_id", bean.getSourceShopId());
        }
        entityWrapper.eq("status", 1);
        entityWrapper.eq("is_deleted", 0);
        entityWrapper.ge("use_time", bean.getCouponUseTimeStart());
        entityWrapper.le("use_time", bean.getCouponUseTimeEnd());
        return couponMapper.selectList(entityWrapper);
    }

    /**
     * 假设一个单号只能使用一个券
     *
     * @param orderCode 订单号
     * @return 优惠券
     */
    @Override
    public Coupon queryUseCoupon(String orderCode) {

        LambdaQueryWrapper<Coupon> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Coupon::getIsDeleted, 0);
        lambdaQueryWrapper.eq(Coupon::getOrderNo, orderCode);
        lambdaQueryWrapper.eq(Coupon::getStatus, CouponStatus.USED.getCode());
        List<Coupon> coupons = couponMapper.selectList(lambdaQueryWrapper);
        if (CollectionUtils.isEmpty(coupons)) {
            return null;
        }
        if (coupons.size() > 1) {
            log.warn("使用orderCode使用了多个优惠券:{}", orderCode);
        }
        return coupons.get(0);
    }

    @Override
    public List<Coupon> queryCouponInfoByStatus(MemberCouponDetailQuery memberCouponDetailQuery) {
        return couponMapper.queryCouponInfoByStatus(memberCouponDetailQuery);
    }

    @Override
    public int queryTotalCountByStatus(MemberCouponDetailQuery memberCouponDetailQuery) {
        QueryWrapper<Coupon> condition = new QueryWrapper<>();
        condition.eq("user_id", memberCouponDetailQuery.getUserId());
        condition.eq("status", memberCouponDetailQuery.getStatus());
        return couponMapper.selectCount(condition);
    }

    @Override
    public List<Coupon> queryCouponInfoByCondition(CouponInfoDto couponInfoDto, Integer originalId) {
        QueryWrapper<Coupon> entityWrapper = new QueryWrapper<>();
        entityWrapper.eq("original_id", originalId);
        entityWrapper.eq("is_deleted", 0);
        if (StringUtils.isNotEmpty(couponInfoDto.getCouponCode())) {
            entityWrapper.eq("coupon_code", couponInfoDto.getCouponCode());
        }
        if (StringUtils.isNotEmpty(couponInfoDto.getOrderNo())) {
            entityWrapper.eq("order_no", couponInfoDto.getOrderNo());
        }

        if (couponInfoDto.getMemberId() != null && couponInfoDto.getMemberId().longValue() != 0) {
            entityWrapper.eq("user_id", couponInfoDto.getMemberId());
        }
        return couponMapper.selectList(entityWrapper);
    }

    @Override
    public List<Coupon> queryUseCouponByOrderNo(String orderNo, List<String> couponCodes) {
        return couponMapper.queryUseCouponByOrderNo(orderNo, couponCodes);
    }

    @Override
    public void updateBatch(List<Integer> idList, Coupon coupon) {
        couponMapper.updateBatch(idList, coupon);
    }

    @Override
    public Coupon queryCouponByCode(String couponCode) {
        return couponMapper.queryCouponByCode(couponCode);
    }

    /**
     * 查询优惠券
     *
     * @param sourceOrderCode 优惠券编码
     * @return 优惠券
     */
    @Override
    public List<Coupon> coupon(String sourceOrderCode) {
        LambdaQueryWrapper<Coupon> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Coupon::getSourceOrderNo, sourceOrderCode);
        queryWrapper.eq(Coupon::getIsDeleted, 0);
        List<Coupon> coupons = couponMapper.selectList(queryWrapper);
        return coupons;
    }

    @Override
    public List<Coupon> list(CouponQueryCommand couponQueryCommand) {
        LambdaQueryWrapper<Coupon> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Coupon::getIsDeleted, 0);
        queryWrapper.eq(StringUtils.isNotBlank(couponQueryCommand.getGivenOrderCode()), Coupon::getSourceOrderNo, couponQueryCommand.getGivenOrderCode());
        queryWrapper.eq(StringUtils.isNotBlank(couponQueryCommand.getUseOrderCode()), Coupon::getOrderNo, couponQueryCommand.getUseOrderCode());
        queryWrapper.in(CollectionUtils.isNotEmpty(couponQueryCommand.getCouponCodes()), Coupon::getCouponCode, couponQueryCommand.getCouponCodes());
        List<Coupon> coupons = couponMapper.selectList(queryWrapper);
        return coupons;
    }

    @Override
    public int returnCoupon(Coupon coupon, String sourceOrderCode) {
        log.info("退还优惠券: orderCode={} sourceOrderCode={} useShopId={} useTime={}", coupon.getOrderNo(), sourceOrderCode, coupon.getUseShopId(), coupon.getUseTime());
        Coupon update = new Coupon();
        update.setOrderNo("");
        update.setUseTime(0);
        update.setStatus(CouponStatus.UNUSED.getCode());
        int i = couponMapper.update(update, new LambdaQueryWrapper<Coupon>().eq(Coupon::getId, coupon.getId()).gt(Coupon::getUseTimeEnd, System.currentTimeMillis() / 1000));
        return i;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int expiredCoupon(List<Coupon> coupons, Operator operator, String refundCode, String sourceOrderCode, Integer currentShopId) {
        for (Coupon coupon : coupons) {
            Coupon update = new Coupon();
            update.setId(coupon.getId());
            update.setStatus(CouponStatus.RETURN.getCode());
            update.setUT((int) (System.currentTimeMillis() / 1000));
            boolean i = this.update(update, new LambdaQueryWrapper<Coupon>().eq(Coupon::getId, coupon.getId())
                    .eq(Coupon::getStatus, CouponStatus.UNUSED.getCode()));
            if (i) {
                CouponEventLog couponEventLog = CouponEventLog.create(CASH_GIVEN_EXPIRED,
                        coupon.getCouponCode(),
                        refundCode,
                        sourceOrderCode,
                        CouponEventLog.Remark.create(CouponStatus.UNUSED, CouponStatus.EXPIRED, CASH_GIVEN_EXPIRED.getMsg()),
                        operator,
                        currentShopId);
                couponEventLogMapper.insert(couponEventLog);
            }
        }
        return 0;
    }

    @Override
    public void batchInsert(List<Coupon> couponList) {
        couponMapper.insertBatch(couponList);
    }

    @Override
    public Coupon queryByIdOrCode(Integer id, String couponId, Long userId) {
        return baseMapper.queryByIdOrCode(id, couponId, userId);
    }

    @Override
    public Coupon queryCouponByUserId(Integer userId, Integer couponId, Integer activityId, Integer start, Integer end) {
        return couponMapper.queryCouponByUserId(userId, couponId, activityId, start, end);
    }


    /**
     * 查询个数
     * @param bean
     * @return
     */
   /* @Override
    public Integer queryCouponListDetailCount(QueryCouponCount bean) {
        return couponMapper.selectCount(new QueryWrapper<Coupon>()
                .eq("coupon_type",bean.getCouponType())
                .eq("status",bean.getStatus())
                .eq("original_id",bean.getOriginalId())
                .eq("coupon_code",bean.getCouponCode())
                .eq("source_order_no",bean.getSourceOrderNo())
                .ge("c_t",bean.getStartTime())
                .le("c_t",bean.getEndTime())
                *//*.ge("c_t",bean.getStartTime())
                .le("c_t",bean.getEndTime())*//*
                .between("c_t",bean.getStartTime(),bean.getEndTime())
        );
    }*/

   /* @Override
    public Integer queryCouponListDetailCount(QueryCouponCount bean) {
        return couponMapper.queryCouponListDetail(bean);
    }*/

    /**
     * 查询个数
     * @param bean
     * @return
     */
    @Override
    public Integer queryCouponListDetailCount(QueryCouponCount bean) {

        return couponMapper.queryCouponListDetail(bean);
    }

    /**
     * 分页查询优惠券管理后台查询详情页
     *
     * @param couponQueryListCommand
     * @return
     */
    @Override
    @Options(resultSetType = ResultSetType.FORWARD_ONLY, fetchSize = 1000)
    @ResultType(Coupon.class)
    public Page<Coupon> pageCoupon(CouponQueryListCommand couponQueryListCommand) {
        return couponMapper.selectPage(new Page<>(couponQueryListCommand.getPageNum(), couponQueryListCommand.getPageSize()),
                new LambdaQueryWrapper<Coupon>().eq(Coupon::getIsDeleted, 0)
                        .eq(couponQueryListCommand.getStatus() != null, Coupon::getStatus, couponQueryListCommand.getStatus())
                        .eq(couponQueryListCommand.getCouponType() != null, Coupon::getCouponType, couponQueryListCommand.getCouponType())
                        .eq(couponQueryListCommand.getOriginalId() != null, Coupon::getOriginalId, couponQueryListCommand.getOriginalId())
                        .eq(StringUtils.isNotEmpty(couponQueryListCommand.getCouponCode()), Coupon::getCouponCode, couponQueryListCommand.getCouponCode())
                        .eq(StringUtils.isNotEmpty(couponQueryListCommand.getSourceOrderNo()), Coupon::getSourceOrderNo, couponQueryListCommand.getSourceOrderNo())
                        .ge( couponQueryListCommand.getStartTime() != null, Coupon::getCT, couponQueryListCommand.getStartTime())
                        .le(couponQueryListCommand.getEndTime() != null, Coupon::getCT, couponQueryListCommand.getEndTime())
                        .orderByDesc(Coupon::getCT)
        );
    }

    @Override
    public List<Coupon> queryById(List<Integer> idList) {
        return couponMapper.queryByIdList(idList);
    }

    @Override
    public List<Coupon> queryByUserAndTemplate(List<Integer> userList, List<Integer> templateList) {
        return baseMapper.queryByUserAndOriginal(userList, templateList);
    }

    @Override
    public List<Coupon> queryByUser(Integer userId, Integer status, Integer channelId) {
        List<Integer> channelList = null;
        if (null != channelId) {
            channelList = Arrays.asList(channelId, ChannelEnums.ALL.getCode());
        }
        Integer useTimeEnd = null;
        if(status.equals(CouponStatus.UNUSED.getCode())) {
            useTimeEnd = TimeUtil.getTimeStampByDate(new Date());
        }
        return baseMapper.queryByUser(userId, status, channelList, useTimeEnd);
    }

    @Override
    public com.qmfresh.coupon.services.platform.domain.shared.Page<CouponDetailVO> queryCouponDetail(QueryCouponDetailCommand command) {
        com.qmfresh.coupon.services.platform.domain.shared.Page<CouponDetailVO> couponDetailVOPage = new com.qmfresh.coupon.services.platform.domain.shared.Page<>();
        Integer totalCount = baseMapper.countCouponDetail(command.getTemplateId(),command.getUserId(),command.getCouponCode());
        if (null == totalCount || 0 >= totalCount) {
            return couponDetailVOPage;
        }
        List<Coupon> couponList = baseMapper.queryCouponDetail(command.getTemplateId(),command.getUserId(),command.getCouponCode(),command.getStart(),command.getPageSize());
        List<CouponDetailVO> couponDetailVOList = couponList.stream().map(Coupon::createCouponDetailVo).collect(Collectors.toList());
        couponDetailVOPage.setTotalCount(totalCount.longValue());
        couponDetailVOPage.setRecords(couponDetailVOList);
        return couponDetailVOPage;
    }

    @Override
    public int sendNum(Integer templateId, Integer status) {
        return baseMapper.sendNum(templateId,status);
    }

    @Override
    public int useNum(Integer templateId, Integer status) {
        return baseMapper.useNum(templateId,status);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<Coupon> newSendCoupon(SendCouponCommand sendCouponCommand) {
        //优惠券信息插入
        List<Coupon> couponList = new ArrayList<>();
        for(SendCouponVO sendCouponVO : sendCouponCommand.getSendCouponVO()) {
            couponList.addAll(sendCouponVO.createCoupon(sendCouponCommand.getSourceShopId(), sendCouponCommand.getSourceOrderNo(), sendCouponCommand.getUserId(), sendCouponCommand.getPromotionId()));
        }
        if (CollectionUtils.isEmpty(couponList)) {
            return null;
        }
        super.saveBatch(couponList);
        List<String> couponIdList = couponList.stream().map(Coupon :: getIdStr).collect(Collectors.toList());
        //发放记录插入
        sendLogMapper.insertSendLog(new SendLog(sendCouponCommand.getBusinessCode(), sendCouponCommand.getBusinessType(), couponIdList));
        return couponList;
    }

}
