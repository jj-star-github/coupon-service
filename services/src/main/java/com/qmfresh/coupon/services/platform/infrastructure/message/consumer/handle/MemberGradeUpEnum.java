package com.qmfresh.coupon.services.platform.infrastructure.message.consumer.handle;

/**
 */
public enum MemberGradeUpEnum {

    GREEN_TO_RED(10001,10002,202),
    RED_TO_GOLD(10002,10003,203),
    GREEN_TO_GOLD(10001,10003,204);

    /**
     * 升级前等级
     */
    private Integer from;
    /**
     * 升级后等级
     */
    private Integer to;
    /**
     * 升级类型
     */
    private Integer activityType;

    MemberGradeUpEnum(Integer from, Integer to, Integer activityType) {
        this.from = from;
        this.to = to;
        this.activityType = activityType;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public Integer getActivityType() {
        return activityType;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    public static Integer getGradeChangeType(Integer from, Integer to){
        for(MemberGradeUpEnum memberGradeChangeEnum : MemberGradeUpEnum.values()){
            if(memberGradeChangeEnum.getFrom().equals(from) && memberGradeChangeEnum.getTo().equals(to)){
                return memberGradeChangeEnum.getActivityType();
            }
        }
        return null;
    }
}
