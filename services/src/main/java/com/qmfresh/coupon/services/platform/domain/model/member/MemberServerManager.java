package com.qmfresh.coupon.services.platform.domain.model.member;




import java.util.List;

/**
 * 发券日志
 */
public interface MemberServerManager {

    List<Integer> getUserIdList(Integer memberType, Integer pageIndex, Integer pageSize);

}