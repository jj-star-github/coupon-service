package com.qmfresh.coupon.services.platform.infrastructure.message.consumer.handle;

import lombok.Data;

import java.io.Serializable;

/**
 */
@Data
public class MemberGradeUpBean implements Serializable {
    
    private static final long serialVersionUID = 7668858956743145305L;
    
    private Long userId;
    /**
     * 发放张数
     */
    private Integer sendNum;

    /**
     * 注册渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;

    /**
     * 触发条件
     */
    private Integer activityType;

    /**
     * 给用户定向发券 1：定向发券，其他：非定向发券
     */
    private Integer sendToUserFlag;

}
