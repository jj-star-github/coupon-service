package com.qmfresh.coupon.services.platform.domain.model.coupon;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.coupon.services.entity.CouponEventLog;

/**
 * 券操作
 */
public interface CouponEventLogManager extends IService<CouponEventLog> {

    CouponEventLog queryEventLog(Integer bizType, String bizCode, String sourceBizCode);

}
