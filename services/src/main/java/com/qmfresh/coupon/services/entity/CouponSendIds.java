package com.qmfresh.coupon.services.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxc
 * @since 2019-08-02
 */
@TableName("t_coupon_send_ids")
public class CouponSendIds extends Model<CouponSendIds> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增长
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 优惠券活动id
     */
	@TableField("coupon_id")
	private Integer couponId;
    /**
     * 关联优惠券发放表id
     */
	@TableField("send_id")
	private Integer sendId;
    /**
     * 创建时间
     */
	@TableField("c_t")
	private Integer cT;
    /**
     * 更新时间
     */
	@TableField("u_t")
	private Integer uT;
    /**
     * 有效性
     */
	@TableField("is_deleted")
	private Integer isDeleted;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCouponId() {
		return couponId;
	}

	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}

	public Integer getSendId() {
		return sendId;
	}

	public void setSendId(Integer sendId) {
		this.sendId = sendId;
	}

	public Integer getCT() {
		return cT;
	}

	public void setCT(Integer cT) {
		this.cT = cT;
	}

	public Integer getUT() {
		return uT;
	}

	public void setUT(Integer uT) {
		this.uT = uT;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}


}
