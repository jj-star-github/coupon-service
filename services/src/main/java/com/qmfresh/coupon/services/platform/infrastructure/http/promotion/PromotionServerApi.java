package com.qmfresh.coupon.services.platform.infrastructure.http.promotion;


import com.qmfresh.coupon.services.common.business.ServiceResult;

public interface PromotionServerApi {

    /**
     * 补打优惠券前校验订单信息
     * @param param
     * @return
     */
    PromotionContext executeStrategy(PromotionProtocol param);


    ServiceResult<String> getPromotionDetailNew(Long activityId);

}
