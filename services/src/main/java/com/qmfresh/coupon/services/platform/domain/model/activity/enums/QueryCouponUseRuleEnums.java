package com.qmfresh.coupon.services.platform.domain.model.activity.enums;

/**
 * @Description:
 * @Version: V1.0
 */
public enum QueryCouponUseRuleEnums {

    FULL_DISCOUNT(1,"满减优惠券"),
    DISCOUNT(2,"折扣优惠券"),
    RANDOM_MONEY(3,"随机金额优惠券");


    private QueryCouponUseRuleEnums(Integer code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    private String detail;
}
