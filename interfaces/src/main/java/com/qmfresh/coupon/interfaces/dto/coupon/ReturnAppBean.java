/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/5
 * Description:ReturnAppBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 */
public class ReturnAppBean implements Serializable {
    
    private static final long serialVersionUID = -104090470922745447L;

    /**
     * 可用列表
     */
    private List<ReturnAppCanUseBean> canUseList;
    /**
     * 不可用列表
     */
    private List<ReturnAppCanNotUseBean> unUseList;

    public List<ReturnAppCanUseBean> getCanUseList() {
        return canUseList;
    }

    public void setCanUseList(List<ReturnAppCanUseBean> canUseList) {
        this.canUseList = canUseList;
    }

    public List<ReturnAppCanNotUseBean> getUnUseList() {
        return unUseList;
    }

    public void setUnUseList(List<ReturnAppCanNotUseBean> unUseList) {
        this.unUseList = unUseList;
    }
}
