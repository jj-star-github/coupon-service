/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/31
 * Description:QueryCouponInfoBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import com.qmfresh.coupon.interfaces.dto.base.PageQuery;

import java.io.Serializable;

/**
 * @author lxc
 */
public class QueryCouponInfoBean extends PageQuery implements Serializable {

    private static final long serialVersionUID = -4919349854975918984L;

    /**
     * 券类型1:满减,2:折扣,3:随机金额
     */
    private Integer couponType;
    /**
     * 优惠券状态
     */
    private Integer couponStatus;

    public Integer getCouponStatus() {
        return couponStatus;
    }

    public void setCouponStatus(Integer couponStatus) {
        this.couponStatus = couponStatus;
    }

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }
}
