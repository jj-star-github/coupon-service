/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:SetQueryCouponRequestBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import com.qmfresh.coupon.interfaces.dto.base.PageQuery;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author lxc
 */
public class SettQueryCouponRequestBean extends PageQuery implements Serializable {
    
    private static final long serialVersionUID = 7610719419646423748L;

    /**
     * 会员id
     */
    private Long userId;
    /**
     * 这边订单的总金额
     */
    private BigDecimal totalAmount;
    /**
     * 1级分类对应的数据
     */
    private List<SettlementClass1Bean> class1Ids;
    /**
     * 2级分类对应的数据
     */
    private List<SettlementClass2Bean> class2Ids;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<SettlementClass1Bean> getClass1Ids() {
        return class1Ids;
    }

    public void setClass1Ids(List<SettlementClass1Bean> class1Ids) {
        this.class1Ids = class1Ids;
    }

    public List<SettlementClass2Bean> getClass2Ids() {
        return class2Ids;
    }

    public void setClass2Ids(List<SettlementClass2Bean> class2Ids) {
        this.class2Ids = class2Ids;
    }
}
