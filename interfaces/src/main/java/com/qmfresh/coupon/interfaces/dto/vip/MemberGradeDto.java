package com.qmfresh.coupon.interfaces.dto.vip;

import java.io.Serializable;

/**
 * @ClassName MemberGradeDto
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/27 10:52
 */
public class MemberGradeDto implements Serializable {
    private String gradeName;
    /**
     * 10001=青番茄会员
     * 10002=红番茄会员
     * 10003=金番茄会员
     */
    private Integer gradeId;
    private Integer userId;
    /**
     * 注册渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLimitChannel() {
        return limitChannel;
    }

    public void setLimitChannel(Integer limitChannel) {
        this.limitChannel = limitChannel;
    }
}
