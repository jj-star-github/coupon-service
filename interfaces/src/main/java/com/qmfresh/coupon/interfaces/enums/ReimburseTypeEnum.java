package com.qmfresh.coupon.interfaces.enums;

/**
 * @ClassName ReimburseTypeEnum
 * @Description TODO
 * @Author xbb
 * @Date 2020/4/2 17:11
 */
public enum ReimburseTypeEnum {

    NOT_REIMBURSE(0,"未回退"),
    IS_REIMBURSE(1,"已回退");

    private Integer code;

    private String desc;

    ReimburseTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
