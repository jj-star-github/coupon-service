package com.qmfresh.coupon.interfaces.dto.member;

import java.io.Serializable;

/**
 * 可发放优惠券
 * @since 2020-05-15
 * @author shenmeng
 */
public class MemberAvilableCoupon implements Serializable {

    private static final long serialVersionUID = -2296528715358279117L;
    /**
     * 优惠券id
     */
    private Integer couponId;
    /**
     * 优惠券名称
     */
    private String couponName;
    /**
     * 优惠券使用门槛
     */
    private String couponCondition;
    /**
     * 优惠券类型
     */
    private String couponType;
    /**
     * 优惠内容
     */
    private String useInstruction;
    /**
     * 使用期限
     */
    private String useTime;
    /**
     * 适应商品
     */
    private String applicableGoodsType;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponCondition() {
        return couponCondition;
    }

    public void setCouponCondition(String couponCondition) {
        this.couponCondition = couponCondition;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public String getUseInstruction() {
        return useInstruction;
    }

    public void setUseInstruction(String useInstruction) {
        this.useInstruction = useInstruction;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public String getApplicableGoodsType() {
        return applicableGoodsType;
    }

    public void setApplicableGoodsType(String applicableGoodsType) {
        this.applicableGoodsType = applicableGoodsType;
    }
}
