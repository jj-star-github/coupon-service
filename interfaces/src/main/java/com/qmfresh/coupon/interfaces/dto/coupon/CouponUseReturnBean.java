/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/11
 * Description:CouponUseReturnBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class CouponUseReturnBean implements Serializable {

    private static final long serialVersionUID = 2133071858658328332L;
    /**
     * 成功与否标识
     */
    private boolean successFlag;
    /**
     * 失败原因
     */
    private String reason;

    public boolean isSuccessFlag() {
        return successFlag;
    }

    public void setSuccessFlag(boolean successFlag) {
        this.successFlag = successFlag;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
