/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:SettlementClass1Bean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class SettlementClass2Bean implements Serializable {

    private static final long serialVersionUID = 5402575269583149244L;
    /**
     * 2级分类
     */
    private Integer class2Id;
    /**
     * 2级分类对应的金额
     */
    private BigDecimal class2Amount;

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public BigDecimal getClass2Amount() {
        return class2Amount;
    }

    public void setClass2Amount(BigDecimal class2Amount) {
        this.class2Amount = class2Amount;
    }
}
