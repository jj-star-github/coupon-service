package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;


public class UseCouponSendMQDTO implements Serializable {
    private static final long serialVersionUID = 7668858956743145305L;
    /**
     * 券id
     */
    private Integer couponId;
    /**
     * 券活动id
     */
    private Integer originalId;
    /**
     * 券码
     */
    private String couponCode;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    private Integer couponCondition;
    /**
     * 订单所需金额
     */
    private Integer needMoney;
    /**
     * 券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponType;
    /**
     * 券面优惠金额
     */
    private BigDecimal subMoney;
    /**
     * 折扣
     */
    private Integer couponDiscount;
    /**
     * 使用券的订单号
     */
    private String orderNo;
    /**
     * 订单金额
     */
    private BigDecimal orderAmount;
    /**
     * 发放优惠券的门店id
     */
    private Integer sourceShopId;
    /**
     * 使用优惠券的门店id
     */
    private Integer useShopId;
    /**
     * 优惠券实际抵扣金额
     */
    private BigDecimal couponPrice;
    /**
     * 优惠券使用时间
     */
    private Integer useTime;

    public Integer getUseTime() {
        return useTime;
    }

    public void setUseTime(Integer useTime) {
        this.useTime = useTime;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getOriginalId() {
        return originalId;
    }

    public void setOriginalId(Integer originalId) {
        this.originalId = originalId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getCouponCondition() {
        return couponCondition;
    }

    public void setCouponCondition(Integer couponCondition) {
        this.couponCondition = couponCondition;
    }

    public Integer getNeedMoney() {
        return needMoney;
    }

    public void setNeedMoney(Integer needMoney) {
        this.needMoney = needMoney;
    }

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public BigDecimal getSubMoney() {
        return subMoney;
    }

    public void setSubMoney(BigDecimal subMoney) {
        this.subMoney = subMoney;
    }

    public Integer getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(Integer couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getSourceShopId() {
        return sourceShopId;
    }

    public void setSourceShopId(Integer sourceShopId) {
        this.sourceShopId = sourceShopId;
    }

    public Integer getUseShopId() {
        return useShopId;
    }

    public void setUseShopId(Integer useShopId) {
        this.useShopId = useShopId;
    }

    public BigDecimal getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }
}
