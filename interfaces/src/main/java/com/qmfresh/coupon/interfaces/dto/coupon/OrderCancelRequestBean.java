package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * 订单撤单优惠券还原
 */
public class OrderCancelRequestBean implements Serializable {

    private static final long serialVersionUID = -1399807317434977912L;
    /**
     * 订单ID
     */
    private String orderId;
    /**
     * 优惠券码
     */
    private List<String> couponsCodes;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<String> getCouponsCodes() {
        return couponsCodes;
    }

    public void setCouponsCodes(List<String> couponsCodes) {
        this.couponsCodes = couponsCodes;
    }
}
