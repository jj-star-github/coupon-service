/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/31
 * Description:ApprovalCouponSendBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class ApprovalCouponSendBean implements Serializable {

    private static final long serialVersionUID = -4564048030026705474L;
    /**
     * 审批人id
     */
    private Integer approvalId;
    /**
     * 审批人姓名
     */
    private String approvalName;
    /**
     * 审批意见1:审批通过,2:审批不通过
     */
    private Integer status;
    /**
     * 审批备注
     */
    private String remarks;
    /**
     * 优惠券发放活动的id
     */
    private Integer couponSendId;

    public Integer getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Integer approvalId) {
        this.approvalId = approvalId;
    }

    public String getApprovalName() {
        return approvalName;
    }

    public void setApprovalName(String approvalName) {
        this.approvalName = approvalName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getCouponSendId() {
        return couponSendId;
    }

    public void setCouponSendId(Integer couponSendId) {
        this.couponSendId = couponSendId;
    }
}
