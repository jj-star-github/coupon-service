package com.qmfresh.coupon.interfaces.enums;

/**
 * @ClassName RequestSourceEnum
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/26 11:05
 */
public enum RequestSourceEnum {

    MY_COUPON_INFO(1,"我的优惠券"),
    FRONT_PAGE(2,"首页弹窗");

    private Integer code;

    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    RequestSourceEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
