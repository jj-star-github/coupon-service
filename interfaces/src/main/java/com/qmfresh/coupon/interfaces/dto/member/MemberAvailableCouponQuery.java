package com.qmfresh.coupon.interfaces.dto.member;

import com.qmfresh.coupon.interfaces.dto.base.PageQuery;

import java.util.List;

/**
 * 查询可发放优惠券
 * @since 2020-05-15
 * @author shenmeng
 */
public class MemberAvailableCouponQuery extends PageQuery {

    /**
     * 可发放方式
     */
    private List<Integer> sendTypeList;

    public List<Integer> getSendTypeList() {
        return sendTypeList;
    }

    public void setSendTypeList(List<Integer> sendTypeList) {
        this.sendTypeList = sendTypeList;
    }
}
