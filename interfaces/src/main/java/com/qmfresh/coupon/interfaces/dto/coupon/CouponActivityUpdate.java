/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/1
 * Description:CouponActivityUpdate.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 */
public class CouponActivityUpdate implements Serializable {
    
    private static final long serialVersionUID = -9058183089441953167L;

    /**
     * 券id
     */
    private List<Integer> couponIdList;
    /**
     * 修改的状态
     */
    private Integer modifyStatus;

    public List<Integer> getCouponIdList() {
        return couponIdList;
    }

    public void setCouponIdList(List<Integer> couponIdList) {
        this.couponIdList = couponIdList;
    }

    public Integer getModifyStatus() {
        return modifyStatus;
    }

    public void setModifyStatus(Integer modifyStatus) {
        this.modifyStatus = modifyStatus;
    }
}
