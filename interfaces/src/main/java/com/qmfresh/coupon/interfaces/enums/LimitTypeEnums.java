/*
 * Copyright (C) 2018-2021 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.coupon.interfaces.enums;

/**
 * @author lxc
 * @Date 2020/3/27 0027 17:18
 */
public enum LimitTypeEnums {

    NEVER_EXPIRED(1, "永久有效"),
    WILL_EXPIRED(2, "限制期限");

    private Integer code;
    private String detail;

    LimitTypeEnums(Integer code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
