/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/3
 * Description:QueryCouponSendListBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class QueryCouponSendListBean implements Serializable {
    
    private static final long serialVersionUID = -7799377556233238723L;
    /**
     * 发放方式(1:系统自动触发2:运营手动触发3:用户自主领取4:线上促销5:线下促销)
     */
    private Integer sendType;
    /**
     * 发放状态(0:审批中,1:审批通过,2:审批不通过)
     */
    private Integer sendStatus;

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public Integer getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(Integer sendStatus) {
        this.sendStatus = sendStatus;
    }
}
