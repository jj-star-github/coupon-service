/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/8
 * Description:ProQueryCouponReturnBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 * 促销查询可用优惠券返回
 */
public class ProQueryCouponReturnBean implements Serializable {
    
    private static final long serialVersionUID = -8805882351596051216L;
    /**
     * 优惠券活动id
     */
    private Integer id;
    /**
     * 优惠券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponType;
    /**
     * 券活动名称
     */
    private String couponActivityName;
    /**
     * 券名称
     */
    private String couponName;
    
    /**
     * 优惠说明
     */
    private String useInstruction;
    /**
     * 还剩余券数量
     */
    private Long stillHas;
    /**
     * 券发行总量
     */
    private Long totalNum;
    /**
     * 创建人
     */
    private String createdName;
    /**
     * 创建时间
     */
    private Integer createdTime;
    /**
     * 有效期
     */
    private String validityPeriod;

    /**
     * 发放状态(0:未发放1:审核中2:生效中3:已过期)
     */
    private Integer status;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    private Integer couponCondition;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;
    /**
     * 折扣
     */
    private Integer discount;
    /**
     * 随机最小值
     */
    private Integer randomMoneyStart;
    /**
     * 随机最大值
     */
    private Integer randomMoneyEnd;
    /**
     * 券活动所需金额
     */
    private Integer needMoney;
    /**
     * 券活动减免金额
     */
    private Integer subMoney;

    public Integer getNeedMoney() {
        return needMoney;
    }

    public void setNeedMoney(Integer needMoney) {
        this.needMoney = needMoney;
    }

    public Integer getSubMoney() {
        return subMoney;
    }

    public void setSubMoney(Integer subMoney) {
        this.subMoney = subMoney;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCouponCondition() {
        return couponCondition;
    }

    public void setCouponCondition(Integer couponCondition) {
        this.couponCondition = couponCondition;
    }


    public Integer getLimitChannel() {
        return limitChannel;
    }

    public void setLimitChannel(Integer limitChannel) {
        this.limitChannel = limitChannel;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getRandomMoneyStart() {
        return randomMoneyStart;
    }

    public void setRandomMoneyStart(Integer randomMoneyStart) {
        this.randomMoneyStart = randomMoneyStart;
    }

    public Integer getRandomMoneyEnd() {
        return randomMoneyEnd;
    }

    public void setRandomMoneyEnd(Integer randomMoneyEnd) {
        this.randomMoneyEnd = randomMoneyEnd;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public Integer getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Integer createdTime) {
        this.createdTime = createdTime;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public Long getStillHas() {
        return stillHas;
    }

    public void setStillHas(Long stillHas) {
        this.stillHas = stillHas;
    }

    public Long getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Long totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public String getCouponActivityName() {
        return couponActivityName;
    }

    public void setCouponActivityName(String couponActivityName) {
        this.couponActivityName = couponActivityName;
    }

    public String getUseInstruction() {
        return useInstruction;
    }

    public void setUseInstruction(String useInstruction) {
        this.useInstruction = useInstruction;
    }
}
