package com.qmfresh.coupon.interfaces.dto.vip;

import java.util.Map;

/**
 * @ClassName MemberGradeChangeEventDto
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/27 10:51
 */
public class MemberGradeChangeEventDto {
    /**
     * 事件id
     */
    private String eventId;
    /**
     * 业务id (业务的订单号）
     */
    private String bizId;
    /**
     * 业务描述
     */
    private String bizDesc;
    /**
     * 事件创建的时间戳
     */
    private Long createTimestamp;
    /**
     * 当前等级
     */
    private MemberGradeDto from;
    /**
     * 等级变化后
     */
    private MemberGradeDto to;
    /**
     * 一些事件的扩展字段
     */
    private Map<String, Object> remark;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    public String getBizDesc() {
        return bizDesc;
    }

    public void setBizDesc(String bizDesc) {
        this.bizDesc = bizDesc;
    }

    public Long getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Long createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public MemberGradeDto getFrom() {
        return from;
    }

    public void setFrom(MemberGradeDto from) {
        this.from = from;
    }

    public MemberGradeDto getTo() {
        return to;
    }

    public void setTo(MemberGradeDto to) {
        this.to = to;
    }

    public Map<String, Object> getRemark() {
        return remark;
    }

    public void setRemark(Map<String, Object> remark) {
        this.remark = remark;
    }
}
