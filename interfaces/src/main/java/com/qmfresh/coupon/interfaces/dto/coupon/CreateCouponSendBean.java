/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/31
 * Description:CreateCouponSend.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 */
public class CreateCouponSendBean implements Serializable {

    private static final long serialVersionUID = 8114416194119715337L;
    /**
     * id
     */
    private Integer id;
    /**
     * 发放规则
     */
    private CouponSendRule couponSendRule;
    /**
     * 需要发放的优惠券,此处仅需要存放已经创建的优惠券活动的id即可
     */
    private List<Integer> needSendCouponIds;
    /**
     * 创建者id
     */
    private Integer createdId;
    /**
     * 创建者名称
     */
    private String createdName;

    private Integer operatorId;
    private String operatorName;

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CouponSendRule getCouponSendRule() {
        return couponSendRule;
    }

    public void setCouponSendRule(CouponSendRule couponSendRule) {
        this.couponSendRule = couponSendRule;
    }

    public List<Integer> getNeedSendCouponIds() {
        return needSendCouponIds;
    }

    public void setNeedSendCouponIds(List<Integer> needSendCouponIds) {
        this.needSendCouponIds = needSendCouponIds;
    }

    public Integer getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Integer createdId) {
        this.createdId = createdId;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }
}
