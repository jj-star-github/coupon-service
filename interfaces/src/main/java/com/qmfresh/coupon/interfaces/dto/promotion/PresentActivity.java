package com.qmfresh.coupon.interfaces.dto.promotion;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 赠品活动
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public class PresentActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 促销活动id
     */
    private Long activityId;

    /**
     * 赠品活动类型：1.促销活动，2.积分兑换活动
     */
    private Integer activityType;
    /**
     * 赠品活动名称
     */
    private String name;

    /**
     * 活动开始时间
     */
    private Integer effectBeginTime;
    /**
     * 活动结束时间
     */
    private Integer effectEndTime;

    /**
     * 已赠送量
     */
    private BigDecimal sendNum;
    /**
     * 状态:0.失效，1.新建，2.生效(已绑定活动)
     */
    private Integer status;
    /**
     * 创建人id
     */
    private Long createUserId;
    /**
     * 创建人
     */
    private String createUserName;
    private Integer cT;
    private Integer uT;
    private Integer isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getActivityType() {
        return activityType;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEffectBeginTime() {
        return effectBeginTime;
    }

    public void setEffectBeginTime(Integer effectBeginTime) {
        this.effectBeginTime = effectBeginTime;
    }

    public Integer getEffectEndTime() {
        return effectEndTime;
    }

    public void setEffectEndTime(Integer effectEndTime) {
        this.effectEndTime = effectEndTime;
    }

    public BigDecimal getSendNum() {
        return sendNum;
    }

    public void setSendNum(BigDecimal sendNum) {
        this.sendNum = sendNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }

    public Integer getuT() {
        return uT;
    }

    public void setuT(Integer uT) {
        this.uT = uT;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }


}
