/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/8
 * Description:CouponModifyReturnBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 */
public class CouponModifyReturnBean implements Serializable {
    
    private static final long serialVersionUID = -3202894773387665707L;
    /**
     * 已经被其他活动关联的名称集合
     */
    private List<String> alreadyUsed;
    /**
     * 成功与否
     */
    private boolean flag;

    public List<String> getAlreadyUsed() {
        return alreadyUsed;
    }

    public void setAlreadyUsed(List<String> alreadyUsed) {
        this.alreadyUsed = alreadyUsed;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
