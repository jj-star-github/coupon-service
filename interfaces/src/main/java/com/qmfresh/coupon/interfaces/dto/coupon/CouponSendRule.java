/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/31
 * Description:SendRule.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class CouponSendRule implements Serializable {

    private static final long serialVersionUID = 5532201113984875088L;
    /**
     * 发放方式 1:系统自动触发2:运营手动触发3:用户自主领取4:线上促销5:线下促销
     */
    private Integer sendType;
    /**
     * 时间限制类型 1:永久有效,2:有限制期限
     */
    private Integer timeLimitType;
    /**
     * 时间限制起始
     */
    private Integer timeLimitStart;
    /**
     * 时间限制截止
     */
    private Integer timeLimitEnd;
    /**
     * 触发条件 1:新用户注册,后续待补充
     */
    private Integer triggerCondition;
    /**
     * 发放优惠券数量
     */
    private Long sendNum;
    /**
     * 每次发放张数
     */
    private Integer sendNumPerTime;

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public Integer getTimeLimitType() {
        return timeLimitType;
    }

    public void setTimeLimitType(Integer timeLimitType) {
        this.timeLimitType = timeLimitType;
    }

    public Integer getTimeLimitStart() {
        return timeLimitStart;
    }

    public void setTimeLimitStart(Integer timeLimitStart) {
        this.timeLimitStart = timeLimitStart;
    }

    public Integer getTimeLimitEnd() {
        return timeLimitEnd;
    }

    public void setTimeLimitEnd(Integer timeLimitEnd) {
        this.timeLimitEnd = timeLimitEnd;
    }

    public Integer getTriggerCondition() {
        return triggerCondition;
    }

    public void setTriggerCondition(Integer triggerCondition) {
        this.triggerCondition = triggerCondition;
    }

    public Long getSendNum() {
        return sendNum;
    }

    public void setSendNum(Long sendNum) {
        this.sendNum = sendNum;
    }

    public Integer getSendNumPerTime() {
        return sendNumPerTime;
    }

    public void setSendNumPerTime(Integer sendNumPerTime) {
        this.sendNumPerTime = sendNumPerTime;
    }
}
