package com.qmfresh.coupon.interfaces.dto.member;

import java.io.Serializable;

/**
 * 会员优惠券明细
 * @since 2020-05-15
 * @author shenmeng
 */
public class MemberCouponDetail implements Serializable {

    private static final long serialVersionUID = -3686719798106439073L;
    /**
     * 优惠券id
     */
    private Integer couponId;
    /**
     * 发券门店
     */
    private Integer sourceShopId;
    /**
     * 优惠券来源
     */
    private String sendType;
    /**
     * 优惠券名称
     */
    private String couponName;
    /**
     * 获得券的时间
     */
    private String cT;
    /**
     * 使用门槛
     */
    private String couponCondition;
    /**
     * 优惠券类型
     */
    private String couponType;
    /**
     * 优惠内容
     */
    private String useInstruction;
    /**
     * 使用期限
     */
    private String useTime;
    /**
     * 适应商品
     */
    private String applicableGoodsType;
    /**
     * 订单号
     */
    private String sourceOrderNo;
    /**
     * 券使用时间
     */
    private String  couponUseTime;

    private  String couponCode;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getSourceShopId() {
        return sourceShopId;
    }

    public void setSourceShopId(Integer sourceShopId) {
        this.sourceShopId = sourceShopId;
    }

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getcT() {
        return cT;
    }

    public void setcT(String cT) {
        this.cT = cT;
    }

    public String getCouponCondition() {
        return couponCondition;
    }

    public void setCouponCondition(String couponCondition) {
        this.couponCondition = couponCondition;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public String getUseInstruction() {
        return useInstruction;
    }

    public void setUseInstruction(String useInstruction) {
        this.useInstruction = useInstruction;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public String getApplicableGoodsType() {
        return applicableGoodsType;
    }

    public void setApplicableGoodsType(String applicableGoodsType) {
        this.applicableGoodsType = applicableGoodsType;
    }

    public String getSourceOrderNo() {
        return sourceOrderNo;
    }

    public void setSourceOrderNo(String sourceOrderNo) {
        this.sourceOrderNo = sourceOrderNo;
    }

    public String getCouponUseTime() {
        return couponUseTime;
    }

    public void setCouponUseTime(String couponUseTime) {
        this.couponUseTime = couponUseTime;
    }
}
