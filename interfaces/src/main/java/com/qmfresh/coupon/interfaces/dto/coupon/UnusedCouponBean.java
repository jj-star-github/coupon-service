package com.qmfresh.coupon.interfaces.dto.coupon;

import java.math.BigDecimal;

/**
 * @ClassName UnusedCouponBean
 * @Description 结合了ReturnAppCanNotUseBean和ReturnAppCanUseBean
 * @Author xbb
 * @Date 2020/3/26 9:51
 */
public class UnusedCouponBean {

    /**
     * 券id
     */
    private Integer id;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 使用说明
     */
    private String useInstruction;
    /**
     * 有效期
     */
    private String validityPeriod;
    /**
     * 优惠金额
     */
    private BigDecimal preferentialMoney;
    /**
     * 优惠类型1:固定金额2:折扣
     */
    private Integer preferentialType;
    /**
     * 折扣
     */
    private Integer discount;
    /**
     * 券活动名称
     */
    private String couponActivityName;
    /**
     * 今日、明日是否到期 0:没有过期,1:今日过期，2:明日过期
     */
    private Integer expiredFlag;

    /**
     * 券适用商品类型(1:全部可用2:指定可用3:指定不可用)
     */
    private Integer applicableGoodsType;
    /**
     * 不可用原因
     */
    private String unUseReason;

    private String couponCode;

    private Integer couponId;

    private Integer status;

    private Integer sourceShopId;

    private Integer useShopId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getUseInstruction() {
        return useInstruction;
    }

    public void setUseInstruction(String useInstruction) {
        this.useInstruction = useInstruction;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public BigDecimal getPreferentialMoney() {
        return preferentialMoney;
    }

    public void setPreferentialMoney(BigDecimal preferentialMoney) {
        this.preferentialMoney = preferentialMoney;
    }

    public Integer getPreferentialType() {
        return preferentialType;
    }

    public void setPreferentialType(Integer preferentialType) {
        this.preferentialType = preferentialType;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getCouponActivityName() {
        return couponActivityName;
    }

    public void setCouponActivityName(String couponActivityName) {
        this.couponActivityName = couponActivityName;
    }

    public Integer getExpiredFlag() {
        return expiredFlag;
    }

    public void setExpiredFlag(Integer expiredFlag) {
        this.expiredFlag = expiredFlag;
    }

    public Integer getApplicableGoodsType() {
        return applicableGoodsType;
    }

    public void setApplicableGoodsType(Integer applicableGoodsType) {
        this.applicableGoodsType = applicableGoodsType;
    }

    public String getUnUseReason() {
        return unUseReason;
    }

    public void setUnUseReason(String unUseReason) {
        this.unUseReason = unUseReason;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSourceShopId() {
        return sourceShopId;
    }

    public void setSourceShopId(Integer sourceShopId) {
        this.sourceShopId = sourceShopId;
    }

    public Integer getUseShopId() {
        return useShopId;
    }

    public void setUseShopId(Integer useShopId) {
        this.useShopId = useShopId;
    }
}
