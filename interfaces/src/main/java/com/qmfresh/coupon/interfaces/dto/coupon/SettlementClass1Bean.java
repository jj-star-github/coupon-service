/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:SettlementClass1Bean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class SettlementClass1Bean implements Serializable {
    
    private static final long serialVersionUID = 4513100681735143063L;
    /**
     * 1级分类
     */
    private Integer class1Id;
    /**
     * 1级分类对应的金额
     */
    private BigDecimal class1Amount;

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public BigDecimal getClass1Amount() {
        return class1Amount;
    }

    public void setClass1Amount(BigDecimal class1Amount) {
        this.class1Amount = class1Amount;
    }
}
