package com.qmfresh.coupon.interfaces.dto.member;

import com.qmfresh.coupon.interfaces.dto.base.PageQuery;

/**
 * 查询会员优惠券明细
 * @since 2020-05-15
 * @author shenmeng
 */
public class MemberCouponDetailQuery extends PageQuery {

    /**
     * 会员用户id
     */
    private Integer userId;
    /**
     * 券使用状态 (0:未使用1:已使用2:已过期)
     */
    private Integer status;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
