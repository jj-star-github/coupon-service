/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/31
 * Description:QueryCouponReturnBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class QueryCouponReturnBean implements Serializable {
    
    private static final long serialVersionUID = 398897935107450548L;
    /**
     * id
     */
    private Integer id;
    /**
     * 券类型
     */
    private Integer couponType;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 优惠券内容
     */
    private String couponContent;
    /**
     * 已领取数量
     */
    private Long alreadyGet;
    /**
     * 还剩余数量
     */
    private Long stillHas;
    /**
     * 已使用数量
     */
    private Long alreadyUsed;
    /**
     * 支付金额
     */
    private BigDecimal payAmount;
    /**
     * 客单价
     */
    private BigDecimal avgAmount;
    /**
     * 状态
     */
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponContent() {
        return couponContent;
    }

    public void setCouponContent(String couponContent) {
        this.couponContent = couponContent;
    }

    public Long getAlreadyGet() {
        return alreadyGet;
    }

    public void setAlreadyGet(Long alreadyGet) {
        this.alreadyGet = alreadyGet;
    }

    public Long getStillHas() {
        return stillHas;
    }

    public void setStillHas(Long stillHas) {
        this.stillHas = stillHas;
    }

    public Long getAlreadyUsed() {
        return alreadyUsed;
    }

    public void setAlreadyUsed(Long alreadyUsed) {
        this.alreadyUsed = alreadyUsed;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public BigDecimal getAvgAmount() {
        return avgAmount;
    }

    public void setAvgAmount(BigDecimal avgAmount) {
        this.avgAmount = avgAmount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
