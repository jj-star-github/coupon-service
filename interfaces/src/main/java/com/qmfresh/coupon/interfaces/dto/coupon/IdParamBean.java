/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/3
 * Description:CommonParamBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class IdParamBean  implements Serializable {
    
    private static final long serialVersionUID = -8668198006934448435L;

    /**
     * 券活动发放id
     */
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
