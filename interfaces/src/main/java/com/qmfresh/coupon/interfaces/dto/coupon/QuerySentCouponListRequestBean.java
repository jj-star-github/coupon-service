package com.qmfresh.coupon.interfaces.dto.coupon;

import com.qmfresh.coupon.interfaces.dto.base.PageQuery;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @program: coupon-service
 * @description: 查询发放到用户手中的优惠券列表入参
 * @author: xbb
 * @create: 2019-12-17 17:43
 **/
public class QuerySentCouponListRequestBean extends PageQuery implements Serializable {

    private static final long serialVersionUID = -359357151187588126L;

    /**
     * 券码
     */
    private String couponCode;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponType;
    /**
     * 优惠金额
     */
    private BigDecimal subMoney;
    /**
     * 折扣
     */
    private Integer couponDiscount;
    /**
     * 券使用开始时间
     */
    private Integer useTimeStart;
    /**
     * 券使用结束时间
     */
    private Integer useTimeEnd;
    /**
     * 券使用的实际时间（开始时间）
     */
    private Integer couponUseTimeStart;

    /**
     * 券使用的实际时间（结束时间）
     */
    private Integer couponUseTimeEnd;
    /**
     * 用券订单号
     */
    private String useOrderNo;
    /**
     * 绑定的用户id
     */
    private Long userId;
    /**
     * 来源系统
     */
    private Integer sourceSystem;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;
    /**
     * 优惠券绑定的订单号
     */
    private String sourceOrderNo;
    /**
     * 发券的门店id
     */
    private Integer sourceShopId;
    /**
     * 用券的门店id
     */
    private Integer useShopId;
    /**
     * 使用状态(0:未使用1:已使用2:已过期)
     */
    private Integer status;
    /**
     * 1: 我的优惠券，2：首页弹框展示
     */
    private Integer requestSource;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public BigDecimal getSubMoney() {
        return subMoney;
    }

    public void setSubMoney(BigDecimal subMoney) {
        this.subMoney = subMoney;
    }

    public Integer getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(Integer couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public Integer getUseTimeStart() {
        return useTimeStart;
    }

    public void setUseTimeStart(Integer useTimeStart) {
        this.useTimeStart = useTimeStart;
    }

    public Integer getUseTimeEnd() {
        return useTimeEnd;
    }

    public void setUseTimeEnd(Integer useTimeEnd) {
        this.useTimeEnd = useTimeEnd;
    }

    public String getUseOrderNo() {
        return useOrderNo;
    }

    public void setUseOrderNo(String useOrderNo) {
        this.useOrderNo = useOrderNo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(Integer sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public Integer getLimitChannel() {
        return limitChannel;
    }

    public void setLimitChannel(Integer limitChannel) {
        this.limitChannel = limitChannel;
    }

    public String getSourceOrderNo() {
        return sourceOrderNo;
    }

    public void setSourceOrderNo(String sourceOrderNo) {
        this.sourceOrderNo = sourceOrderNo;
    }

    public Integer getSourceShopId() {
        return sourceShopId;
    }

    public void setSourceShopId(Integer sourceShopId) {
        this.sourceShopId = sourceShopId;
    }

    public Integer getUseShopId() {
        return useShopId;
    }

    public void setUseShopId(Integer useShopId) {
        this.useShopId = useShopId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCouponUseTimeStart() {
        return couponUseTimeStart;
    }

    public void setCouponUseTimeStart(Integer couponUseTimeStart) {
        this.couponUseTimeStart = couponUseTimeStart;
    }

    public Integer getCouponUseTimeEnd() {
        return couponUseTimeEnd;
    }

    public void setCouponUseTimeEnd(Integer couponUseTimeEnd) {
        this.couponUseTimeEnd = couponUseTimeEnd;
    }

    public Integer getRequestSource() {
        return requestSource;
    }

    public void setRequestSource(Integer requestSource) {
        this.requestSource = requestSource;
    }
}
