/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/5
 * Description:UserIdParamBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class UserIdParamBean implements Serializable {
    
    private static final long serialVersionUID = 7668858956743145305L;
    
    private Long userId;
    /**
     * 发放张数
     */
    private Integer sendNum;

    /**
     * 注册渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;

    /**
     * 触发条件
     */
    private Integer triggerCondition;

    /**
     * 给用户定向发券 1：定向发券，其他：非定向发券
     */
    private Integer sendToUserFlag;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getSendNum() {
        return sendNum;
    }

    public void setSendNum(Integer sendNum) {
        this.sendNum = sendNum;
    }

    public Integer getLimitChannel() {
        return limitChannel;
    }

    public void setLimitChannel(Integer limitChannel) {
        this.limitChannel = limitChannel;
    }

    public Integer getTriggerCondition() {
        return triggerCondition;
    }

    public void setTriggerCondition(Integer triggerCondition) {
        this.triggerCondition = triggerCondition;
    }

    public Integer getSendToUserFlag() {
        return sendToUserFlag;
    }

    public void setSendToUserFlag(Integer sendToUserFlag) {
        this.sendToUserFlag = sendToUserFlag;
    }
}
