/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/30
 * Description:ActivityGoodRule.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 */
public class ActivityGoodsRule implements Serializable {

    private static final long serialVersionUID = 2646739371770644806L;
    /**
     * 指定1级可用
     */
    private List<Integer> class1Ids;
    /**
     * 指定2级可用
     */
    private List<Integer> class2Ids;
    /**
     * skuIds
     */
    private List<Integer> skuIds;
    /**
     * ssuIds
     */
    private List<Integer> ssuIds;

    public List<Integer> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Integer> skuIds) {
        this.skuIds = skuIds;
    }

    public List<Integer> getSsuIds() {
        return ssuIds;
    }

    public void setSsuIds(List<Integer> ssuIds) {
        this.ssuIds = ssuIds;
    }

    public List<Integer> getClass1Ids() {
        return class1Ids;
    }

    public void setClass1Ids(List<Integer> class1Ids) {
        this.class1Ids = class1Ids;
    }

    public List<Integer> getClass2Ids() {
        return class2Ids;
    }

    public void setClass2Ids(List<Integer> class2Ids) {
        this.class2Ids = class2Ids;
    }

    public ActivityGoodsRule(List<Integer> class1Ids,List<Integer> class2Ids,List<Integer> skuIds,List<Integer> ssuIds){
        this.class1Ids=class1Ids;
        this.class2Ids = class2Ids;
        this.skuIds = skuIds;
        this.ssuIds = ssuIds;
    }
}
