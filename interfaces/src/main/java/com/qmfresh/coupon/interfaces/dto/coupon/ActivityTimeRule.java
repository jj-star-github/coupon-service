/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/30
 * Description:CouponActivityTimeRule.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class ActivityTimeRule implements Serializable {

    private static final long serialVersionUID = 7601201713181515230L;
    /**
     * type=1 的起始时间
     */
    private Integer timeStart;
    /**
     * type=1 的截止时间
     */
    private Integer timeEnd;
    /**
     * 领券当日起多少天可用
     */
    private Integer todayCanUse;
    /**
     * 领券次日起多少天可用
     */
    private Integer tomorrowCanUse;
    /**
     * 是否全天，0是1否
     */
    private Integer isAllDay;
    /**
     * isAllDay = 1 的小时段开始
     */
    private String hmsStart;
    /**
     * isAllDay = 1 的小时段结束
     */
    private String hmsEnd;

    public Integer getIsAllDay() {
        return isAllDay;
    }

    public void setIsAllDay(Integer isAllDay) {
        this.isAllDay = isAllDay;
    }

    public String getHmsStart() {
        return hmsStart;
    }

    public void setHmsStart(String hmsStart) {
        this.hmsStart = hmsStart;
    }

    public String getHmsEnd() {
        return hmsEnd;
    }

    public void setHmsEnd(String hmsEnd) {
        this.hmsEnd = hmsEnd;
    }

    public Integer getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Integer timeStart) {
        this.timeStart = timeStart;
    }

    public Integer getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Integer timeEnd) {
        this.timeEnd = timeEnd;
    }

    public Integer getTodayCanUse() {
        return todayCanUse;
    }

    public void setTodayCanUse(Integer todayCanUse) {
        this.todayCanUse = todayCanUse;
    }

    public Integer getTomorrowCanUse() {
        return tomorrowCanUse;
    }

    public void setTomorrowCanUse(Integer tomorrowCanUse) {
        this.tomorrowCanUse = tomorrowCanUse;
    }
}
