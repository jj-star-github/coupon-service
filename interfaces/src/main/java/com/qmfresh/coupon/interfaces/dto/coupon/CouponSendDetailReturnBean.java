/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/6
 * Description:CouponSendDetailReturnBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 */
public class CouponSendDetailReturnBean implements Serializable {
    
    private static final long serialVersionUID = -3229037355759864327L;

    /**
     * id
     */
    private Integer id;
    /**
     * 发放规则
     */
    private CouponSendRule couponSendRule;
    /**
     * 关联的优惠券详情
     */
    private List<CouponDetailRelationBean> detailRelationBean;

    /**
     * 创建者id
     */
    private Integer createdId;
    /**
     * 创建者名称
     */
    private String createdName;
    /**
     * 审核人id
     */
    private Integer approvalId;
    /**
     * 审核人名称
     */
    private String approvalName;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 备注
     */
    private String remarks;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Integer approvalId) {
        this.approvalId = approvalId;
    }

    public String getApprovalName() {
        return approvalName;
    }

    public void setApprovalName(String approvalName) {
        this.approvalName = approvalName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CouponSendRule getCouponSendRule() {
        return couponSendRule;
    }

    public void setCouponSendRule(CouponSendRule couponSendRule) {
        this.couponSendRule = couponSendRule;
    }

    public List<CouponDetailRelationBean> getDetailRelationBean() {
        return detailRelationBean;
    }

    public void setDetailRelationBean(List<CouponDetailRelationBean> detailRelationBean) {
        this.detailRelationBean = detailRelationBean;
    }

    public Integer getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Integer createdId) {
        this.createdId = createdId;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }
}
