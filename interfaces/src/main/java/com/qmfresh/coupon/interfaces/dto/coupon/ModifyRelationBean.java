/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/2
 * Description:ModifyRelationBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 */
public class ModifyRelationBean implements Serializable {
    
    private static final long serialVersionUID = -6667324656073472949L;

    /**
     * 需修改的新ID值
     */
    private Integer newId;
    /**
     * id
     */
    private Integer originalId;
    
    private List<Integer> modifyIdList;

    public List<Integer> getModifyIdList() {
        return modifyIdList;
    }

    public void setModifyIdList(List<Integer> modifyIdList) {
        this.modifyIdList = modifyIdList;
    }

    public Integer getNewId() {
        return newId;
    }

    public void setNewId(Integer newId) {
        this.newId = newId;
    }

    public Integer getOriginalId() {
        return originalId;
    }

    public void setOriginalId(Integer originalId) {
        this.originalId = originalId;
    }
}
