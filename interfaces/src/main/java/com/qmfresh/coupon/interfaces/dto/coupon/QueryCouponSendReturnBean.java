/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/3
 * Description:QueryCouponSendReturnBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class QueryCouponSendReturnBean implements Serializable {
    
    private static final long serialVersionUID = 2520750908611042664L;
    /**
     * id
     */
    private Integer id;
    /**
     * 发放方式
     */
    private Integer sendType;
    /**
     * 有效期类型
     */
    private Integer expiredType;
    /**
     * 期限起始(针对系统自动触发)
     */
    private Integer expiredTimeStart;
    /**
     * 期限截止(针对系统自动触发)
     */
    private Integer expiredTimeEnd;
    /**
     * 触发条件(针对系统自动触发)
     * 1.新用户注册;201.青番茄升红番茄;202.红番茄升金番茄;203.青番茄升金番茄;301.青番茄会员;302.红番茄会员;303.金番茄会员
     */
    private Integer triggerCondition;
    /**
     * 发送量
     */
    private Long sendNum;
    /**
     * 发放活动状态
     */
    private Integer status;
    /**
     * 备注
     */
    private String remarks;

    private Integer cT;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public Integer getExpiredType() {
        return expiredType;
    }

    public void setExpiredType(Integer expiredType) {
        this.expiredType = expiredType;
    }

    public Integer getExpiredTimeStart() {
        return expiredTimeStart;
    }

    public void setExpiredTimeStart(Integer expiredTimeStart) {
        this.expiredTimeStart = expiredTimeStart;
    }

    public Integer getExpiredTimeEnd() {
        return expiredTimeEnd;
    }

    public void setExpiredTimeEnd(Integer expiredTimeEnd) {
        this.expiredTimeEnd = expiredTimeEnd;
    }

    public Integer getTriggerCondition() {
        return triggerCondition;
    }

    public void setTriggerCondition(Integer triggerCondition) {
        this.triggerCondition = triggerCondition;
    }

    public Long getSendNum() {
        return sendNum;
    }

    public void setSendNum(Long sendNum) {
        this.sendNum = sendNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }
}
