/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/30
 * Description:couponActivityRule.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class ActivityConditionRule implements Serializable {

    private static final long serialVersionUID = 4897562962054189758L;
    /**
     * 折扣(折扣优惠券适用)
     */
    private Integer discount;
    /**
     * 最多优惠金额(折扣优惠券适用)
     */
    private BigDecimal maxDiscountMoney;
    /**
     * 减免金额(满减适用)
     */
    private Integer subMoney;
    /**
     * 随机减免金额起始金额
     */
    private Integer randomMoneyStart;
    /**
     * 随机减免金额截止金额
     */
    private Integer randomMoneyEnd;
    /**
     * 用户每次领取最大张数
     */
    private Integer useReceiveNum;
    /**
     * 券模板最大发放张数
     */
    private Integer couponMaxSendNum;
    /**
     * 门店说明
     */
    private String shopInstruction;

    public Integer getUseReceiveNum() {
        return useReceiveNum;
    }

    public void setUseReceiveNum(Integer useReceiveNum) {
        this.useReceiveNum = useReceiveNum;
    }

    public Integer getCouponMaxSendNum() {
        return couponMaxSendNum;
    }

    public void setCouponMaxSendNum(Integer couponMaxSendNum) {
        this.couponMaxSendNum = couponMaxSendNum;
    }

    public String getShopInstruction() {
        return shopInstruction;
    }

    public void setShopInstruction(String shopInstruction) {
        this.shopInstruction = shopInstruction;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public BigDecimal getMaxDiscountMoney() {
        return maxDiscountMoney;
    }

    public void setMaxDiscountMoney(BigDecimal maxDiscountMoney) {
        this.maxDiscountMoney = maxDiscountMoney;
    }

    public Integer getSubMoney() {
        return subMoney;
    }

    public void setSubMoney(Integer subMoney) {
        this.subMoney = subMoney;
    }

    public Integer getRandomMoneyStart() {
        return randomMoneyStart;
    }

    public void setRandomMoneyStart(Integer randomMoneyStart) {
        this.randomMoneyStart = randomMoneyStart;
    }

    public Integer getRandomMoneyEnd() {
        return randomMoneyEnd;
    }

    public void setRandomMoneyEnd(Integer randomMoneyEnd) {
        this.randomMoneyEnd = randomMoneyEnd;
    }
}
