/*
 * Copyright (C) 2018-2021 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.coupon.interfaces.enums;

/**
 * 会员系统和优惠券系统会员对应值
 *
 * @author lxc
 * @Date 2020/3/27 0027 16:38
 */
public enum MemberFinalEnum {

    GREEN_MEMBER(10001, 301, "青番茄会员"),
    RED_MEMBER(10002, 302, "红番茄会员"),
    GOLD_MEMBER(10003, 303, "金番茄会员");

    /**
     * 会员系统里面对应的值
     */
    private Integer typeInMember;
    /**
     * 优惠券里面对应的值
     */
    private Integer typeInCoupon;
    /**
     * 描述
     */
    private String desc;

    MemberFinalEnum(Integer typeInMember, Integer typeInCoupon, String desc) {
        this.typeInMember = typeInMember;
        this.typeInCoupon = typeInCoupon;
        this.desc = desc;
    }

    public Integer getTypeInMember() {
        return typeInMember;
    }

    public void setTypeInMember(Integer typeInMember) {
        this.typeInMember = typeInMember;
    }

    public Integer getTypeInCoupon() {
        return typeInCoupon;
    }

    public void setTypeInCoupon(Integer typeInCoupon) {
        this.typeInCoupon = typeInCoupon;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
