package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 *
 * @author shenmeng
 * @since 2020-05-21
 */
public class CouponInfoDto implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 优惠券发放ID
	 */
	private Integer couponSendId;

    /**
     * 原优惠券活动的id
     */
	private Integer originalId;
    /**
     * 券码
     */
	private String couponCode;
    /**
     * 订单号
     */
	private String orderNo;
    /**
     * 绑定的用户id
     */
	private Long memberId;
	/**
	 * 用券的门店id
	 */
	private Integer useShopId;
	/**
	 * 优惠券使用时间
	 */
	private String useTime;
    /**
     * 使用状态(0:未使用1:已使用2:已过期)
     */
	private Integer status;
    /**
     * 发放时间
     */
	private String cT;

	public Integer getOriginalId() {
		return originalId;
	}

	public void setOriginalId(Integer originalId) {
		this.originalId = originalId;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Integer getUseShopId() {
		return useShopId;
	}

	public void setUseShopId(Integer useShopId) {
		this.useShopId = useShopId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getcT() {
		return cT;
	}

	public void setcT(String cT) {
		this.cT = cT;
	}

	public String getUseTime() {
		return useTime;
	}

	public void setUseTime(String useTime) {
		this.useTime = useTime;
	}

	public Integer getCouponSendId() {
		return couponSendId;
	}

	public void setCouponSendId(Integer couponSendId) {
		this.couponSendId = couponSendId;
	}
}
