/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:CashQuerySendCouponBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author lxc
 */
public class CashQuerySendCouponRequestBean implements Serializable {
    
    private static final long serialVersionUID = 3541248946779625659L;
    /**
     * 会员号，非必填
     */
    private Long userId;
    /**
     * 请求发放数量
     */
    private Integer sendNum;
    /**
     * 券活动id
     */
    private Integer activityId;
    /**
     * 促销活动id
     */
    private Integer promotionId;
    /**
     * 来源订单id
     */
    private String sourceOrderNo;
    /**
     * 发券的门店id
     */
    private Integer sourceShopId;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;

    /**
     * 实付
     */
    private BigDecimal realAmount;

    /**
     * 优惠券码
     */
    private List<String> couponCode;

    private String businessCode;
    private Integer businessType;

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getSendNum() {
        return sendNum;
    }

    public void setSendNum(Integer sendNum) {
        this.sendNum = sendNum;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getSourceOrderNo() {
        return sourceOrderNo;
    }

    public void setSourceOrderNo(String sourceOrderNo) {
        this.sourceOrderNo = sourceOrderNo;
    }

    public Integer getSourceShopId() {
        return sourceShopId;
    }

    public void setSourceShopId(Integer sourceShopId) {
        this.sourceShopId = sourceShopId;
    }

    public Integer getLimitChannel() {
        return limitChannel;
    }

    public void setLimitChannel(Integer limitChannel) {
        this.limitChannel = limitChannel;
    }

    public BigDecimal getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(BigDecimal realAmount) {
        this.realAmount = realAmount;
    }

    public List<String> getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(List<String> couponCode) {
        this.couponCode = couponCode;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }
}
