package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

public class ReceiveCouponDTO implements Serializable {
    /**
     * 会员id
     */
    private Integer userId;
    /**
     * 优惠券id
     */
    private Integer couponId;
    /**
     * 促销活动id
     */
    private Integer activityId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }
}
