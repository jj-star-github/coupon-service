/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/26
 * Description:createActivityBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class CreateActivityBean implements Serializable {

    private static final long serialVersionUID = 5944559152630280996L;
    /**
     * 原促销活动id
     */
    private Integer originalId;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 使用门槛
     */
    private Integer useCondition;
    /**
     * 订单满足条件的金额
     */
    private Integer needMoney;
    /**
     * 券类型
     */
    private Integer couponType;
    /**
     * 活动条件规则
     */
    private ActivityConditionRule conditionRule;
    /**
     * 用券时间类型
     */
    private Integer useTimeType;
    /**
     * 活动时间规则
     */
    private ActivityTimeRule timeRule;
    /**
     * 适用商品类型
     */
    private Integer applicableGoodsType;
    /**
     * 活动商品规则
     */
    private ActivityGoodsRule goodsRule;
    /**
     * 使用说明
     */
    private String useInstruction;
    /**
     * 创建人id
     */
    private Integer createId;
    /**
     * 创建人名称
     */
    private String createName;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;
    /**
     * 适用商品类型
     */
    private Integer applicableShopType;

    private Integer operatorId;
    private String operatorName;

    public Integer getApplicableShopType() {
        return applicableShopType;
    }

    public void setApplicableShopType(Integer applicableShopType) {
        this.applicableShopType = applicableShopType;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public Integer getLimitChannel() {
        return limitChannel;
    }

    public void setLimitChannel(Integer limitChannel) {
        this.limitChannel = limitChannel;
    }

    public Integer getOriginalId() {
        return originalId;
    }

    public void setOriginalId(Integer originalId) {
        this.originalId = originalId;
    }

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public Integer getUseTimeType() {
        return useTimeType;
    }

    public void setUseTimeType(Integer useTimeType) {
        this.useTimeType = useTimeType;
    }

    public Integer getApplicableGoodsType() {
        return applicableGoodsType;
    }

    public void setApplicableGoodsType(Integer applicableGoodsType) {
        this.applicableGoodsType = applicableGoodsType;
    }

    public Integer getCreateId() {
        return createId;
    }

    public void setCreateId(Integer createId) {
        this.createId = createId;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getUseCondition() {
        return useCondition;
    }

    public void setUseCondition(Integer useCondition) {
        this.useCondition = useCondition;
    }

    public Integer getNeedMoney() {
        return needMoney;
    }

    public void setNeedMoney(Integer needMoney) {
        this.needMoney = needMoney;
    }

    public ActivityConditionRule getConditionRule() {
        return conditionRule;
    }

    public void setConditionRule(ActivityConditionRule conditionRule) {
        this.conditionRule = conditionRule;
    }

    public ActivityTimeRule getTimeRule() {
        return timeRule;
    }

    public void setTimeRule(ActivityTimeRule timeRule) {
        this.timeRule = timeRule;
    }

    public ActivityGoodsRule getGoodsRule() {
        return goodsRule;
    }

    public void setGoodsRule(ActivityGoodsRule goodsRule) {
        this.goodsRule = goodsRule;
    }

    public String getUseInstruction() {
        return useInstruction;
    }

    public void setUseInstruction(String useInstruction) {
        this.useInstruction = useInstruction;
    }
}
