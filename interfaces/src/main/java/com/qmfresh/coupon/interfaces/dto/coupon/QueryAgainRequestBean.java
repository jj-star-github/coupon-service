/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/11
 * Description:QueryAgainRequestBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class QueryAgainRequestBean implements Serializable {
    
    private static final long serialVersionUID = -8739461569071687922L;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 来源渠道
     */
    private Integer sourceSystem;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 优惠券id
     */
    private Integer couponId;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(Integer sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
