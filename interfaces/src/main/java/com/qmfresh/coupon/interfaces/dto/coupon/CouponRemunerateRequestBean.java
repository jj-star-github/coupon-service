/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/11
 * Description:CouponUseBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class CouponRemunerateRequestBean implements Serializable {

    private static final long serialVersionUID = -1399807317434977912L;
    /**
     * 优惠券id
     */
    private Integer couponId;
    /**
     * 用户id
     */
    private Long userId;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
