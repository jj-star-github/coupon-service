/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/3
 * Description:CouponTypeEnums.java
 */
package com.qmfresh.coupon.interfaces.enums;

/**
 * @author lxc
 */
public enum CouponSendTypeEnums {

    SEND_STATUS_DEFAULT(0, "默认券发放状态"),
    SYSTEM_AUTO_SEND(1, "系统自动发放"),
    ONLINE_SEND(4, "线上促销"),
    OFFLINE_SEND(5, "下线促销"),
    IS_DELETED_DEFAULT(0, "有效性默认0"),
    TIME_HAVA_LIMIT(2, "有时间限制"),
    TIME_NO_LIMIT(1, "长期有效"),
    APPROVAL_ING(0, "审批中"),
    APPROVAL_SUCCESS(1, "审批通过"),
    APPROVAL_FALSE(2, "审批驳回"),
    NEW_MEM_REGIST(1, "新用户注册"),
    VIP_UPGRADE_ONE(2,"会员升级"),
    VIP_SEND_SCHEDULE(3,"会员定时发券");

    private Integer code;
    private String detail;

    private CouponSendTypeEnums(Integer code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
