/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/3
 * Description:CouponTypeEnums.java
 */
package com.qmfresh.coupon.interfaces.enums;

/**
 * @author lxc
 */
@Deprecated
public enum CouponTypeEnums {

    NO_USE_CONDITION(0, "无门槛使用"),
    MONEY_OF_ORDER_AVAILABLE(1, "订单满多少金额可用"),
    COUPON_MANJIAN(1, "满减"),
    COUPON_DISCOUNT(2, "折扣"),
    COUPON_RANDOM_REDUCE(3, "金额随机立减"),
    IS_DELETED_DEFAULT(0, "有效性默认0"),
    COUPON_ACTIVITY_APPROVAL_ING(1, "优惠券活动审核中"),
    COUPON_ACTIVITY_NOT_SEND(0, "优惠券未发放"),
    COUPON_ACTIVITY_USEING(2, "优惠券活动生效中"),
    COUPON_ACTIVITY_EXPIRED(3, "优惠券活动已过期"),
    COUPON_ACTIVITY_NOT_APPROVED(4,"优惠券活动审核未通过"),
    COUPON_USED(1, "优惠券已使用"),
    COUPON_EXPIRED(2, "优惠券已过期"),
    COUPON_UNUSED(0, "优惠券未使用"),
    TIME_USE_FROM_TO(1, "从。。。到什么"),
    BEGIN_FROM_GET(2, "领券当天起"),
    BEGIN_FROM_TOMORROW(3, "领券此日起"),
    ALL_GOODS_CAN_USE(1, "所有商品可用"),
    GOODS_CAN_USE(2, "指定可用"),
    GOODS_CAN_NOT_USE(3, "指定不可用"),
    FIXED_AMOUNT_TYPE(1, "固定金额类型"),
    DISCOUNT_AMOUNT_TYPE(2, "折扣金额类型"),
    COUPON_SEND_APPROVING(0,"优惠券发放审批中"),
    COUPON_SEND_APPROVED(1,"优惠券发放审批通过"),
    COUPON_SEND_NOT_APPROVED(2,"优惠券发放审核未通过"),
    COUPON_SEND_STOP(3,"优惠券发放暂停"),
    COUPON_SEND_LIMITED(0,"优惠券发放已达上限，不可发券"),
    COUPON_SEND_SENDTYPE_ONE(1,"系统自动触发"),
    COUPON_SEND_SENDTYPE_TWO(2,"运营手动触发"),
    COUPON_SEND_SENDTYPE_THREE(3,"用户自主领取"),
    COUPON_SEND_SENDTYPE_FOUR(4,"线上促销"),
    COUPON_SEND_SENDTYPE_FIVE(5,"线下促销"),
    COUPON_SEND_TIME_UNLIMIT(1,"长期有效"),
    COUPON_SEND_TIME_LIMITED(2,"有时间限制"),
    IS_DELETED(1,"数据已失效");

    private Integer code;
    private String detail;

    private CouponTypeEnums(Integer code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
