package com.qmfresh.coupon.interfaces.enums;

/**
 * @ClassName MemberGradeChangeEnum
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/27 11:36
 */
public enum MemberGradeChangeEnum {

    GREEN_TO_RED(10001,10002,202),
    RED_TO_GOLD(10002,10003,204),
    GREEN_TO_GOLD(10001,10003,203);

    /**
     * 升级前等级
     */
    private Integer from;
    /**
     * 升级后等级
     */
    private Integer to;
    /**
     * 升级类型
     */
    private Integer gradeChangeType;

    MemberGradeChangeEnum(Integer from, Integer to, Integer gradeChangeType) {
        this.from = from;
        this.to = to;
        this.gradeChangeType = gradeChangeType;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public Integer getGradeChangeType() {
        return gradeChangeType;
    }

    public void setGradeChangeType(Integer gradeChangeType) {
        this.gradeChangeType = gradeChangeType;
    }

    public static Integer getGradeChangeType(Integer from,Integer to){
        for(MemberGradeChangeEnum  memberGradeChangeEnum : MemberGradeChangeEnum.values()){
            if(memberGradeChangeEnum.getFrom().equals(from) && memberGradeChangeEnum.getTo().equals(to)){
                return memberGradeChangeEnum.getGradeChangeType();
            }
        }
        return null;
    }
}
