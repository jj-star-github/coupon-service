package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 *
 * @author shenmeng
 * @since 20200520
 */
public class CouponSendDetailDto implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 发放Id
     */
    private  Integer sendId;
    /**
     * 券活动Id
     */
    private  Integer couponId;
    /**
     * 券名称
     */
    private  String couponName;
    /**
     * 优惠券类型
     */
    private String couponType;
    /**
     * 可使用商品
     */
    private String applicableGoodsType;
    /**
     * 使用门槛
     */
    private String couponCondition;
    /**
     * 面值
     */
    private String subMoney;
    /**
     * 发放活动状态,长期有效,时间限制;
     */
    private String sendStatus;
    /**
     * 有效期
     */
    private String effectiveTime;
    /**
     * 总发放张数
     */
    private Long totalSendNum;
    /**
     * 已发放张数
     */
    private Long alreadySendNum;
    /**
     * 已使用张数
     */
    private Long alreadyUseNum;
    /**
     * 未使用张数
     */
    private Long notUseNum;
    /**
     * 过期张数
     */
    private Long expireNum;
    /**
     * 发放方式
     */
    private String sendType;

    private Integer sendNumPerTime;

    private String remark;

    public Integer getSendId() {
        return sendId;
    }

    public void setSendId(Integer sendId) {
        this.sendId = sendId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public String getApplicableGoodsType() {
        return applicableGoodsType;
    }

    public void setApplicableGoodsType(String applicableGoodsType) {
        this.applicableGoodsType = applicableGoodsType;
    }

    public String getCouponCondition() {
        return couponCondition;
    }

    public void setCouponCondition(String couponCondition) {
        this.couponCondition = couponCondition;
    }

    public String getSubMoney() {
        return subMoney;
    }

    public void setSubMoney(String subMoney) {
        this.subMoney = subMoney;
    }

    public String getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }

    public String getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(String effectiveTime) {
        this.effectiveTime = effectiveTime;
    }
    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public Long getTotalSendNum() {
        return totalSendNum;
    }

    public void setTotalSendNum(Long totalSendNum) {
        this.totalSendNum = totalSendNum;
    }

    public Long getAlreadySendNum() {
        return alreadySendNum;
    }

    public void setAlreadySendNum(Long alreadySendNum) {
        this.alreadySendNum = alreadySendNum;
    }

    public Long getAlreadyUseNum() {
        return alreadyUseNum;
    }

    public void setAlreadyUseNum(Long alreadyUseNum) {
        this.alreadyUseNum = alreadyUseNum;
    }

    public Long getNotUseNum() {
        return notUseNum;
    }

    public void setNotUseNum(Long notUseNum) {
        this.notUseNum = notUseNum;
    }

    public Long getExpireNum() {
        return expireNum;
    }

    public void setExpireNum(Long expireNum) {
        this.expireNum = expireNum;
    }

    public Integer getSendNumPerTime() {
        return sendNumPerTime;
    }

    public void setSendNumPerTime(Integer sendNumPerTime) {
        this.sendNumPerTime = sendNumPerTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
