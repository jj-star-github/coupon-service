package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

public class CouponForAppReturnBean implements Serializable {
    private static final long serialVersionUID = -2634608309253218048L;

    /**
     * 券Id
     */
    private Integer couponId;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券码
     */
    private String couponCode;
    /**
     * 券门槛(0:无门槛,1:订单满多少可用)
     */
    private Integer couponCondition;
    /**
     * 券门槛所需金额
     */
    private Integer needMoney;
    /**
     * 券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponType;
    /**
     * 优惠金额
     */
    private BigDecimal subMoney;
    /**
     * 折扣
     */
    private Integer couponDiscount;
    /**
     * 随机最大值
     */
    private Integer randomMoneyEnd;
    /**
     * 随机最小值
     */
    private Integer randomMoneyStart;
    /**
     * 券适用商品类型(1:全部可用2:指定可用3:指定不可用)
     */
    private Integer applicableGoodsType;
    /**
     * 1级分类
     */
    private String class1Ids;
    /**
     * 2级分类
     */
    private String class2Ids;
    /**
     * 拼接券说明（门槛+所需+减免）
     */
    private String couponRemark;


    public Integer getRandomMoneyEnd() {
        return randomMoneyEnd;
    }

    public void setRandomMoneyEnd(Integer randomMoneyEnd) {
        this.randomMoneyEnd = randomMoneyEnd;
    }

    public Integer getRandomMoneyStart() {
        return randomMoneyStart;
    }

    public void setRandomMoneyStart(Integer randomMoneyStart) {
        this.randomMoneyStart = randomMoneyStart;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getCouponCondition() {
        return couponCondition;
    }

    public void setCouponCondition(Integer couponCondition) {
        this.couponCondition = couponCondition;
    }

    public Integer getNeedMoney() {
        return needMoney;
    }

    public void setNeedMoney(Integer needMoney) {
        this.needMoney = needMoney;
    }

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public BigDecimal getSubMoney() {
        return subMoney;
    }

    public void setSubMoney(BigDecimal subMoney) {
        this.subMoney = subMoney;
    }

    public Integer getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(Integer couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public Integer getApplicableGoodsType() {
        return applicableGoodsType;
    }

    public void setApplicableGoodsType(Integer applicableGoodsType) {
        this.applicableGoodsType = applicableGoodsType;
    }

    public String getClass1Ids() {
        return class1Ids;
    }

    public void setClass1Ids(String class1Ids) {
        this.class1Ids = class1Ids;
    }

    public String getClass2Ids() {
        return class2Ids;
    }

    public void setClass2Ids(String class2Ids) {
        this.class2Ids = class2Ids;
    }

    public String getCouponRemark() {
        return couponRemark;
    }

    public void setCouponRemark(String couponRemark) {
        this.couponRemark = couponRemark;
    }
}
