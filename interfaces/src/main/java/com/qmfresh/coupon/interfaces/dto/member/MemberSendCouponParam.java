package com.qmfresh.coupon.interfaces.dto.member;

import java.io.Serializable;
import java.util.List;

/**
 * 需要发放券会员参数
 * @since 2020-05-15
 * @author shenmeng
 */
public class MemberSendCouponParam implements Serializable {

    private static final long serialVersionUID = 6393881799154509296L;
    /**
     * 发放人用户id
     */
    private  Integer userId;
    /**
     * 券发放数量
     */
    private Integer sendNum;
    /**
     * 发放券原因
     */
    private String sendReason;
    /**
     * 发放券Id
     */
    private Integer couponId;
    /**
     * 发放券名称
     */
    private String couponName;
    /**
     * 需要发放优惠券会员用户Id
     */
    private List<Integer> userIdList;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSendNum() {
        return sendNum;
    }

    public void setSendNum(Integer sendNum) {
        this.sendNum = sendNum;
    }

    public String getSendReason() {
        return sendReason;
    }

    public void setSendReason(String sendReason) {
        this.sendReason = sendReason;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public List<Integer> getUserIdList() {
        return userIdList;
    }

    public void setUserIdList(List<Integer> userIdList) {
        this.userIdList = userIdList;
    }
}
