/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/1
 * Description:UseCouponBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class UseCouponBean implements Serializable {
    
    private static final long serialVersionUID = -1199160497698981012L;
    /**
     * 券活动id
     */
    private Integer couponId;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 订单金额
     */
    private BigDecimal orderAmount;
    /**
     * 客户类型(1:新客户2:老客户)
     */
    private Integer memType;
    /**
     * 此单购买的商品数量
     */
    private Integer goodsNum;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getMemType() {
        return memType;
    }

    public void setMemType(Integer memType) {
        this.memType = memType;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }
}
