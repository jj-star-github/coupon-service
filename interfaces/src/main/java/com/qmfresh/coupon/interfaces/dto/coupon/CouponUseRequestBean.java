/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/11
 * Description:CouponUseBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class CouponUseRequestBean implements Serializable {
    
    private static final long serialVersionUID = 8757338761918281518L;
    /**
     * 优惠券id
     */
    private Integer couponId;
    /**
     * 订单金额
     */
    private BigDecimal totalAmount;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 用这张券购买的商品数量
     */
    private Integer goodsNum;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 来源系统：拼团，线上，线下......(保留字段)
     */
    private Integer sourceSystem;
    /**
     * 使用优惠券的门店id
     */
    private Integer shopId;
    /**
     * 优惠券抵扣金额
     */
    private BigDecimal couponPrice;

    private Long useTimestamp;

    /**
     *  渠道 1：收银 5：线上商城  10：外卖 15：门店订货
     */
    private Integer bizChannel;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(Integer sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public BigDecimal getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Long getUseTimestamp() {
        return useTimestamp;
    }

    public void setUseTimestamp(Long useTimestamp) {
        this.useTimestamp = useTimestamp;
    }

    public Integer getBizChannel() {
        return bizChannel;
    }

    public void setBizChannel(Integer bizChannel) {
        this.bizChannel = bizChannel;
    }
}
