/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/8
 * Description:ProQueryTempBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 */
public class ProQueryTempBean implements Serializable {

    private static final long serialVersionUID = 1735123262275496669L;
    /**
     * 优惠券发放id
     */
    private Integer couponSendId;
    /**
     * 优惠券活动id集合
     */
    private List<Integer> couponIds;
    /**
     * 发放量
     */
    private Long sendNum;

    public Integer getCouponSendId() {
        return couponSendId;
    }

    public void setCouponSendId(Integer couponSendId) {
        this.couponSendId = couponSendId;
    }

    public List<Integer> getCouponIds() {
        return couponIds;
    }

    public void setCouponIds(List<Integer> couponIds) {
        this.couponIds = couponIds;
    }

    public Long getSendNum() {
        return sendNum;
    }

    public void setSendNum(Long sendNum) {
        this.sendNum = sendNum;
    }
}
