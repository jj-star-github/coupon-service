/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/9
 * Description:CashQuerySendCouponReturnBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class CashQuerySendCouponReturnBean implements Serializable {
    
    private static final long serialVersionUID = -2634608309253218048L;
    
    /**
     * 券Id
     */
    private Integer couponId;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券码
     */
    private String couponCode;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 有效期
     */
    private String validityPeriod;
    /**
     * 优惠券绑定的订单号
     */
    private String sourceOrderNo;
    /**
     * 券抵扣金额
     */
    private BigDecimal couponPrice;
    /**
     * 发券的门店id
     */
    private Integer sourceShopId;
    /**
     * 用券的门店id
     */
    private Integer useShopId;
    /**
     * 优惠金额
     */
    private BigDecimal subMoney;
    /**
     * 折扣
     */
    private Integer couponDiscount;
    /**
     * 优惠券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponType;
    /**
     * 券适用商品类型(1:全部可用2:指定可用3:指定不可用)
     */
    private Integer applicableGoodsType;

    /**
     * -1：已过期，1: 明日过期，2：后日过期,3：两天后过期
     */
    private Integer expiredFlag;

    /**
     * 优惠券使用时间
     */
    private Integer useTime;

    /**
     * 券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponCondition;

    /**
     * 发券时间
     */
    private Integer createTime;

    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;

    /**
     * 券活动名称
     */
    private String couponActivityName;

    /**
     * UserId
     */
    private Long userId;

    /**
     * 券可用小时段
     */
    private String useHMSRule;
    /**
     * 是否全天，0是1否
     */
    private Integer isAllDay;

    public Integer getIsAllDay() {
        return isAllDay;
    }

    public void setIsAllDay(Integer isAllDay) {
        this.isAllDay = isAllDay;
    }

    public String getUseHMSRule() {
        return useHMSRule;
    }

    public void setUseHMSRule(String useHMSRule) {
        this.useHMSRule = useHMSRule;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCouponActivityName() {
        return couponActivityName;
    }

    public void setCouponActivityName(String couponActivityName) {
        this.couponActivityName = couponActivityName;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getUseInstruction() {
        return useInstruction;
    }

    public void setUseInstruction(String useInstruction) {
        this.useInstruction = useInstruction;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public String getSourceOrderNo() {
        return sourceOrderNo;
    }

    public void setSourceOrderNo(String sourceOrderNo) {
        this.sourceOrderNo = sourceOrderNo;
    }

    public BigDecimal getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Integer getSourceShopId() {
        return sourceShopId;
    }

    public void setSourceShopId(Integer sourceShopId) {
        this.sourceShopId = sourceShopId;
    }

    public Integer getUseShopId() {
        return useShopId;
    }

    public void setUseShopId(Integer useShopId) {
        this.useShopId = useShopId;
    }

    public BigDecimal getSubMoney() {
        return subMoney;
    }

    public void setSubMoney(BigDecimal subMoney) {
        this.subMoney = subMoney;
    }

    public Integer getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(Integer couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public Integer getApplicableGoodsType() {
        return applicableGoodsType;
    }

    public void setApplicableGoodsType(Integer applicableGoodsType) {
        this.applicableGoodsType = applicableGoodsType;
    }

    public Integer getExpiredFlag() {
        return expiredFlag;
    }

    public void setExpiredFlag(Integer expiredFlag) {
        this.expiredFlag = expiredFlag;
    }

    public Integer getUseTime() {
        return useTime;
    }

    public void setUseTime(Integer useTime) {
        this.useTime = useTime;
    }

    public Integer getCouponCondition() {
        return couponCondition;
    }

    public void setCouponCondition(Integer couponCondition) {
        this.couponCondition = couponCondition;
    }

    public Integer getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Integer createTime) {
        this.createTime = createTime;
    }

    public Integer getLimitChannel() {
        return limitChannel;
    }

    public void setLimitChannel(Integer limitChannel) {
        this.limitChannel = limitChannel;
    }
}
