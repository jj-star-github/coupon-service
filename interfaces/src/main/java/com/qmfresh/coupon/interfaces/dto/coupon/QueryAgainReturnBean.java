/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/11
 * Description:QueryAgainReturnBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lxc
 */
public class QueryAgainReturnBean implements Serializable {

    private static final long serialVersionUID = 4031910127021350119L;
    /**
     * 券id
     */
    private Integer id;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 使用说明
     */
    private String useInstruction;
    /**
     * 有效期
     */
    private String validityPeriod;
    /**
     * 优惠金额
     */
    private BigDecimal preferentialMoney;
    /**
     * 优惠类型1:固定金额2:折扣
     */
    private Integer preferentialType;
    /**
     * 折扣
     */
    private Integer discount;
    /**
     * 券活动名称
     */
    private String couponActivityName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getUseInstruction() {
        return useInstruction;
    }

    public void setUseInstruction(String useInstruction) {
        this.useInstruction = useInstruction;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public BigDecimal getPreferentialMoney() {
        return preferentialMoney;
    }

    public void setPreferentialMoney(BigDecimal preferentialMoney) {
        this.preferentialMoney = preferentialMoney;
    }

    public Integer getPreferentialType() {
        return preferentialType;
    }

    public void setPreferentialType(Integer preferentialType) {
        this.preferentialType = preferentialType;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getCouponActivityName() {
        return couponActivityName;
    }

    public void setCouponActivityName(String couponActivityName) {
        this.couponActivityName = couponActivityName;
    }
}
