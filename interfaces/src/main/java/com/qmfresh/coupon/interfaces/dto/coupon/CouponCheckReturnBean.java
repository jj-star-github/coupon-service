package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @program: coupon-service
 * @description:
 * @author: xbb
 * @create: 2019-11-25 16:07
 **/
public class CouponCheckReturnBean implements Serializable {

    /**
     * 券id
     */
    private Integer couponId;
    /**
     * 成功标记 0：成功，1：失败
     */
    private Boolean successFlag;

    /**
     * 失败原因
     */
    private String reason;

    /**
     * 优惠券展示的中间名称
     */
    private String couponName;

    /**
     * 使用说明
     */
    private String useInstruction;

    /**
     * 有效期
     */
    private String validityPeriod;

    /**
     * 优惠类型 1：固定金额 2：折扣
     */
    private Integer preferentialType;

    /**
     * 折扣
     */
    private Integer discount;

    /**
     * 具体优惠金额，如果preferentialType是2，代表最多优惠的金额;
     */
    private BigDecimal preferentialMoney;

    /**
     * 优惠券展示的最大名称
     */
    private String couponActivityName;

    /**
     * 今日、明日是否到期,0:没有过期,1:今日即将过期，2:明日即将过期
     */
    private Integer expiredFlag;

    /**
     * 1.优惠券已失效 2.优惠券已被使用 3. 补打失败，优惠券失败
     */
    private Integer type;
    /**
     * 优惠码
     */
    private String couponCode;
    /**
     * 优惠券绑定的订单号
     */
    private String sourceOrderNo;
    /**
     * 优惠券使用时间/退货时间
     */
    private Long useCouponTime;
    /**
     * 被赠送的用户id
     */
    private Integer bindUserId;
    /**
     * 退货门店/使用门店
     */
    private Integer useShopId;
    /**
     * 使用订单号/退货订单
     */
    private String useOrderCode;
    /**
     * 活动id
     */
    private Integer originalId;
    /**
     * 可用优惠券的商品列表
     */
    private List<GoodsInfo> usefulGoodsList;


    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Boolean getSuccessFlag() {
        return successFlag;
    }

    public void setSuccessFlag(Boolean successFlag) {
        this.successFlag = successFlag;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getUseInstruction() {
        return useInstruction;
    }

    public void setUseInstruction(String useInstruction) {
        this.useInstruction = useInstruction;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public Integer getPreferentialType() {
        return preferentialType;
    }

    public void setPreferentialType(Integer preferentialType) {
        this.preferentialType = preferentialType;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public BigDecimal getPreferentialMoney() {
        return preferentialMoney;
    }

    public void setPreferentialMoney(BigDecimal preferentialMoney) {
        this.preferentialMoney = preferentialMoney;
    }

    public String getCouponActivityName() {
        return couponActivityName;
    }

    public void setCouponActivityName(String couponActivityName) {
        this.couponActivityName = couponActivityName;
    }

    public Integer getExpiredFlag() {
        return expiredFlag;
    }

    public void setExpiredFlag(Integer expiredFlag) {
        this.expiredFlag = expiredFlag;
    }

    public List<GoodsInfo> getUsefulGoodsList() {
        return usefulGoodsList;
    }

    public void setUsefulGoodsList(List<GoodsInfo> usefulGoodsList) {
        this.usefulGoodsList = usefulGoodsList;
    }

    public String getSourceOrderNo() {
        return sourceOrderNo;
    }

    public void setSourceOrderNo(String sourceOrderNo) {
        this.sourceOrderNo = sourceOrderNo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Long getUseCouponTime() {
        return useCouponTime;
    }

    public void setUseCouponTime(Long useCouponTime) {
        this.useCouponTime = useCouponTime;
    }

    public Integer getUseShopId() {
        return useShopId;
    }

    public void setUseShopId(Integer useShopId) {
        this.useShopId = useShopId;
    }

    public Integer getBindUserId() {
        return bindUserId;
    }

    public void setBindUserId(Integer bindUserId) {
        this.bindUserId = bindUserId;
    }

    public String getUseOrderCode() {
        return useOrderCode;
    }

    public void setUseOrderCode(String useOrderCode) {
        this.useOrderCode = useOrderCode;
    }

    public Integer getOriginalId() {
        return originalId;
    }

    public void setOriginalId(Integer originalId) {
        this.originalId = originalId;
    }
}
