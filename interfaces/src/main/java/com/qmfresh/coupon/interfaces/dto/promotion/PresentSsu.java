package com.qmfresh.coupon.interfaces.dto.promotion;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 赠品商品配置
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public class PresentSsu  implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long activityId;
    /**
     * 满赠id
     */
    private Long presentId;
    private Integer skuId;
    private Integer ssuFormatId;
    private String ssuName;
    /**
     * 赠送量
     */
    private BigDecimal giveNum;
    /**
     * 限量
     */
    private BigDecimal limitNum;
    /**
     * 赠品图片
     */
    private String picture;
    /**
     * 赠品状态
     */
    private Integer status;

    private Integer cT;

    private Integer uT;

    private Integer isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getPresentId() {
        return presentId;
    }

    public void setPresentId(Long presentId) {
        this.presentId = presentId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }

    public BigDecimal getGiveNum() {
        return giveNum;
    }

    public void setGiveNum(BigDecimal giveNum) {
        this.giveNum = giveNum;
    }

    public BigDecimal getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(BigDecimal limitNum) {
        this.limitNum = limitNum;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }

    public Integer getuT() {
        return uT;
    }

    public void setuT(Integer uT) {
        this.uT = uT;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }



}
