package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;
import java.util.List;

/**
 * @program: coupon-service
 * @description:
 * @author: xbb
 * @create: 2019-11-25 16:06
 **/
public class CouponCheckRequestBean implements Serializable {

    /**
     * 用户id  线上商城必传
     */
    private Long userId;

    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 来源系统：拼团，线上，线下......(保留字段)
     */
    private Integer sourceSystem;

    /**
     * 商品信息数组
     */
    private List<GoodsInfo> goodsList;

    /**
     * 门店id
     */
    private Integer shopId;

    /**
     * 优惠券码
     */
    private String couponCode;

    /**
     * 优惠券id
     */
    private String couponId;

    /**
     *  渠道 1：收银 5：线上商城  10：外卖 15：门店订货  5：线上商城 必传
     */
    private Integer bizChannel;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(Integer sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public List<GoodsInfo> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<GoodsInfo> goodsList) {
        this.goodsList = goodsList;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public Integer getBizChannel() {
        return bizChannel;
    }

    public void setBizChannel(Integer bizChannel) {
        this.bizChannel = bizChannel;
    }

    @Override
    public String toString() {
        return "CouponCheckRequestBean{" +
                "userId=" + userId +
                ", orderNo='" + orderNo + '\'' +
                ", sourceSystem=" + sourceSystem +
                ", goodsList=" + goodsList +
                ", shopId=" + shopId +
                ", couponCode='" + couponCode + '\'' +
                ", couponId='" + couponId + '\'' +
                ", bizChannel='" + bizChannel + '\'' +
                '}';
    }
}
