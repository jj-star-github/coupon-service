/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/8/8
 * Description:ProQueryRequestBean.java
 */
package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

/**
 * @author lxc
 */
public class ProQueryCouponRequestBean implements Serializable {
    
    private static final long serialVersionUID = -5551310232387567593L;
    /**
     * 审批状态
     */
    private Integer approvalStatus;
    /**
     * 渠道，线上还是线下(4线上,5线下)
     */
    private Integer orderChannel;

    public Integer getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(Integer approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Integer getOrderChannel() {
        return orderChannel;
    }

    public void setOrderChannel(Integer orderChannel) {
        this.orderChannel = orderChannel;
    }
}
