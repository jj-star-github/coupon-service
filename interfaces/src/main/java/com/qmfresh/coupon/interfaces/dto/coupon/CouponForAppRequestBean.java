package com.qmfresh.coupon.interfaces.dto.coupon;

import java.io.Serializable;

public class CouponForAppRequestBean implements Serializable {
    private static final long serialVersionUID = -2634608309253218048L;
    /**
     * user_id
     */
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
